Parts
- Delay -
active: true
lowMin: 100.0
lowMax: 100.0
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 50.0
highMax: 50.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1500.0
highMax: 1500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: true
lowMin: 75.0
lowMax: 75.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Shape - 
shape: square
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 2
scaling0: 0.46938777
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 100.0
relative: false
scalingCount: 2
scaling0: 0.5102041
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 5.0
highMax: 15.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 100.0
highMax: 200.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 45.0
highMax: 135.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 1.0
lowMax: 360.0
highMin: 180.0
highMax: 180.0
relative: true
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -600.0
highMax: -600.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 0.06122449
scaling2: 1.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.20547946
timeline2: 1.0
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.45882353
colors2: 0.16078432
colors3: 1.0
colors4: 0.5686275
colors5: 0.32156864
colors6: 1.0
colors7: 0.4862745
colors8: 0.3019608
timelineCount: 3
timeline0: 0.0
timeline1: 0.5026087
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 1.0
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.0010
timeline2: 0.89726025
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: false
behind: false
- Image Path -
/home/prorokus/dev/slingshot/part1.png


Wave
- Delay -
active: true
lowMin: 0.0
lowMax: 0.0
- Duration - 
lowMin: 1.0
lowMax: 1.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 500.0
highMax: 500.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: point
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 2
scaling0: 0.48979592
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Velocity - 
active: false
- Angle - 
active: false
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 3
colors0: 1.0
colors1: 0.7019608
colors2: 0.3882353
timelineCount: 1
timeline0: 0.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 3
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
timelineCount: 3
timeline0: 0.0
timeline1: 0.0010
timeline2: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
- Image Path -
/home/prorokus/dev/slingshot/wave.png


Snow
- Delay -
active: true
lowMin: 300.0
lowMax: 300.0
- Duration - 
lowMin: 300.0
lowMax: 300.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 15.0
highMax: 15.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 750.0
highMax: 750.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: square
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 40.0
highMax: 40.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 40.0
highMax: 40.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 20.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: true
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -5.0
highMax: 5.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -50.0
highMax: -50.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Tint - 
colorsCount: 6
colors0: 1.0
colors1: 0.5764706
colors2: 0.101960786
colors3: 1.0
colors4: 0.49019608
colors5: 0.12941177
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 0.75438595
scaling2: 0.4385965
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.19178082
timeline2: 0.65753424
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
- Image Path -
/home/prorokus/dev/slingshot/snow.png


Dust
- Delay -
active: false
- Duration - 
lowMin: 100.0
lowMax: 100.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 150.0
highMax: 150.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 1500.0
highMax: 1500.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: square
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 125.0
highMax: 125.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 125.0
highMax: 125.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 175.0
highMax: 175.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 10.0
highMax: 30.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 360.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: true
lowMin: 0.0
lowMax: 60.0
highMin: 60.0
highMax: 0.0
relative: false
scalingCount: 2
scaling0: 0.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Wind - 
active: false
- Gravity - 
active: false
- Tint - 
colorsCount: 9
colors0: 0.3137255
colors1: 0.14117648
colors2: 0.050980393
colors3: 0.3137255
colors4: 0.16470589
colors5: 0.047058824
colors6: 0.5019608
colors7: 0.28627452
colors8: 0.21176471
timelineCount: 3
timeline0: 0.0
timeline1: 0.5026087
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 4
scaling0: 0.0
scaling1: 1.0
scaling2: 0.0
scaling3: 0.0
timelineCount: 4
timeline0: 0.0
timeline1: 0.020547945
timeline2: 0.60273975
timeline3: 1.0
- Options - 
attached: false
continuous: false
aligned: false
additive: true
behind: false
- Image Path -
/home/prorokus/dev/slingshot/fire3.png
