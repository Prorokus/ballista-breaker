Dust
- Delay -
active: false
- Duration - 
lowMin: 1000.0
lowMax: 1000.0
- Count - 
min: 0
max: 200
- Emission - 
lowMin: 0.0
lowMax: 0.0
highMin: 5.0
highMax: 5.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Life - 
lowMin: 0.0
lowMax: 0.0
highMin: 7000.0
highMax: 8000.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Life Offset - 
active: false
- X Offset - 
active: false
- Y Offset - 
active: false
- Spawn Shape - 
shape: square
- Spawn Width - 
lowMin: 0.0
lowMax: 0.0
highMin: 300.0
highMax: 300.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Spawn Height - 
lowMin: 0.0
lowMax: 0.0
highMin: 400.0
highMax: 400.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Scale - 
lowMin: 0.0
lowMax: 0.0
highMin: 3.0
highMax: 6.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Velocity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -4.0
highMax: 4.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Angle - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: 0.0
highMax: 0.0
relative: false
scalingCount: 2
scaling0: 1.0
scaling1: 1.0
timelineCount: 2
timeline0: 0.0
timeline1: 1.0
- Rotation - 
active: false
- Wind - 
active: false
- Gravity - 
active: true
lowMin: 0.0
lowMax: 0.0
highMin: -4.0
highMax: -4.0
relative: false
scalingCount: 1
scaling0: 1.0
timelineCount: 1
timeline0: 0.0
- Tint - 
colorsCount: 9
colors0: 1.0
colors1: 0.9607843
colors2: 0.31764707
colors3: 1.0
colors4: 0.8784314
colors5: 0.5686275
colors6: 1.0
colors7: 0.9098039
colors8: 0.5803922
timelineCount: 3
timeline0: 0.0
timeline1: 0.47329193
timeline2: 1.0
- Transparency - 
lowMin: 0.0
lowMax: 0.0
highMin: 1.0
highMax: 1.0
relative: false
scalingCount: 6
scaling0: 0.0
scaling1: 0.3859649
scaling2: 0.28070176
scaling3: 0.0877193
scaling4: 0.15789473
scaling5: 0.0
timelineCount: 6
timeline0: 0.0
timeline1: 0.15753424
timeline2: 0.38356164
timeline3: 0.63013697
timeline4: 0.86986303
timeline5: 1.0
- Options - 
attached: false
continuous: true
aligned: false
additive: true
behind: false
- Image Path -
/home/prorokus/dev/slingshot/art_src2/fire-texture-black.png
