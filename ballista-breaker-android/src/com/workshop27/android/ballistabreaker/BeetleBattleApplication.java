package com.workshop27.android.ballistabreaker;

import android.app.Application;
import android.content.Context;

import com.scoreloop.client.android.core.model.Client;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;

public class BeetleBattleApplication extends Application {

	// Declare the Scoreloop Client
	private static Client client;

	static void init(final Context android_game_context) {
		if (client == null) {
			client = new Client(android_game_context,
					"QvXh51EzjI9IEmAJ2drRUBH/BLnphV10uIQq8wtxJnly6TKApxbFhw==",
					null);
		}
	}

	@Override
	public void onCreate() {
		super.onCreate();
		init(this);
		ScoreloopManagerSingleton.init(this,
				"QvXh51EzjI9IEmAJ2drRUBH/BLnphV10uIQq8wtxJnly6TKApxbFhw==");
	}

	@Override
	public void onTerminate() {
		super.onTerminate();
		ScoreloopManagerSingleton.destroy();
	}
}