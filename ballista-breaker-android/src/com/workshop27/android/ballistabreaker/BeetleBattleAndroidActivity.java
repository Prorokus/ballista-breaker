package com.workshop27.android.ballistabreaker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.workshop27.android.shop.PurchaseDatabase;
import com.workshop27.android.shop.ShopActionResolverAndroid;
import com.workshop27.android.shop.ShopAndroidHandler;
import com.workshop27.ballistabreaker.GdxGame;

public class BeetleBattleAndroidActivity extends AndroidApplication {
	private PurchaseDatabase mPurchaseDatabase;
	private ShopActionResolverAndroid mShopActionResolverAndroid;
	private GdxGame mGame;

	private AlarmManager am;

	public static boolean isBonusReceived = false;

	private boolean isAlarmRestored = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		am = (AlarmManager) getSystemService(BeetleBattleAndroidActivity.ALARM_SERVICE);

		AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
		cfg.useGL20 = true;

		ShopAndroidHandler.mContext = this;
		mPurchaseDatabase = new PurchaseDatabase(this);
		mPurchaseDatabase.initDb();
		mShopActionResolverAndroid = new ShopActionResolverAndroid(this,
				mPurchaseDatabase);
		mShopActionResolverAndroid.initShop();

		initialize(mGame = new GdxGame(new ActionResolverAndroid(this),
				mShopActionResolverAndroid), cfg);

		isAlarmRestored = false;

		try {
			isBonusReceived = GdxGame.files.getOptionsAndProgress("shop",
					"bonus").equals("true");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Bundle extras = getIntent().getExtras();
		if (extras != null && !isBonusReceived) {
			String value = extras.getString("AppRestored");
			if (value.equals("notification")) {
				GdxGame.files.setOptionsAndProgress("shop", "bonus", "true");
				GdxGame.showPrizeMsg = true;
				// Add bonus
				mPurchaseDatabase.addBonus(1, 20);
				isAlarmRestored = true;
				isBonusReceived = true;
			}
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		case KeyEvent.KEYCODE_MENU:
			if (mGame.getScreen().equals(mGame.gameScreen)) {
				mGame.gameScreen.onMenuClick();
			} else if (mGame.getScreen().equals(mGame.mainMenuScreen)) {
				mGame.mainMenuScreen.onMenuClick();
			}
			return true;
		}
		return false;
	}

	private void setOneTimeAlarm() {
		Intent intent24 = new Intent(this, TimeAlarm24.class);
		PendingIntent pendingIntent24 = PendingIntent.getBroadcast(this, 0,
				intent24, PendingIntent.FLAG_ONE_SHOT);

		Intent intent48 = new Intent(this, TimeAlarm72.class);
		PendingIntent pendingIntent48 = PendingIntent.getBroadcast(this, 1,
				intent48, PendingIntent.FLAG_ONE_SHOT);

		Intent intent72 = new Intent(this, TimeAlarm168.class);
		PendingIntent pendingIntent72 = PendingIntent.getBroadcast(this, 2,
				intent72, PendingIntent.FLAG_ONE_SHOT);

		am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
				+ (24 * 60 * 60 * 1000), pendingIntent24);
		am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
				+ (72 * 60 * 60 * 1000), pendingIntent48);
		am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis()
				+ (168 * 60 * 60 * 1000), pendingIntent72);
	}

	private void clearOneTimeAlarm() {
		Intent intent24 = new Intent(this, TimeAlarm24.class);
		PendingIntent pendingIntent24 = PendingIntent.getBroadcast(this, 0,
				intent24, PendingIntent.FLAG_ONE_SHOT);

		Intent intent48 = new Intent(this, TimeAlarm72.class);
		PendingIntent pendingIntent48 = PendingIntent.getBroadcast(this, 1,
				intent48, PendingIntent.FLAG_ONE_SHOT);

		Intent intent72 = new Intent(this, TimeAlarm168.class);
		PendingIntent pendingIntent72 = PendingIntent.getBroadcast(this, 2,
				intent72, PendingIntent.FLAG_ONE_SHOT);

		// Cancel alarms
		try {
			am.cancel(pendingIntent24);
		} catch (Exception e) {
			Log.e("Beetle Breaker", "AlarmManager update was not canceled. "
					+ e.toString());
		}

		// Cancel alarms
		try {
			am.cancel(pendingIntent48);
		} catch (Exception e) {
			Log.e("Beetle Breaker", "AlarmManager update was not canceled. "
					+ e.toString());
		}

		// Cancel alarms
		try {
			am.cancel(pendingIntent72);
		} catch (Exception e) {
			Log.e("Beetle Breaker", "AlarmManager update was not canceled. "
					+ e.toString());
		}
	}

	@Override
	public void onPause() {
		if (!isBonusReceived) {
			setOneTimeAlarm();
		}
		mPurchaseDatabase.close();
		super.onPause();
		mShopActionResolverAndroid.onPause();

		if (mGame.getScreen().equals(mGame.gameScreen)) {
			mGame.gameScreen.onPause();
		}
	}

	@Override
	public void onResume() {
		mPurchaseDatabase.open();
		super.onResume();
		mShopActionResolverAndroid.onResume();

		if (!isBonusReceived || isAlarmRestored) {
			clearOneTimeAlarm();
		}
	}

	@Override
	public void onStart() {
		super.onStart();
		mShopActionResolverAndroid.onStart();
	}

	@Override
	public void onStop() {
		super.onStop();
		mShopActionResolverAndroid.onStop();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		this.mShopActionResolverAndroid.onActivityResult(requestCode,
				resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mShopActionResolverAndroid.onDestroy();
	}
}