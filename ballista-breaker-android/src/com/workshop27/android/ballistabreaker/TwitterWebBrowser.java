package com.workshop27.android.ballistabreaker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class TwitterWebBrowser extends Activity {
	/** Called when the activity is first created. */

	private static final String OAUTH_CALLBACK_SCHEME = "x-beatlebreaker-oauth-twitter";
	private static final String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME
			+ "://callback";
	protected boolean isCallBacked;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		Intent intent = getIntent();
		String value = intent.getStringExtra("authUrl");

		WebView webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);

		webView.loadUrl(value);

		webView.setWebViewClient(new MyWebViewClient() {

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {
				if (url.contains(OAUTH_CALLBACK_URL) && !isCallBacked) {
					Intent intent = new Intent(TwitterWebBrowser.this,
							TwitterActivity.class);
					intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
					Uri uri = Uri.parse(url);
					intent.setData(uri);
					startActivity(intent);
					isCallBacked = true;
					finish();
				}

			}
		});

	}

	static public class MyWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}
}