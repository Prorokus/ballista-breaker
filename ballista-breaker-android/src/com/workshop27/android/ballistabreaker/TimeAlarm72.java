package com.workshop27.android.ballistabreaker;

import com.workshop27.android.constants.Constants;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class TimeAlarm72 extends BroadcastReceiver {

	NotificationManager nm;

	@Override
	public void onReceive(Context context, Intent intent) {
		String DebtName = null;

		nm = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		CharSequence from = "Beetle Breaker";

		Intent notificationIntent = new Intent(context,
				BeetleBattleAndroidActivity.class);
		notificationIntent.putExtra("AppRestored", "notification");
		notificationIntent.getExtras();
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);

		Notification notif = new Notification(R.drawable.ic_launcher,
				"Open App and get free bonus!", System.currentTimeMillis());
		notif.setLatestEventInfo(context, from,
				Constants.NOTIFICATION_MESSAGE_72_TEXT, contentIntent);
		notif.flags = Notification.DEFAULT_LIGHTS
				| Notification.FLAG_AUTO_CANCEL;
		nm.notify(1, notif);
	}
}
