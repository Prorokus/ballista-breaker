package com.workshop27.android.ballistabreaker;

import java.util.Map;

import android.content.Intent;
import android.os.Handler;
import android.widget.Toast;

import com.flurry.android.FlurryAgent;
import com.scoreloop.client.android.ui.EntryScreenActivity;
import com.scoreloop.client.android.ui.ScoreloopManagerSingleton;
import com.workshop27.ballistabreaker.ActionResolver;
import com.workshop27.ballistabreaker.constants.Constants;

public class ActionResolverAndroid implements ActionResolver {
	Handler uiThread;
	BeetleBattleAndroidActivity mContext; // Your class here which extends
	// AndroidApplication
	ScoreloopHandler handler;

	public ActionResolverAndroid(BeetleBattleAndroidActivity mContext) {
		uiThread = new Handler(); // This binds the handler to the "main"
									// thread, see documentation of handler
		this.mContext = mContext;
		handler = new ScoreloopHandler(mContext);
	}

	public void showScoreloop() {
		Intent intent = new Intent(mContext, EntryScreenActivity.class);
		mContext.startActivity(intent);
	}

	public void submitScore(final int score) {
		uiThread.post(new Runnable() {
			public void run() {

				handler.submitScore(score);
				handler.getRankingForScore(score);
			}
		});
	}

	public void refreshScores() {

		uiThread.post(new Runnable() {
			public void run() {
				Toast.makeText(mContext, "Refreshing scores",
						Toast.LENGTH_SHORT).show();
				handler.getGlobalHighscores();
				handler.getTodayHighscores();
			}
		});
	}

	public void bootstrap() {
		uiThread.post(new Runnable() {
			public void run() {
				handler.getGlobalHighscores();
				handler.getTodayHighscores();

				// Upload local scores, if any
				ScoreloopManagerSingleton.get().submitLocalScores(null);
			}
		});
	}

	@Override
	public boolean checkInetConnection() {
		return handler.checkInetConnection();
	}

	@Override
	public void startFacebook(String message, int action) {
		Intent myIntent = new Intent(mContext, FacebookActivity.class);
		myIntent.putExtra("message", message);
		myIntent.putExtra("action", action);
		mContext.startActivity(myIntent);

	}

	@Override
	public void startTwitter(String message, int action) {
		Intent myIntent = new Intent(mContext, TwitterActivity.class);
		myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
		myIntent.putExtra("message", message);
		myIntent.putExtra("action", action);
	//	GdxGame.action = action;
		mContext.startActivityForResult(myIntent, 1);

	}

	@Override
	public void startFlurry() {
		FlurryAgent.onStartSession(mContext, Constants.FLURRY_API_KEY);

	}

	@Override
	public void stopFlurry() {
		FlurryAgent.onEndSession(mContext);

	}

	@Override
	public void flurryAgentLogEvent(String eventId) {
		FlurryAgent.logEvent(eventId);

	}

	@Override
	public void flurryAgentLogEvent(String eventId, boolean timed) {
		FlurryAgent.logEvent(eventId, timed);

	}

	@Override
	public void flurryAgentLogEvent(String eventId,
			Map<String, String> parameters) {
		FlurryAgent.logEvent(eventId, parameters);

	}

	@Override
	public void flurryAgentLogEvent(String eventId,
			Map<String, String> parameters, boolean timed) {
		FlurryAgent.logEvent(eventId, parameters, timed);

	}

	@Override
	public void flurryAgentOnPageView() {
		FlurryAgent.onPageView();

	}

	@Override
	public void flurryAgentEndTimedEvent(String eventId) {
		FlurryAgent.endTimedEvent(eventId);

	}

	@Override
	public void startBrowser(String url) {
		Intent myIntent = new Intent(mContext, WebBrowser.class);
		myIntent.putExtra("Url", url);
		myIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		mContext.startActivity(myIntent);

	}

}
