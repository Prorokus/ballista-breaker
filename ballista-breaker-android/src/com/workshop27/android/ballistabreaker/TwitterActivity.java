package com.workshop27.android.ballistabreaker;

import java.util.Scanner;

import oauth.signpost.OAuth;
import oauth.signpost.OAuthConsumer;
import oauth.signpost.OAuthProvider;
import oauth.signpost.basic.DefaultOAuthProvider;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;
import oauth.signpost.exception.OAuthCommunicationException;
import oauth.signpost.exception.OAuthExpectationFailedException;
import oauth.signpost.exception.OAuthMessageSignerException;
import oauth.signpost.exception.OAuthNotAuthorizedException;
import winterwell.jtwitter.OAuthSignpostClient;
import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import com.workshop27.android.constants.Constants;
import com.workshop27.android.shop.PurchaseDatabase;
import com.workshop27.android.shop.ShopAndroidHandler;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.BBInventory;

public class TwitterActivity extends Activity {
	private PurchaseDatabase mPurchaseDatabase;
	private static final String TAG = "OAuthDemo";

	private static final String OAUTH_CALLBACK_URL = Constants.OAUTH_CALLBACK_SCHEME
			+ "://callback";

	private OAuthSignpostClient oauthClient;
	private OAuthConsumer mConsumer;
	private OAuthProvider mProvider;
	private Twitter twitter;
	SharedPreferences prefs;
	private String message;
	private int action;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.social);

		Intent intent = getIntent();
		message = intent.getStringExtra("message");
		action = intent.getIntExtra("action", -1);
		// action = GdxGame.action;
		// GdxGame.action = -1;
		//
		// if (action == -1) {
		// Intent myIntent = new Intent(this,
		// BeetleBattleAndroidActivity.class);
		// this.startActivity(myIntent);
		// finish();
		// return;
		// }

		try {

			ConnectivityManager cMgr = (ConnectivityManager) this
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cMgr.getActiveNetworkInfo();
			String status = netInfo.getState().toString();

			if (status.equals("CONNECTED")) {
				connectToTwitter();
			} else {
				Toast.makeText(getApplicationContext(),
						"No connection available", Toast.LENGTH_SHORT).show();
				finish();
			}

		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Connection refused",
					Toast.LENGTH_SHORT).show();
			finish();
		}

	}

	private void connectToTwitter() {
		// mConsumer = new DefaultOAuthConsumer(OAUTH_KEY, OAUTH_SECRET);
		mConsumer = new CommonsHttpOAuthConsumer(Constants.OAUTH_KEY,
				Constants.OAUTH_SECRET);
		mProvider = new DefaultOAuthProvider(
				"https://api.twitter.com/oauth/request_token",
				"https://api.twitter.com/oauth/access_token",
				"https://api.twitter.com/oauth/authorize");

		// Read the prefs to see if we have token
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		String token = prefs.getString("token", null);
		String tokenSecret = prefs.getString("tokenSecret", null);
		if (token != null && tokenSecret != null) {
			// We have token, use it
			mConsumer.setTokenWithSecret(token, tokenSecret);
			// Make a Twitter object
			oauthClient = new OAuthSignpostClient(Constants.OAUTH_KEY,
					Constants.OAUTH_SECRET, token, tokenSecret);
			twitter = new Twitter(Constants.TWITTER_USER, oauthClient);
		}

		if (twitter == null) {
			new OAuthAuthorizeTask().execute();
			return;
		}

		new PostStatusTask().execute(message);

	}

	/*
	 * Callback once we are done with the authorization of this app with
	 * Twitter.
	 */
	@Override
	public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		Log.d(TAG, "intent: " + intent);

		// Check if this is a callback from OAuth
		Uri uri = intent.getData();
		if (uri != null
				&& uri.getScheme().equals(Constants.OAUTH_CALLBACK_SCHEME)) {
			Log.d(TAG, "callback: " + uri.getPath());

			String verifier = uri.getQueryParameter(OAuth.OAUTH_VERIFIER);
			Log.d(TAG, "verifier: " + verifier);

			new RetrieveAccessTokenTask().execute(verifier);
		}

	}

	/* Responsible for starting the Twitter authorization */
	class OAuthAuthorizeTask extends AsyncTask<Void, Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			String authUrl;
			String message = null;
			try {
				authUrl = mProvider.retrieveRequestToken(mConsumer,
						OAUTH_CALLBACK_URL);
				Intent myIntent = new Intent(TwitterActivity.this,
						TwitterWebBrowser.class);
				myIntent.putExtra("authUrl", authUrl);
				TwitterActivity.this.startActivity(myIntent);

			} catch (OAuthMessageSignerException e) {
				message = "OAuthMessageSignerException";
				e.printStackTrace();
			} catch (OAuthNotAuthorizedException e) {
				message = "OAuthNotAuthorizedException";
				e.printStackTrace();
			} catch (OAuthExpectationFailedException e) {
				message = "OAuthExpectationFailedException";
				e.printStackTrace();
			} catch (OAuthCommunicationException e) {
				message = "OAuthCommunicationException";
				e.printStackTrace();
			}
			return message;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result != null) {
				Toast.makeText(TwitterActivity.this, result, Toast.LENGTH_LONG)
						.show();
				finish();
			}
		}
	}

	/* Responsible for retrieving access tokens from twitter */
	class RetrieveAccessTokenTask extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			String message = null;
			String verifier = params[0];
			try {
				// Get the token
				Log.d(TAG, "mConsumer: " + mConsumer);
				Log.d(TAG, "mProvider: " + mProvider);
				mProvider.retrieveAccessToken(mConsumer, verifier);
				String token = mConsumer.getToken();
				String tokenSecret = mConsumer.getTokenSecret();
				mConsumer.setTokenWithSecret(token, tokenSecret);

				Log.d(TAG, String.format(
						"verifier: %s, token: %s, tokenSecret: %s", verifier,
						token, tokenSecret));

				// Store token in prefs
				prefs.edit().putString("token", token)
						.putString("tokenSecret", tokenSecret).commit();

				// Make a Twitter object
				oauthClient = new OAuthSignpostClient(Constants.OAUTH_KEY,
						Constants.OAUTH_SECRET, token, tokenSecret);
				twitter = new Twitter("Beetle Breaker", oauthClient);

				Log.d(TAG, "token: " + token);
			} catch (OAuthMessageSignerException e) {
				message = "OAuthMessageSignerException";
				e.printStackTrace();
			} catch (OAuthNotAuthorizedException e) {
				message = "OAuthNotAuthorizedException";
				e.printStackTrace();
			} catch (OAuthExpectationFailedException e) {
				message = "OAuthExpectationFailedException";
				e.printStackTrace();
			} catch (OAuthCommunicationException e) {
				message = "OAuthCommunicationException";
				e.printStackTrace();
			}
			return message;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (result != null) {
				Toast.makeText(TwitterActivity.this, result, Toast.LENGTH_LONG)
						.show();
				finish();
			} else {
				new PostStatusTask().execute(message);
			}
		}
	}

	/* Responsible for posting new status to Twitter */
	class PostStatusTask extends AsyncTask<String, Void, String> {
		@Override
		protected String doInBackground(String... params) {
			try {
				switch (action) {
				case 1:
					twitter.setStatus(Constants.TWITTER_HASHTAG + " "
							+ Constants.ADVERTISING_GAME_MESSAGE + "\n"
							+ Constants.GOOGLE_PLAY_URL);
					break;
				case 2:
					Scanner sc = new Scanner(message).useDelimiter("-");
					int x = sc.nextInt();
					int y = sc.nextInt();
					int z = sc.nextInt();
					String string = String.format(
							Constants.SCORED_GAME_MESSAGE, x, y, z);
					twitter.setStatus(Constants.TWITTER_HASHTAG + " " + string
							+ "\n" + Constants.GOOGLE_PLAY_URL);
					break;

				default:

					break;
				}

				return "Successfully posted to Twitter";

			} catch (TwitterException e) {
				return "This post is already posted";
			}
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Toast.makeText(TwitterActivity.this, result, Toast.LENGTH_LONG)
					.show();
			if (result.equals("This post is already posted")) {
				finish();
				return;
			}
			if (action == 2) {
				finish();
				return;
			}
			mPurchaseDatabase = new PurchaseDatabase(TwitterActivity.this);

			int bonusId = Integer.valueOf(mPurchaseDatabase
					.getBonusID("twitter"));

			mPurchaseDatabase.addBonus(bonusId, 1);
			mPurchaseDatabase.updateSocialDates("twitter",
					String.valueOf(System.currentTimeMillis()));

			GdxGame.showSocialPrizeMsg = true;

			switch (bonusId) {
			case 0:
				BBInventory.updateLaser(
						ShopAndroidHandler.getLaserOwnedItems(), true);

				GdxGame.socialPrizeId = 2;

				break;
			case 1:
				BBInventory.updateCoockie(
						ShopAndroidHandler.getCoockieOwnedItems(), true);

				GdxGame.socialPrizeId = 1;

				break;
			case 2:
				BBInventory.updateBeetle1(
						ShopAndroidHandler.getBeetle1OwnedItems(), true);

				GdxGame.socialPrizeId = 4;

				break;
			case 3:
				BBInventory.updateBeetle2(
						ShopAndroidHandler.getBeetle2OwnedItems(), true);

				GdxGame.socialPrizeId = 3;

				break;
			case 4:
				BBInventory.updateExpl(ShopAndroidHandler.getExplOwnedItems(),
						true);

				GdxGame.socialPrizeId = 5;

				break;
			case 5:
				BBInventory.updateEgg1(ShopAndroidHandler.getEgg1OwnedItems(),
						true);

				GdxGame.socialPrizeId = 6;

				break;
			case 6:
				BBInventory.updateEgg2(ShopAndroidHandler.getEgg2OwnedItems(),
						true);

				GdxGame.socialPrizeId = 7;

				break;
			default:
				break;
			}

			finish();
		}

	}

}