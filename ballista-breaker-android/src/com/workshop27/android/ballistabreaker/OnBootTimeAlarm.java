package com.workshop27.android.ballistabreaker;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnBootTimeAlarm extends BroadcastReceiver {
		private AlarmManager am;

		@Override
		public void onReceive(Context context, Intent intent) {
		    am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

			Intent intentToRestart24 = new Intent(context, TimeAlarm24.class);
			PendingIntent pendingIntent24 = PendingIntent.getBroadcast(context, 0, intentToRestart24, PendingIntent.FLAG_ONE_SHOT);
			
			Intent intentToRestart48 = new Intent(context, TimeAlarm72.class);
			PendingIntent pendingIntent48 = PendingIntent.getBroadcast(context, 1, intentToRestart48, PendingIntent.FLAG_ONE_SHOT);
			
			Intent intentToRestart72 = new Intent(context, TimeAlarm168.class);
			PendingIntent pendingIntent72 = PendingIntent.getBroadcast(context, 2, intentToRestart72, PendingIntent.FLAG_ONE_SHOT);
			
			am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (24 * 60 * 60 * 1000), pendingIntent24);		
			am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (72 * 60 * 60 * 1000), pendingIntent48);
			am.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + (168 * 60 * 60 * 1000), pendingIntent72);
		}
}
