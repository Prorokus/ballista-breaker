package com.workshop27.android.ballistabreaker;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebBrowser extends Activity {
	/** Called when the activity is first created. */

	private static final String OAUTH_CALLBACK_SCHEME = "x-beatlebreaker-oauth-twitter";
	private static final String OAUTH_CALLBACK_URL = OAUTH_CALLBACK_SCHEME
			+ "://callback";
	protected boolean isCallBacked;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.webview);
		Intent intent = getIntent();
		String url = intent.getStringExtra("Url");

		WebView webView = (WebView) findViewById(R.id.webview);
		webView.getSettings().setJavaScriptEnabled(true);

		webView.loadUrl(url);

		webView.setWebViewClient(new MyWebViewClient() {

			@Override
			public void onPageStarted(WebView view, String url, Bitmap favicon) {

			}
		});

	}

	static public class MyWebViewClient extends WebViewClient {

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, String url) {
			view.loadUrl(url);
			return true;
		}
	}
}