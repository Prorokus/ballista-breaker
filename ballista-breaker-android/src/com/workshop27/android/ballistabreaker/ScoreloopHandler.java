package com.workshop27.android.ballistabreaker;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.scoreloop.client.android.core.controller.RankingController;
import com.scoreloop.client.android.core.controller.RequestController;
import com.scoreloop.client.android.core.controller.RequestControllerObserver;
import com.scoreloop.client.android.core.controller.ScoreController;
import com.scoreloop.client.android.core.controller.ScoresController;
import com.scoreloop.client.android.core.model.Score;
import com.scoreloop.client.android.core.model.SearchList;
import com.scoreloop.client.android.core.model.Session;

public class ScoreloopHandler {

	private BeetleBattleAndroidActivity mContext; // Your class here which
													// extends
													// AndroidApplication

	public ScoreloopHandler(BeetleBattleAndroidActivity context) {
		mContext = context;
	}

	public void submitScore(int scoreValue) {
		Log.d("Scoreloop", "submitting score");
		final Score score = new Score((double) scoreValue, null);
		final ScoreController myScoreController = new ScoreController(
				new ScoreSubmitObserver());
		myScoreController.submitScore(score);
	}

	public void getRankingForScore(int scoreValue) {
		Score score = new Score((double) scoreValue, null);
		RankingController controller = new RankingController(
				new RankingRequestObserver());
		controller.loadRankingForScore(score);
	}

	public void getGlobalHighscores() {
		ScoresController myScoresController = new ScoresController(
				new GlobalRankObserver());
		myScoresController.setSearchList(SearchList.getGlobalScoreSearchList());
		myScoresController.setRangeLength(15);
		myScoresController.loadRangeForUser(Session.getCurrentSession()
				.getUser());
	}

	public void getTodayHighscores() {
		ScoresController myScoresController = new ScoresController(
				new DailyRankObserver());
		myScoresController.setSearchList(SearchList
				.getTwentyFourHourScoreSearchList());
		myScoresController.setRangeLength(15);
		myScoresController.loadRangeForUser(Session.getCurrentSession()
				.getUser());
	}

	public boolean checkInetConnection() {
		ConnectivityManager conMgr = (ConnectivityManager) mContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo i = conMgr.getActiveNetworkInfo();
		if (i == null)
			return false;
		if (!i.isConnected())
			return false;
		if (!i.isAvailable())
			return false;
		return true;
	}

	private class RankingRequestObserver implements RequestControllerObserver {

		@Override
		public void requestControllerDidFail(RequestController arg0,
				Exception arg1) {

		}

		@Override
		public void requestControllerDidReceiveResponse(
				RequestController controller) {
			//Ranking ranking = ((RankingController) controller).getRanking();
			//final int rank = ranking.getRank();
			mContext.postRunnable(new Runnable() {
				@Override
				public void run() {
					// This code runs on the "libgdx" thread.
					// Use a local variable to store the rank in the MainProject
					// and set it here
				}
			});
		}
	}

	private class ScoreSubmitObserver implements RequestControllerObserver {

		@Override
		public void requestControllerDidFail(
				final RequestController requestController,
				final Exception exception) {
			Log.d("Scoreloop",
					"score submitted exception " + exception.getMessage());
		}

		@Override
		public void requestControllerDidReceiveResponse(
				final RequestController requestController) {
			Log.d("Scoreloop", "score submitted successsfully");

		}
	}

	private class DailyRankObserver implements RequestControllerObserver {

		@Override
		public void requestControllerDidFail(RequestController arg0,
				Exception arg1) {

		}

		@Override
		public void requestControllerDidReceiveResponse(
				RequestController controller) {
		//	final java.util.List<Score> retrievedScores = ((ScoresController) controller)
		//			.getScores();
			mContext.postRunnable(new Runnable() {
				@Override
				public void run() {
					// This code runs on the "libgdx" thread.
					// Use a local variable to store the daily ranks in the
					// MainProject and set it here
				}
			});
		}
	}

	private class GlobalRankObserver implements RequestControllerObserver {

		@Override
		public void requestControllerDidFail(RequestController arg0,
				Exception arg1) {

		}

		@Override
		public void requestControllerDidReceiveResponse(
				RequestController controller) {
	//		final java.util.List<Score> retrievedScores = ((ScoresController) controller)
	//				.getScores();
			mContext.postRunnable(new Runnable() {
				@Override
				public void run() {
					// This code runs on the "libgdx" thread.
					// Use a local variable to store the global ranks in the
					// MainProject and set it here
				}
			});
		}

	}
}
