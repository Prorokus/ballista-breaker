package com.workshop27.android.ballistabreaker;

import java.io.IOException;

import android.app.backup.BackupAgentHelper;
import android.app.backup.BackupDataInput;
import android.app.backup.BackupDataOutput;
import android.app.backup.FileBackupHelper;
import android.os.ParcelFileDescriptor;

public class DbFileBackupAgent extends BackupAgentHelper {
    // The name of the SharedPreferences file
	private static final String DB_NAME = "../databases/purchase.db";

    // A key to uniquely identify the set of backup data
    static final String FILES_BACKUP_KEY = "myfiles";
    
    @Override
    public void onCreate(){
    	FileBackupHelper dbs = new FileBackupHelper(this, DB_NAME);
    	addHelper("dbs", dbs);
    }

    /**
     * We want to ensure that the UI is not trying to rewrite the data file
     * while we're reading it for backup, so we override this method to
     * supply the necessary locking.
     */
    @Override
    public void onBackup(ParcelFileDescriptor oldState, BackupDataOutput data, ParcelFileDescriptor newState) throws IOException {
        // Hold the lock while the FileBackupHelper performs the backup operation
            super.onBackup(oldState, data, newState);
    }    
    
    /**
     * Adding locking around the file rewrite that happens during restore is
     * similarly straightforward.
     */
    @Override
    public void onRestore(BackupDataInput data, int appVersionCode,
            ParcelFileDescriptor newState) throws IOException {
        // Hold the lock while the FileBackupHelper restores the file from
        // the data provided here.
            super.onRestore(data, appVersionCode, newState);
    }    
}
