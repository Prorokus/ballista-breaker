package com.workshop27.android.ballistabreaker;

import java.util.Scanner;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.easy.facebook.android.apicall.GraphApi;
import com.easy.facebook.android.error.EasyFacebookError;
import com.easy.facebook.android.facebook.FBLoginManager;
import com.easy.facebook.android.facebook.Facebook;
import com.easy.facebook.android.facebook.LoginListener;
import com.workshop27.android.constants.Constants;
import com.workshop27.android.shop.PurchaseDatabase;
import com.workshop27.android.shop.ShopAndroidHandler;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.BBInventory;

public class FacebookActivity extends Activity implements LoginListener {
	/** Called when the activity is first created. */

	private FBLoginManager fbLoginManager;
	private ProgressDialog progressDialog;
	private PurchaseDatabase mPurchaseDatabase;
	private int action;
	private String message;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.social);

		try {

			ConnectivityManager cMgr = (ConnectivityManager) this
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cMgr.getActiveNetworkInfo();
			String status = netInfo.getState().toString();

			if (status.equals("CONNECTED")) {
				connectToFacebook();
			} else {
				Toast.makeText(getApplicationContext(),
						"No connection available", Toast.LENGTH_SHORT).show();
				finish();
			}

		} catch (Exception e) {
			Toast.makeText(getApplicationContext(), "Connection refused",
					Toast.LENGTH_SHORT).show();
			finish();
		}

	}

	public void connectToFacebook() {

		String permissions[] = { /*
								 * "user_about_me", "user_activities",
								 * "user_birthday", "user_checkins",
								 * "user_education_history", "user_events",
								 * "user_groups", "user_hometown",
								 * "user_interests", "user_likes",
								 * "user_location", "user_notes",
								 * "user_online_presence",
								 * "user_photo_video_tags", "user_photos",
								 * "user_relationships",
								 * "user_relationship_details",
								 * "user_religion_politics", "user_status",
								 * "user_videos", "user_website",
								 * "user_work_history", "email",
								 * "read_friendlists", "read_insights",
								 * "read_mailbox", "read_requests",
								 * "read_stream", "xmpp_login",
								 * "ads_management", "create_event",
								 * "manage_friendlists", "manage_notifications",
								 * "offline_access", "publish_checkins", ,
								 * "rsvp_event", "sms", "publish_actions",
								 * "manage_pages"
								 */

		"publish_stream" };

		Intent intent = getIntent();
		message = intent.getStringExtra("message");
		action = intent.getIntExtra("action", -1);

		fbLoginManager = new FBLoginManager(this, R.layout.social,
				Constants.FACEBOOK_APP_ID, permissions);

		if (fbLoginManager.existsSavedFacebook()) {
			fbLoginManager.loadFacebook();
		} else {
			fbLoginManager.login();
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode,
			android.content.Intent data) {
		fbLoginManager.loginSuccess(data);
	}

	private class LoginSuccess extends AsyncTask<Facebook, Void, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = ProgressDialog.show(FacebookActivity.this,
					"Facebook", "Please, wait...");
		}

		@Override
		protected Void doInBackground(Facebook... params) {			
			GraphApi graphApi = new GraphApi(params[0]);
			try {

				switch (action) {
				case 1:

					graphApi.setStatus(Constants.ADVERTISING_GAME_MESSAGE
							+ "\n" + Constants.GOOGLE_PLAY_URL,
							Constants.ICON_URL);
					break;
				case 2:
					Scanner sc = new Scanner(message).useDelimiter("-");
					int x = sc.nextInt();
					int y = sc.nextInt();
					int z = sc.nextInt();
					String string = String.format(
							Constants.SCORED_GAME_MESSAGE, x, y, z);
					graphApi.setStatus(string + "\n"
							+ Constants.GOOGLE_PLAY_URL, Constants.ICON_URL);
					break;

				default:

					break;
				}

			} catch (EasyFacebookError e) {
				Log.d("TAG: ", e.toString());
				finish();
			}

			runOnUiThread(new Runnable() {
				@Override
				public void run() {
					fbLoginManager
							.displayToast("Successfully posted to Facebook");
					progressDialog.dismiss();
					if (action == 2) {
						finish();
						return;
					}
					mPurchaseDatabase = new PurchaseDatabase(
							FacebookActivity.this);

					int bonusId = Integer.valueOf(mPurchaseDatabase
							.getBonusID("facebook"));

					mPurchaseDatabase.addBonus(bonusId, 1);
					mPurchaseDatabase.updateSocialDates("facebook",
							String.valueOf(System.currentTimeMillis()));
					
					GdxGame.showSocialPrizeMsg = true;
					
					switch (bonusId) {
					case 0:
						BBInventory.updateLaser(
								ShopAndroidHandler.getLaserOwnedItems(), true);
						
						GdxGame.socialPrizeId = 2;
						
						break;
					case 1:
						BBInventory
								.updateCoockie(ShopAndroidHandler
										.getCoockieOwnedItems(), true);
						
						GdxGame.socialPrizeId = 1;
						
						break;
					case 2:
						BBInventory
								.updateBeetle1(ShopAndroidHandler
										.getBeetle1OwnedItems(), true);
						
						GdxGame.socialPrizeId = 4;
						
						break;
					case 3:
						BBInventory
								.updateBeetle2(ShopAndroidHandler
										.getBeetle2OwnedItems(), true);
						
						GdxGame.socialPrizeId = 3;
						
						break;
					case 4:
						BBInventory.updateExpl(
								ShopAndroidHandler.getExplOwnedItems(), true);
						
						GdxGame.socialPrizeId = 5;
						
						break;
					case 5:
						BBInventory.updateEgg1(
								ShopAndroidHandler.getEgg1OwnedItems(), true);
						
						GdxGame.socialPrizeId = 6;
						
						break;
					case 6:
						BBInventory.updateEgg2(
								ShopAndroidHandler.getEgg2OwnedItems(), true);
						
						GdxGame.socialPrizeId = 7;
						
						break;
					default:
						break;
					}

					finish();
				}
			});

			return null;
		}

	}

	public void loginSuccess(Facebook facebook) {
		new LoginSuccess().execute(facebook);
	}

	public void logoutSuccess() {
		fbLoginManager.displayToast("Logout Success!");
	}

	public void loginFail() {
		fbLoginManager.displayToast("Login Epic Failed!");
	}
}