package com.workshop27.android.entities;

import org.kroz.activerecord.ActiveRecordBase;

public class Social extends ActiveRecordBase {
	public String socialNetwork;
	public String postDate;
	public String bonusId;
	public String bonusGenerateDate;

	public Social(String socialNetwork, String postDate, String bonusId,
			String bonusGenerateDate) {
		this.socialNetwork = socialNetwork;
		this.postDate = postDate;
		this.bonusId = bonusId;
		this.bonusGenerateDate = bonusGenerateDate;
	}

	public Social() {
	}

	public String getSocialNetwork() {
		return socialNetwork;
	}

	public void setSocialNetwork(String socialNetwork) {
		this.socialNetwork = socialNetwork;
	}

	public String getPostDate() {
		return postDate;
	}

	public void setPostDate(String postDate) {
		this.postDate = postDate;
	}

	public String getBonusId() {
		return bonusId;
	}

	public void setBonusId(String bonusId) {
		this.bonusId = bonusId;
	}

	public String getBonusGenerateDate() {
		return bonusGenerateDate;
	}

	public void setBonusGenerateDate(String bonusGenerateDate) {
		this.bonusGenerateDate = bonusGenerateDate;
	}

}
