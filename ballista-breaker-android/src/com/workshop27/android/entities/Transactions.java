package com.workshop27.android.entities;

import org.kroz.activerecord.ActiveRecordBase;

public class Transactions extends ActiveRecordBase {
	public String orderId;
    public String state;
    public String productId;
    public String purchaseTime;
    public String developerPayload;
    public String quantity;

	public Transactions(String orderId, String state, String productId ,String purchaseTime ,String developerPayload, String quantity) {
    	this.orderId = orderId;
    	this.state = state;
    	this.productId = productId;
    	this.purchaseTime = purchaseTime;
    	this.developerPayload = developerPayload;
    	this.quantity = quantity;
    }
    
    public Transactions() {
    }
   
    public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getPurchaseTime() {
		return purchaseTime;
	}

	public void setPurchaseTime(String purchaseTime) {
		this.purchaseTime = purchaseTime;
	}

	public String getDeveloperPayload() {
		return developerPayload;
	}

	public void setDeveloperPayload(String developerPayload) {
		this.developerPayload = developerPayload;
	}
	
    public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
}    


