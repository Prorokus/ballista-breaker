package com.workshop27.android.entities;

import org.kroz.activerecord.ActiveRecordBase;

public class Used extends ActiveRecordBase {
    public String productId;
    public String quantity;

    public Used(String productId, String quantity) {
    	this.productId = productId;
    	this.quantity = quantity;
    }
    
    public Used() {
    }

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
} 
