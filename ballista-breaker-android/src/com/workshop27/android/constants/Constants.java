package com.workshop27.android.constants;

public class Constants {

	public static final String ADVERTISING_GAME_MESSAGE = "Beetle Breaker is the best game in the world";
	public static final String SCORED_GAME_MESSAGE = "I scored %d points on the %d level to the %d stage";
	public static final String ICON_URL = "http://dl.dropbox.com/u/95805438/ic_launcher.png";
	public static final String FACEBOOK_APP_ID = "476788575696352";
	public static final String TWITTER_HASHTAG = "#bbreaker";
	public static final String OAUTH_KEY = "T0wOJOVRnYAnwlddFmDevQ";
	public static final String OAUTH_SECRET = "QWWoGqcYrCVxEk4zLu4x96n51OFCnQ8b8HzEwwwjlSI";
	public static final String OAUTH_CALLBACK_SCHEME = "x-beatlebreaker-oauth-twitter";
	public static final String TWITTER_USER = "YOUR_EMAIL_GOES_HERE";
	public static final String GOOGLE_PLAY_URL = "https://play.google.com/store/apps/details?id=com.masterofcode.android.bbreaker";

	// NOTIFICATION MESSAGE
	public static final String NOTIFICATION_MESSAGE_24_TEXT = "Ooh-la-la! Play Beetle Breaker now and get a free surprise bonus!";
	public static final String NOTIFICATION_MESSAGE_72_TEXT = "Whee! Some more free goodies in Beetle Breaker just for you!";
	public static final String NOTIFICATION_MESSAGE_168_TEXT = "Just feeling generous today! See what's up for grabs in BB!";
}
