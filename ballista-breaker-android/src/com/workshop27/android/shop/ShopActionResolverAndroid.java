package com.workshop27.android.shop;

import android.content.Intent;
import android.os.Handler;

import com.workshop27.android.ballistabreaker.BeetleBattleAndroidActivity;
import com.workshop27.ballistabreaker.ShopActionResolver;

public class ShopActionResolverAndroid implements ShopActionResolver {
	Handler uiThread;
	ShopAndroidHandler shopAndroidHandler;
	PurchaseDatabase mPurchaseDatabase;

	public ShopActionResolverAndroid(BeetleBattleAndroidActivity mContext,
			PurchaseDatabase purchaseDatabase) {
		this.mPurchaseDatabase = purchaseDatabase;
		uiThread = new Handler(); // This binds the handler to the "main"
									// thread, see documentation of handler
	}

	public void onPause() {
		this.shopAndroidHandler.onPause();
	}

	public void onResume() {
		this.shopAndroidHandler.onResume();
	}

	public void onStart() {
		this.shopAndroidHandler.onStart();
	}

	public void onStop() {
		this.shopAndroidHandler.onStop();
	}

	public void onDestroy() {
		this.shopAndroidHandler.onDestroy();
	}
	
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	this.shopAndroidHandler.onActivityResult(requestCode, resultCode, data);
    }

	@Override
	public void initShop() {
		if (this.shopAndroidHandler == null) {
			this.shopAndroidHandler = new ShopAndroidHandler(uiThread,
					this.mPurchaseDatabase);
		}
	}

	@Override
	public int getLaserOwnedItems() {
		return ShopAndroidHandler.getLaserOwnedItems();
	}

	@Override
	public int getBeetle1OwnedItems() {
		return ShopAndroidHandler.getBeetle1OwnedItems();
	}

	@Override
	public int getBeetle2OwnedItems() {
		return ShopAndroidHandler.getBeetle2OwnedItems();
	}

	@Override
	public int getCoockieOwnedItems() {
		return ShopAndroidHandler.getCoockieOwnedItems();
	}

	@Override
	public int getExplOwnedItems() {
		return ShopAndroidHandler.getExplOwnedItems();
	}

	@Override
	public int getEgg1OwnedItems() {
		return ShopAndroidHandler.getEgg1OwnedItems();
	}

	@Override
	public int getEgg2OwnedItems() {
		return ShopAndroidHandler.getEgg2OwnedItems();
	}

	@Override
	public void buyItem(int buyId) {
		shopAndroidHandler.onBuyButtonPressed(buyId);
	}

	@Override
	public void show() {
		shopAndroidHandler.show();
	}

	@Override
	public void updateItem(String productId, int quantity) {
		shopAndroidHandler.updateItem(productId, quantity);
	}

	@Override
	public String getPostTime(String socialNetwork) {
		return ShopAndroidHandler.getFacebookPostTime(socialNetwork);
	}

	@Override
	public String getBonusGenerateTime(String socialNetwork) {
		return ShopAndroidHandler.getBonusGenerateTime(socialNetwork);
	}

	@Override
	public void generateNewDayBonus(String socialNetwork) {
		ShopAndroidHandler.generateNewDayBonus(socialNetwork);

	}

	@Override
	public String getBonusId(String socialNetwork) {		
		return ShopAndroidHandler.getBonusId(socialNetwork);
	}
}
