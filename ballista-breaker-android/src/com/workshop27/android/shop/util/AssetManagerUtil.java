package com.workshop27.android.shop.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.AssetManager;
import android.util.Log;

public class AssetManagerUtil {

	public static class AssetManagerHolder {
		public static final AssetManagerUtil HOLDER_INSTANCE = new AssetManagerUtil();
	}

	private static Context context;

	public static AssetManagerUtil getInstance(Context context) {
		AssetManagerUtil.context = context;
		return AssetManagerHolder.HOLDER_INSTANCE;
	}

	public String getCurrentFolder() {

		PackageManager m = context.getPackageManager();
		String s = context.getPackageName();
		try {
			PackageInfo p = m.getPackageInfo(s, 0);
			s = p.applicationInfo.dataDir;
		} catch (NameNotFoundException e) {
			Log.w("yourtag", "Error Package name not found ", e);
		}
		return s;
	}

	public void copyAssets() {
		AssetManager assetManager = context.getAssets();

		InputStream in = null;
		OutputStream out = null;
		String filename = "ic_launcher.png";
		try {
			in = assetManager.open(filename);
			out = new FileOutputStream(getCurrentFolder() + "/" + filename);
			copyFile(in, out);
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
		} catch (IOException e) {
			Log.e("tag", "Failed to copy asset file: " + filename, e);
		}

	}

	private void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
	}

}
