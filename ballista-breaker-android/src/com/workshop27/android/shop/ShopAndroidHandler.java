package com.workshop27.android.shop;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.backup.BackupManager;
import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import com.flurry.android.FlurryAgent;
import com.workshop27.android.ballistabreaker.BeetleBattleAndroidActivity;
import com.workshop27.android.shop.util.IabHelper;
import com.workshop27.android.shop.util.IabResult;
import com.workshop27.android.shop.util.Inventory;
import com.workshop27.android.shop.util.Purchase;
import com.workshop27.ballistabreaker.engine.BBInventory;

public class ShopAndroidHandler {
	// Debug tag, for logging
	static final String TAG = "BeetleBreaker";

	private String ololoVerb2 = "XBA23LGGywWTga9PWZ+RDr+4fmuj7YtpyWTegZ60NPcVMTaqjJISZSkIj6ynp0GjfxFWtLTYblJYz6z55Zc8acLBKSY4LI";

	static PurchaseDatabase mPurchaseDatabase;

	private ProgressDialog mProgressDialog;

	public static BeetleBattleAndroidActivity mContext;

	private BackupManager mBackupManager;

	// The helper object
	private IabHelper mHelper;

	private String ololoVerb1 = "KVgAVDNyhZb/93Fk4CojY0Q3dkkXOvz2ISou8KLCOu6h";

	static final String SKU_TEST_PURCHASE = "android.test.purchased";

	private Handler mHandler;

	public ShopAndroidHandler(Handler uiThread,
			PurchaseDatabase purchaseDatabase) {
		mPurchaseDatabase = purchaseDatabase;

		mBackupManager = new BackupManager(mContext);

		onCreate(uiThread);
		onStart();
	}

	/** Called when the activity is first created. */
	private void onCreate(Handler uiThread) {
		mHandler = uiThread;

		StringBuilder sb = new StringBuilder();
		sb.append("MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCg");
		sb.append("KCAQEAiYBNoOMLQPh5rm8");
		sb.append("ULxAhCNpOeQKvW93S4vOkoE6t");
		sb.append(ololoVerb1);
		sb.append(ololoVerb2);
		sb.append("OdXOomTWZp4cLQ5rp+DlQUIjwRSBpgL0aXBw19LLWOSKedzjxDkucLMU5fJ7zRISHQeRv1n7LW7qs0CivsBGwX9oCojk5QOLVCgA");
		sb.append("+orTmIK6SDVHUD7T52RT+HcYfAyxAbbyw5ppt//R6dJ7sg55MNFcUYncjz+EBGhwIDAQAB");
		String base64EncodedPublicKey = sb.toString();

		// compute your public key and store it in base64EncodedPublicKey
		Log.d(TAG, "Creating IAB helper.");
		mHelper = new IabHelper(mContext, base64EncodedPublicKey);

		// enable debug logging (for a production application, you should set
		// this to false).
		mHelper.enableDebugLogging(true);

		Log.d(TAG, "Starting setup.");
		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			public void onIabSetupFinished(IabResult result) {
				if (!result.isSuccess()) {
					// Oh noes, there was a problem.
					complain("Problem setting up in-app billing: " + result);
					Log.d(TAG, "Problem setting up In-app Billing: " + result);
				}
				// Hooray, IAB is fully set up!
				Log.d(TAG, "Setup successful. Querying inventory.");
				mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		});

		mProgressDialog = new ProgressDialog(mContext);
	}

	// updates UI to reflect model
	private void updateAllUiElements() {
		BBInventory.updateLaser(getLaserOwnedItems(), true);
		BBInventory.updateBeetle1(getBeetle1OwnedItems(), true);
		BBInventory.updateBeetle2(getBeetle2OwnedItems(), true);
		BBInventory.updateCoockie(getCoockieOwnedItems(), true);
		BBInventory.updateExpl(getExplOwnedItems(), true);
		BBInventory.updateEgg1(getEgg1OwnedItems(), true);
		BBInventory.updateEgg2(getEgg2OwnedItems(), true);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + ","
				+ data);

		// Pass on the activity result to the helper for handling
		if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
			// not handled, so handle it ourselves (here's where you'd
			// perform any handling of activity results not related to in-app
			// billing...
		} else {
			Log.d(TAG, "onActivityResult handled by IABUtil.");
		}
	}

	// Listener that's called when we finish querying the items we own
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
		public void onQueryInventoryFinished(IabResult result,
				Inventory inventory) {
			Log.d(TAG, "Query inventory finished.");
			if (result.isFailure()) {
				complain("Failed to query inventory: " + result);
				return;
			}

			Log.d(TAG, "Query inventory was successful.");

			// Check for gas delivery -- if we own gas, we should fill up the
			// tank immediately
			if (inventory.hasPurchase(SKU_TEST_PURCHASE)) {
				Log.d(TAG, "We have gas. Consuming it.");
				mHelper.consumeAsync(inventory.getPurchase(SKU_TEST_PURCHASE),
						mConsumeFinishedListener);
				return;
			}

			updateAllUiElements();

			mHandler.post(new Runnable() {
				public void run() {
					setWaitScreen(false);
				}
			});

			Log.d(TAG, "Initial inventory query finished; enabling main UI.");
		}
	};

	// Callback for when a purchase is finished
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
			Log.d(TAG, "Purchase finished: " + result + ", purchase: "
					+ purchase);

			if (result.isFailure()) {
				// Oh noes!
				// complain("Error purchasing: " + result);

				mHandler.post(new Runnable() {
					public void run() {
						setWaitScreen(false);
					}
				});

				return;
			}

			Log.d(TAG, "Purchase successful.");

			if (purchase.getSku().equals(SKU_TEST_PURCHASE)) {
				// bought 1/4 tank of gas. So consume it.
				Log.d(TAG, "Purchase is gas. Starting gas consumption.");
				mHelper.consumeAsync(purchase, mConsumeFinishedListener);
			}
		}
	};

	// Called when consumption is complete
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			Log.d(TAG, "Consumption finished. Purchase: " + purchase
					+ ", result: " + result);

			// We know this is the "gas" sku because it's the only one we
			// consume,
			// so we don't check which sku was consumed. If you have more than
			// one
			// sku, you probably should check...
			if (result.isSuccess()) {
				// successfully consumed, so we apply the effects of the item in
				// our
				// game world's logic, which in our case means filling the gas
				// tank a bit
				Log.d(TAG, "Consumption successful. Provisioning.");

				mPurchaseDatabase.updatePurchase(purchase.getOrderId(),
						purchase.getSku(), purchase.getPurchaseState(),
						purchase.getPurchaseTime(),
						purchase.getDeveloperPayload());

				alert("Thanks for your purchase %)");
			} else {
				complain("Error while consuming: " + result);
			}

			updateAllUiElements();
			mBackupManager.dataChanged();

			mHandler.post(new Runnable() {
				public void run() {
					setWaitScreen(false);
				}
			});

			Log.d(TAG, "End consumption flow.");
			FlurryAgent.logEvent(String.valueOf(purchase.getPurchaseState()));
		}
	};

	// Enables or disables the "please wait" screen.
	void setWaitScreen(boolean set) {
		if (set) {
			mProgressDialog
					.setMessage("Working... Please don't leave this screen!");
			mProgressDialog.setIndeterminate(false);
			mProgressDialog.setMax(100);
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

		else {
			mProgressDialog.hide();
		}
	}

	void complain(String message) {
		Log.e(TAG, "**** BeetleBreaker Error: " + message);
		alert("Error: " + message);
	}

	void alert(String message) {
		AlertDialog.Builder bld = new AlertDialog.Builder(mContext);
		bld.setMessage(message);
		bld.setNeutralButton("OK", null);
		Log.d(TAG, "Showing alert dialog: " + message);
		bld.create().show();
	}

	public void show() {
		BBInventory.updateLaser(getLaserOwnedItems(), false);
		BBInventory.updateBeetle1(getBeetle1OwnedItems(), false);
		BBInventory.updateBeetle2(getBeetle2OwnedItems(), false);
		BBInventory.updateCoockie(getCoockieOwnedItems(), false);
		BBInventory.updateExpl(getExplOwnedItems(), false);
		BBInventory.updateEgg1(getEgg1OwnedItems(), false);
		BBInventory.updateEgg2(getEgg2OwnedItems(), false);
	}

	public void onStart() {
	}

	public void onStop() {

	}

	public void onResume() {
	}

	public void onPause() {
	}

	public void onDestroy() {
		if (mHelper != null)
			mHelper.dispose();
		mHelper = null;
	}

	public static int getLaserOwnedItems() {
		return mPurchaseDatabase.getItemLeft("android.laser.purchased");
	}

	public static int getBeetle1OwnedItems() {
		return mPurchaseDatabase.getItemLeft("android.beetle1.purchased");
	}

	public static int getBeetle2OwnedItems() {
		return mPurchaseDatabase.getItemLeft("android.beetle2.purchased");
	}

	public static int getCoockieOwnedItems() {
		return mPurchaseDatabase.getItemLeft("android.coockie.purchased");
	}

	public static int getExplOwnedItems() {
		return mPurchaseDatabase.getItemLeft("android.expl.purchased");
	}

	public static int getEgg1OwnedItems() {
		return mPurchaseDatabase.getItemLeft("android.egg1.purchased");
	}

	public static int getEgg2OwnedItems() {
		return mPurchaseDatabase.getItemLeft("android.egg2.purchased");
	}

	public static String getFacebookPostTime(String socialNetwork) {
		return mPurchaseDatabase.getLastPostTime(socialNetwork);
	}

	public static String getBonusGenerateTime(String socialNetwork) {
		return mPurchaseDatabase.getLastBonusGenerateTime(socialNetwork);
	}

	public static void generateNewDayBonus(String socialNetwork) {
		mPurchaseDatabase.updateBonusId(socialNetwork,
				String.valueOf(System.currentTimeMillis()));
	}

	public static String getBonusId(String socialNetwork) {
		return mPurchaseDatabase.getBonusID(socialNetwork);
	}

	/**
	 * Called when a button is pressed.
	 */
	public void onBuyButtonPressed(int buyId) {
		Log.d(TAG, "Buy gas button clicked.");

		// launch the gas purchase UI flow.
		// We will be notified of completion via mPurchaseFinishedListener
		mHandler.post(new Runnable() {
			public void run() {
				setWaitScreen(true);
			}
		});

		Log.d(TAG, "Launching purchase flow for gas.");

		if (buyId == 1) {
			mHelper.launchPurchaseFlow(mContext, SKU_TEST_PURCHASE, buyId,
					mPurchaseFinishedListener);
		}

		else if (buyId == 2) {
			mHelper.launchPurchaseFlow(mContext, SKU_TEST_PURCHASE, buyId,
					mPurchaseFinishedListener);
		}

		else if (buyId == 3) {
			mHelper.launchPurchaseFlow(mContext, SKU_TEST_PURCHASE, buyId,
					mPurchaseFinishedListener);
		}

		else if (buyId == 4) {
			mHelper.launchPurchaseFlow(mContext, SKU_TEST_PURCHASE, buyId,
					mPurchaseFinishedListener);
		}

		else if (buyId == 5) {
			mHelper.launchPurchaseFlow(mContext, SKU_TEST_PURCHASE, buyId,
					mPurchaseFinishedListener);
		}

		else if (buyId == 6) {
			mHelper.launchPurchaseFlow(mContext, SKU_TEST_PURCHASE, buyId,
					mPurchaseFinishedListener);
		}

		else if (buyId == 7) {
			mHelper.launchPurchaseFlow(mContext, SKU_TEST_PURCHASE, buyId,
					mPurchaseFinishedListener);
		}

		else if (buyId == 8) {
			mHelper.launchPurchaseFlow(mContext, SKU_TEST_PURCHASE, buyId,
					mPurchaseFinishedListener);
		}
	}

	public void updateItem(String productId, int quantity) {
		mPurchaseDatabase.updateUsedItem(productId,
				mPurchaseDatabase.getItemUsedTimes(productId) + quantity);

		if (productId.equals("android.laser.purchased")) {
			BBInventory.updateLaser(getLaserOwnedItems(), false);
		}

		else if (productId.equals("android.coockie.purchased")) {
			BBInventory.updateCoockie(getCoockieOwnedItems(), false);
		}

		else if (productId.equals("android.beetle1.purchased")) {
			BBInventory.updateBeetle1(getBeetle1OwnedItems(), false);
		}

		else if (productId.equals("android.beetle2.purchased")) {
			BBInventory.updateBeetle2(getBeetle2OwnedItems(), false);
		}

		else if (productId.equals("android.expl.purchased")) {
			BBInventory.updateExpl(getExplOwnedItems(), false);
		}

		else if (productId.equals("android.egg1.purchased")) {
			BBInventory.updateEgg1(getEgg1OwnedItems(), false);
		}

		else if (productId.equals("android.egg2.purchased")) {
			BBInventory.updateEgg2(getEgg2OwnedItems(), false);
		}
	}

}
