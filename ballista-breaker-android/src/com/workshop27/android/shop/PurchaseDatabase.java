package com.workshop27.android.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.kroz.activerecord.ActiveRecordBase;
import org.kroz.activerecord.ActiveRecordException;
import org.kroz.activerecord.Database;
import org.kroz.activerecord.DatabaseBuilder;
import org.kroz.activerecord.EntitiesHelper;
import org.kroz.activerecord.utils.Crypto;

import android.content.Context;
import com.workshop27.android.entities.Social;
import com.workshop27.android.entities.Transactions;
import com.workshop27.android.entities.Used;
import com.workshop27.android.shop.util.IabHelper;

public class PurchaseDatabase {
	private static final String DATABASE_NAME = "purchase.db";
	private static final int DATABASE_VERSION = 7;

	private ActiveRecordBase mDatabase;

	private Context mContext;

	public synchronized ActiveRecordBase getDatabase() {
		return mDatabase;
	}

	public PurchaseDatabase(Context context) {
		this.mContext = context;

		DatabaseBuilder builder = new DatabaseBuilder(DATABASE_NAME);
		builder.addClass(Transactions.class);
		builder.addClass(Used.class);
		builder.addClass(Social.class);
		Database.setBuilder(builder);

		try {
			mDatabase = ActiveRecordBase.open(context, DATABASE_NAME,
					DATABASE_VERSION);
		} catch (ActiveRecordException e) {
			e.printStackTrace();
		}
	}

	public void addBonus(int type, int count) {
		long purchaseTime = UUID.randomUUID().getMostSignificantBits();

		try {
			switch (type) {
			case 0:
				insertOrder("transactionId.android.laser.purchased",
						"android.laser.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						purchaseTime, "", count);
				break;
			case 1:
				insertOrder("transactionId.android.coockie.purchased",
						"android.coockie.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						purchaseTime, "", count);
				break;
			case 2:
				insertOrder("transactionId.android.beetle1.purchased",
						"android.beetle1.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						purchaseTime, "", count);
				break;
			case 3:
				insertOrder("transactionId.android.beetle2.purchased",
						"android.beetle2.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						purchaseTime, "", count);
				break;
			case 4:
				insertOrder("transactionId.android.expl.purchased",
						"android.expl.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						purchaseTime, "", count);
				break;
			case 5:
				insertOrder("transactionId.android.egg1.purchased",
						"android.egg1.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						purchaseTime, "", count);
				break;
			case 6:
				insertOrder("transactionId.android.egg2.purchased",
						"android.egg2.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						purchaseTime, "", count);

			default:

				break;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void initDb() {
		try {
			if (getItemPurchasedTimes("android.laser.purchased") == 0
					&& getItemPurchasedTimes("android.coockie.purchased") == 0
					&& getItemPurchasedTimes("android.beetle1.purchased") == 0
					&& getItemPurchasedTimes("android.beetle2.purchased") == 0
					&& getItemPurchasedTimes("android.expl.purchased") == 0
					&& getItemPurchasedTimes("android.egg1.purchased") == 0
					&& getItemPurchasedTimes("android.egg2.purchased") == 0) {
				insertOrder("transactionId.android.laser.purchased",
						"android.laser.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						12345, "", 2);
				insertOrder("transactionId.android.coockie.purchased",
						"android.coockie.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						12345, "", 2);
				insertOrder("transactionId.android.beetle1.purchased",
						"android.beetle1.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						12345, "", 2);
				insertOrder("transactionId.android.beetle2.purchased",
						"android.beetle2.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						12345, "", 2);
				insertOrder("transactionId.android.expl.purchased",
						"android.expl.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						12345, "", 2);
				insertOrder("transactionId.android.egg1.purchased",
						"android.egg1.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						12345, "", 2);
				insertOrder("transactionId.android.egg2.purchased",
						"android.egg2.purchased", IabHelper.BILLING_RESPONSE_RESULT_OK,
						12345, "", 2);

				initUsedTable("android.laser.purchased", 0);
				initUsedTable("android.coockie.purchased", 0);
				initUsedTable("android.beetle1.purchased", 0);
				initUsedTable("android.beetle2.purchased", 0);
				initUsedTable("android.expl.purchased", 0);
				initUsedTable("android.egg1.purchased", 0);
				initUsedTable("android.egg2.purchased", 0);

				Random randomGenerator = new Random();
				int randomBonus = randomGenerator.nextInt(7);

				addSocial("facebook", "0", String.valueOf(randomBonus), "0");
				randomBonus = randomGenerator.nextInt(7);
				addSocial("twitter", "0", String.valueOf(randomBonus), "0");

			}

		} catch (ActiveRecordException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close() {
		mDatabase.close();
	}

	public void open() {
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}

	/**
	 * Inserts a purchased product into the database. There may be multiple rows
	 * in the table for the same product if it was purchased multiple times or
	 * if it was refunded.
	 * 
	 * @param orderId
	 *            the order ID (matches the value in the product list)
	 * @param productId
	 *            the product ID (sku)
	 * @param state
	 *            the state of the purchase
	 * @param purchaseTime
	 *            the purchase time (in milliseconds since the epoch)
	 * @param developerPayload
	 *            the developer provided "payload" associated with the order.
	 * @throws Exception
	 */
	private void insertOrder(String orderId, String productId, int state, long purchaseTime, String developerPayload,
			int quantity) throws Exception {
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		Transactions transactions = mDatabase.newEntity(Transactions.class);
		transactions.setOrderId(Crypto.encrypt(this.mContext, orderId + purchaseTime));
		
		transactions.setProductId(Crypto.encrypt(this.mContext, productId));
		transactions.setState(Crypto.encrypt(this.mContext, Integer.toString(state)));
		transactions.setPurchaseTime(Crypto.encrypt(this.mContext, Long.toString(purchaseTime)));
		transactions.setQuantity(Crypto.encrypt(this.mContext, Integer.toString(quantity)));

		if (developerPayload == null) {
			developerPayload = "";
		}
		transactions.setDeveloperPayload(Crypto.encrypt(this.mContext,
				developerPayload));

		try {
			transactions.save();
		} catch (ActiveRecordException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Save the object to the database
	}

	private int getItemPurchasedTimes(String productId) {
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		int quantity = 0;

		try {
			List<Transactions> transactionsList = mDatabase.find(
					Transactions.class, "PRODUCT_ID=?",
					new String[] { Crypto.encrypt(this.mContext, productId) });
			List<String> checkedId = new ArrayList<String>();
			String idToCheck;

			// Count the number of times the product was purchased - times
			// product used = product left
			for (Transactions transaction : transactionsList) {
				int state = Integer.parseInt(Crypto.decrypt(this.mContext, transaction.getState()));
				
				idToCheck = Crypto.decrypt(this.mContext, transaction.getOrderId());

				if (state == IabHelper.BILLING_RESPONSE_RESULT_OK
						&& !checkedId.contains(idToCheck)) {
					quantity += Integer.parseInt(Crypto.decrypt(this.mContext,
							transaction.getQuantity()));
					checkedId.add(idToCheck);
				}

				if (transaction.isOpen()) {
					transaction = null;
				}
			}

			checkedId.clear();
			checkedId = null;

		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return quantity;
	}

	private void initUsedTable(String productId, int quantity) {
		try {
			Used usedItem = mDatabase.newEntity(Used.class);
			usedItem.setProductId(Crypto.encrypt(this.mContext, productId));
			usedItem.setQuantity(Crypto.encrypt(this.mContext,
					String.valueOf(quantity)));
			usedItem.save();

		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void addSocial(String socialNetwork, String date, String bonusId,
			String bonusGenerateDate) throws ActiveRecordException {
		// Create a Note entry by requesting it from the database connector
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		Social social = new Social(socialNetwork, date, bonusId,
				bonusGenerateDate);
		Social moveSocial = mDatabase.newEntity(Social.class);

		EntitiesHelper.copyFieldsWithoutID(moveSocial, social);

		// Set the attributes
		try {
			social.socialNetwork = Crypto.encrypt(this.mContext, socialNetwork);
			social.postDate = Crypto.encrypt(this.mContext, date);
			social.bonusId = Crypto.encrypt(this.mContext, bonusId);
			social.bonusGenerateDate = Crypto.encrypt(this.mContext,
					bonusGenerateDate);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		try {
			moveSocial.save();
		} catch (ActiveRecordException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // Save the object to the database
		mDatabase.close();
	}

	public void updateSocialDates(String socialNetwork, String date) {

		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		try {
			List<Social> social = mDatabase
					.find(Social.class, "SOCIAL_NETWORK=?",

							new String[] { Crypto.encrypt(this.mContext,
									socialNetwork) });

			social.get(0).setPostDate(date);

			if (!social.get(0).isOpen()) {
				social.get(0).open();
			}

			social.get(0).update();

		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void updateBonusDates(String socialNetwork, String date) {

		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		try {
			List<Social> social = mDatabase
					.find(Social.class, "SOCIAL_NETWORK=?",

							new String[] { Crypto.encrypt(this.mContext,
									socialNetwork) });

			social.get(0).setBonusGenerateDate(date);

			if (!social.get(0).isOpen()) {
				social.get(0).open();
			}

			social.get(0).update();

		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void updateBonusId(String socialNetwork, String generateTime) {

		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		try {
			List<Social> social = mDatabase
					.find(Social.class, "SOCIAL_NETWORK=?",

							new String[] { Crypto.encrypt(this.mContext,
									socialNetwork) });

			Random randomGenerator = new Random();
			int bonusId = randomGenerator.nextInt(7);
			social.get(0).setBonusId(String.valueOf(bonusId));
			social.get(0).setBonusGenerateDate(generateTime);		
			if (!social.get(0).isOpen()) {
				social.get(0).open();
			}

			social.get(0).update();

		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public String getLastPostTime(String socialNetwork) {
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		String date = "0";
		try {
			List<Social> social = mDatabase
					.find(Social.class, "SOCIAL_NETWORK=?",
							new String[] { Crypto.encrypt(this.mContext,
									socialNetwork) });
			if (social.size() > 0) {
				// Count the number of times the product was purchased - times
				// product used = product left
				date = Crypto.decrypt(this.mContext, social.get(0)
						.getPostDate());

			}
		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return date;
	}

	public String getLastBonusGenerateTime(String socialNetwork) {
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		String date = "0";
		try {
			List<Social> social = mDatabase
					.find(Social.class, "SOCIAL_NETWORK=?",
							new String[] { Crypto.encrypt(this.mContext,
									socialNetwork) });
			if (social.size() > 0) {
				// Count the number of times the product was purchased - times
				// product used = product left
				date = Crypto.decrypt(this.mContext, social.get(0)
						.getBonusGenerateDate());

			}
		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return date;
	}

	public String getBonusID(String socialNetwork) {
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		String date = "0";
		try {
			List<Social> social = mDatabase
					.find(Social.class, "SOCIAL_NETWORK=?",
							new String[] { Crypto.encrypt(this.mContext,
									socialNetwork) });
			if (social.size() > 0) {
				// Count the number of times the product was purchased - times
				// product used = product left
				date = Crypto
						.decrypt(this.mContext, social.get(0).getBonusId());

			}
		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return date;
	}

	public void updateUsedItem(String productId, int quantity) {
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		/*
		 * if (quantity == 0) { try { List<Used> used =
		 * mDatabase.find(Used.class, "PRODUCT_ID=?", new String[] {
		 * Crypto.encrypt(this.mContext, productId) });
		 * 
		 * if (used.size() > 0) { used.get(0).delete(); } } catch
		 * (ActiveRecordException e1) { // TODO Auto-generated catch block
		 * e1.printStackTrace(); } catch (Exception e) { // TODO Auto-generated
		 * catch block e.printStackTrace(); }
		 * 
		 * return; }
		 */

		try {
			List<Used> used = mDatabase.find(Used.class, "PRODUCT_ID=?",
					new String[] { Crypto.encrypt(this.mContext, productId) });
			if (used.size() > 0) {
				used.get(0)
						.setQuantity(
								Crypto.encrypt(this.mContext,
										String.valueOf(quantity)));

				if (!used.get(0).isOpen()) {
					used.get(0).open();
				}

				used.get(0).update();
				used.remove(0);
			}
		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int getItemUsedTimes(String productId) {
		if (!mDatabase.isOpen()) {
			try {
				mDatabase.open();
			} catch (ActiveRecordException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		int quantity = -1;

		try {
			List<Used> usedList = mDatabase.find(Used.class, "PRODUCT_ID=?",
					new String[] { Crypto.encrypt(this.mContext, productId) });
			if (usedList.size() > 0) {
				// Count the number of times the product was purchased - times
				// product used = product left
				quantity = Integer.parseInt(Crypto.decrypt(this.mContext,
						usedList.get(0).getQuantity()));

				usedList.remove(0);
			}
		} catch (ActiveRecordException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return quantity;
	}

	public int getItemLeft(String productId) {
		final int used = getItemUsedTimes(productId);
		if (used == -1) {
			return 0;
		}

		return getItemPurchasedTimes(productId) - used;
	}

	/**
	 * Adds the given purchase information to the database and returns the total
	 * number of times that the given product has been purchased.
	 * 
	 * @param orderId
	 *            a string identifying the order
	 * @param productId
	 *            the product ID (sku)
	 * @param purchaseState
	 *            the purchase state of the product
	 * @param purchaseTime
	 *            the time the product was purchased, in milliseconds since the
	 *            epoch (Jan 1, 1970)
	 * @param developerPayload
	 *            the developer provided "payload" associated with the order
	 * @return the number of times the given product has been purchased.
	 */
	public synchronized int updatePurchase(String orderId, String productId, int purchaseState, long purchaseTime,
			String developerPayload) {
		try {
			// updateUsedItem(productId, getItemUsedTimes(productId) + 5);
			insertOrder("transactionId.android.laser.purchased",
					"android.laser.purchased", purchaseState, java.lang.System.currentTimeMillis(),
					developerPayload, 5);
			insertOrder("transactionId.android.coockie.purchased",
					"android.coockie.purchased", purchaseState, java.lang.System.currentTimeMillis(),
					developerPayload, 5);
			insertOrder("transactionId.android.beetle1.purchased",
					"android.beetle1.purchased", purchaseState, java.lang.System.currentTimeMillis(),
					developerPayload, 5);
			insertOrder("transactionId.android.beetle2.purchased",
					"android.beetle2.purchased", purchaseState, java.lang.System.currentTimeMillis(),
					developerPayload, 5);
			insertOrder("transactionId.android.expl.purchased",
					"android.expl.purchased", purchaseState, java.lang.System.currentTimeMillis(),
					developerPayload, 5);
			insertOrder("transactionId.android.egg1.purchased",
					"android.egg1.purchased", purchaseState, java.lang.System.currentTimeMillis(),
					developerPayload, 5);
			insertOrder("transactionId.android.egg2.purchased",
					"android.egg2.purchased", purchaseState, java.lang.System.currentTimeMillis(),
					developerPayload, 5);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		int itemLeft = getItemLeft(productId);

		return itemLeft;
	}
}
