package com.workshop27.ballistabreaker.interfaces;

import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.workshop27.ballistabreaker.engine.GameObjectAccessor;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public abstract class AbstractBonusBeetle extends GameObjectAccessor {
	public enum BonusBeetleState {
		INFLY, BONUS, TAKEN, ACTIVATING, DEACTIVATING;
	}

	private int bonusValueCount = -1;
	private int initialBonusValueCount = -1;

	protected BonusBeetleState activeState;

	private int turnsVisibleBegin;
	private int turnsVisibleEnd;
	private Integer type;

	private float x0;

	private float y0;

	private boolean isStatic = false;

	public boolean isBlickTimerActive = false;

	public AbstractBonusBeetle(String name, String skinName) {
		super(name, skinName, false);
	}

	public void resetConsumableBonusValue() {
		setBonusValueCount(getInitialBonusValueCount());
	}

	public void setInitialBonusValueCount(int i) {
		this.initialBonusValueCount = i;
		resetConsumableBonusValue();
	}

	private int getInitialBonusValueCount() {
		return this.initialBonusValueCount;
	}

	public void setBonusValueCount(int i) {
		this.bonusValueCount = i;
	}

	public int getBonusValueCount() {
		return this.bonusValueCount;
	}

	public void startAnimation() {
		super.mDrawParticlesFirst = true;
		createParticle("magicFog", true);

		RotateToAction thisRotate1 = rotateTo(3, 2);
		RotateToAction thisRotate2 = rotateTo(-3, 2);
		super.addAction(forever(sequence(thisRotate1, thisRotate2)));

		MoveToAction thisMove1 = moveTo(this.getX(), this.getY() - 3, 1);
		MoveToAction thisMove2 = moveTo(this.getX(), this.getY() + 3, 1);
		super.addAction(forever(sequence(thisMove1, thisMove2)));
	}

	public void recalcCoordinates() {

	}

	public void setStatic(int s) {
		this.isStatic = s == 0 ? true : false;
	}

	public boolean isStatic() {
		return this.isStatic;
	}

	public void stopAnimation() {
		super.clearActions();
		super.setRotation(0);
		super.setScaleX(1);
		super.setScaleY(1);
	}

	public BonusBeetleState getActiveState() {
		return this.activeState;
	}

	public void setActiveState(BonusBeetleState activeState) {
		this.activeState = activeState;
	}

	public abstract void playBlick();

	public void setX(float x) {
//		this.setX(x);
		this.x0 = x;
	}

	public float getX() {
		return this.x0;
	}

	public void setY(float y) {
//		this.setY(y);
		this.y0 = y;
	}

	public float getY() {
		return this.y0;
	}

	public void setTurnsVisibleBegin(int turnsVisibleBegin) {
		this.turnsVisibleBegin = turnsVisibleBegin;
	}

	public void setTurnsVisibleEnd(int turnsVisibleEnd) {
		this.turnsVisibleEnd = turnsVisibleEnd;
	}

	public Integer getType() {
		return this.type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public int getTurnsVisibleBegin() {
		return this.turnsVisibleBegin;
	}

	public int getTurnsVisibleEnd() {
		return this.turnsVisibleEnd;
	}

}
