package com.workshop27.ballistabreaker.interfaces;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.engine.AudioPlayer;
import com.workshop27.ballistabreaker.engine.BBInventory.AvaliableBonuses;
import com.workshop27.ballistabreaker.engine.GameObject;
import com.workshop27.ballistabreaker.engine.PixmapHelper;
import com.workshop27.ballistabreaker.rocks.KarateBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateTo;

public abstract class AbstractBeetle extends GameObject implements Beetle {
	public enum BeetleState {
		SLEEP, ACTIVE, ANIM_TO_FLY, INFLY, BREAKING, ARMED, EXPLODING, EXPLODE, HIDDEN, FADING;
	}

	public boolean isFadeOut;

	public static float explodeCounter;

	protected BeetleState activeState;

	public float beetleSpeedX = 0;
	public float beetleSpeedY = 0;

	public float start_deceleration;
	public float deceleration;

	public int mineCount = 0;
	public boolean explodeOnTouch = true;

	private Integer type;

	public float speedCoeficient;

	private float prevX = 0;
	private float prevY = 0;
	private final Vector2 movingVec = new Vector2(0, 0);
	
	private final Vector2 v = new Vector2(0, 0);

	public boolean inRage = false;

	private boolean correctAngleAfterFirstStep = false;

	private float updateTimer = 0.1f;

	private float newAngle;

	private int turnsVisibleBegin;

	private int turnsVisibleEnd;

	private boolean isBonusBeetle;

	private float lastExplosionX = 0;

	private float lastExplosionY = 0;

	private final static Vector2 lastExplosionPos = new Vector2(0, 0);

	private int pongCounter = 0;

	public AbstractBeetle(String name, String skinName) {
		super(name, skinName, false);

        this.setTouchable(Touchable.childrenOnly);
	}

	public void setLastExplosionX(float lastExplosionX) {
		this.lastExplosionX = lastExplosionX;
	}

	public void setLastExplosionY(float lastExplosionY) {
		this.lastExplosionY = lastExplosionY;
	}

	public boolean isBonusBeetle() {
		return isBonusBeetle;
	}

	public void setBonusBeetle(boolean isBonusBeetle) {
		this.isBonusBeetle = isBonusBeetle;
	}

	public void setPongCounter(int p) {
		this.pongCounter = p;
	}

	public int getPongCounter() {
		return this.pongCounter;
	}

	public void resetPontCounter() {
		this.pongCounter = Constants.EGG_PONG_COUNTER;
	}

	public boolean abstractTouchDown() {
		if (getState() == BeetleState.SLEEP) {

			if (GameScreen.bManager.getActiveBeetle() != null
					&& GameScreen.bManager.getActiveBeetle().getState() != BeetleState.INFLY
					&& GameScreen.bManager.getActiveBeetle().getState() != BeetleState.ANIM_TO_FLY) {

				GameScreen.bManager.getActiveBeetle().setState(
						BeetleState.SLEEP);

				setState(BeetleState.ACTIVE);
				setPositionXYLiterally(Slingshot.getBombCenterX() - getWidth()
						/ 2, Slingshot.getBombCenterY() - getHeight() / 2);
				this.setRotation(Slingshot.getBombAngle());
				Slingshot.animateArmed();
				return true;

			} else if (GameScreen.bManager.getActiveBeetle() == null) {
				setState(BeetleState.ACTIVE);
				setPositionXYLiterally(Slingshot.getBombCenterX() - getWidth()
						/ 2, Slingshot.getBombCenterY() - getHeight() / 2);
				this.setRotation(Slingshot.getBombAngle());
				Slingshot.animateArmed();
				return true;
			}
		}

		return false;
	}

	public int getTurnsVisibleBegin() {
		return turnsVisibleBegin;
	}

	public int getTurnsVisibleEnd() {
		return turnsVisibleEnd;
	}

//	public void setX(float x) {
//		this.setX(x);
//	}
//
//	public void setY(float y) {
//		this.setY(y);
//	}

	@Override
	public void fly() {

		if (GameScreen.isGamePaused()) {
			return;
		}

		if (this.getOriginX() == 0 && this.getWidth() != 0) {
			this.setOriginX(this.getWidth() / 2);
			this.setOriginY(this.getHeight() / 2);
		}

		if (getState() == BeetleState.INFLY) {
			if (this.getColor().a < 0.05f) {
				this.setState(BeetleState.SLEEP);
			}

			// ----CHECK ON TOUCH EXPLOSION----
			if (this.explodeOnTouch
					&& GameScreen.objectToDestroy.checkCollision(
							this.getCenter().x, this.getCenter().y,
							this.getWidth(), this.getHeight())) {
				setState(BeetleState.ARMED);

				final float centerX = GameScreen.objectToDestroy.localPoint.x;
				final float centerY = GameScreen.objectToDestroy.getHeight()
						- GameScreen.objectToDestroy.localPoint.y;

				if (getType() == 10 || getType() == 11) {
					AudioPlayer.playSound("bomb_plant");
					this.clearParticle("p4");

					if (getType() == 10 && getState() != BeetleState.FADING) {
						GameScreen.bManager.checkConnections(this, this.getName());
					}

					if (getType() == 11 && getState() != BeetleState.FADING) {
						GameScreen.bManager.checkConnections(this, this.getName());
					}

					return;
				} else {
					explode(centerX, centerY);
					return;
				}
			}

			// ----CHECK STOP EXPLOSION----
			if ((beetleSpeedX > -30 && beetleSpeedX < 30)
					&& (beetleSpeedY > -30 && beetleSpeedY < 30)) {
				setState(BeetleState.ARMED);
				this.mineCount = 0;

				Vector2 localPoint = new Vector2(0, 0);
				
				v.x = this.getCenter().x;
				v.y = this.getCenter().y;
				localPoint = GameScreen.objectToDestroy.parentToLocalCoordinates(v);

				explode(localPoint.x, GameScreen.objectToDestroy.getHeight()
						- localPoint.y);

				if (getType() == 10 || getType() == 11) {
					AudioPlayer.playSound("bomb_plant");
					this.clearParticle("p4");

					if (getType() == 10) {
						GameScreen.bManager.checkConnections(this, getName());
					}

					if (getType() == 11) {
						GameScreen.bManager.checkConnections(this, getName());
					}
				}

				return;
			}

			// ----NO EXPLOSION - FLY----
			updateTimer -= Gdx.graphics.getDeltaTime();
			if (updateTimer <= 0) {
				if (deceleration == Integer.MAX_VALUE) {

				} else {
					deceleration -= deceleration * 0.17f;
				}
				if (beetleSpeedX > 0) {
					beetleSpeedX = beetleSpeedX - beetleSpeedX / deceleration;
				} else if (beetleSpeedX < 0) {
					beetleSpeedX = beetleSpeedX + -(beetleSpeedX)
							/ deceleration;
				}

				if (beetleSpeedY > 0) {
					beetleSpeedY = beetleSpeedY - beetleSpeedY / deceleration;
				} else if (beetleSpeedY < 0) {
					beetleSpeedY = beetleSpeedY + -(beetleSpeedY)
							/ deceleration;
				}

				updateTimer += 0.1f;
			}

			Vector2 v = new Vector2(this.getCenter().x - lastExplosionX,
					this.getCenter().y - lastExplosionY);
			if (getType() == 5
					&& GameScreen.objectToDestroy.checkCollision(
							this.getCenter().x, this.getCenter().y,
							this.getWidth(), this.getHeight())
					&& v.len() > Constants.KARATE_RADIUS) {
				GameScreen.bManager.shotKarateEgg(((KarateBeetle) this)
						.getNames().get(this.mineCount), this.getRotation());
				lastExplosionX = this.getCenter().x;
				lastExplosionY = this.getCenter().y;
			}

			this.prevX = super.getX();
			this.prevY = super.getY();
			super.setX(super.getX() + (beetleSpeedX * Gdx.graphics.getDeltaTime()));
			super.setY(super.getY() + (beetleSpeedY * Gdx.graphics.getDeltaTime()));

			// ToDo Костыль который выравнивает
			// угол жука если его по-дибильному
			// запустили
			if (!this.correctAngleAfterFirstStep) {
				movingVec.x = super.getX() - this.prevX;
				movingVec.y = super.getY() - this.prevY;
				this.setRotation(movingVec.angle() - 90);
				this.newAngle = this.getRotation();
				this.correctAngleAfterFirstStep = true;
			}

			if (super.getX() < 0) {
				if (getType() != 5 || getType() == 5
						&& super.getX() < super.getWidth() * (-1)) {
					AudioPlayer.playSound("hit");

					movingVec.x = super.getX() - this.prevX;
					movingVec.y = super.getY() - this.prevY;

					this.newAngle = (movingVec.angle() - 90) * (-1);
					if (this.newAngle > 90) {
						this.newAngle -= 360;
					} else if (this.newAngle < -90) {
						this.newAngle += 360;
					}

					this.clearActions();
					this.addAction(rotateTo(this.newAngle, 0.15f));

					beetleSpeedX = -beetleSpeedX; // Reflect along normal
					super.setX(0); // Re-position the ball at the edge
					if (getType() == 5) {
						this.setState(BeetleState.EXPLODE);
						this.clearParticle("p4");
					}

					if (getType() == 10 || getType() == 11) {
						if (--this.pongCounter <= 0) {
							this.setState(BeetleState.FADING);
						}
					}
				}

			} else if (getX() + this.getWidth() > GdxGame.VIRTUAL_GAME_WIDTH) {
				if (getType() != 5
						|| getType() == 5
						&& getX() + this.getWidth() > GdxGame.VIRTUAL_GAME_WIDTH
								+ super.getWidth()) {
					AudioPlayer.playSound("hit");

					movingVec.x = super.getX() - this.prevX;
					movingVec.y = super.getY() - this.prevY;
					float t = movingVec.angle();
					this.newAngle = (movingVec.angle() - 90) * (-1);

					this.clearActions();
					this.addAction(rotateTo(this.newAngle, 0.15f));

					beetleSpeedX = -beetleSpeedX;
					super.setX(GdxGame.VIRTUAL_GAME_WIDTH - this.getWidth());
					if (getType() == 5) {
						this.setState(BeetleState.EXPLODE);
						this.clearParticle("p4");
					}

					if (getType() == 10 || getType() == 11) {
						if (--this.pongCounter <= 0) {
							this.setState(BeetleState.FADING);
						}
					}
				}
			}

			// May cross both x and y bounds
			if (getY() < 0) {
				AudioPlayer.playSound("return_back");

				this.clearParticle("p4");
				setState(BeetleState.EXPLODE);
				if (GameScreen.reloadTimer <= 0) {
					GameScreen.reloadTimer = Constants.RELOADING_DELAY;
				}

				if (getType() == 10 || getType() == 11) {
					if (--this.pongCounter <= 0) {
						this.setState(BeetleState.FADING);
					}
				}

			} else if (super.getY() + this.getHeight() > GdxGame.VIRTUAL_GAME_HEIGHT) {
				if (getType() != 5
						|| getType() == 5
						&& super.getY() + this.getHeight() > GdxGame.VIRTUAL_GAME_HEIGHT
								+ super.getHeight()) {
					AudioPlayer.playSound("hit");

					movingVec.x = super.getX() - this.prevX;
					movingVec.y = super.getY() - this.prevY;
					float t = movingVec.angle();
					// this.newAngle = movingVec.angle() - 90;

					if (this.newAngle >= 0) {
						this.newAngle = this.newAngle * (-1) + 180;
					} else {
						this.newAngle = this.newAngle * (-1) - 180;
					}

					this.clearActions();
					this.addAction(rotateTo(this.newAngle, 0.15f));

					beetleSpeedY = -beetleSpeedY;
					setY(GdxGame.VIRTUAL_GAME_HEIGHT - this.getHeight());
					if (getType() == 5) {
						this.setState(BeetleState.EXPLODE);
						this.clearParticle("p4");
					}

					if (getType() == 10 || getType() == 11) {
						if (--this.pongCounter <= 0) {
							this.setState(BeetleState.FADING);
						}
					}
				}
			}
		}

		// ----NO EXPLOSION - NO FLY----
		else if (getState() == BeetleState.ACTIVE
				|| getState() == BeetleState.ANIM_TO_FLY) {

			setPositionXYLiterally(Slingshot.getBombCenterX() - getWidth() / 2,
					Slingshot.getBombCenterY() - getHeight() / 2);

			if (getState() != BeetleState.ANIM_TO_FLY) {
				this.setRotation(Slingshot.getBombAngle());
			}

			this.newAngle = this.getRotation();
		}

	}

	@Override
	public BeetleState getState() {
		return this.activeState;

	}

	@Override
	public void setState(BeetleState newState) {
		if (newState == BeetleState.EXPLODE
				&& GameScreen.bManager.getActiveBeetle() == this) {
			GameScreen.bManager.setActiveBeetle(null);

			// GameScreen.bManager.loadNextBeetle();
			if (GameScreen.reloadTimer <= 0) {
				GameScreen.reloadTimer = Constants.RELOADING_DELAY;
			}

			GameScreen.bManager.pixelsLabelValue = PixmapHelper.pixelsDestroyed;
			GameScreen.bManager.pixelsLabelValue = GameScreen.bManager.pixelsLabelValue
					/ GameScreen.objectToDestroy.pixelsToDestroy * 100;

			GameScreen.jar.setProgress(GameScreen.bManager.pixelsLabelValue);
		}

		this.activeState = newState;
		onStateChanged(this.activeState);
	}

	@Override
	public void onStateChanged(BeetleState newState) {

		if (newState == BeetleState.ACTIVE) {
			GameScreen.bManager.setActiveBeetle(this);
			this.correctAngleAfterFirstStep = false;
		}

		if (newState == BeetleState.INFLY) {
			if (this.getName().equals("boom-shop")) {
				GdxGame.mInventory
						.activeBonusesRemove(AvaliableBonuses.BOOM_BUG);
			}

			else if (this.getName().equals("karate-shop")) {
				GdxGame.mInventory
						.activeBonusesRemove(AvaliableBonuses.KARATE_BUG);
			}
		}

		if (newState == BeetleState.FADING) {
			AlphaAction f = fadeOut(0.25f);
			this.addAction(f);

			new Timer().schedule(new TimerTask() {
				@Override
				public void run() {
					setState(BeetleState.EXPLODE);
				}
			}, 250);
		}
	}

	public Integer getType() {
		return type;
	}

	@Override
	public void init(HashMap<String, Integer> params) {
		speedCoeficient = params.get("speedCoeficient") / 10.0f;
		deceleration = params.get("deceleration");
		start_deceleration = params.get("deceleration");
		type = params.get("type");
	}

	public static Vector2 getLastExplPos() {
		return lastExplosionPos;
	}

	@Override
	public void explode(float centerX, float centerY) {
		AbstractBeetle.explodeCounter++;
		final float bombX = this.getCenter().x;
		final float bombY = this.getCenter().y;

		lastExplosionPos.x = bombX;
		lastExplosionPos.y = bombY;

		final float cenX = centerX;
		final float cenY = centerY;

		final AbstractBeetle beetle = this;

		final boolean thisBeetleInRage = this.inRage;

		GameScreen.mOffsetX = 0;
		GameScreen.mOffsetY = 0;

		beetle.setState(BeetleState.EXPLODING);
		this.clearParticle("p4");

		if (getType() < 10) {
			// if (GameScreen.reloadTimer <= 0) {
			GameScreen.reloadTimer = Constants.RELOADING_DELAY + 0.5f;
			// }

			GameScreen.particle_explosion.setPositionXYLiterally(bombX, bombY);

			new Thread(new Runnable() {
				@Override
				public void run() {
					GameScreen.particle_explosion.getColor().a = 1;
					GameScreen.particle_explosion.particleStack.get(
							"explosion" + GdxGame.stageNumber).start();
					beetle.setState(BeetleState.EXPLODE);

					if (thisBeetleInRage) {
						GameScreen.objectToDestroy.pixmapHelper.breakBigCircle(
								(int) cenX, (int) cenY);
					} else {
						GameScreen.objectToDestroy.pixmapHelper.breakCircle(
								(int) cenX, (int) cenY);
					}
				}
			}, "breakCircle").start();

			AudioPlayer.playSound("explode1");
			GameScreen.bManager.calcChainExplosion(bombX, bombY, this.getName());

		} else {
			new Thread(new Runnable() {
				@Override
				public void run() {
					beetle.isParticlesPaused = false;

					beetle.setState(BeetleState.EXPLODE);
					beetle.setPositionXYLiterally(bombX, bombY);
					beetle.particleStack.get("explosion" + GdxGame.stageNumber)
							.start();

					if (thisBeetleInRage) {
						GameScreen.objectToDestroy.pixmapHelper.breakBigCircle(
								(int) cenX, (int) cenY);
					} else {
						GameScreen.objectToDestroy.pixmapHelper.breakCircle(
								(int) cenX, (int) cenY);
					}
				}
			}, "breakCircle").start();

			GameScreen.bManager.calcChainExplosion(bombX, bombY, this.getName());
			AudioPlayer.playSound("explode2");
		}

		AudioPlayer.setRandomMusic(true);

		GameScreen.pointsTimer = 1;
	}

	public float getNewAngle() {
		return newAngle;
	}

}
