package com.workshop27.ballistabreaker.interfaces;

import java.util.ArrayList;
import java.util.HashMap;

import com.workshop27.ballistabreaker.interfaces.AbstractBeetle.BeetleState;

public interface Beetle {

	public void explode(float centerX, float cemterY);

	public void fly();

	public ArrayList<String> getNames();

	public BeetleState getState();

	public void setState(BeetleState newState);

	public void onStateChanged(BeetleState newState);

	public void init(HashMap<String, Integer> params);

}
