package com.workshop27.ballistabreaker;

public interface ShopActionResolver {
	public void initShop();

	public void buyItem(int buyId);

	public void show();

	public void updateItem(String productId, int quantity);

	public int getLaserOwnedItems();

	public int getBeetle1OwnedItems();

	public int getBeetle2OwnedItems();

	public int getCoockieOwnedItems();

	public int getExplOwnedItems();

	public int getEgg1OwnedItems();

	public int getEgg2OwnedItems();

	public String getPostTime(String socialNetwork);

	public String getBonusGenerateTime(String socialNetwork);

	public void generateNewDayBonus(String socialNetwork);

	public String getBonusId(String socialNetwork);
}