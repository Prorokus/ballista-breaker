package com.workshop27.ballistabreaker.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.constants.Texts;
import com.workshop27.ballistabreaker.engine.AnimatedTextGameObject;
import com.workshop27.ballistabreaker.engine.AudioPlayer;
import com.workshop27.ballistabreaker.engine.BBInventory;
import com.workshop27.ballistabreaker.engine.GameObject;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.engine.PopupBackground;
import com.workshop27.ballistabreaker.engine.ShopBtn;
import com.workshop27.ballistabreaker.engine.BBInventory.InvState;
import com.workshop27.ballistabreaker.engine.ShowTutorialObject.ArrowPosEnum;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class ShopScreen implements Screen {
	private final GdxGame game;
	public static Group rootGroup;

	private final SpriteBatch spriteBatch;

	private boolean isButtonsInited;

	private Boolean firsEnterFrame = true;

	public static PopupBackground backgroundComplete;
	private static ShopBtn buy_btn;
	private static ShopBtn no_btn;

	private final GameObject yes_no_back;

	public static int mBtnClickedId = 0;

	private final AnimatedTextGameObject _99price;
	private final AnimatedTextGameObject text1;
	private final AnimatedTextGameObject name1;

	public ShopScreen(GdxGame game) {
		spriteBatch = GdxGame.beetleStage.getSpriteBatch();
		this.game = game;

		rootGroup = new Group();
		rootGroup.setName("shopGroup");

		GameObject background = new GameObject("background", false, false);
		background.setPositionXY(0, 0);
		background.folderName = "shop";
		background.loadSkinOnDraw = true;
		rootGroup.addActor(background);

		// ------------------MAGIC FLOWER------------------------------------
		GameObject flower1 = new GameObject("flower1", false, true);
		flower1.skinName = "flower1";
		flower1.setPositionXY(413, 286);
		flower1.folderName = "shop";
		flower1.loadSkinOnDraw = true;

		flower1.setOriginX(9);
		flower1.setOriginY(3);
		flower1.setOnCreateFilter(true);

		RotateToAction flower1_rotate1 = rotateTo(5, 6);
		RotateToAction flower1_rotate2 = rotateTo(-5, 6);
		flower1.addAction(forever(sequence(flower1_rotate1, flower1_rotate2)));

		rootGroup.addActor(flower1);

		GameObject flower2 = new GameObject("flower2", false, true);
		flower2.skinName = "flower2";
		flower2.setPositionXY(5, 30);
		flower2.folderName = "shop";
		flower2.loadSkinOnDraw = true;

		flower2.setOriginX(2);
		flower2.setOriginY(3);
		flower2.setOnCreateFilter(true);

		RotateToAction flower2_rotate1 = rotateTo(5, 6);
		RotateToAction flower2_rotate2 = rotateTo(-5, 6);
		flower2.addAction(forever(sequence(flower2_rotate1, flower2_rotate2)));

		flower1.addActor(flower2);

		GameObject flower3 = new GameObject("flower3", false, true);
		flower3.skinName = "flower3";
		flower3.setPositionXY(-35, 30);
		flower3.folderName = "shop";
		flower3.loadSkinOnDraw = true;

		flower3.setOnCreateFilter(true);

		MoveToAction flower3_move1 = moveTo(-35, 32, 3);
		MoveToAction flower3_move2 = moveTo(-35, 28, 3);
		flower3.addAction(forever(sequence(flower3_move1, flower3_move2)));

		flower1.addActor(flower3);

		GameObject magic_dust = new GameObject("magic_dust", false);
		magic_dust.setOriginX(34);
		magic_dust.setOriginY(38);
		magic_dust.skinName = "";
		magic_dust.createParticle("magicDust", true);
		flower3.addActor(magic_dust);

		// --------------------BREAD-----------------------------------------
		GameObject bread = new GameObject("bread", false, true);
		bread.skinName = "bread";
		bread.setPositionXY(113, 230);
		bread.folderName = "shop";
		bread.loadSkinOnDraw = true;

		rootGroup.addActor(bread);
		// -------------------------------------------------------------------

		// ------------------COOCKIE------------------------------------
		ShopBtn coockies_container = new ShopBtn("coockies_container", false,
				true, this.game);
		coockies_container.skinName = "";
		coockies_container.setPositionXY(45, 386);
		coockies_container.setWidth(167);
		coockies_container.setHeight(136);
		coockies_container.folderName = "shop";

		coockies_container.setOriginX(51);
		coockies_container.setOriginY(0);

		ScaleToAction coockies_scale1 = scaleTo(1.03f, 0.97f, 1);
		ScaleToAction coockies_scale2 = scaleTo(1.0f, 1.0f, 1);
		coockies_container.addAction(forever(sequence(coockies_scale1,
				coockies_scale2)));

		rootGroup.addActor(coockies_container);

		GameObject coockies = new GameObject("coockies2", false, true);
		coockies.skinName = "expl-power";
		coockies.setPositionXY(0, 0);
		coockies.folderName = "shop";
		coockies.loadSkinOnDraw = true;
		coockies.setOnCreateFilter(true);

		coockies_container.addActor(coockies);

		GameObject coockie_price2 = new GameObject("coockie_price2", false,
				true);
		coockie_price2.skinName = "price2";
		coockie_price2.setPositionXY(35, 390);
		coockie_price2.folderName = "shop";
		coockie_price2.loadSkinOnDraw = true;
		coockie_price2.setOnCreateFilter(true);
		rootGroup.addActor(coockie_price2);

		AnimatedTextGameObject coockie_price30 = new AnimatedTextGameObject(
				"coockie_price30", false, "popup", 20);
		coockie_price30.setTextCenteredOnPosition(
				Texts.SHOP_TEXT_PRICE_MUSTACHE, 65, 406, 100, false);
		coockie_price30.setOrigin(coockie_price30.getPositionX(),
				coockie_price30.getPositionY());
		coockie_price30.setScale(0.8f, 0.8f);
		coockie_price30.setColor(0.15f, 0.3f, 0.15f, 1.0f);
		rootGroup.addActor(coockie_price30);

		AnimatedTextGameObject coockie_x3 = new AnimatedTextGameObject(
				"coockie_x3", false, "tutorial", 30);
		coockie_x3.setTextCenteredOnPosition(Texts.SHOP_TEXT_QUANTITY_X5, 109,
				479, 100, false);
		coockie_x3.setOrigin(coockie_x3.getPositionX() + 20,
				coockie_x3.getPositionY() + 30);
		coockie_x3.setColor(0.9f, 0.9f, 0.65f, 1.0f);

		RotateToAction coockie_x3_rotate1 = rotateTo(0, 1);
		RotateToAction coockie_x3_rotate2 = rotateTo(-4, 1);
		coockie_x3.addAction(forever(sequence(coockie_x3_rotate1,
				coockie_x3_rotate2)));

		ScaleToAction coockie_x3_scale1 = scaleTo(1.05f, 0.95f, 1);
		ScaleToAction coockie_x3_scale2 = scaleTo(1.0f, 1.0f, 1);
		coockie_x3.addAction(forever(sequence(coockie_x3_scale1,
				coockie_x3_scale2)));

		rootGroup.addActor(coockie_x3);

		// ------------------BONUS RADIUS------------------------------------
		ShopBtn expl_container = new ShopBtn("expl_container", false, true,
				this.game);
		expl_container.skinName = "";
		expl_container.setPositionXY(100, 566);
		expl_container.setWidth(82);
		expl_container.setHeight(85);
		expl_container.folderName = "shop";

		expl_container.setOriginX(41);
		expl_container.setOriginY(0);

		ScaleToAction expl_scale1 = scaleTo(1.03f, 0.97f, 1);
		ScaleToAction expl_scale2 = scaleTo(1.0f, 1.0f, 1);
		expl_container.addAction(forever(sequence(expl_scale1, expl_scale2)));

		rootGroup.addActor(expl_container);

		GameObject expl = new GameObject("expl", false, true);
		expl.skinName = "expl-range";
		expl.setPositionXY(0, 0);
		expl.folderName = "shop";
		expl.loadSkinOnDraw = true;
		expl.setOnCreateFilter(true);

		expl_container.addActor(expl);

		GameObject expl_price2 = new GameObject("expl_price2", false, true);
		expl_price2.skinName = "price2";
		expl_price2.setPositionXY(84, 568);
		expl_price2.folderName = "shop";
		expl_price2.loadSkinOnDraw = true;
		expl_price2.setOnCreateFilter(true);
		rootGroup.addActor(expl_price2);

		AnimatedTextGameObject expl_price30 = new AnimatedTextGameObject(
				"expl_price30", false, "popup", 20);
		expl_price30.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_RADIUS,
				114, 584, 100, false);
		expl_price30.setOrigin(expl_price30.getPositionX(),
				expl_price30.getPositionY());
		expl_price30.setScale(0.8f, 0.8f);
		expl_price30.setColor(0.15f, 0.3f, 0.15f, 1.0f);
		rootGroup.addActor(expl_price30);

		AnimatedTextGameObject expl_x3 = new AnimatedTextGameObject("expl_x3",
				false, "tutorial", 30);
		expl_x3.setTextCenteredOnPosition(Texts.SHOP_TEXT_QUANTITY_X5, 168,
				652, 100, false);

		expl_x3.setOrigin(expl_x3.getPositionX() + 20,
				expl_x3.getPositionY() + 30);
		expl_x3.setColor(0.9f, 0.9f, 0.65f, 1.0f);

		RotateToAction expl_x3_rotate1 = rotateTo(0, 1);
		RotateToAction expl_x3_rotate2 = rotateTo(-4, 1);
		expl_x3.addAction(forever(sequence(expl_x3_rotate1, expl_x3_rotate2)));

		ScaleToAction expl_x3_scale1 = scaleTo(1.05f, 0.95f, 1);
		ScaleToAction expl_x3_scale2 = scaleTo(1.0f, 1.0f, 1);
		expl_x3.addAction(forever(sequence(expl_x3_scale1, expl_x3_scale2)));

		// ------------------BONUS EGG 1------------------------------------
		ShopBtn egg1_container = new ShopBtn("egg1_container", false, true,
				this.game);
		egg1_container.skinName = "";
		egg1_container.setPositionXY(165, 243);
		egg1_container.setWidth(75);
		egg1_container.setHeight(88);
		egg1_container.folderName = "shop";

		egg1_container.setOriginX(39);
		egg1_container.setOriginY(0);

		ScaleToAction egg1_scale1 = scaleTo(1.03f, 0.97f, 1);
		ScaleToAction egg1_scale2 = scaleTo(1.0f, 1.0f, 1);
		egg1_container.addAction(forever(sequence(egg1_scale1, egg1_scale2)));

		rootGroup.addActor(egg1_container);

		GameObject egg1 = new GameObject("egg1", false, true);
		egg1.skinName = "egg1";
		egg1.setPositionXY(0, 0);
		egg1.folderName = "shop";
		egg1.loadSkinOnDraw = true;
		egg1.setOnCreateFilter(true);

		egg1_container.addActor(egg1);

		GameObject egg1_price2 = new GameObject("egg1_price2", false, true);
		egg1_price2.skinName = "price2";
		egg1_price2.setPositionXY(157, 240);
		egg1_price2.folderName = "shop";
		egg1_price2.loadSkinOnDraw = true;
		egg1_price2.setOnCreateFilter(true);
		rootGroup.addActor(egg1_price2);

		AnimatedTextGameObject egg1_price30 = new AnimatedTextGameObject(
				"egg1_price30", false, "popup", 20);
		egg1_price30.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_EGG1, 187,
				256, 100, false);
		egg1_price30.setOrigin(egg1_price30.getPositionX(),
				egg1_price30.getPositionY());
		egg1_price30.setScale(0.8f, 0.8f);
		egg1_price30.setColor(0.15f, 0.3f, 0.15f, 1.0f);
		rootGroup.addActor(egg1_price30);

		AnimatedTextGameObject egg1_x3 = new AnimatedTextGameObject("egg1_x3",
				false, "tutorial", 30);
		egg1_x3.setTextCenteredOnPosition(Texts.SHOP_TEXT_QUANTITY_X5, 231,
				334, 100, false);
		egg1_x3.setOrigin(egg1_x3.getPositionX() + 20,
				egg1_x3.getPositionY() + 30);
		egg1_x3.setColor(0.9f, 0.9f, 0.65f, 1.0f);

		RotateToAction egg1_x3_rotate1 = rotateTo(0, 1);
		RotateToAction egg1_x3_rotate2 = rotateTo(-4, 1);
		egg1_x3.addAction(forever(sequence(egg1_x3_rotate1, egg1_x3_rotate2)));

		ScaleToAction egg1_x3_scale1 = scaleTo(1.05f, 0.95f, 1);
		ScaleToAction egg1_x3_scale2 = scaleTo(1.0f, 1.0f, 1);
		egg1_x3.addAction(forever(sequence(egg1_x3_scale1, egg1_x3_scale2)));

		rootGroup.addActor(egg1_x3);

		// ------------------BONUS EGG 2------------------------------------
		ShopBtn egg2_container = new ShopBtn("egg2_container", false, true,
				this.game);
		egg2_container.skinName = "";
		egg2_container.setPositionXY(275, 245);
		egg2_container.setWidth(75);
		egg2_container.setHeight(88);
		egg2_container.folderName = "shop";

		egg2_container.setOriginX(38);
		egg2_container.setOriginY(0);

		ScaleToAction egg2_scale1 = scaleTo(1.03f, 0.97f, 1);
		ScaleToAction egg2_scale2 = scaleTo(1.0f, 1.0f, 1);
		egg2_container.addAction(forever(sequence(egg2_scale1, egg2_scale2)));

		rootGroup.addActor(egg2_container);

		GameObject egg2 = new GameObject("egg2", false, true);
		egg2.skinName = "egg2";
		egg2.setPositionXY(0, 0);
		egg2.folderName = "shop";
		egg2.loadSkinOnDraw = true;
		egg2.setOnCreateFilter(true);

		egg2_container.addActor(egg2);

		GameObject egg2_price2 = new GameObject("egg2_price2", false, true);
		egg2_price2.skinName = "price2";
		egg2_price2.setPositionXY(265, 240);
		egg2_price2.folderName = "shop";
		egg2_price2.loadSkinOnDraw = true;
		egg2_price2.setOnCreateFilter(true);
		rootGroup.addActor(egg2_price2);

		AnimatedTextGameObject egg2_price30 = new AnimatedTextGameObject(
				"egg2_price30", false, "popup", 20);
		egg2_price30.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_EGG2, 295,
				256, 100, false);
		egg2_price30.setOrigin(egg2_price30.getPositionX(),
				egg2_price30.getPositionY());
		egg2_price30.setScale(0.8f, 0.8f);
		egg2_price30.setColor(0.15f, 0.3f, 0.15f, 1.0f);
		rootGroup.addActor(egg2_price30);

		AnimatedTextGameObject egg2_x3 = new AnimatedTextGameObject("egg2_x3",
				false, "tutorial", 30);
		egg2_x3.setTextCenteredOnPosition(Texts.SHOP_TEXT_QUANTITY_X5, 339,
				334, 100, false);
		egg2_x3.setOrigin(egg2_x3.getPositionX() + 20,
				egg2_x3.getPositionY() + 30);
		egg2_x3.setColor(0.9f, 0.9f, 0.65f, 1.0f);

		RotateToAction egg2_x3_rotate1 = rotateTo(0, 1);
		RotateToAction egg2_x3_rotate2 = rotateTo(-4, 1);
		egg2_x3.addAction(forever(sequence(egg2_x3_rotate1, egg2_x3_rotate2)));

		ScaleToAction egg2_x3_scale1 = scaleTo(1.05f, 0.95f, 1);
		ScaleToAction egg2_x3_scale2 = scaleTo(1.0f, 1.0f, 1);
		egg2_x3.addAction(forever(sequence(egg2_x3_scale1, egg2_x3_scale2)));

		rootGroup.addActor(egg2_x3);

		// ------------------BUNDLE PACK------------------------------------
		ShopBtn bundle_container = new ShopBtn("bundle_container", false, true,
				this.game);
		bundle_container.skinName = "";
		bundle_container.setPositionXY(310, 391);
		bundle_container.setWidth(132);
		bundle_container.setHeight(104);
		bundle_container.folderName = "shop";

		bundle_container.setOriginX(66);
		bundle_container.setOriginY(10);

		ScaleToAction bundle_scale1 = scaleTo(1.03f, 0.97f, 1);
		ScaleToAction bundle_scale2 = scaleTo(1.0f, 1.0f, 1);
		bundle_container.addAction(forever(sequence(bundle_scale1,
				bundle_scale2)));

		rootGroup.addActor(bundle_container);

		GameObject bundle = new GameObject("bundle", false, true);
		bundle.skinName = "pack";
		bundle.setPositionXY(0, 0);
		bundle.folderName = "shop";
		bundle.loadSkinOnDraw = true;
		bundle.setOnCreateFilter(true);

		bundle_container.addActor(bundle);

		GameObject bundle_price2 = new GameObject("bundle_price2", false, true);
		bundle_price2.skinName = "price2";
		bundle_price2.setPositionXY(308, 391);
		bundle_price2.folderName = "shop";
		bundle_price2.loadSkinOnDraw = true;
		bundle_price2.setOnCreateFilter(true);
		rootGroup.addActor(bundle_price2);

		AnimatedTextGameObject bundle_price30 = new AnimatedTextGameObject(
				"bundle_price30", false, "popup", 20);
		bundle_price30.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_BUNDLE,
				338, 407, 100, false);
		bundle_price30.setOrigin(bundle_price30.getPositionX(),
				bundle_price30.getPositionY());
		bundle_price30.setScale(0.8f, 0.8f);
		bundle_price30.setColor(0.15f, 0.3f, 0.15f, 1.0f);
		rootGroup.addActor(bundle_price30);

		AnimatedTextGameObject bundle_x3 = new AnimatedTextGameObject(
				"bundle_x3", false, "tutorial", 30);
		bundle_x3.setTextCenteredOnPosition(Texts.SHOP_TEXT_QUANTITY_X5, 402,
				510, 100, false);
		bundle_x3.setOrigin(bundle_x3.getPositionX() + 20,
				bundle_x3.getPositionY() + 30);
		bundle_x3.setColor(0.9f, 0.9f, 0.65f, 1.0f);

		RotateToAction bundle_x3_rotate1 = rotateTo(0, 1);
		RotateToAction bundle_x3_rotate2 = rotateTo(-4, 1);
		bundle_x3.addAction(forever(sequence(bundle_x3_rotate1,
				bundle_x3_rotate2)));

		ScaleToAction bundle_x3_scale1 = scaleTo(1.05f, 0.95f, 1);
		ScaleToAction bundle_x3_scale2 = scaleTo(1.0f, 1.0f, 1);
		bundle_x3.addAction(forever(sequence(bundle_x3_scale1,
				bundle_x3_scale2)));

		rootGroup.addActor(bundle_x3);

		// ------------------SIGHT------------------------------------
		ShopBtn sight_container = new ShopBtn("sight_container", false, true,
				this.game);
		sight_container.skinName = "";
		sight_container.setPositionXY(170, 392);
		sight_container.setWidth(87);
		sight_container.setHeight(33);
		sight_container.folderName = "shop";

		sight_container.setOriginX(44);
		sight_container.setOriginY(3);

		ScaleToAction sight_container_scale1 = scaleTo(1.03f, 0.97f, 1);
		ScaleToAction sight_container_scale2 = scaleTo(1.0f, 1.0f, 1);
		sight_container.addAction(forever(sequence(sight_container_scale1,
				sight_container_scale2)));

		rootGroup.addActor(sight_container);

		GameObject laser_sight = new GameObject("laser-sight", false, true);
		laser_sight.skinName = "laser-sight";
		laser_sight.setPositionXY(0, 0);
		laser_sight.folderName = "shop";
		laser_sight.loadSkinOnDraw = true;
		laser_sight.setOnCreateFilter(true);

		sight_container.addActor(laser_sight);

		GameObject sight_price2 = new GameObject("sight_price2", false, true);
		sight_price2.skinName = "price2";
		sight_price2.setPositionXY(201, 390);
		sight_price2.folderName = "shop";
		sight_price2.loadSkinOnDraw = true;
		sight_price2.setOnCreateFilter(true);
		rootGroup.addActor(sight_price2);

		AnimatedTextGameObject sight_price30 = new AnimatedTextGameObject(
				"sight_price30", false, "popup", 20);
		sight_price30.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_LASER,
				231, 406, 100, false);
		sight_price30.setOrigin(sight_price30.getPositionX(),
				sight_price30.getPositionY());
		sight_price30.setScale(0.8f, 0.8f);
		sight_price30.setColor(0.15f, 0.3f, 0.15f, 1.0f);
		rootGroup.addActor(sight_price30);

		AnimatedTextGameObject sight_x3 = new AnimatedTextGameObject(
				"sight_x3", false, "tutorial", 30);
		sight_x3.setTextCenteredOnPosition(Texts.SHOP_TEXT_QUANTITY_X5, 245,
				449, 100, false);
		sight_x3.setOrigin(sight_x3.getPositionX() + 20,
				sight_x3.getPositionY() + 30);
		sight_x3.setColor(0.9f, 0.9f, 0.65f, 1.0f);

		RotateToAction sight_x3_rotate1 = rotateTo(0, 1);
		RotateToAction sight_x3_rotate2 = rotateTo(-4, 1);
		sight_x3.addAction(forever(sequence(sight_x3_rotate1, sight_x3_rotate2)));

		ScaleToAction sight_x3_scale1 = scaleTo(1.05f, 0.95f, 1);
		ScaleToAction sight_x3_scale2 = scaleTo(1.0f, 1.0f, 1);
		sight_x3.addAction(forever(sequence(sight_x3_scale1, sight_x3_scale2)));

		rootGroup.addActor(sight_x3);

		// ------------------KARATE------------------------------------
		ShopBtn karate_container = new ShopBtn("karate_container", false, true,
				this.game);
		karate_container.skinName = "";
		karate_container.setPositionXY(30, 242);
		karate_container.setWidth(97);
		karate_container.setHeight(94);
		karate_container.folderName = "shop";

		karate_container.setOriginX(46);
		karate_container.setOriginY(7);

		ScaleToAction karate_container_scale1 = scaleTo(1.03f, 0.97f, 1);
		ScaleToAction karate_container_scale2 = scaleTo(1.0f, 1.0f, 1);
		karate_container.addAction(forever(sequence(karate_container_scale1, karate_container_scale2)));

		rootGroup.addActor(karate_container);

		GameObject karate_bug = new GameObject("karate-bug", false, true);
		karate_bug.skinName = "karate-bug";
		karate_bug.setPositionXY(0, 0);
		karate_bug.folderName = "shop";
		karate_bug.loadSkinOnDraw = true;
		karate_bug.setOnCreateFilter(true);

		karate_container.addActor(karate_bug);

		GameObject karate_price2 = new GameObject("karate-price2", false, true);
		karate_price2.skinName = "price2";
		karate_price2.setPositionXY(32, 236);
		karate_price2.folderName = "shop";
		karate_price2.loadSkinOnDraw = true;
		karate_price2.setOnCreateFilter(true);
		rootGroup.addActor(karate_price2);

		AnimatedTextGameObject karate_price30 = new AnimatedTextGameObject(
				"karate-price30", false, "popup", 20);
		karate_price30.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_BREAKER,
				62, 252, 100, false);
		karate_price30.setOrigin(karate_price30.getPositionX(),
				karate_price30.getPositionY());
		karate_price30.setScale(0.8f, 0.8f);
		karate_price30.setColor(0.15f, 0.3f, 0.15f, 1.0f);
		rootGroup.addActor(karate_price30);

		AnimatedTextGameObject karate_x3 = new AnimatedTextGameObject(
				"karate-x3", false, "tutorial", 30);
		karate_x3.setTextCenteredOnPosition(Texts.SHOP_TEXT_QUANTITY_X5, 116,
				342, 100, false);
		karate_x3.setOrigin(karate_x3.getPositionX() + 20,
				karate_x3.getPositionY() + 30);
		karate_x3.setColor(0.9f, 0.9f, 0.65f, 1.0f);

		RotateToAction karate_x3_rotate1 = rotateTo(0, 1);
		RotateToAction karate_x3_rotate2 = rotateTo(-4, 1);
		karate_x3.addAction(forever(sequence(karate_x3_rotate1,
				karate_x3_rotate2)));

		ScaleToAction karate_x3_scale1 = scaleTo(1.05f, 0.95f, 1);
		ScaleToAction karate_x3_scale2 = scaleTo(1.0f, 1.0f, 1);
		karate_x3.addAction(forever(sequence(karate_x3_scale1,
				karate_x3_scale2)));

		rootGroup.addActor(karate_x3);

		// ------------------BOOM------------------------------------
		ShopBtn boom_container = new ShopBtn("boom_container", false, true,
				this.game);
		boom_container.skinName = "";
		boom_container.setPositionXY(290, 566);
		boom_container.setWidth(98);
		boom_container.setHeight(77);
		boom_container.folderName = "shop";

		boom_container.setOriginX(50);
		boom_container.setOriginY(6);

		ScaleToAction boom_container_scale1 = scaleTo(1.03f, 0.97f, 1);
		ScaleToAction boom_container_scale2 = scaleTo(1.0f, 1.0f, 1);
		boom_container.addAction(forever(sequence(boom_container_scale1,
				boom_container_scale2)));

		rootGroup.addActor(boom_container);

		GameObject boom_bug = new GameObject("boom-bug", false, true);
		boom_bug.skinName = "boom-bug";
		boom_bug.setPositionXY(0, 0);
		boom_bug.folderName = "shop";
		boom_bug.loadSkinOnDraw = true;
		boom_bug.setOnCreateFilter(true);

		boom_container.addActor(boom_bug);

		GameObject boom_price2 = new GameObject("boom_price2", false, true);
		boom_price2.skinName = "price2";
		boom_price2.setPositionXY(284, 567);
		boom_price2.folderName = "shop";
		boom_price2.loadSkinOnDraw = true;
		boom_price2.setOnCreateFilter(true);
		rootGroup.addActor(boom_price2);

		AnimatedTextGameObject boom_price30 = new AnimatedTextGameObject(
				"boom_price30", false, "popup", 20);
		boom_price30.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_BLASTER,
				314, 583, 100, false);
		boom_price30.setOrigin(boom_price30.getPositionX(),
				boom_price30.getPositionY());
		boom_price30.setScale(0.8f, 0.8f);
		boom_price30.setColor(0.15f, 0.3f, 0.15f, 1.0f);
		rootGroup.addActor(boom_price30);

		AnimatedTextGameObject boom_x3 = new AnimatedTextGameObject("boom_x3",
				false, "tutorial", 30);
		boom_x3.setTextCenteredOnPosition(Texts.SHOP_TEXT_QUANTITY_X5, 368,
				651, 100, false);
		boom_x3.setOrigin(boom_x3.getPositionX() + 20,
				boom_x3.getPositionY() + 30);
		boom_x3.setColor(0.9f, 0.9f, 0.65f, 1.0f);

		RotateToAction boom_x3_rotate1 = rotateTo(0, 1);
		RotateToAction boom_x3_rotate2 = rotateTo(-4, 1);
		boom_x3.addAction(forever(sequence(boom_x3_rotate1, boom_x3_rotate2)));

		ScaleToAction boom_x3_scale1 = scaleTo(1.05f, 0.95f, 1);
		ScaleToAction boom_x3_scale2 = scaleTo(1.0f, 1.0f, 1);
		boom_x3.addAction(forever(sequence(boom_x3_scale1, boom_x3_scale2)));

		// ------------------ ------------------------------------
		GameObject shop_up = new GameObject("shop-up", false, true);
		shop_up.setPositionXY(0, 0);
		shop_up.folderName = "shop";
		shop_up.loadSkinOnDraw = true;
		rootGroup.addActor(shop_up);

		GdxGame.mInventory.setVisible(true);
		rootGroup.addActor(GdxGame.mInventory);

		rootGroup.addActor(expl_x3);
		rootGroup.addActor(boom_x3);

		// ---------------------YES NO BOX-----------------------------------
		backgroundComplete = new PopupBackground("background2", false, true);
		backgroundComplete.setPositionXY(0, 0);
		backgroundComplete.setWidth(GdxGame.VIRTUAL_GAME_WIDTH);
		backgroundComplete.setHeight(GdxGame.VIRTUAL_GAME_HEIGHT);
		backgroundComplete.setVisible(false);
		backgroundComplete.folderName = "shop";
		backgroundComplete.loadSkinOnDraw = true;
		rootGroup.addActor(backgroundComplete);

		yes_no_back = new GameObject("yes_no_back", false, true);
		yes_no_back.skinName = "txt-back";
		yes_no_back.setPositionXY(43, 320);
		yes_no_back.folderName = "shop";
		yes_no_back.loadSkinOnDraw = true;
		backgroundComplete.addActor(yes_no_back);

		_99price = new AnimatedTextGameObject("_99price", false, "tutorial", 45);
		_99price.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_BLASTER, 200,
				80, 480, false);
		_99price.setColor(0.2f, 0.65f, 0.2f, 1.0f);
		yes_no_back.addActor(_99price);

		text1 = new AnimatedTextGameObject("text1", false, "popup", 20);
		text1.setTextCenteredOnPosition(Texts.SHOP_TEXT_BUNDLE, 200, 180, 330,
				false);
		text1.setColor(0.31f, 0.2f, 0.1f, 1.0f);
		yes_no_back.addActor(text1);

		name1 = new AnimatedTextGameObject("name1", false, "tutorial", 45);
		name1.setTextCenteredOnPosition(Texts.SHOP_TEXT_NAME_RADIUS, 200, 250,
				480, false);
		name1.setColor(0.75f, 0.15f, 0.15f, 1.0f);
		yes_no_back.addActor(name1);

		buy_btn = new ShopBtn("buy-btn", false, true, this.game);
		buy_btn.skinName = "buy-btn";
		buy_btn.setPositionXY(81, 362);
		buy_btn.folderName = "shop";
		buy_btn.setTouchable(Touchable.enabled);
		buy_btn.setVisible(false);
		buy_btn.loadSkinOnDraw = true;
		rootGroup.addActor(buy_btn);

		no_btn = new ShopBtn("no-btn", false, true, this.game);
		no_btn.skinName = "no-btn";
		no_btn.setPositionXY(323, 361);
		no_btn.folderName = "shop";
		no_btn.setTouchable(Touchable.enabled);
		no_btn.setVisible(false);
		no_btn.loadSkinOnDraw = true;
		rootGroup.addActor(no_btn);
		// ------------------------------------------------------------------

		// --------------------SPIDER----------------------------------------
		GameObject web = new GameObject("web", false, true);
		web.skinName = "web";
		web.setPositionXY(60, 600);
		web.folderName = "shop";
		web.loadSkinOnDraw = true;
		web.setOnCreateFilter(true);

		web.setOriginX(8);
		web.setOriginY(201);

		web.setRotation(-5);

		RotateToAction web_rotate1 = rotateTo(5, 5);
		RotateToAction web_rotate2 = rotateTo(-5, 5);
		web.addAction(forever(sequence(web_rotate1, web_rotate2)));

		rootGroup.addActor(web);

		GameObject spider = new GameObject("spider", false, true);
		spider.skinName = "spider";
		spider.setPositionXY(-36, -45);
		spider.folderName = "shop";
		spider.loadSkinOnDraw = true;
		spider.setOnCreateFilter(true);

		spider.setOriginX(41);
		spider.setOriginY(50);

		spider.setRotation(-30);

		RotateToAction spider_rotate1 = rotateTo(-10, 5);
		RotateToAction spider_rotate2 = rotateTo(-30, 5);
		spider.addAction(forever(sequence(spider_rotate1, spider_rotate2)));

		web.addActor(spider);

		if (!GdxGame.isShopSpiderAnimationShown) {
			web.setPositionXY(60, 800);
			MoveToAction web_moveTo = moveTo(60, 600, 10);
			web.addAction(web_moveTo);

			GdxGame.isShopSpiderAnimationShown = true;
		}
		// -------------------------------------------------------------------

		rootGroup.setTouchable(Touchable.disabled);
		GdxGame.beetleStage.addActor(rootGroup);
	}

	private static void changePopupMessageText(String name, String text,
			String price) {
		AnimatedTextGameObject aTgO = (AnimatedTextGameObject) rootGroup
				.findActor("name1");
		aTgO.setTextCenteredOnPosition(name, 200, 255, 480, false);

		aTgO = (AnimatedTextGameObject) rootGroup.findActor("text1");
		aTgO.setTextCenteredOnPosition(text, 200, 180, 330, false);

		aTgO = (AnimatedTextGameObject) rootGroup.findActor("_99price");
		aTgO.setTextCenteredOnPosition(price, 200, 80, 480, false);
	}

	public static void showYesNoBox(int buttonId) {
		backgroundComplete.setVisible(true);
		buy_btn.setVisible(true);
		no_btn.setVisible(true);

		switch (buttonId) {
		case 1:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_MUSTACHE,
					Texts.SHOP_TEXT_MUSTACHE, Texts.SHOP_TEXT_PRICE_MUSTACHE);
			break;

		case 2:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_LASER,
					Texts.SHOP_TEXT_LASER, Texts.SHOP_TEXT_PRICE_LASER);
			break;

		case 3:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_BREAKER,
					Texts.SHOP_TEXT_BREAKER, Texts.SHOP_TEXT_PRICE_BREAKER);
			break;

		case 4:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_BLASTER,
					Texts.SHOP_TEXT_BLASTER, Texts.SHOP_TEXT_PRICE_BLASTER);
			break;

		case 5:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_RADIUS,
					Texts.SHOP_TEXT_RADIUS, Texts.SHOP_TEXT_PRICE_RADIUS);
			break;

		case 6:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_EGG1,
					Texts.SHOP_TEXT_EGG1, Texts.SHOP_TEXT_PRICE_EGG1);
			break;

		case 7:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_EGG2,
					Texts.SHOP_TEXT_EGG2, Texts.SHOP_TEXT_PRICE_EGG2);
			break;

		case 8:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_BUNDLE,
					Texts.SHOP_TEXT_BUNDLE, Texts.SHOP_TEXT_PRICE_BLASTER);
			break;

		default:
			break;
		}
	}

	public static void hideYesNoBox() {
		backgroundComplete.setVisible(false);
		buy_btn.setVisible(false);
		no_btn.setVisible(false);
		GdxGame.beetleStage.unfocusAll();
	}

	private void InitButtons() {
		if (BBInventory.coockieCount == null) {
			GdxGame.mInventory.initTxtObjects();
		}

		game.showShop();

		if (GdxGame.tutorialObject.isTutorialUsed("shop_egg2")) {
			return;
		}

		if (!GdxGame.tutorialObject.isTutorialUsed("shop_blaster")) {
			GdxGame.tutorialObject.initTutorial(
					Texts.TUTORIAL_MESSAGE_SHOP_BLASTER, "shop_blaster", 240,
					390, 420, ArrowPosEnum.TOP, 0.75f, rootGroup);
		} else if (!GdxGame.tutorialObject.isTutorialUsed("shop_karate")) {
			GdxGame.tutorialObject.initTutorial(
					Texts.TUTORIAL_MESSAGE_SHOP_KARATE, "shop_karate", 240,
					530, 420, ArrowPosEnum.BOTTOM, 0.1f, rootGroup);
		} else if (!GdxGame.tutorialObject.isTutorialUsed("shop_laser")) {
			GdxGame.tutorialObject.initTutorial(
					Texts.TUTORIAL_MESSAGE_SHOP_LASER, "shop_laser", 240, 620,
					420, ArrowPosEnum.BOTTOM, 0.45f, rootGroup);
		} else if (!GdxGame.tutorialObject.isTutorialUsed("shop_mustache")) {
			GdxGame.tutorialObject.initTutorial(
					Texts.TUTORIAL_MESSAGE_SHOP_MUSTACHE, "shop_mustache", 240,
					650, 420, ArrowPosEnum.BOTTOM, 0.1f, rootGroup);
		} else if (!GdxGame.tutorialObject.isTutorialUsed("shop_radius")) {
			GdxGame.tutorialObject.initTutorial(
					Texts.TUTORIAL_MESSAGE_SHOP_RADIUS, "shop_radius", 240,
					390, 420, ArrowPosEnum.TOP, 0.235f, rootGroup);
		} else if (!GdxGame.tutorialObject.isTutorialUsed("shop_egg1")) {
			GdxGame.tutorialObject.initTutorial(
					Texts.TUTORIAL_MESSAGE_SHOP_EGG1, "shop_egg1", 240, 530,
					420, ArrowPosEnum.BOTTOM, 0.39f, rootGroup);
		} else {
			GdxGame.tutorialObject.initTutorial(
					Texts.TUTORIAL_MESSAGE_SHOP_EGG2, "shop_egg2", 240, 530,
					420, ArrowPosEnum.BOTTOM, 0.66f, rootGroup);
		}
	}

	@Override
	public void render(float delta) {
		Gdx.app.log("x:y",
				Gdx.input.getX() - 10 + " : " + (800 - Gdx.input.getY() - 2));
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glViewport(GdxGame.START_X, GdxGame.START_Y,
				GdxGame.SCREEN_WIDTH, GdxGame.SCREEN_HEIGHT);

		if (!ImageCache.getManager().update()) {
			return;
		}

		if (!this.isButtonsInited) {
			InitButtons();
			this.isButtonsInited = true;
		}

		GdxGame.camera.update();
		spriteBatch.setProjectionMatrix(GdxGame.camera.combined);

		spriteBatch.begin();
		rootGroup.draw(spriteBatch, 1);
		spriteBatch.end();

		rootGroup.act(delta);

		if (!this.firsEnterFrame && Gdx.input.isKeyPressed(Keys.BACK)) {
			AudioPlayer.playSound("tap2");

			if (game.loadingScreen.getPrevScreenName().equals("game")) {
				game.loadingScreen.setNextScreen("game");
				game.loadingScreen.setPrevScreenName("shop");
			}

			else {
				game.loadingScreen.setNextScreen("menu");
				game.loadingScreen.setPrevScreenName("shop");
			}

			dispose();
			game.changeScreen("loading");
		}

		if (!this.firsEnterFrame && Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)) {
			AudioPlayer.playSound("tap2");

			if (game.loadingScreen.getPrevScreenName().equals("game")) {
				game.loadingScreen.setNextScreen("game");
				game.loadingScreen.setPrevScreenName("shop");
			}

			else {
				game.loadingScreen.setNextScreen("menu");
				game.loadingScreen.setPrevScreenName("shop");
			}

			dispose();
			game.changeScreen("loading");
		}

		if (this.firsEnterFrame && !Gdx.input.isKeyPressed(Keys.BACK)) {
			this.firsEnterFrame = false;
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		GdxGame.beetleStage.unfocusAll();
		this.firsEnterFrame = true;

		rootGroup.setTouchable(Touchable.enabled);
		this.isButtonsInited = false;

		GdxGame.mInventory.setState(InvState.FOREVER_OPENED);
		GdxGame.mInventory.setPositionXY(0, 20);
		// this.game.startFlurry();
		this.game.flurryAgentOnPageView();
	}

	@Override
	public void hide() {
		rootGroup.setTouchable(Touchable.disabled);
		// this.game.stopFlurry();
	}

	@Override
	public void pause() {
		this.game.stopFlurry();

	}

	@Override
	public void resume() {
		Texture.setAssetManager(ImageCache.getManager());
		GdxGame.beetleStage.unfocusAll();
		this.game.startFlurry();
	}

	@Override
	public void dispose() {
		rootGroup.clear();
		//GdxGame.beetleStage.removeActor(rootGroup);
		GdxGame.beetleStage.unfocusAll();

		ImageCache.unload("shop");
	}
}
