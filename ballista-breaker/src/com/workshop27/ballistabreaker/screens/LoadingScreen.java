package com.workshop27.ballistabreaker.screens;

import java.util.Calendar;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.GameObject;
import com.workshop27.ballistabreaker.engine.ImageCache;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class LoadingScreen implements Screen {
	private final GdxGame game;

	private Thread loadingThread;
	private Thread imageInitThread;

	private String nexScreenName;
	private String prevScreenName;

	private static int level;
	private static int levelPrev;

	public final Group rootGroup;

	private final GameObject loadingBackground;
	private final GameObject loading_txt;
	private final GameObject loading_dot1;
	private final GameObject loading_dot2;
	private final GameObject loading_dot3;

	private final SpriteBatch spriteBatch;

	private boolean loadingScreenAssetsLoaded;

	public LoadingScreen(GdxGame gdxGame) {
		this.loadingScreenAssetsLoaded = false;

		game = gdxGame;
		spriteBatch = GdxGame.beetleStage.getSpriteBatch();

		rootGroup = new Group();
		rootGroup.setName("loadingGroup");

		// ----------LOADING-------------
		loadingBackground = new GameObject("loading-screen", false, true);
		loadingBackground.setPositionXY(0, 0);
		loadingBackground.folderName = "loading";
		loadingBackground.loadSkinOnDraw = true;
		rootGroup.addActor(loadingBackground);

		GameObject backLight = new GameObject("light", false, true);
		backLight.setPositionXY(10, 130);
		backLight.folderName = "loading";
		backLight.loadSkinOnDraw = true;

		backLight.setOriginX(236);
		backLight.setOriginY(689);
		backLight.setWidth(471);
		backLight.setHeight(689);
		backLight.setOnCreateFilter(true);

		RotateToAction backLight_rotate1 = rotateTo(3, 3);
		RotateToAction backLight_rotate2 = rotateTo(-3, 3);
		backLight.addAction(forever(sequence(backLight_rotate1,
				backLight_rotate2)));

		rootGroup.addActor(backLight);

		GameObject particle_dust = new GameObject("particle_dust", false);
		particle_dust.skinName = "";
		particle_dust.setOriginX(240);
		particle_dust.setOriginY(650);
		particle_dust.createParticle("p3", true);
		rootGroup.addActor(particle_dust);

		loading_txt = new GameObject("loading-txt", false, true);
		loading_txt.setPositionXY(138, 330);
		loading_txt.folderName = "loading";
		loading_txt.loadSkinOnDraw = true;
		rootGroup.addActor(loading_txt);

		loading_dot1 = new GameObject("loading-dot", false, true);
		loading_dot1.setPositionXY(298, 340);
		loading_dot1.folderName = "loading";
		loading_dot1.loadSkinOnDraw = true;
		loading_dot1.getColor().a = 0;
		rootGroup.addActor(loading_dot1);

		loading_dot2 = new GameObject("loading-dot", false, true);
		loading_dot2.setPositionXY(330, 340);
		loading_dot2.folderName = "loading";
		loading_dot2.loadSkinOnDraw = true;
		loading_dot2.getColor().a = 0;
		rootGroup.addActor(loading_dot2);

		loading_dot3 = new GameObject("loading-dot", false, true);
		loading_dot3.setPositionXY(362, 340);
		loading_dot3.folderName = "loading";
		loading_dot3.loadSkinOnDraw = true;
		loading_dot3.getColor().a = 0;
		rootGroup.addActor(loading_dot3);

		GdxGame.beetleStage.addActor(rootGroup);
	}

	public String getNextScreen() {
		return this.nexScreenName;
	}

	public void setNextScreen(String screenName) {
		this.nexScreenName = screenName;
	}

	public void setNextScreen(String screenName, int level) {
		this.nexScreenName = screenName;
		LoadingScreen.levelPrev = LoadingScreen.level;
		LoadingScreen.level = level;
	}

	public int getLevel() {
		return LoadingScreen.level;
	}

	public String getPrevScreenName() {
		return prevScreenName;
	}

	public void setPrevScreenName(String prevScreenName) {
		this.prevScreenName = prevScreenName;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glViewport(GdxGame.START_X, GdxGame.START_Y,
				GdxGame.SCREEN_WIDTH, GdxGame.SCREEN_HEIGHT);

		if (ImageCache.getManager().update() && loadingThread != null
				&& !loadingThread.isAlive() && imageInitThread != null
				&& !imageInitThread.isAlive()) {
			loadingThread = null;
			imageInitThread = null;
			game.changeScreen(this.nexScreenName);
		}

		else if (ImageCache.getManager().update() && imageInitThread == null
				&& loadingThread != null && !loadingThread.isAlive()) {
			if (this.nexScreenName.equals("game")) {
				setSkins(GameScreen.rootGroup);
			}

			else if (this.nexScreenName.equals("menu")) {
				setSkins(game.mainMenuScreen.rootGroup);

				setSkins(game.tutorialScreen.rootGroup);
			}

			else if (this.nexScreenName.equals("stage")) {
				setSkins(StageScreen.rootGroup);
			}

			else if (this.nexScreenName.equals("shop")) {
				setSkins(ShopScreen.rootGroup);
			}
		}

		// if (this.nexScreenName == "stage" && this.prevScreenName == "menu") {
		// return;
		// }

		if (!this.loadingScreenAssetsLoaded
				&& ImageCache.getManager().isLoaded(
						"data/atlas/loading/LoadingScreen1.jpg")
				&& ImageCache.getManager().isLoaded(
						"data/atlas/loading/LoadingScreen2.png")
				&& ImageCache.getManager().isLoaded(
						"data/atlas/particles/particles1.png")) {
			this.loadingScreenAssetsLoaded = true;

			AlphaAction loading_fadeIn1 = fadeIn(0.15f);
			AlphaAction loading_fadeIn2 = fadeIn(0.15f);
			AlphaAction loading_fadeIn3 = fadeIn(0.15f);
			AlphaAction loading_fadeOut1 = fadeOut(0.15f);
			AlphaAction loading_fadeOut2 = fadeOut(0.15f);
			AlphaAction loading_fadeOut3 = fadeOut(0.15f);

			loading_dot1.addAction(forever(sequence(loading_fadeIn1, delay(0.5f), loading_fadeOut1, delay(2.5f), delay(0.1f))));
			
			loading_dot2.addAction(forever(sequence(loading_fadeIn2, delay(1.0f), loading_fadeOut2, delay(1.5f), delay(0.6f))));
			
			loading_dot3.addAction(forever(sequence(loading_fadeIn3, delay(1.5f), loading_fadeOut3, delay(0.5f), delay(1.1f))));
		}

		rootGroup.act(delta);

		GdxGame.camera.update();
		spriteBatch.setProjectionMatrix(GdxGame.camera.combined);

		if (this.loadingScreenAssetsLoaded) {

			spriteBatch.begin();
			rootGroup.draw(spriteBatch, 1);
			spriteBatch.end();
		}
	}

	private void setSkins(Actor actor) {
		final Group gg = (Group) actor;

		imageInitThread = new Thread(new Runnable() {
			@Override
			public void run() {

				for (int i = 0; i < gg.getChildren().size; i++) {
					try {
						GameObject go = (GameObject) gg.getChildren().get(i);
						if (go.skinName != null && go.skinRegion == null) {
							go.setSkin();
						}

						if (go.getChildren().size != 0) {
							setSkins(go);
						}

					} catch (final Exception ex) {
						// Log.e("error", "set skins failed");

					}
				}
			}
		}, "set skins");
		imageInitThread.start();
	}

	private void clearSkins(Actor actor) {
		final Group gg = (Group) actor;

		for (int i = 0; i < gg.getChildren().size; i++) {
			try {
				GameObject go = (GameObject) gg.getChildren().get(i);
				go.clearSkin();

				if (go.getChildren().size != 0) {
					clearSkins(go);
				}

			} catch (final Exception ex) {
				// Log.e("error", "clear skins failed");
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		loadingThread = null;
		imageInitThread = null;

		GdxGame.camera.position.set(GdxGame.VIRTUAL_GAME_WIDTH / 2,
				GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);

		if (this.nexScreenName.equals("menu") && game.tutorialScreen == null) {
			ImageCache.load("tutorial");

			this.loadingThread = new Thread(new Runnable() {
				@Override
				public void run() {
					game.tutorialScreen = new TutorialScreen(game);
				}
			}, "loading TutorialScreen");
			loadingThread.start();
		}

		else if (this.nexScreenName.equals("logo")) {
			ImageCache.load("particles");
			ImageCache.load("menu");
			ImageCache.load("tutorial");
			ImageCache.load("inventory");

			this.loadingThread = new Thread(new Runnable() {
				@Override
				public void run() {
					game.initScreens();
					setNextScreen("menu");
					setPrevScreenName("logo");

					// Load tutorial
					game.tutorialScreen = new TutorialScreen(game);
				}
			}, "loading TutorialScreen");
			loadingThread.start();
		}

		else if (this.nexScreenName.equals("game")
				&& this.prevScreenName.equals("game")) {
			this.game.flurryAgentEndTimedEvent("level" + levelPrev);
			this.game.flurryAgentLogEvent("level" + level, true);
			this.loadingThread = new Thread(new Runnable() {
				@Override
				public void run() {
					GameScreen.particle_explosion.clearParticle("explosion"
							+ GdxGame.stageNumber);
					GameScreen.particle_explosion.createParticle("explosion"
							+ GdxGame.stageNumber, true);

					game.gameScreen.initBonusBeetles(LoadingScreen.level);
					game.gameScreen.initBeetles(LoadingScreen.level);
					game.gameScreen.startLevel(LoadingScreen.level);
				}
			}, "init beetles and bonuses and start level");
			loadingThread.start();
		}

		else if (this.nexScreenName.equals("game")
				&& !this.prevScreenName.equals("shop")) {
			this.game.flurryAgentLogEvent("level" + level, true);
			if (game.regularTutorial && game.fanTutorial && game.minerTutorial
					&& game.boomTutorial && game.karateTutorial) {
				Group g = (Group) GdxGame.beetleStage.getRoot().findActor("tutorialGroup");
				GdxGame.beetleStage.getRoot().removeActor(g);
				ImageCache.unload("tutorial");
				game.tutorialScreen = null;
			}

			GdxGame.dayMinutes = Calendar.getInstance().get(Calendar.HOUR) % 2;

			ImageCache.unload("menu");

			ImageCache.load("main");

			if (GdxGame.dayMinutes == 0) {
				ImageCache.load("background2");
			} else {
				ImageCache.load("background1");
			}

			final Group mg = (Group) GdxGame.beetleStage.getRoot().findActor("menuGroup");
			final Group sg = (Group) GdxGame.beetleStage.getRoot()
					.findActor("stageGroup");

			this.loadingThread = new Thread(new Runnable() {
				@Override
				public void run() {

					clearSkins(mg);

					for (int i = 0; i < mg.getChildren().size; i++) {
						try {
							Button b = (Button) mg.getChildren().get(i);
							b.getStyle().down = null;
							b.getStyle().up = null;
						} catch (final Exception ex1) {
							// Log.e("error", "clear skins failed");
						}
					}

					clearSkins(sg);

					for (int i = 0; i < sg.getChildren().size; i++) {
						try {
							Button b = (Button) sg.getChildren().get(i);
							b.getStyle().down = null;
							b.getStyle().up = null;
						} catch (final Exception ex1) {
							// Log.e("error", "clear skins failed");

						}
					}

					game.gameScreen.initBonusBeetles(LoadingScreen.level);
					game.gameScreen.initBeetles(LoadingScreen.level);
					game.gameScreen.startLevel(LoadingScreen.level);
				}
			}, "clear skins");
			loadingThread.start();
		}

		else if (this.nexScreenName.equals("stage")
				&& this.prevScreenName.equals("game")) {
			this.game.flurryAgentEndTimedEvent("level" + level);

			ImageCache.load("menu");

			ImageCache.unload("main");
			ImageCache.unload("background1");

			final Group gg = (Group) GdxGame.beetleStage.getRoot().findActor("gameGroup");

			this.loadingThread = new Thread(new Runnable() {
				@Override
				public void run() {

					clearSkins(gg);

					for (int i = 0; i < gg.getChildren().size; i++) {
						try {
							Button b = (Button) gg.getChildren().get(i);
							b.getStyle().down = null;
							b.getStyle().up = null;
						} catch (final Exception ex1) {
							// Log.e("error", "clear skins failed");

						}
					}
				}
			}, "clear skins");
			loadingThread.start();

			// game.loadingScreen.setPrevScreenName("");
		}

		else if (this.nexScreenName.equals("shop")) {
			ImageCache.load("shop");

			this.loadingThread = new Thread(new Runnable() {
				@Override
				public void run() {
					game.shopScreen = new ShopScreen(game);
				}
			}, "loading ShopScreen");
			loadingThread.start();
		}

		if (loadingThread == null) {
			this.loadingThread = new Thread(new Runnable() {
				@Override
				public void run() {
				}
			}, "start empty thread");
			loadingThread.start();
		}
		// this.game.startFlurry();
		this.game.flurryAgentOnPageView();
	}

	@Override
	public void hide() {
		// this.game.stopFlurry();

	}

	@Override
	public void pause() {
		this.game.stopFlurry();

	}

	@Override
	public void resume() {
		this.game.startFlurry();

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
