package com.workshop27.ballistabreaker.screens;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.constants.Texts;
import com.workshop27.ballistabreaker.engine.AudioPlayer;
import com.workshop27.ballistabreaker.engine.GameObject;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.engine.LeafObject;
import com.workshop27.ballistabreaker.engine.StageBackgroundObject;
import com.workshop27.ballistabreaker.engine.TextGameObject;
import com.workshop27.ballistabreaker.engine.ShowTutorialObject.ArrowPosEnum;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class StageScreen implements Screen {

	protected GameScreen gameScreen;
	private final GdxGame game;

	private boolean isButtonsInited;
	private boolean isButtonsAdded;

	public static Group rootGroup;

	private final SpriteBatch spriteBatch;

	private Boolean firsEnterFrame = true;

	private GameObject level1;
	private GameObject level2;
	private GameObject level3;
	private GameObject level4;
	private GameObject level5;
	private GameObject level6;
	private GameObject level7;
	private GameObject level8;

	private GameObject level9;
	private GameObject level10;
	private GameObject level11;
	private GameObject level12;
	private GameObject level13;
	private GameObject level14;
	private GameObject level15;
	private GameObject level16;

	private GameObject level17;
	private GameObject level18;
	private GameObject level19;
	private GameObject level20;
	private GameObject level21;
	private GameObject level22;
	private GameObject level23;
	private GameObject level24;

	private GameObject level25;
	private GameObject level26;
	private GameObject level27;
	private GameObject level28;
	private GameObject level29;
	private GameObject level30;
	private GameObject level31;
	private GameObject level32;
	private GameObject level33;
	private GameObject level34;
	private GameObject level35;
	private GameObject level36;
	private GameObject level37;
	private GameObject level38;
	private GameObject level39;
	private GameObject level40;
	private GameObject level41;
	private GameObject level42;
	private GameObject level43;
	private GameObject level44;
	private GameObject level45;
	private GameObject level46;
	private GameObject level47;
	private GameObject level48;

	private GameObject level49;
	private GameObject level50;
	private GameObject level51;
	private GameObject level52;
	private GameObject level53;
	private GameObject level54;
	private GameObject level55;
	private GameObject level56;
	private GameObject level57;
	private GameObject level58;
	private GameObject level59;
	private GameObject level60;
	private GameObject level61;
	private GameObject level62;
	private GameObject level63;
	private GameObject level64;
	private GameObject level65;
	private GameObject level66;
	private GameObject level67;
	private GameObject level68;
	private GameObject level69;
	private GameObject level70;
	private GameObject level71;
	private GameObject level72;
	protected boolean unlock;
	private TextGameObject needStars2;
	private TextGameObject needStars3;
	private final StageBackgroundObject background4;

	private final TextGameObject comingSoon;
	public static StageBackgroundObject background1;
	public static StageBackgroundObject background2;
	public static StageBackgroundObject background3;
	public static GameObject lock2;
	public static GameObject lock3;
	public static GameObject lock4;

	public static GameObject dot1;
	public static GameObject dot2;
	public static GameObject dot3;
	public static GameObject dot4;

	public StageScreen(GdxGame game) {
		spriteBatch = GdxGame.beetleStage.getSpriteBatch();
		this.game = game;

		rootGroup = new Group();
		rootGroup.setName("stageGroup");

		background1 = new StageBackgroundObject("stage1-back", false, false);
		background1.setPositionXY(0, 0);
		background1.folderName = "menu";
		background1.loadSkinOnDraw = true;
		rootGroup.addActor(background1);

		GameObject stage1 = new GameObject("stage1", false, true);
		stage1.setPositionXY(180, 60);
		stage1.folderName = "menu";
		stage1.loadSkinOnDraw = true;
		rootGroup.addActor(stage1);

		background2 = new StageBackgroundObject("stage2-back", false, false);
		background2.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH, 0);
		background2.folderName = "menu";
		background2.loadSkinOnDraw = true;
		rootGroup.addActor(background2);

		GameObject stage2 = new GameObject("stage2", false, true);
		stage2.setPositionXY(180 + GdxGame.VIRTUAL_GAME_WIDTH, 60);
		stage2.folderName = "menu";
		stage2.loadSkinOnDraw = true;
		rootGroup.addActor(stage2);

		GameObject stage2Art = new GameObject("jelly-spot", false, true);
		stage2Art.setPositionXY(46 + GdxGame.VIRTUAL_GAME_WIDTH, 250);
		stage2Art.folderName = "menu";
		stage2Art.loadSkinOnDraw = true;
		rootGroup.addActor(stage2Art);

		background3 = new StageBackgroundObject("stage3-back", false, false);
		background3.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH * 2, 0);
		background3.folderName = "menu";
		background3.loadSkinOnDraw = true;
		rootGroup.addActor(background3);

		GameObject stage3 = new GameObject("stage3", false, true);
		stage3.setPositionXY(180 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 60);
		stage3.folderName = "menu";
		stage3.loadSkinOnDraw = true;
		rootGroup.addActor(stage3);

		GameObject stage3Art = new GameObject("stone-spots", false, true);
		stage3Art.setPositionXY(98 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 280);
		stage3Art.folderName = "menu";
		stage3Art.loadSkinOnDraw = true;
		rootGroup.addActor(stage3Art);

		background4 = new StageBackgroundObject("stage3-back", false, false);
		background4.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH * 3, 0);
		background4.folderName = "menu";
		background4.loadSkinOnDraw = true;
		rootGroup.addActor(background4);

		GameObject backLight1 = new GameObject("light", false, true);
		backLight1.setPositionXY(40, 400);
		backLight1.folderName = "menu";
		backLight1.loadSkinOnDraw = true;

		backLight1.setOriginX(200);
		backLight1.setOriginY(417);
		backLight1.setWidth(400);
		backLight1.setHeight(417);
		backLight1.setOnCreateFilter(true);

		RotateToAction backLight_rotate1 = rotateTo(3, 3);
		RotateToAction backLight_rotate2 = rotateTo(-3, 3);
		backLight1.addAction(forever(sequence(backLight_rotate1,
				backLight_rotate2)));

		rootGroup.addActor(backLight1);

		GameObject backLight2 = new GameObject("light", false, true);
		backLight2.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH + 40, 400);
		backLight2.folderName = "menu";
		backLight2.loadSkinOnDraw = true;

		backLight2.setOriginX(200);
		backLight2.setOriginY(417);
		backLight2.setWidth(400);
		backLight2.setHeight(417);
		backLight2.setOnCreateFilter(true);

		RotateToAction backLight_rotate12 = rotateTo(3, 3);
		RotateToAction backLight_rotate22 = rotateTo(-3, 3);
		backLight2.addAction(forever(sequence(backLight_rotate12,
				backLight_rotate22)));

		rootGroup.addActor(backLight2);

		GameObject backLight3 = new GameObject("light", false, true);
		backLight3.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH * 2 + 40, 400);
		backLight3.folderName = "menu";
		backLight3.loadSkinOnDraw = true;

		backLight3.setOriginX(200);
		backLight3.setOriginY(417);
		backLight3.setWidth(400);
		backLight3.setHeight(417);
		backLight3.setOnCreateFilter(true);

		RotateToAction backLight_rotate13 = rotateTo(3, 3);
		RotateToAction backLight_rotate23 = rotateTo(-3, 3);
		backLight3.addAction(forever(sequence(backLight_rotate13, backLight_rotate23)));

		rootGroup.addActor(backLight3);

		GameObject particle_dust = new GameObject("particle_dust", false);
		particle_dust.setOriginX(240);
		particle_dust.setOriginY(650);
		particle_dust.skinName = "";
		particle_dust.createParticle("p3", true);
		rootGroup.addActor(particle_dust);

		GameObject particle_dust2 = new GameObject("particle_dust", false);
		particle_dust2.setOriginX(240 + GdxGame.VIRTUAL_GAME_WIDTH);
		particle_dust2.setOriginY(650);
		particle_dust2.skinName = "";
		particle_dust2.createParticle("p3", true);
		rootGroup.addActor(particle_dust2);

		GameObject particle_dust3 = new GameObject("particle_dust", false);
		particle_dust3.setOriginX(240 + 2 * GdxGame.VIRTUAL_GAME_WIDTH);
		particle_dust3.setOriginY(650);
		particle_dust3.skinName = "";
		particle_dust3.createParticle("p3", true);
		rootGroup.addActor(particle_dust3);

		lock2 = new GameObject("lock", false, true);
		lock2.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH, 0);
		lock2.folderName = "menu";
		lock2.loadSkinOnDraw = true;
		lock2.setVisible(true);
		rootGroup.addActor(lock2);

		lock3 = new GameObject("lock", false, true);
		lock3.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH * 2, 0);
		lock3.folderName = "menu";
		lock3.loadSkinOnDraw = true;
		lock3.setVisible(true);
		rootGroup.addActor(lock3);

		lock4 = new GameObject("coming", false, true);
		lock4.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH * 3, 0);
		lock4.folderName = "menu";
		lock4.loadSkinOnDraw = true;
		lock4.setVisible(true);
		rootGroup.addActor(lock4);

		comingSoon = new TextGameObject("coming", false, 30);
		comingSoon.setPositionXY(150 + GdxGame.VIRTUAL_GAME_WIDTH * 3, 375);
		comingSoon.setText("COMING SOON");

		rootGroup.addActorAfter(lock4, comingSoon);

		dot1 = new GameObject("pagination-off", false, true);
		dot1.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH / 2 - 25, 120);
		dot1.folderName = "menu";
		dot1.loadSkinOnDraw = true;
		rootGroup.addActor(dot1);

		dot2 = new GameObject("pagination-off", false, true);
		dot2.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH / 2 + 25, 120);
		dot2.folderName = "menu";
		dot2.loadSkinOnDraw = true;
		rootGroup.addActor(dot2);

		dot3 = new GameObject("pagination-on", false, true);
		dot3.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH / 2 - 75, 120);
		dot3.folderName = "menu";
		dot3.loadSkinOnDraw = true;
		rootGroup.addActor(dot3);

		dot4 = new GameObject("pagination-off", false, true);
		dot4.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH / 2 + 75, 120);
		dot4.folderName = "menu";
		dot4.loadSkinOnDraw = true;
		rootGroup.addActor(dot4);

		rootGroup.setTouchable(Touchable.disabled);
		GdxGame.beetleStage.addActor(rootGroup);

	}

	// public void movePaginator() {
	// if (dot1 == null) {
	// dot1 = new GameObject("pagination-off", false, true);
	// dot1.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH * 2, 0);
	// dot1.folderName = "menu";
	// dot1.loadSkinOnDraw = true;
	// rootGroup.addActor(dot1);
	// }
	// if (dot2 == null) {
	// dot2 = new GameObject("pagination-off", false, true);
	// dot2.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH * 2, 0);
	// dot2.folderName = "menu";
	// dot2.loadSkinOnDraw = true;
	// rootGroup.addActor(dot2);
	//
	// }
	// if (dot3 == null) {
	// dot3 = new GameObject("pagination-on", false, true);
	// dot3.setPositionXY(GdxGame.VIRTUAL_GAME_WIDTH * 2, 0);
	// dot3.folderName = "menu";
	// dot3.loadSkinOnDraw = true;
	// rootGroup.addActor(dot3);
	//
	// }
	// }

	public void InitButtons() {

		if (needStars2 == null) {

			needStars2 = new TextGameObject("points", false, 45);
			needStars2.setPositionXY(285 + GdxGame.VIRTUAL_GAME_WIDTH, 370);
			needStars2
					.setText(""
							+ (GdxGame.collectedStars + "/" + Constants.STARS_TO_UNLOCK_SECOND_STAGE));

			rootGroup.addActorAfter(lock2, needStars2);
		}

		if (GdxGame.collectedStars >= Constants.STARS_TO_UNLOCK_SECOND_STAGE) {
			needStars2.setVisible(false);
		}

		if (needStars3 == null) {
			needStars3 = new TextGameObject("points", false, 45);
			needStars3.setPositionXY(285 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 370);
			needStars3
					.setText(""
							+ (GdxGame.collectedStars + "/" + Constants.STARS_TO_UNLOCK_THIRD_STAGE));
			rootGroup.addActorAfter(lock3, needStars3);
		}

		if (GdxGame.collectedStars >= Constants.STARS_TO_UNLOCK_THIRD_STAGE) {
			needStars3.setVisible(false);
		}

		Random randomGenerator = new Random();

		if (level24 == null) {
			level24 = new LeafObject("level24-btn", "level24", false, game, 24,
					"level24", "left", 62, 72, 350, 290);
			level24.setPositionXY(300, 220);
			level24.setOriginX(111);
			level24.setOriginY(132);

			RotateToAction board_rotate1 = rotateTo(
					-randomGenerator.nextInt(50) / 10,
					randomGenerator.nextInt(4) + 1);
			RotateToAction board_rotate2 = rotateTo(
					randomGenerator.nextInt(50) / 10,
					randomGenerator.nextInt(4) + 1);
			level24.addAction(forever(sequence(board_rotate1, board_rotate2)));

			level24.folderName = "menu";
			level24.setSkin();
			level24.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level24);

		}

		if (level23 == null) {
			level23 = new LeafObject("level23-btn", "level23", false, game, 23,
					"level23", "right", 0, 72, 400, 420);
			level23.setPositionXY(355, 330);
			level23.setOriginX(42);
			level23.setOriginY(131);

			RotateToAction board_rotate1 = rotateTo(
					-randomGenerator.nextInt(50) / 10,
					randomGenerator.nextInt(4) + 1);
			RotateToAction board_rotate2 = rotateTo(
					randomGenerator.nextInt(50) / 10,
					randomGenerator.nextInt(4) + 1);
			level23.addAction(forever(sequence(board_rotate1, board_rotate2)));

			level23.folderName = "menu";
			level23.setSkin();
			level23.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level23);
		}

		if (level22 == null) {
			level22 = new LeafObject("level22-btn", "level22", false, game, 22,
					"level22", "left", 66, 37, 345, 500);
			level22.setPositionXY(295, 420);
			level22.setOriginX(108);
			level22.setOriginY(69);

			level22.folderName = "menu";
			level22.setSkin();
			level22.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level22);

		}

		if (level21 == null) {
			level21 = new LeafObject("level21-btn", "level21", false, game, 21,
					"level21", "right", 8, 69, 420, 530);
			level21.setPositionXY(395, 450);
			level21.setOriginX(21);
			level21.setOriginY(116);

			level21.folderName = "menu";
			level21.setSkin();
			level21.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level21);

		}

		if (level20 == null) {
			level20 = new LeafObject("level20-btn", "level20", false, game, 20,
					"level20", "left", 66, 35, 360, 600);
			level20.setPositionXY(335, 520);
			level20.setOriginX(92);
			level20.setOriginY(87);

			level20.folderName = "menu";
			level20.setSkin();
			level20.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level20);

		}

		if (level19 == null) {
			level19 = new LeafObject("level19-btn", "level19", false, game, 19,
					"level19", "left", 54, 42, 380, 660);
			level19.setPositionXY(355, 600);
			level19.setOriginX(82);
			level19.setOriginY(72);

			level19.folderName = "menu";
			level19.setSkin();
			level19.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level19);

		}

		if (level18 == null) {
			level18 = new LeafObject("level18-btn", "level18", false, game, 18,
					"level18", "right", 3, 35, 415, 730);
			level18.setPositionXY(415, 650);
			level18.setOriginX(14);
			level18.setOriginY(96);

			level18.folderName = "menu";
			level18.setSkin();
			level18.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level18);

		}

		if (level17 == null) {
			level17 = new LeafObject("level17-btn", "level17", false, game, 17,
					"level17", "left", 46, 52, 370, 740);
			level17.setPositionXY(345, 700);
			level17.setOriginX(75);
			level17.setOriginY(78);

			level17.folderName = "menu";
			level17.setSkin();
			level17.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level17);

		}

		if (level16 == null) {
			level16 = new LeafObject("level16-btn", "level16", false, game, 16,
					"level16", "right", 5, 70, 270, 310);
			level16.setPositionXY(240, 230);
			level16.setOriginX(32);
			level16.setOriginY(147);

			level16.folderName = "menu";
			level16.setSkin();
			level16.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level16);

		}

		if (level15 == null) {
			level15 = new LeafObject("level15-btn", "level15", false, game, 15,
					"level15", "left", 74, 46, 210, 410);
			level15.setPositionXY(165, 330);
			level15.setOriginX(117);
			level15.setOriginY(113);

			level15.folderName = "menu";
			level15.setSkin();
			level15.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level15);
		}

		if (level14 == null) {
			level14 = new LeafObject("level14-btn", "level14", false, game, 14,
					"level14", "left", 67, 37, 195, 500);
			level14.setPositionXY(180, 440);
			level14.setOriginX(95);
			level14.setOriginY(86);

			level14.folderName = "menu";
			level14.setSkin();
			level14.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level14);
		}

		if (level13 == null) {
			level13 = new LeafObject("level13-btn", "level13", false, game, 13,
					"level13", "right", 3, 22, 240, 600);
			level13.setPositionXY(245, 510);
			level13.setOriginX(0);
			level13.setOriginY(84);

			level13.folderName = "menu";
			level13.setSkin();
			level13.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level13);
		}

		if (level12 == null) {
			level12 = new LeafObject("level12-btn", "level12", false, game, 12,
					"level12", "left", 70, 22, 160, 610);
			level12.setPositionXY(175, 530);
			level12.setOriginX(58);
			level12.setOriginY(107);

			level12.folderName = "menu";
			level12.setSkin();
			level12.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level12);
		}

		if (level11 == null) {
			level11 = new LeafObject("level11-btn", "level11", false, game, 11,
					"level11", "left", 56, 8, 180, 680);
			level11.setPositionXY(155, 620);
			level11.setOriginX(82);
			level11.setOriginY(73);

			level11.folderName = "menu";
			level11.setSkin();
			level11.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level11);
		}

		if (level10 == null) {
			level10 = new LeafObject("level10-btn", "level10", false, game, 10,
					"level10", "right", 2, 50, 250, 690);
			level10.setPositionXY(240, 650);
			level10.setOriginX(19);
			level10.setOriginY(93);

			level10.folderName = "menu";
			level10.setSkin();
			level10.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level10);
		}

		if (level9 == null) {
			level9 = new LeafObject("level9-btn", "level9", false, game, 9,
					"level9", "left", 42, 50, 220, 740);
			level9.setPositionXY(190, 700);
			level9.setOriginX(72);
			level9.setOriginY(85);

			level9.folderName = "menu";
			level9.setSkin();
			level9.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level9);
		}

		if (level8 == null) {
			level8 = new LeafObject("level8-btn", "level8", false, game, 8,
					"level8", "right", 4, 17, 90, 350);
			level8.setPositionXY(85, 260);
			level8.setOriginX(14);
			level8.setOriginY(130);

			level8.folderName = "menu";
			level8.setSkin();
			level8.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level8);
		}

		if (level7 == null) {
			level7 = new LeafObject("level7-btn", "level7", false, game, 7,
					"level7", "left", 97, 17, -20, 440);
			level7.setPositionXY(15, 310);
			level7.setOriginX(74);
			level7.setOriginY(131);

			level7.folderName = "menu";
			level7.setSkin();
			level7.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level7);
		}

		if (level6 == null) {
			level6 = new LeafObject("level6-btn", "level6", false, game, 6,
					"level6", "right", 0, 12, 65, 490);
			level6.setPositionXY(65, 410);
			level6.setOriginX(5);
			level6.setOriginY(80);

			level6.folderName = "menu";
			level6.setSkin();
			level6.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level6);
		}

		if (level5 == null) {
			level5 = new LeafObject("level5-btn", "level5", false, game, 5,
					"level5", "left", 70, 22, -10, 530);
			level5.setPositionXY(5, 450);
			level5.setOriginX(58);
			level5.setOriginY(109);

			level5.folderName = "menu";
			level5.setSkin();
			level5.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level5);
		}

		if (level4 == null) {
			level4 = new LeafObject("level4-btn", "level4", false, game, 4,
					"level4", "right", 3, 22, 65, 590);
			level4.setPositionXY(60, 520);
			level4.setOriginX(12);
			level4.setOriginY(108);

			level4.folderName = "menu";
			level4.setSkin();
			level4.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level4);
		}

		if (level3 == null) {
			level3 = new LeafObject("level3-btn", "level3", false, game, 3,
					"level3", "left", 54, 42, 30, 650);
			level3.setPositionXY(0, 580);
			level3.setOriginX(64);
			level3.setOriginY(72);

			level3.folderName = "menu";
			level3.setSkin();
			level3.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level3);
		}

		if (level2 == null) {
			level2 = new LeafObject("level2-btn", "level2", false, game, 2,
					"level2", "right", 2, 50, 85, 690);
			level2.setPositionXY(80, 630);
			level2.setOriginX(12);
			level2.setOriginY(92);

			level2.folderName = "menu";
			level2.setSkin();
			level2.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level2);

		}

		if (level1 == null) {
			level1 = new LeafObject("level1-btn", "level1", false, game, 1,
					"level1", "left", 42, 50, 42, 740);
			level1.setPositionXY(20, 700);
			level1.setOriginX(72);
			level1.setOriginY(77);

			level1.folderName = "menu";
			level1.setSkin();
			level1.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level1);

		}

		// Stage 2

		if (level48 == null) {
			level48 = new LeafObject("level24-btn", "level48", false, game, 48,
					"level48", "left", 62, 72,
					350 + GdxGame.VIRTUAL_GAME_WIDTH, 290);
			level48.setPositionXY(300 + GdxGame.VIRTUAL_GAME_WIDTH, 220);
			level48.setOriginX(111);
			level48.setOriginY(132);

			level48.folderName = "menu";
			level48.setSkin();
			level48.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level48);

		}

		if (level47 == null) {
			level47 = new LeafObject("level23-btn", "level47", false, game, 47,
					"level47", "right", 0, 72,
					400 + GdxGame.VIRTUAL_GAME_WIDTH, 420);
			level47.setPositionXY(355 + GdxGame.VIRTUAL_GAME_WIDTH, 330);
			level47.setOriginX(42);
			level47.setOriginY(131);

			level47.folderName = "menu";
			level47.setSkin();
			level47.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level47);
		}

		if (level46 == null) {
			level46 = new LeafObject("level22-btn", "level46", false, game, 46,
					"level46", "left", 66, 37,
					345 + GdxGame.VIRTUAL_GAME_WIDTH, 500);
			level46.setPositionXY(295 + GdxGame.VIRTUAL_GAME_WIDTH, 420);
			level46.setOriginX(108);
			level46.setOriginY(69);

			level46.folderName = "menu";
			level46.setSkin();
			level46.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level46);

		}

		if (level45 == null) {
			level45 = new LeafObject("level21-btn", "level45", false, game, 45,
					"level45", "right", 8, 69,
					420 + GdxGame.VIRTUAL_GAME_WIDTH, 530);
			level45.setPositionXY(395 + GdxGame.VIRTUAL_GAME_WIDTH, 450);
			level45.setOriginX(21);
			level45.setOriginY(116);

			level45.folderName = "menu";
			level45.setSkin();
			level45.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level45);

		}

		if (level44 == null) {
			level44 = new LeafObject("level20-btn", "level44", false, game, 44,
					"level44", "left", 66, 35,
					360 + GdxGame.VIRTUAL_GAME_WIDTH, 600);
			level44.setPositionXY(335 + GdxGame.VIRTUAL_GAME_WIDTH, 520);
			level44.setOriginX(92);
			level44.setOriginY(87);

			level44.folderName = "menu";
			level44.setSkin();
			level44.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level44);

		}

		if (level43 == null) {
			level43 = new LeafObject("level19-btn", "level43", false, game, 43,
					"level43", "left", 54, 42,
					380 + GdxGame.VIRTUAL_GAME_WIDTH, 660);
			level43.setPositionXY(355 + GdxGame.VIRTUAL_GAME_WIDTH, 600);
			level43.setOriginX(82);
			level43.setOriginY(72);

			level43.folderName = "menu";
			level43.setSkin();
			level43.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level43);

		}

		if (level42 == null) {
			level42 = new LeafObject("level18-btn", "level42", false, game, 42,
					"level42", "right", 3, 35,
					415 + GdxGame.VIRTUAL_GAME_WIDTH, 730);
			level42.setPositionXY(415 + GdxGame.VIRTUAL_GAME_WIDTH, 650);
			level42.setOriginX(14);
			level42.setOriginY(96);

			level42.folderName = "menu";
			level42.setSkin();
			level42.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level42);

		}

		if (level41 == null) {
			level41 = new LeafObject("level17-btn", "level41", false, game, 41,
					"level41", "left", 46, 52,
					370 + GdxGame.VIRTUAL_GAME_WIDTH, 740);
			level41.setPositionXY(345 + GdxGame.VIRTUAL_GAME_WIDTH, 700);
			level41.setOriginX(75);
			level41.setOriginY(78);

			level41.folderName = "menu";
			level41.setSkin();
			level41.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level41);

		}

		if (level40 == null) {
			level40 = new LeafObject("level16-btn", "level40", false, game, 40,
					"level40", "right", 5, 70,
					270 + GdxGame.VIRTUAL_GAME_WIDTH, 310);
			level40.setPositionXY(240 + GdxGame.VIRTUAL_GAME_WIDTH, 230);
			level40.setOriginX(32);
			level40.setOriginY(147);

			level40.folderName = "menu";
			level40.setSkin();
			level40.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level40);

		}

		if (level39 == null) {
			level39 = new LeafObject("level15-btn", "level39", false, game, 39,
					"level39", "left", 74, 46,
					210 + GdxGame.VIRTUAL_GAME_WIDTH, 410);
			level39.setPositionXY(165 + GdxGame.VIRTUAL_GAME_WIDTH, 330);
			level39.setOriginX(117);
			level39.setOriginY(113);

			level39.folderName = "menu";
			level39.setSkin();
			level39.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level39);
		}

		if (level38 == null) {
			level38 = new LeafObject("level14-btn", "level38", false, game, 38,
					"level38", "left", 67, 37,
					195 + GdxGame.VIRTUAL_GAME_WIDTH, 500);
			level38.setPositionXY(180 + GdxGame.VIRTUAL_GAME_WIDTH, 440);
			level38.setOriginX(95);
			level38.setOriginY(86);

			level38.folderName = "menu";
			level38.setSkin();
			level38.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level38);
		}

		if (level37 == null) {
			level37 = new LeafObject("level13-btn", "level37", false, game, 37,
					"level37", "right", 3, 22,
					240 + GdxGame.VIRTUAL_GAME_WIDTH, 600);
			level37.setPositionXY(245 + GdxGame.VIRTUAL_GAME_WIDTH, 510);
			level37.setOriginX(0);
			level37.setOriginY(84);

			level37.folderName = "menu";
			level37.setSkin();
			level37.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level37);
		}

		if (level36 == null) {
			level36 = new LeafObject("level12-btn", "level36", false, game, 36,
					"level36", "left", 70, 22,
					160 + GdxGame.VIRTUAL_GAME_WIDTH, 610);
			level36.setPositionXY(175 + GdxGame.VIRTUAL_GAME_WIDTH, 530);
			level36.setOriginX(58);
			level36.setOriginY(107);

			level36.folderName = "menu";
			level36.setSkin();
			level36.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level36);
		}

		if (level35 == null) {
			level35 = new LeafObject("level11-btn", "level35", false, game, 35,
					"level35", "left", 56, 8, 180 + GdxGame.VIRTUAL_GAME_WIDTH,
					680);
			level35.setPositionXY(155 + GdxGame.VIRTUAL_GAME_WIDTH, 620);
			level35.setOriginX(82);
			level35.setOriginY(73);

			level35.folderName = "menu";
			level35.setSkin();
			level35.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level35);
		}

		if (level34 == null) {
			level34 = new LeafObject("level10-btn", "level34", false, game, 34,
					"level34", "right", 2, 50,
					250 + GdxGame.VIRTUAL_GAME_WIDTH, 690);
			level34.setPositionXY(240 + GdxGame.VIRTUAL_GAME_WIDTH, 650);
			level34.setOriginX(19);
			level34.setOriginY(93);

			level34.folderName = "menu";
			level34.setSkin();
			level34.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level34);
		}

		if (level33 == null) {
			level33 = new LeafObject("level9-btn", "level33", false, game, 33,
					"level33", "left", 42, 50,
					220 + GdxGame.VIRTUAL_GAME_WIDTH, 740);
			level33.setPositionXY(190 + GdxGame.VIRTUAL_GAME_WIDTH, 700);
			level33.setOriginX(72);
			level33.setOriginY(85);

			level33.folderName = "menu";
			level33.setSkin();
			level33.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level33);
		}

		if (level32 == null) {
			level32 = new LeafObject("level8-btn", "level32", false, game, 32,
					"level32", "right", 4, 17, 90 + GdxGame.VIRTUAL_GAME_WIDTH,
					350);
			level32.setPositionXY(85 + GdxGame.VIRTUAL_GAME_WIDTH, 260);
			level32.setOriginX(14);
			level32.setOriginY(130);

			level32.folderName = "menu";
			level32.setSkin();
			level32.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level32);
		}

		if (level31 == null) {
			level31 = new LeafObject("level7-btn", "level31", false, game, 31,
					"level31", "left", 97, 17,
					-20 + GdxGame.VIRTUAL_GAME_WIDTH, 440);
			level31.setPositionXY(15 + GdxGame.VIRTUAL_GAME_WIDTH, 310);
			level31.setOriginX(74);
			level31.setOriginY(131);

			level31.folderName = "menu";
			level31.setSkin();
			level31.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level31);
		}

		if (level30 == null) {
			level30 = new LeafObject("level6-btn", "level30", false, game, 30,
					"level30", "right", 0, 12, 65 + GdxGame.VIRTUAL_GAME_WIDTH,
					490);
			level30.setPositionXY(65 + GdxGame.VIRTUAL_GAME_WIDTH, 410);
			level30.setOriginX(5);
			level30.setOriginY(80);

			level30.folderName = "menu";
			level30.setSkin();
			level30.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level30);
		}

		if (level29 == null) {
			level29 = new LeafObject("level5-btn", "level29", false, game, 29,
					"level29", "left", 70, 22,
					-10 + GdxGame.VIRTUAL_GAME_WIDTH, 530);
			level29.setPositionXY(5 + GdxGame.VIRTUAL_GAME_WIDTH, 450);
			level29.setOriginX(58);
			level29.setOriginY(109);

			level29.folderName = "menu";
			level29.setSkin();
			level29.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level29);
		}

		if (level28 == null) {
			level28 = new LeafObject("level4-btn", "level28", false, game, 28,
					"level28", "right", 3, 22, 65 + GdxGame.VIRTUAL_GAME_WIDTH,
					590);
			level28.setPositionXY(60 + GdxGame.VIRTUAL_GAME_WIDTH, 520);
			level28.setOriginX(12);
			level28.setOriginY(108);

			level28.folderName = "menu";
			level28.setSkin();
			level28.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level28);
		}

		if (level27 == null) {
			level27 = new LeafObject("level3-btn", "level27", false, game, 27,
					"level27", "left", 54, 42, 30 + GdxGame.VIRTUAL_GAME_WIDTH,
					650);
			level27.setPositionXY(0 + GdxGame.VIRTUAL_GAME_WIDTH, 580);
			level27.setOriginX(64);
			level27.setOriginY(72);

			level27.folderName = "menu";
			level27.setSkin();
			level27.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level27);
		}

		if (level26 == null) {
			level26 = new LeafObject("level2-btn", "level26", false, game, 26,
					"level26", "right", 2, 50, 85 + GdxGame.VIRTUAL_GAME_WIDTH,
					690);
			level26.setPositionXY(80 + GdxGame.VIRTUAL_GAME_WIDTH, 630);
			level26.setOriginX(12);
			level26.setOriginY(92);

			level26.folderName = "menu";
			level26.setSkin();
			level26.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level26);

		}

		if (level25 == null) {
			level25 = new LeafObject("level1-btn", "level25", false, game, 25,
					"level25", "left", 42, 50, 42 + GdxGame.VIRTUAL_GAME_WIDTH,
					740);
			level25.setPositionXY(20 + GdxGame.VIRTUAL_GAME_WIDTH, 700);
			level25.setOriginX(72);
			level25.setOriginY(77);

			level25.folderName = "menu";
			level25.setSkin();
			level25.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock2, level25);

		}

		// Stage 3

		if (level72 == null) {
			level72 = new LeafObject("level24-btn", "level72", false, game, 72,
					"level72", "left", 62, 72,
					350 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 290);
			level72.setPositionXY(300 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 220);
			level72.setOriginX(111);
			level72.setOriginY(132);

			level72.folderName = "menu";
			level72.setSkin();
			level72.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level72);

		}

		if (level71 == null) {
			level71 = new LeafObject("level23-btn", "level71", false, game, 71,
					"level71", "right", 0, 72,
					400 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 420);
			level71.setPositionXY(355 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 330);
			level71.setOriginX(42);
			level71.setOriginY(131);

			level71.folderName = "menu";
			level71.setSkin();
			level71.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level71);
		}

		if (level70 == null) {
			level70 = new LeafObject("level22-btn", "level70", false, game, 70,
					"level70", "left", 66, 37,
					345 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 500);
			level70.setPositionXY(295 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 420);
			level70.setOriginX(108);
			level70.setOriginY(69);

			level70.folderName = "menu";
			level70.setSkin();
			level70.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level70);

		}

		if (level69 == null) {
			level69 = new LeafObject("level21-btn", "level69", false, game, 69,
					"level69", "right", 8, 69,
					420 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 530);
			level69.setPositionXY(395 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 450);
			level69.setOriginX(21);
			level69.setOriginY(116);

			level69.folderName = "menu";
			level69.setSkin();
			level69.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level69);

		}

		if (level68 == null) {
			level68 = new LeafObject("level20-btn", "level68", false, game, 68,
					"level68", "left", 66, 35,
					360 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 600);
			level68.setPositionXY(335 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 520);
			level68.setOriginX(92);
			level68.setOriginY(87);

			level68.folderName = "menu";
			level68.setSkin();
			level68.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level68);

		}

		if (level67 == null) {
			level67 = new LeafObject("level19-btn", "level67", false, game, 67,
					"level67", "left", 54, 42,
					380 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 660);
			level67.setPositionXY(355 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 600);
			level67.setOriginX(82);
			level67.setOriginY(72);

			level67.folderName = "menu";
			level67.setSkin();
			level67.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level67);

		}

		if (level66 == null) {
			level66 = new LeafObject("level18-btn", "level66", false, game, 66,
					"level66", "right", 3, 35,
					415 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 730);
			level66.setPositionXY(415 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 650);
			level66.setOriginX(14);
			level66.setOriginY(96);

			level66.folderName = "menu";
			level66.setSkin();
			level66.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level66);

		}

		if (level65 == null) {
			level65 = new LeafObject("level17-btn", "level65", false, game, 65,
					"level65", "left", 46, 52,
					370 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 740);
			level65.setPositionXY(345 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 700);
			level65.setOriginX(75);
			level65.setOriginY(78);

			level65.folderName = "menu";
			level65.setSkin();
			level65.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level65);

		}

		if (level64 == null) {
			level64 = new LeafObject("level16-btn", "level64", false, game, 64,
					"level64", "right", 5, 70,
					270 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 310);
			level64.setPositionXY(240 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 230);
			level64.setOriginX(32);
			level64.setOriginY(147);

			level64.folderName = "menu";
			level64.setSkin();
			level64.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level64);

		}

		if (level63 == null) {
			level63 = new LeafObject("level15-btn", "level63", false, game, 63,
					"level63", "left", 74, 46,
					210 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 410);
			level63.setPositionXY(165 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 330);
			level63.setOriginX(117);
			level63.setOriginY(113);

			level63.folderName = "menu";
			level63.setSkin();
			level63.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level63);
		}

		if (level62 == null) {
			level62 = new LeafObject("level14-btn", "level62", false, game, 62,
					"level62", "left", 67, 37,
					195 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 500);
			level62.setPositionXY(180 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 440);
			level62.setOriginX(95);
			level62.setOriginY(86);

			level62.folderName = "menu";
			level62.setSkin();
			level62.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level62);
		}

		if (level61 == null) {
			level61 = new LeafObject("level13-btn", "level61", false, game, 61,
					"level61", "right", 3, 22,
					240 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 600);
			level61.setPositionXY(245 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 510);
			level61.setOriginX(0);
			level61.setOriginY(84);

			level61.folderName = "menu";
			level61.setSkin();
			level61.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level61);
		}

		if (level60 == null) {
			level60 = new LeafObject("level12-btn", "level60", false, game, 60,
					"level60", "left", 70, 22,
					160 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 610);
			level60.setPositionXY(175 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 530);
			level60.setOriginX(58);
			level60.setOriginY(107);

			level60.folderName = "menu";
			level60.setSkin();
			level60.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level60);
		}

		if (level59 == null) {
			level59 = new LeafObject("level11-btn", "level59", false, game, 59,
					"level59", "left", 56, 8,
					180 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 680);
			level59.setPositionXY(155 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 620);
			level59.setOriginX(82);
			level59.setOriginY(73);

			level59.folderName = "menu";
			level59.setSkin();
			level59.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level59);
		}

		if (level58 == null) {
			level58 = new LeafObject("level10-btn", "level58", false, game, 58,
					"level58", "right", 2, 50,
					250 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 690);
			level58.setPositionXY(240 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 650);
			level58.setOriginX(19);
			level58.setOriginY(93);

			level58.folderName = "menu";
			level58.setSkin();
			level58.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level58);
		}

		if (level57 == null) {
			level57 = new LeafObject("level9-btn", "level57", false, game, 57,
					"level57", "left", 42, 50,
					220 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 740);
			level57.setPositionXY(190 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 700);
			level57.setOriginX(72);
			level57.setOriginY(85);

			level57.folderName = "menu";
			level57.setSkin();
			level57.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level57);
		}

		if (level56 == null) {
			level56 = new LeafObject("level8-btn", "level56", false, game, 56,
					"level56", "right", 4, 17,
					90 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 350);
			level56.setPositionXY(85 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 260);
			level56.setOriginX(14);
			level56.setOriginY(130);

			level56.folderName = "menu";
			level56.setSkin();
			level56.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level56);
		}

		if (level55 == null) {
			level55 = new LeafObject("level7-btn", "level55", false, game, 55,
					"level55", "left", 97, 17, -20 + GdxGame.VIRTUAL_GAME_WIDTH
							* 2, 440);
			level55.setPositionXY(15 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 310);
			level55.setOriginX(74);
			level55.setOriginY(131);

			level55.folderName = "menu";
			level55.setSkin();
			level55.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level55);
		}

		if (level54 == null) {
			level54 = new LeafObject("level6-btn", "level54", false, game, 54,
					"level54", "right", 0, 12,
					65 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 490);
			level54.setPositionXY(65 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 410);
			level54.setOriginX(5);
			level54.setOriginY(80);

			level54.folderName = "menu";
			level54.setSkin();
			level54.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level54);
		}

		if (level53 == null) {
			level53 = new LeafObject("level5-btn", "level53", false, game, 53,
					"level53", "left", 70, 22, -10 + GdxGame.VIRTUAL_GAME_WIDTH
							* 2, 530);
			level53.setPositionXY(5 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 450);
			level53.setOriginX(58);
			level53.setOriginY(109);

			level53.folderName = "menu";
			level53.setSkin();
			level53.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level53);
		}

		if (level52 == null) {
			level52 = new LeafObject("level4-btn", "level52", false, game, 52,
					"level52", "right", 3, 22,
					65 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 590);
			level52.setPositionXY(60 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 520);
			level52.setOriginX(12);
			level52.setOriginY(108);

			level52.folderName = "menu";
			level52.setSkin();
			level52.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level52);
		}

		if (level51 == null) {
			level51 = new LeafObject("level3-btn", "level51", false, game, 51,
					"level51", "left", 54, 42,
					30 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 650);
			level51.setPositionXY(0 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 580);
			level51.setOriginX(64);
			level51.setOriginY(72);

			level51.folderName = "menu";
			level51.setSkin();
			level51.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level51);
		}

		if (level50 == null) {
			level50 = new LeafObject("level2-btn", "level50", false, game, 50,
					"level50", "right", 2, 50,
					85 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 690);
			level50.setPositionXY(80 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 630);
			level50.setOriginX(12);
			level50.setOriginY(92);

			level50.folderName = "menu";
			level50.setSkin();
			level50.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level50);

		}

		if (level49 == null) {
			level49 = new LeafObject("level1-btn", "level49", false, game, 49,
					"level49", "left", 42, 50,
					42 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 740);
			level49.setPositionXY(20 + GdxGame.VIRTUAL_GAME_WIDTH * 2, 700);
			level49.setOriginX(72);
			level49.setOriginY(77);

			level49.folderName = "menu";
			level49.setSkin();
			level49.setOnCreateFilter(true);
			rootGroup.addActorBefore(lock3, level49);

		}

		//

		if (!this.isButtonsAdded) {
			rootGroup.addActor(level1);
			rootGroup.addActor(level2);
			rootGroup.addActor(level3);

			this.isButtonsAdded = true;
		}

		if (!GdxGame.tutorialObject.isTutorialUsed("stage_level")) {
			GdxGame.tutorialObject.initTutorial(
					Texts.TUTORIAL_MESSAGE_STAGE_LEVEL, "stage_level", 240,
					530, 420, ArrowPosEnum.TOP, 0.075f, rootGroup);
		} else {
			GdxGame.tutorialObject.initTutorial(Texts.TUTORIAL_MESSAGE_STAGE,
					"stage_stage", 240, 530, 420, ArrowPosEnum.TOP, 0.075f,
					rootGroup);
		}
	}

	@Override
	public void render(float delta) {

		// Gdx.app.log("FPS:",
		// String.valueOf(Gdx.graphics.getFramesPerSecond()));

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glViewport(GdxGame.START_X, GdxGame.START_Y,
				GdxGame.SCREEN_WIDTH, GdxGame.SCREEN_HEIGHT);

		if (!ImageCache.getManager().update()) {
			return;
		}

		if (!this.isButtonsInited) {
			InitButtons();
			this.isButtonsInited = true;
		}

		GdxGame.camera.update();
		GdxGame.tweenManager.update(delta);
		spriteBatch.setProjectionMatrix(GdxGame.camera.combined);

		spriteBatch.begin();
		rootGroup.draw(spriteBatch, 1);
		spriteBatch.end();

		rootGroup.act(delta);

		if (!this.firsEnterFrame && Gdx.input.isKeyPressed(Keys.BACK)) {
			AudioPlayer.playSound("tap2");
			game.loadingScreen.setNextScreen("menu");
			game.loadingScreen.setPrevScreenName("stage");
			game.changeScreen("loading");
		}

		if (this.firsEnterFrame && !Gdx.input.isKeyPressed(Keys.BACK)) {
			this.firsEnterFrame = false;
		}

		if (!this.firsEnterFrame && Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)) {
			AudioPlayer.playSound("tap2");
			game.loadingScreen.setNextScreen("menu");
			game.loadingScreen.setPrevScreenName("stage");
			game.changeScreen("loading");
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {

		GdxGame.camera.position.set(GdxGame.lastPosition,
				GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);
		this.firsEnterFrame = true;

		if (game.loadingScreen.getPrevScreenName().equals("game")) {
			for (int i = 1; i <= Constants.LEVELS_COUNT; i++) {
				LeafObject leaf = (LeafObject) rootGroup.findActor("level" + i);

				try {
					leaf.setStars(GdxGame.files.getOptionsAndProgress("levels",
							"level" + i));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		GdxGame.collectedStars = 0;
		for (int i = 1; i <= Constants.LEVELS_COUNT; i++) {
			int stars = 0;
			try {
				stars = Integer.parseInt(GdxGame.files.getOptionsAndProgress(
						"levels", "level" + String.valueOf(i)));
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (stars != -1) {
				GdxGame.collectedStars += stars;
			}

		}
		if (GdxGame.collectedStars >= Constants.STARS_TO_UNLOCK_SECOND_STAGE) {
			lock2.setVisible(false);

		}
		if (GdxGame.collectedStars >= Constants.STARS_TO_UNLOCK_THIRD_STAGE) {
			lock3.setVisible(false);

		}
		if (needStars2 != null) {
			needStars2
					.setText(""
							+ (GdxGame.collectedStars + "/" + Constants.STARS_TO_UNLOCK_SECOND_STAGE));
		}
		if (needStars3 != null) {
			needStars3
					.setText(""
							+ (GdxGame.collectedStars + "/" + Constants.STARS_TO_UNLOCK_THIRD_STAGE));
		}

		rootGroup.setTouchable(Touchable.enabled);
		this.isButtonsInited = false;

		AudioPlayer.pauseAllMusic();
		AudioPlayer.playMusic("night_theme0");
		// this.game.startFlurry();
		this.game.flurryAgentOnPageView();
	}

	@Override
	public void hide() {
		GdxGame.beetleStage.unfocusAll();
		rootGroup.setTouchable(Touchable.disabled);
		// this.game.stopFlurry();
	}

	@Override
	public void pause() {
		this.game.stopFlurry();

	}

	@Override
	public void resume() {
		Texture.setAssetManager(ImageCache.getManager());
		this.game.startFlurry();
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

}
