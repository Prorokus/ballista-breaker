package com.workshop27.ballistabreaker.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.constants.Texts;
import com.workshop27.ballistabreaker.engine.*;
import com.workshop27.ballistabreaker.engine.ShowTutorialObject.ArrowPosEnum;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class MainMenuScreen implements Screen {

	private final GameObject background;
	public static Group rootGroup;

	private boolean isButtonsInited;
	private boolean isButtonsAdded;

	private final GdxGame game;

	private final SpriteBatch spriteBatch;

	private Boolean firsEnterFrame = true;

	HdCompatibleButton soundBtn;
	HdCompatibleButton helpBtn;
	HdCompatibleButton musicBtn;
	HdCompatibleButton vibroBtn;
	HdCompatibleButton startBtn;
	HdCompatibleButton shopBtn;
	HdCompatibleButton scoreLoopBtn;
	HdCompatibleButton facebookBtn;
	HdCompatibleButton twitterBtn;
	HdCompatibleButton settingsBtn;
	HdCompatibleButton infoBtn;
	HdCompatibleButton eulaBtn;
	HdCompatibleButton privacyBtn;
	HdCompatibleButton termsBtn;
	HdCompatibleButton cancelBtn;

	GameObject explosion_body;
	GameObject particle_explosion;
	GameObject grass;
	GameObject about;

	private final GameObject eyelids;

	private final GameObject loading;
	private final GameObject loading_dot1;
	private final GameObject loading_dot2;
	private final GameObject loading_dot3;

	private final GameObject column;
	private final GameObject play_btn_shade;
	private final GameObject play_btn_explode;

	private final PopupBackground backgroundYesNo;
	private final GameObject yes_no_back;
	private final InGameShopBtn ok_btn;
	private boolean isFacebookInited;
	private boolean isTwitterInited;
	private boolean isTwitterBonusInited;
	private boolean isFacebookBonusInited;
	private final GameObject facebookBonus;
	private final GameObject twitterBonus;

	public MainMenuScreen(GdxGame game) {

		spriteBatch = GdxGame.beetleStage.getSpriteBatch();

		this.game = game;

		rootGroup = new Group();
		
		rootGroup = new Group();
		rootGroup.setName("menuGroup");

		background = new GameObject("main-background", false, false);
		background.setPositionXY(0, 0);
		background.folderName = "menu";
		rootGroup.addActor(background);
		rootGroup.setTouchable(Touchable.disabled);

		GameObject grass0 = new GameObject("grass0", false, true);
		grass0.setPositionXY(-14, 170);
		grass0.setOriginX(55);
		grass0.setOriginY(0);
		grass0.setOnCreateFilter(true);
		grass0.addAction(forever(sequence((rotateTo(-2, 5)), rotateTo(2, 5))));
		grass0.folderName = "menu";
		rootGroup.addActor(grass0);

		GameObject grass1 = new GameObject("grass1", false, true);
		grass1.setPositionXY(444, 110);
		grass1.setOriginX(5);
		grass1.setOriginY(0);
		grass1.setOnCreateFilter(true);
		grass1.addAction(forever(sequence(rotateTo(-3, 3), rotateTo(3, 3))));
		grass1.folderName = "menu";
		rootGroup.addActor(grass1);

		GameObject board_body = new GameObject("board_body", false, true);
		board_body.skinName = null;
		board_body.setPositionXY(195, 740);
		board_body.folderName = "menu";
		board_body.setOriginX(22);
		board_body.setOriginY(200);
		board_body.setRotation(2);

		RotateToAction board_rotate1 = rotateTo(-1.5f, 8);
		RotateToAction board_rotate2 = rotateTo(1.5f, 8);
		board_body.addAction(forever(sequence(board_rotate1, board_rotate2)));

		rootGroup.addActor(board_body);

		GameObject board = new GameObject("beetletable", false, true);
		board.setPositionXY(-196, -340);
		board.folderName = "menu";
		board.setOriginX(167);
		board.setOriginY(244);
		board.setRotation(0.75f);
		board.setOnCreateFilter(true);

		RotateToAction board2_rotate1 = rotateTo(-0.75f, 4);
		RotateToAction board2_rotate2 = rotateTo(0.75f, 4);
		RotateToAction board2_rotate3 = rotateTo(0f, 4);
		board.addAction(forever(sequence(board2_rotate3, board2_rotate1,board2_rotate3, board2_rotate2)));

		board_body.addActor(board);

		GameObject rope = new GameObject("rope", false, true);
		rope.setPositionXY(0, 0);
		rope.folderName = "menu";
		rope.setOnCreateFilter(true);
		board_body.addActor(rope);

		GameObject particle_smoke = new GameObject("particle_smoke", false);
		particle_smoke.skinName = "";
		particle_smoke.setPositionXY(155, -65);
		particle_smoke.createParticle("p5", true);
		board_body.addActor(particle_smoke);

		explosion_body = new GameObject("box-explode-container", false, false);
		explosion_body.setPositionXY(0, 0);
		explosion_body.folderName = "menu";
		explosion_body.skinName = null;
		explosion_body.setVisible(false);
		rootGroup.addActor(explosion_body);

		GameObject box_explode = new GameObject("box-explode", false, true);
		box_explode.setPositionXY(90, 220);
		box_explode.folderName = "menu";
		explosion_body.addActor(box_explode);

		GameObject particle_smoke1 = new GameObject("particle_smoke1", false);
		particle_smoke1.skinName = "";
		particle_smoke1.setPositionXY(202, 360);
		particle_smoke1.createParticle("p5", true);
		explosion_body.addActor(particle_smoke1);

		GameObject particle_smoke2 = new GameObject("particle_smoke2", false);
		particle_smoke2.skinName = "";
		particle_smoke2.setPositionXY(245, 345);
		particle_smoke2.createParticle("p5", true);
		explosion_body.addActor(particle_smoke2);

		GameObject particle_smoke3 = new GameObject("particle_smoke3", false);
		particle_smoke3.skinName = "";
		particle_smoke3.setPositionXY(283, 358);
		particle_smoke3.createParticle("p5", true);
		explosion_body.addActor(particle_smoke3);

		GameObject particle_smoke4 = new GameObject("particle_smoke4", false);
		particle_smoke4.skinName = "";
		particle_smoke4.setPositionXY(278, 380);
		particle_smoke4.createParticle("p5", true);
		explosion_body.addActor(particle_smoke4);

		GameObject particle_smoke5 = new GameObject("particle_smoke5", false);
		particle_smoke5.skinName = "";
		particle_smoke5.setPositionXY(234, 382);
		particle_smoke5.createParticle("p5", true);
		explosion_body.addActor(particle_smoke5);

		grass = new GameObject("grass", false, true);
		grass.setPositionXY(0, 0);
		grass.folderName = "menu";
		rootGroup.addActor(grass);

		/*
		 * GameObject eyes1 = new GameObject("eye1", false, true);
		 * eyes1.setPositionXY(210, 300); eyes1.folderName = "menu";
		 * 
		 * FadeIn eyelids_fadeIn2 = FadeIn.$(0.1f); FadeOut eyelids_fadeOut2 =
		 * FadeOut.$(0.1f); eyes1.action(Forever.$(Delay.$(
		 * Sequence.$(eyelids_fadeOut2, Delay.$(eyelids_fadeIn2, 0.3f)), 4)));
		 * 
		 * explosion_body.addActor(eyes1);
		 * 
		 * GameObject eyes2 = new GameObject("eye2", false, true);
		 * eyes2.setPositionXY(189, 280); eyes2.folderName = "menu";
		 * 
		 * FadeIn eyelids_fadeIn3 = FadeIn.$(0.1f); FadeOut eyelids_fadeOut3 =
		 * FadeOut.$(0.1f); eyes2.action(Forever.$(Delay.$(
		 * Sequence.$(eyelids_fadeOut3, Delay.$(eyelids_fadeIn3, 0.3f)), 6)));
		 * 
		 * explosion_body.addActor(eyes2);
		 * 
		 * GameObject eyes3 = new GameObject("eye3", false, true);
		 * eyes3.setPositionXY(202, 334); eyes3.folderName = "menu";
		 * 
		 * FadeIn eyelids_fadeIn4 = FadeIn.$(0.1f); FadeOut eyelids_fadeOut4 =
		 * FadeOut.$(0.1f); eyes3.action(Forever.$(Delay.$(
		 * Sequence.$(eyelids_fadeOut4, Delay.$(eyelids_fadeIn4, 0.3f)), 8)));
		 * 
		 * explosion_body.addActor(eyes3);
		 * 
		 * GameObject eyes4 = new GameObject("eye4", false, true);
		 * eyes4.setPositionXY(242, 319); eyes4.folderName = "menu";
		 * 
		 * FadeIn eyelids_fadeIn5 = FadeIn.$(0.1f); FadeOut eyelids_fadeOut5 =
		 * FadeOut.$(0.1f); eyes4.action(Forever.$(Delay.$(
		 * Sequence.$(eyelids_fadeOut5, Delay.$(eyelids_fadeIn5, 0.3f)), 10)));
		 * 
		 * explosion_body.addActor(eyes4);
		 * 
		 * GameObject eyes5 = new GameObject("eye5", false, true);
		 * eyes5.setPositionXY(230, 270); eyes5.folderName = "menu";
		 * 
		 * FadeIn eyelids_fadeIn6 = FadeIn.$(0.1f); FadeOut eyelids_fadeOut6 =
		 * FadeOut.$(0.1f); eyes5.action(Forever.$(Delay.$(
		 * Sequence.$(eyelids_fadeOut6, Delay.$(eyelids_fadeIn6, 0.3f)), 12)));
		 * 
		 * explosion_body.addActor(eyes5);
		 */

		// ----EYELIDS----
		eyelids = new GameObject("eyes", false);
		eyelids.setPositionXY(427, 677);
		eyelids.folderName = "menu";

		AlphaAction eyelids_fadeIn1 = fadeIn(0.1f);
		AlphaAction eyelids_fadeOut1 = fadeOut(0.1f);

		rootGroup.addActor(eyelids);

		// ----------LOADING-------------
		loading = new GameObject("loading", false, true);
		loading.setPositionXY(150, 276);
		loading.folderName = "menu";
		loading.getColor().a = 0;
		rootGroup.addActor(loading);

		loading_dot1 = new GameObject("loading-dot", false, true);
		loading_dot1.setPositionXY(311, 287);
		loading_dot1.folderName = "menu";
		loading_dot1.getColor().a = 0;
		rootGroup.addActor(loading_dot1);

		loading_dot2 = new GameObject("loading-dot", false, true);
		loading_dot2.setPositionXY(339, 287);
		loading_dot2.folderName = "menu";
		loading_dot2.getColor().a = 0;
		rootGroup.addActor(loading_dot2);

		loading_dot3 = new GameObject("loading-dot", false, true);
		loading_dot3.setPositionXY(371, 287);
		loading_dot3.folderName = "menu";
		loading_dot3.getColor().a = 0;
		rootGroup.addActor(loading_dot3);

		column = new GameObject("column", false, true);
		column.setPositionXY(15, 45);
		column.folderName = "menu";
		rootGroup.addActor(column);

		play_btn_shade = new GameObject("play-btn-shade", false, true);
		play_btn_shade.setPositionXY(186, 348);
		play_btn_shade.folderName = "menu";
		rootGroup.addActor(play_btn_shade);

		play_btn_explode = new GameObject("play-btn-explode", false, true);
		play_btn_explode.setVisible(false);
		play_btn_explode.setPositionXY(177, 336);
		play_btn_explode.folderName = "menu";
		rootGroup.addActor(play_btn_explode);

		particle_explosion = new GameObject("particle_explosion", false);
		particle_explosion.getColor().a = 0;
		particle_explosion.skinName = "";
		particle_explosion.createParticle("menuExplosion", true);
		particle_explosion.setPositionXY(255, 400);
		rootGroup.addActor(particle_explosion);

		// ------------------SHOP YES/NO BOX-------------------
		backgroundYesNo = new PopupBackground("coming", false, true);
		backgroundYesNo.setPositionXY(0, 0);
		backgroundYesNo.setWidth(GdxGame.VIRTUAL_GAME_WIDTH);
		backgroundYesNo.setHeight(GdxGame.VIRTUAL_GAME_HEIGHT);
		backgroundYesNo.folderName = "menu";
		backgroundYesNo.setVisible(false);
		rootGroup.addActor(backgroundYesNo);

		yes_no_back = new GameObject("yes_no_back", false, true);
		yes_no_back.skinName = "txt-back";
		yes_no_back.setPositionXY(48, 360);
		yes_no_back.folderName = "menu";
		yes_no_back.loadSkinOnDraw = true;
		yes_no_back.setVisible(false);
		rootGroup.addActor(yes_no_back);

		GameObject prize = new GameObject("prize", false, true);
		prize.skinName = "inv-laser";
		prize.setPositionXY(80, 40);
		prize.folderName = "menu";
		prize.loadSkinOnDraw = true;
		yes_no_back.addActor(prize);

		AnimatedTextGameObject text1 = new AnimatedTextGameObject("text1",
				false, "popup", 20);
		text1.setTextCenteredOnPosition(Texts.SOCIAL_TEXT_FB_REWARD, 200, 170,
				330, false);
		text1.setColor(0.31f, 0.2f, 0.1f, 1.0f);
		yes_no_back.addActor(text1);

		AnimatedTextGameObject name1 = new AnimatedTextGameObject("name1",
				false, "tutorial", 45);
		name1.setTextCenteredOnPosition(Texts.SHOP_TEXT_NAME_LASER, 200, 250,
				480, false);
		name1.setColor(0.75f, 0.15f, 0.15f, 1.0f);
		yes_no_back.addActor(name1);

		facebookBonus = new GameObject("facebookBonus", false, true);
		facebookBonus.skinName = "social-boom";
		facebookBonus.setPositionXY(10, 720);
		facebookBonus.folderName = "menu";
		facebookBonus.loadSkinOnDraw = true;
		rootGroup.addActor(facebookBonus);

		twitterBonus = new GameObject("twitterBonus", false, true);
		twitterBonus.skinName = "social-karate";
		twitterBonus.setPositionXY(50, 720);
		twitterBonus.folderName = "menu";
		twitterBonus.loadSkinOnDraw = true;
		rootGroup.addActor(twitterBonus);

		ok_btn = new InGameShopBtn("ok-btn", false, true, this.game, this);
		ok_btn.skinName = "buy-btn";
		ok_btn.setPositionXY(320, 400);
		ok_btn.folderName = "menu";
		ok_btn.setTouchable(Touchable.enabled);
		ok_btn.setVisible(false);
		ok_btn.loadSkinOnDraw = true;
		rootGroup.addActor(ok_btn);

		about = new GameObject("about", false, true);
		about.setPositionXY(0, 0);
		about.folderName = "menu";
		about.setVisible(false);
		rootGroup.addActor(about);

		// -------------------------------------------------------------

		GdxGame.beetleStage.addActor(rootGroup);
	}

	private static void changePopupMessageText(String name, String skinName,
			int msgId) {
		AnimatedTextGameObject aTgO = (AnimatedTextGameObject) rootGroup
				.findActor("name1");
		aTgO.setTextCenteredOnPosition(name, 200, 255, 480, false);

		aTgO = (AnimatedTextGameObject) rootGroup.findActor("text1");
		if (msgId == 0) {
			aTgO.setTextCenteredOnPosition(Texts.SOCIAL_TEXT_FB_REWARD, 200,
					180, 330, false);
		} else {
			aTgO.setTextCenteredOnPosition(Texts.NOTIF_TEXT_REWARD, 200, 180,
					330, false);
		}

		GameObject prize = (GameObject) rootGroup.findActor("prize");
		prize.skinName = skinName;
		prize.setWidth(0);
		prize.setHeight(0);
		prize.setSkin();
	}

	public void showPrizeMsg(int prizeId, int msgId) {
		backgroundYesNo.setVisible(true);
		yes_no_back.setVisible(true);
		ok_btn.setVisible(true);

		switch (prizeId) {
		case 1:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_MUSTACHE,
					"inv-coockie", msgId);
			break;

		case 2:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_LASER, "inv-laser",
					msgId);
			break;

		case 3:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_BREAKER, "inv-karate",
					msgId);
			break;

		case 4:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_BLASTER, "inv-bomb",
					msgId);
			break;

		case 5:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_RADIUS, "inv-radius",
					msgId);
			break;

		case 6:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_EGG1, "inv-egg1", msgId);
			break;

		case 7:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_EGG2, "inv-egg2", msgId);
			break;

		default:
			break;
		}
	}

	public void hideYesNoBox() {
		backgroundYesNo.setVisible(false);
		yes_no_back.setVisible(false);
		ok_btn.setVisible(false);
	}

	public void InitButtons() {

		column.setVisible(false);

		if (scoreLoopBtn == null) {
			scoreLoopBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("board-btn")));
			scoreLoopBtn.setPositionXY(166, 45);
		} else {
			scoreLoopBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("board-btn"));
		}

		if (facebookBtn == null) {
			facebookBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("facebook-btn")));
			facebookBtn.setPositionXY(10, 720);
		} else {
			facebookBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("facebook-btn"));
		}

		rootGroup.addActorBefore(facebookBonus, facebookBtn);

		if (twitterBtn == null) {
			twitterBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("twitter-btn")));
			twitterBtn.setPositionXY(50, 720);
		} else {
			twitterBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("twitter-btn"));
		}

		rootGroup.addActorBefore(twitterBonus, twitterBtn);

		try {
			if (GdxGame.files.getOptionsAndProgress("options", "sound").equals(
					"on")) {
				if (soundBtn == null) {
					soundBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("sound")));
					soundBtn.setPositionXY(10, 270);
				} else {
					soundBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("sound"));
				}
			} else {
				if (soundBtn == null) {
					soundBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("sound-off")));
					soundBtn.setPositionXY(10, 270);
				} else {
					soundBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("sound-off"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		soundBtn.setVisible(false);
		if (helpBtn == null) {
			helpBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("help")));
			helpBtn.setPositionXY(321, 45);
		} else {
			helpBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("help"));
		}

		if (infoBtn == null) {
			infoBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("info")));
			infoBtn.setPositionXY(399, 45);
		} else {
			infoBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("info"));
		}

		if (eulaBtn == null) {
			eulaBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("eula")));
			eulaBtn.setPositionXY(90, 370);
		} else {
			eulaBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("eula"));
		}

		if (privacyBtn == null) {
			privacyBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("privacy")));
			privacyBtn.setPositionXY(90, 310);
		} else {
			privacyBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("privacy"));
		}

		if (termsBtn == null) {
			termsBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("terms")));
			termsBtn.setPositionXY(90, 250);
		} else {
			termsBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("terms"));
		}

		if (cancelBtn == null) {
			cancelBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("cancle")));
			cancelBtn.setPositionXY(380, 630);
		} else {
			cancelBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("cancle"));
		}

		try {
			if (GdxGame.files.getOptionsAndProgress("options", "music").equals(
					"on")) {
				if (musicBtn == null) {
					musicBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("music")));
					musicBtn.setPositionXY(10, 197);
				} else {
					musicBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("music"));
				}
			} else {
				if (musicBtn == null) {
					musicBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("music-off")));
					musicBtn.setPositionXY(10, 197);
				} else {
					musicBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("music-off"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		musicBtn.setVisible(false);
		try {
			if (GdxGame.files.getOptionsAndProgress("options", "vibro").equals(
					"on")) {
				if (vibroBtn == null) {
					vibroBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("vibro")));
					vibroBtn.setPositionXY(10, 121);
				} else {
					vibroBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("vibro"));
				}
			} else {
				if (vibroBtn == null) {
					vibroBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("vibro-off")));
					vibroBtn.setPositionXY(10, 121);
				} else {
					vibroBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/menu/pack", TextureAtlas.class)
							.findRegion("vibro-off"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		vibroBtn.setVisible(false);
		if (startBtn == null) {
			startBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("play-btn")));
			startBtn.setPositionXY(194, 354);
		} else {
			startBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("play-btn"));
		}

		if (shopBtn == null) {
			shopBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("shop-btn")), new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("shop-btn")));
			shopBtn.setPositionXY(243, 45);
		} else {
			shopBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("shop-btn"));

			shopBtn.getStyle().down = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("shop-btn"));
		}

		if (settingsBtn == null) {
			settingsBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("settings-btn")));
			settingsBtn.setPositionXY(10, 45);
		} else {
			settingsBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/menu/pack", TextureAtlas.class)
					.findRegion("settings-btn"));
		}

		about.setVisible(false);
		cancelBtn.setVisible(false);
		eulaBtn.setVisible(false);
		privacyBtn.setVisible(false);
		termsBtn.setVisible(false);

		if (!this.isButtonsAdded) {
			rootGroup.addActorBefore(backgroundYesNo, soundBtn);
			rootGroup.addActorBefore(backgroundYesNo, helpBtn);
			rootGroup.addActorBefore(backgroundYesNo, musicBtn);
			rootGroup.addActorBefore(backgroundYesNo, vibroBtn);
			rootGroup.addActorBefore(backgroundYesNo, scoreLoopBtn);
			rootGroup.addActorBefore(backgroundYesNo, shopBtn);
			rootGroup.addActorBefore(backgroundYesNo, infoBtn);
			rootGroup.addActorBefore(backgroundYesNo, startBtn);
			rootGroup.addActor(cancelBtn);
			rootGroup.addActor(eulaBtn);
			rootGroup.addActor(privacyBtn);
			rootGroup.addActor(termsBtn);

			rootGroup.addActorBefore(backgroundYesNo, settingsBtn);

			settingsBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					soundBtn.setVisible(!soundBtn.isVisible());
					vibroBtn.setVisible(!vibroBtn.isVisible());
					musicBtn.setVisible(!musicBtn.isVisible());
					column.setVisible(!column.isVisible());
				}
			});

			helpBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");

					game.tutorialScreen.setPage(1);
					game.setScreen(game.tutorialScreen);
				}
			});

			infoBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");

					about.setVisible(true);
					cancelBtn.setVisible(true);
					eulaBtn.setVisible(true);
					privacyBtn.setVisible(true);
					termsBtn.setVisible(true);
				}
			});

			eulaBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					game.startBrowser(Constants.EULA_URL);
				}
			});

			privacyBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					game.startBrowser(Constants.PRIVACY_POLICY);
				}
			});

			termsBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					game.startBrowser(Constants.TERMS_OF_SERVICE);
				}
			});

			cancelBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");

					about.setVisible(false);
					cancelBtn.setVisible(false);
					eulaBtn.setVisible(false);
					privacyBtn.setVisible(false);
					termsBtn.setVisible(false);
				}
			});

			shopBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					game.loadingScreen.setNextScreen("shop");
					game.loadingScreen.setPrevScreenName("menu");

					rootGroup.setTouchable(Touchable.disabled);
					game.changeScreen("loading");
				}
			});

			startBtn.addListener(new ClickListener() {
				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					game.loadingScreen.setNextScreen("stage");
					game.loadingScreen.setPrevScreenName("menu");

					particle_explosion.getColor().a = 1;
					particle_explosion.particleStack.get("menuExplosion")
							.start();
					AudioPlayer.playSound("explode1");
					startBtn.setVisible(false);
					explosion_body.setVisible(true);

					play_btn_shade.setVisible(false);
					play_btn_explode.setVisible(true);

					AlphaAction loading_fadeIn1 = fadeIn(0.25f);
					AlphaAction loading_fadeIn2 = fadeIn(0.25f);
					AlphaAction loading_fadeIn3 = fadeIn(0.25f);
					AlphaAction loading_fadeIn4 = fadeIn(0.25f);

					loading.addAction(loading_fadeIn1);
					loading_dot1.addAction(loading_fadeIn2);
					loading_dot2.addAction(loading_fadeIn3);
					loading_dot3.addAction(loading_fadeIn4);

					rootGroup.setTouchable(Touchable.disabled);
					game.changeScreenAfter("loading", 0);
				}
			});

			soundBtn.addListener(new ClickListener() {

				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");

					try {
						if (GdxGame.files.getOptionsAndProgress("options",
								"sound").equals("on")) {
							GdxGame.files.setOptionsAndProgress("options",
									"sound", "off");
							
							soundBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("sound-off"));
						} else {
							GdxGame.files.setOptionsAndProgress("options",
									"sound", "on");
							soundBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("sound"));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			musicBtn.addListener(new ClickListener() {

				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");

					try {
						if (GdxGame.files.getOptionsAndProgress("options",
								"music").equals("on")) {
							AudioPlayer.pauseMusic("night_theme0");

							GdxGame.files.setOptionsAndProgress("options",
									"music", "off");
							musicBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("music-off"));
						} else {
							GdxGame.files.setOptionsAndProgress("options",
									"music", "on");
							musicBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("music"));

							AudioPlayer.playMusic("night_theme0");
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			vibroBtn.addListener(new ClickListener() {

				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");

					try {
						if (GdxGame.files.getOptionsAndProgress("options",
								"vibro").equals("on")) {
							GdxGame.files.setOptionsAndProgress("options",
									"vibro", "off");
							vibroBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("vibro-off"));
							} else {
							Gdx.input.vibrate(25);

							GdxGame.files.setOptionsAndProgress("options",
									"vibro", "on");
							vibroBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/menu/pack", TextureAtlas.class).findRegion("vibro"));
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			});

			scoreLoopBtn.addListener(new ClickListener() {

				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					game.showScoreloop();
				}
			});

			facebookBtn.addListener(new ClickListener() {

				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					game.startFacebook("Test Facebook", 1);
					game.flurryAgentLogEvent("facebook post for bonus");

				}
			});

			twitterBtn.addListener(new ClickListener() {

				@Override
				public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					AudioPlayer.playSound("tap3");
					game.startTwitter("Test Twitter", 1);
					game.flurryAgentLogEvent("twitter post for bonus");

				}
			});

			this.isButtonsAdded = true;
		}

		GdxGame.tutorialObject.initTutorial(Texts.TUTORIAL_MESSAGE_FB_AD,
				"fb_ad", 240, 530, 420, ArrowPosEnum.TOP, 0.075f, rootGroup);
	}

	@Override
	public void render(float delta) {
		// Gdx.app.log("x:y", Gdx.input.getX() + " : " + Gdx.input.getY());
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		Gdx.gl.glViewport(GdxGame.START_X, GdxGame.START_Y,
				GdxGame.SCREEN_WIDTH, GdxGame.SCREEN_HEIGHT);

		if (!ImageCache.getManager().update()) {
			return;
		}

		if (!this.isButtonsInited) {
			InitButtons();
			this.isButtonsInited = true;
		}

		if (!this.isFacebookInited) {
			initFacebook();
			this.isFacebookInited = true;
		}

		if (!this.isTwitterInited) {
			initTwitter();
			this.isTwitterInited = true;
		}

		if (!this.isFacebookBonusInited) {
			generateDayBonus("facebook", facebookBonus);
			this.isFacebookBonusInited = true;
		}

		if (!this.isTwitterBonusInited) {
			generateDayBonus("twitter", twitterBonus);
			this.isTwitterBonusInited = true;
		}

		GdxGame.camera.update();
		spriteBatch.setProjectionMatrix(GdxGame.camera.combined);

		spriteBatch.begin();
		rootGroup.draw(spriteBatch, 1);
		spriteBatch.end();

		rootGroup.act(delta);

		if (!this.firsEnterFrame && Gdx.input.isKeyPressed(Keys.BACK)) {
			// AudioPlayer.playSound("tap2");
			dispose();
			Gdx.app.exit();
		}

		if (this.firsEnterFrame && !Gdx.input.isKeyPressed(Keys.BACK)) {
			this.firsEnterFrame = false;
		}

		/*
		 * if (!this.firsEnterFrame &&
		 * Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)) { dispose();
		 * Gdx.app.exit(); }
		 * 
		 * if (this.firsEnterFrame &&
		 * !Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)) { this.firsEnterFrame =
		 * false; }
		 */
	}

	public void onMenuClick() {
		if (about.isVisible()) {
			about.setVisible(false);
			cancelBtn.setVisible(false);
			eulaBtn.setVisible(false);
			privacyBtn.setVisible(false);
			termsBtn.setVisible(false);
		} else {
			about.setVisible(true);
			cancelBtn.setVisible(true);
			eulaBtn.setVisible(true);
			privacyBtn.setVisible(true);
			termsBtn.setVisible(true);
		}
	}

	private void initFacebook() {
		if (System.currentTimeMillis() - Constants.DAY_IN_MILLISECONDS > Long
				.parseLong(this.game.getPostTime("facebook"))) {
			facebookBtn.setVisible(true);
			facebookBonus.setVisible(true);
		} else {
			facebookBtn.setVisible(false);
			facebookBonus.setVisible(false);
		}

	}

	private void initTwitter() {
		if (System.currentTimeMillis() - Constants.DAY_IN_MILLISECONDS > Long
				.parseLong(this.game.getPostTime("twitter"))) {
			twitterBtn.setVisible(true);
			twitterBonus.setVisible(true);
		} else {
			twitterBtn.setVisible(false);
			twitterBonus.setVisible(false);
		}

	}

	private void generateDayBonus(String socialNetwork, GameObject socialObject) {
		if (System.currentTimeMillis() - Constants.DAY_IN_MILLISECONDS > Long
				.parseLong(this.game.getBonusGenerateTime(socialNetwork))) {
			this.game.generateNewDayBonus(socialNetwork);

			switch (Integer.valueOf(this.game.getBonusId(socialNetwork))) {
			case 0:
				socialObject.skinName = "social-laser";
				break;
			case 1:
				socialObject.skinName = "social-ekpl";
				break;
			case 2:
				socialObject.skinName = "social-boom";
				break;
			case 3:
				socialObject.skinName = "social-karate";
				break;
			case 4:
				socialObject.skinName = "social-range";
				break;
			case 5:
				socialObject.skinName = "social-egg1";
				break;
			case 6:
				socialObject.skinName = "social-egg2";
				break;

			default:

				break;
			}
			socialObject.setHeight(0);
			socialObject.setWidth(0);
			socialObject.setSkin();

		}

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {

		if (game.loadingScreen.getPrevScreenName().equals("logo")) {
		//	this.game.setScreen(game.splashScreen);
		}

		game.loadingScreen.setPrevScreenName("menu");
		game.loadingScreen.setNextScreen("stage");

		this.firsEnterFrame = true;
		rootGroup.setTouchable(Touchable.enabled);
		this.isButtonsInited = false;

		play_btn_shade.setVisible(true);
		play_btn_explode.setVisible(false);

		if (startBtn != null) {
			loading.getColor().a = 0;
			loading_dot1.getColor().a = 0;
			loading_dot2.getColor().a = 0;
			loading_dot3.getColor().a = 0;

			startBtn.setVisible(true);
			explosion_body.setVisible(false);

		}

		AudioPlayer.pauseAllMusic();
		AudioPlayer.playMusic("night_theme0");

		if (GdxGame.showPrizeMsg) {
			showPrizeMsg(2, 1);
			GdxGame.showPrizeMsg = false;
		}

		isTwitterInited = false;
		isFacebookInited = false;
		isTwitterBonusInited = false;
		isFacebookBonusInited = false;

		this.game.startFlurry();
		this.game.flurryAgentOnPageView();

	}

	@Override
	public void hide() {
		rootGroup.setTouchable(Touchable.disabled);
		// this.game.stopFlurry();
		GdxGame.tutorialObject.visible = false;
	}

	@Override
	public void pause() {
		this.game.stopFlurry();
	}

	@Override
	public void resume() {
		Texture.setAssetManager(ImageCache.getManager());
		isTwitterInited = false;
		isFacebookInited = false;
		isTwitterBonusInited = false;
		isFacebookBonusInited = false;

		this.game.startFlurry();

		if (GdxGame.showSocialPrizeMsg) {
			showPrizeMsg(GdxGame.socialPrizeId, 0);
			GdxGame.showSocialPrizeMsg = false;
			GdxGame.socialPrizeId = -1;
		}
	}

	@Override
	public void dispose() {
		if (GameScreen.objectToDestroy != null) {
			GameScreen.objectToDestroy.pixmapHelper.dispose();
			GameScreen.objectToDestroy = null;
		}
		AudioPlayer.clearAllMusic();
		AudioPlayer.clearAllSounds();

		GdxGame.beetleStage.unfocusAll();
		GdxGame.beetleStage.clear();
		ImageCache.getManager().dispose();
	}
}
