package com.workshop27.ballistabreaker.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.RepeatAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.AudioPlayer;
import com.workshop27.ballistabreaker.engine.GameObject;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.engine.PopupBackground;
import com.workshop27.ballistabreaker.engine.TutorialButton;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class TutorialScreen implements Screen {
	private GdxGame game;
	private SpriteBatch spriteBatch;

	public Group rootGroup;
	public GameObject page1Group;
	public GameObject page2Group;
	public GameObject page3Group;
	public GameObject page4Group;
	public GameObject page5Group;

	private TutorialButton arrow_l;
	private TutorialButton arrow_r;

	public int currPage = -1;

	private float deltaCounter = 0;

	private boolean isVisible = false;
	private int tapCounter;
	private int oldPage;

	private void initObjects(GdxGame game) {
		spriteBatch = GdxGame.beetleStage.getSpriteBatch();
		this.game = game;

		rootGroup = new Group();
		rootGroup.setName("tutorialGroup");
		
		page1Group = new GameObject("page1Group", false);
		page1Group.skinName = "";

		page2Group = new GameObject("page2Group", false);
		page2Group.skinName = "";

		page3Group = new GameObject("page3Group", false);
		page3Group.skinName = "";

		page4Group = new GameObject("page4Group", false);
		page4Group.skinName = "";

		page5Group = new GameObject("page5Group", false);
		page5Group.skinName = "";

		rootGroup.setTouchable(Touchable.disabled);

		PopupBackground background = new PopupBackground("background", false,
				false);
		background.setPositionXY(0, 0);
		background.folderName = "tutorial";
		rootGroup.addActor(background);

		GameObject light = new GameObject("light", false, true);
		light.setPositionXY(9, 123);
		light.setOriginX(236);
		light.setOriginY(688);
		light.folderName = "tutorial";
		light.setOnCreateFilter(true);
		rootGroup.addActor(light);

		RotateToAction light_rotate1 = rotateTo(3, 3);
		RotateToAction light_rotate2 = rotateTo(-3, 3);
		light.addAction(forever(sequence(light_rotate1, light_rotate2)));

		GameObject particle_dust = new GameObject("particle_dust", false);
		particle_dust.skinName = "";
		particle_dust.setOriginX(240);
		particle_dust.setOriginY(650);
		particle_dust.createParticle("p3", true);
		rootGroup.addActor(particle_dust);

		GameObject back = new GameObject("back", false, true);
		back.setPositionXY(18, 30);
		back.folderName = "tutorial";
		rootGroup.addActor(back);

		// --------------REGULAR PAGE 1------------------------------
		GameObject regular_trace1 = new GameObject("fan-trace1", false, true);
		regular_trace1.setPositionXY(197, 115);
		regular_trace1.folderName = "tutorial";
		page1Group.addActor(regular_trace1);

		GameObject regular = new GameObject("regular", false, true);
		regular.setPositionXY(55, 270);
		regular.folderName = "tutorial";

		page1Group.addActor(regular);

		GameObject regular_txt = new GameObject("regular-txt", false, true);
		regular_txt.setPositionXY(51, 367);
		regular_txt.folderName = "tutorial";
		page1Group.addActor(regular_txt);

		GameObject regular_name = new GameObject("regular-name", false, true);
		regular_name.setPositionXY(143, 565);
		regular_name.folderName = "tutorial";
		page1Group.addActor(regular_name);

		// --------------FAN PAGE 2------------------------------
		GameObject fan_line1 = new GameObject("line1", "line", false);
		fan_line1.setPositionXY(240, 60);
		fan_line1.folderName = "tutorial";
		fan_line1.getColor().a = 0;

		fan_line1.addAction(forever(sequence(delay(0.0f), fadeIn(0f), delay(4f), fadeOut(0f))));

		page2Group.addActor(fan_line1);

		GameObject fan_line2 = new GameObject("line2", "line", false);
		fan_line2.setPositionXY(240, 95);
		fan_line2.folderName = "tutorial";
		fan_line2.getColor().a = 0;

		fan_line2.addAction(forever(sequence(delay(0.25f), fadeIn(0f), delay(3.75f), fadeOut(0f))));

		page2Group.addActor(fan_line2);

		GameObject fan_line3 = new GameObject("line3", "line", false);
		fan_line3.setPositionXY(240, 130);
		fan_line3.folderName = "tutorial";
		fan_line3.getColor().a = 0;

		fan_line3.addAction(forever(sequence(delay(0.5f), fadeIn(0f), delay(3.5f), fadeOut(0f))));
		page2Group.addActor(fan_line3);

		GameObject fan_line4 = new GameObject("line4", "line", false);
		fan_line4.setPositionXY(240, 165);
		fan_line4.folderName = "tutorial";
		fan_line4.getColor().a = 0;

		fan_line4.addAction(forever(sequence(delay(0.75f), fadeIn(0f), delay(3.25f), fadeOut(0f))));

		page2Group.addActor(fan_line4);

		GameObject fan_line5 = new GameObject("line5", "line", false);
		fan_line5.setPositionXY(240, 200);
		fan_line5.folderName = "tutorial";
		fan_line5.getColor().a = 0;

		fan_line5.addAction(forever(sequence(delay(1.0f), fadeIn(0f), delay(3.0f), fadeOut(0f))));
		page2Group.addActor(fan_line5);

		GameObject fan_line6 = new GameObject("line6", "line", false);
		fan_line6.setPositionXY(240, 235);
		fan_line6.folderName = "tutorial";
		fan_line6.getColor().a = 0;

		fan_line6.addAction(forever(sequence(delay(1.25f), fadeIn(0f), delay(2.75f), fadeOut(0f))));
		page2Group.addActor(fan_line6);

		GameObject fan_trace2 = new GameObject("fan-trace2", false, true);
		fan_trace2.setPositionXY(188, 350 - 40);
		fan_trace2.folderName = "tutorial";
		page2Group.addActor(fan_trace2);

		GameObject fan_egg1 = new GameObject("fan-egg1", "fan-egg", false);
		fan_egg1.setPositionXY(220, 250);
		fan_egg1.folderName = "tutorial";
		fan_egg1.getColor().a = 0;

		fan_egg1.addAction(forever(sequence(delay(0.5f), fadeIn(0f), moveTo(158, 354, 1f), delay(2.5f), fadeOut(0f), moveTo(220, 250, 0f))));

		page2Group.addActor(fan_egg1);

		GameObject fan_egg2 = new GameObject("fan-egg2", "fan-egg", false);
		fan_egg2.setPositionXY(220, 250);
		fan_egg2.folderName = "tutorial";
		fan_egg2.getColor().a = 0;

		fan_egg2.addAction(forever(sequence(delay(0.5f), fadeIn(0f), moveTo(292, 354, 1f), delay(2.5f), fadeOut(0f), moveTo(220, 250, 0f))));

		page2Group.addActor(fan_egg2);

		GameObject fan_egg3 = new GameObject("fan-egg3", "fan-egg", false);
		fan_egg3.setPositionXY(220, 250);
		fan_egg3.folderName = "tutorial";
		fan_egg3.getColor().a = 0;

		fan_egg3.addAction(forever(sequence(delay(0.5f), fadeIn(0f), moveTo(226f, 390f, 1f), delay(2.5f), fadeOut(0f), moveTo(220, 250, 0f))));

		page2Group.addActor(fan_egg3);

		GameObject fan_txt = new GameObject("fan-txt", false, true);
		fan_txt.setPositionXY(67, 400);
		fan_txt.folderName = "tutorial";
		page2Group.addActor(fan_txt);

		GameObject fan_name = new GameObject("fan-name", false, true);
		fan_name.setPositionXY(140, 550);
		fan_name.folderName = "tutorial";
		page2Group.addActor(fan_name);

		GameObject fan = new GameObject("fan", false, true);
		fan.setPositionXY(172, 0);
		fan.folderName = "tutorial";

		fan.addAction(forever(sequence(fadeIn(0f), moveTo(172, 238, 0.5f), fadeOut(0f), delay(2.5f), moveTo(172, 0, 0f), delay(1f))));
		page2Group.addActor(fan);

		// --------------MINER PAGE 3------------------------------
		GameObject miner_trace = new GameObject("miner-trace", false, true);
		miner_trace.setPositionXY(104, 82 - 20);
		miner_trace.folderName = "tutorial";
		page3Group.addActor(miner_trace);

		GameObject miner_egg1 = new GameObject("miner-egg1", "fan-egg", false);
		miner_egg1.setPositionXY(131, 135 - 20);
		miner_egg1.folderName = "tutorial";
		miner_egg1.getColor().a = 0;

		miner_egg1.addAction(forever(sequence(delay(0.75f), fadeIn(0f), delay(4.25f), fadeOut(0f))));

		page3Group.addActor(miner_egg1);

		GameObject miner_egg2 = new GameObject("miner-egg2", "fan-egg", false);
		miner_egg2.setPositionXY(169, 200 - 20);
		miner_egg2.folderName = "tutorial";
		miner_egg2.getColor().a = 0;

		miner_egg2.addAction(forever(sequence(delay(1.5f), fadeIn(0f), delay(3.5f), fadeOut(0f))));

		page3Group.addActor(miner_egg2);

		GameObject miner_egg3 = new GameObject("miner-egg3", "fan-egg", false);
		miner_egg3.setPositionXY(208, 262 - 20);
		miner_egg3.folderName = "tutorial";
		miner_egg3.getColor().a = 0;

		miner_egg3.addAction(forever(sequence(delay(2.25f), fadeIn(0f), delay(2.75f), fadeOut(0f))));

		page3Group.addActor(miner_egg3);

		GameObject miner_txt = new GameObject("miner-txt", false, true);
		miner_txt.setPositionXY(60, 400);
		miner_txt.folderName = "tutorial";
		page3Group.addActor(miner_txt);

		GameObject miner_name = new GameObject("miner-name", false, true);
		miner_name.setPositionXY(138, 565);
		miner_name.folderName = "tutorial";
		page3Group.addActor(miner_name);

		GameObject miner = new GameObject("miner", false, true);
		miner.setPositionXY(80, 46);
		miner.folderName = "tutorial";

		miner.addAction(forever(sequence(fadeIn(0f), moveTo(270, 348, 4f), fadeOut(0f), delay(1f), moveTo(80, 46, 0f))));
		page3Group.addActor(miner);

		GameObject miner_expl = new GameObject("explosion", false, true);
		miner_expl.setPositionXY(270, 348);
		miner_expl.folderName = "tutorial";
		miner_expl.getColor().a = 0;

		miner_expl.addAction(forever(sequence(delay(4.0f), fadeIn(0.5f), fadeOut(0.5f))));

		page3Group.addActor(miner_expl);

		// --------------BOOM PAGE 4------------------------------
		GameObject boom_trace = new GameObject("boom-trace", false, true);
		boom_trace.setPositionXY(145, 185 - 10);
		boom_trace.folderName = "tutorial";
		page4Group.addActor(boom_trace);

		// GameObject miner_txt = new GameObject("miner-txt", false, true);
		// miner_txt.setPositionXY(60, 435);
		// miner_txt.folderName = "tutorial";
		// page3Group.addActor(miner_txt);

		GameObject boom_txt = new GameObject("boom-txt", false, true);
		boom_txt.setPositionXY(60, 400);
		boom_txt.folderName = "tutorial";
		page4Group.addActor(boom_txt);

		GameObject boom_name = new GameObject("boom-name", false, true);
		boom_name.setPositionXY(138, 565);
		boom_name.folderName = "tutorial";
		page4Group.addActor(boom_name);

		GameObject boom = new GameObject("boom", false, true);
		boom.setPositionXY(100, 175);
		boom.folderName = "tutorial";
		boom.getColor().a = 0;

		boom.addAction(forever(sequence(fadeIn(0f), moveTo(150, 330, 0.5f), fadeOut(0f), moveTo(74, 84, 0f), delay(1.5f))));
		page4Group.addActor(boom);

		GameObject boom_expl = new GameObject("explosion-boom", "explosion",
				false);
		boom_expl.setPositionXY(150, 330);
		boom_expl.folderName = "tutorial";
		boom_expl.getColor().a = 0;

		boom_expl.addAction(forever(sequence(delay(0.5f), fadeIn(0.5f), delay(0.5f), fadeOut(0.5f))));

		page4Group.addActor(boom_expl);

		// --------------KARATE PAGE 5------------------------------
		GameObject karate_trace = new GameObject("karate-trace", false, true);
		karate_trace.setPositionXY(139, 172);
		karate_trace.folderName = "tutorial";
		page5Group.addActor(karate_trace);

		// GameObject miner_txt = new GameObject("miner-txt", false, true);
		// miner_txt.setPositionXY(60, 435);
		// miner_txt.folderName = "tutorial";
		// page5Group.addActor(miner_txt);

		GameObject karate_txt = new GameObject("karate-txt", false, true);
		karate_txt.setPositionXY(60, 400);
		karate_txt.folderName = "tutorial";
		page5Group.addActor(karate_txt);

		GameObject karate_name = new GameObject("karate-name", false, true);
		karate_name.setPositionXY(138, 565);
		karate_name.folderName = "tutorial";
		page5Group.addActor(karate_name);

		GameObject automatic = new GameObject("automatic", false, true);
		automatic.setPositionXY(210, 100);
		automatic.folderName = "tutorial";
		page5Group.addActor(automatic);

		GameObject karate_expl1 = new GameObject("explosion", false, true);
		karate_expl1.setPositionXY(109, 170);
		karate_expl1.folderName = "tutorial";
		karate_expl1.getColor().a = 0;

		RepeatAction explosion2 = forever(sequence(delay(0.5f),
				fadeIn(0.5f), fadeOut(0.5f), delay(0.5f)));
		karate_expl1.addAction(explosion2);
		page5Group.addActor(karate_expl1);

		GameObject karate_expl2 = new GameObject("explosion", false, true);
		karate_expl2.setPositionXY(160, 284);
		karate_expl2.folderName = "tutorial";
		karate_expl2.getColor().a = 0;

		RepeatAction explosion3 = forever(sequence(delay(1.5f),
				fadeIn(0.5f), fadeOut(0f)));
		karate_expl2.addAction(explosion3);

		page5Group.addActor(karate_expl2);

		GameObject karate = new GameObject("karate", false, true);
		karate.setPositionXY(75, 80);
		karate.folderName = "tutorial";
		RepeatAction karateMove = forever(sequence(moveTo(198, 365, 2f),
				moveTo(75, 80, 0f)));
		karate.addAction(karateMove);
		page5Group.addActor(karate);
		// ------------------------------------------------------------------------

		rootGroup.addActor(page1Group);
		rootGroup.addActor(page2Group);
		rootGroup.addActor(page3Group);
		rootGroup.addActor(page4Group);
		rootGroup.addActor(page5Group);

		GameObject taping_hand = new GameObject("taping_hand", false);
		taping_hand.skinName = "";
		rootGroup.addActor(taping_hand);

		GameObject tap = new GameObject("tap", false, true);
		tap.setPositionXY(253, 203);
		tap.setOriginX(42);
		tap.setOriginY(37);
		tap.getColor().a = 0.0f;
		tap.folderName = "tutorial";
		taping_hand.addActor(tap);

		ScaleToAction tap_scale1 = scaleTo(1.0f, 1.0f, 0.25f);
		ScaleToAction tap_scale2 = scaleTo(0, 0, 0.25f);
		AlphaAction tapFadeIn1 = fadeIn(0.25f);
		AlphaAction tapFadeOut1 = fadeOut(0.25f);

		//tapScaleSequence1 = sequence(delay(0.25f), tap_scale1, delay(2.0f), tap_scale2, delay(2.0f));
		//tapFadeSequence1 = sequence(delay(0.25f), tapFadeIn1, delay(2.0f), tapFadeOut1, delay(2.0f));

		GameObject hand = new GameObject("hand", false, true);
		hand.setPositionXY(278, 60);
		hand.getColor().a = 0.0f;
		hand.setOriginX(18);
		hand.setOriginY(174);
		hand.folderName = "tutorial";
		hand.setOnCreateFilter(true);
		taping_hand.addActor(hand);

		ScaleToAction hand_scale1 = scaleTo(1.5f, 1.5f, 0.5f);
		ScaleToAction hand_scale2 = scaleTo(1.0f, 1.0f, 0.5f);
		AlphaAction handFadeOut1 = fadeOut(0.25f);
		AlphaAction handFadeIn1 = fadeIn(0.25f);
		RotateToAction handRotate1 = rotateTo(-30, 0.5f);
		RotateToAction handRotate2 = rotateTo(0, 0.5f);

		//handScaleSequence1 = sequence(hand_scale2, delay(2.0f), hand_scale1, delay(1.75f));
		//handFadeSequence1 = sequence(handFadeIn1, delay(2.5f), handFadeOut1, delay(1.75f));
		//handRotateSequence1 = sequence(handRotate2, delay(2.0f), handRotate1, delay(1.75f));

		GameObject help = new GameObject("help", false, true);
		help.setPositionXY(150, 675);
		help.folderName = "tutorial";
		rootGroup.addActor(help);

		arrow_l = new TutorialButton("arrow", false, true, "left", this);
		arrow_l.setPositionXY(20, 350);
		arrow_l.setOriginX(55);
		arrow_l.setOriginY(43);
		arrow_l.folderName = "tutorial";
		arrow_l.setTouchable(Touchable.enabled);
		rootGroup.addActor(arrow_l);

		arrow_r = new TutorialButton("arrow", false, true, "right", this);
		arrow_r.setPositionXY(350, 350);
		arrow_r.setOriginX(55);
		arrow_r.setOriginY(43);
		arrow_r.setScaleX(-1);
		arrow_r.setTouchable(Touchable.enabled);
		arrow_r.folderName = "tutorial";
		rootGroup.addActor(arrow_r);

		TutorialButton close = new TutorialButton("close", false, true,
				"close", this);
		close.setPositionXY(410, 730);
		close.setTouchable(Touchable.enabled);
		close.folderName = "tutorial";
		rootGroup.addActor(close);

		GdxGame.beetleStage.addActor(rootGroup);

		page1Group.setVisible(false);
		page2Group.setVisible(false);
		page3Group.setVisible(false);
		page4Group.setVisible(false);
		page5Group.setVisible(false);

		arrow_l.setVisible(false);
		arrow_r.setVisible(false);
	}

	public TutorialScreen(GdxGame game) {
		initObjects(game);
	}

	public void setVisible(boolean v) {
		this.isVisible = v;
	}

	public boolean getVisible() {
		return this.isVisible;
	}

	private void playPage1Anim() {
		GameObject taping_hand = (GameObject) rootGroup
				.findActor("taping_hand");
		taping_hand.clearActions();
		taping_hand.setY(0);

		GameObject tap = (GameObject) rootGroup.findActor("tap");
		tap.clearActions();
		tap.setPositionXY(158, 293);
		tap.getColor().a = 0.0f;
		tap.setScaleX(0);
		tap.setScaleY(0);

		//tap.action(tapScaleSequence1.copy());
		//tap.action(tapFadeSequence1.copy());

		GameObject hand = (GameObject) rootGroup.findActor("hand");
		hand.clearActions();
		hand.setPositionXY(183, 150);
		hand.setScaleX(1.5f);
		hand.setScaleY(1.5f);
		hand.getColor().a = 0;
		hand.setRotation(-30);

		//hand.action(handScaleSequence1.copy());
		//hand.action(handFadeSequence1.copy());
		//hand.action(handRotateSequence1.copy());

		taping_hand.addAction(forever(sequence(delay(0.5f),
				moveTo(0, -200 * GdxGame.HD_X_RATIO, 1), delay(3.25f),
				moveTo(0, 0, 0))));
	}

	private void playPage2Anim() {
		GameObject taping_hand = (GameObject) rootGroup
				.findActor("taping_hand");
		taping_hand.clearActions();
		taping_hand.setY(0);

		GameObject tap = (GameObject) rootGroup.findActor("tap");
		tap.clearActions();
		tap.setPositionXY(303, 203);
		tap.getColor().a = 0.0f;
		tap.setScaleX(0);
		tap.setScaleY(0);

		//tap.action(tapScaleSequence1.copy());
		//tap.action(tapFadeSequence1.copy());

		GameObject hand = (GameObject) rootGroup.findActor("hand");
		hand.clearActions();
		hand.setPositionXY(328, 60);
		hand.setScaleX(1.5f);
		hand.setScaleY(1.5f);
		hand.getColor().a = 0;
		hand.setRotation(-30);

		//hand.action(handScaleSequence1.copy());
		//hand.action(handFadeSequence1.copy());
		//hand.action(handRotateSequence1.copy());
	}

	private void playPage3Anim() {
		GameObject taping_hand = (GameObject) rootGroup
				.findActor("taping_hand");
		taping_hand.clearActions();
		taping_hand.setY(0);

		GameObject tap = (GameObject) rootGroup.findActor("tap");
		tap.clearActions();
		tap.setPositionXY(303, 203);
		tap.getColor().a = 0.0f;
		tap.setScaleX(0);
		tap.setScaleY(0);

		//tap.action(tapScaleSequence1.copy());
		//tap.action(tapFadeSequence1.copy());

		GameObject hand = (GameObject) rootGroup.findActor("hand");
		hand.clearActions();
		hand.setPositionXY(328, 60);
		hand.setScaleX(1.5f);
		hand.setScaleY(1.5f);
		hand.getColor().a = 0;
		hand.setRotation(-30);

		//hand.action(handScaleSequence1.copy());
		//hand.action(handFadeSequence1.copy());
		//hand.action(handRotateSequence1.copy());
	}

	private void playPage4Anim() {
		GameObject taping_hand = (GameObject) rootGroup
				.findActor("taping_hand");
		taping_hand.clearActions();
		taping_hand.setY(0);

		GameObject tap = (GameObject) rootGroup.findActor("tap");
		tap.clearActions();
		tap.setPositionXY(303, 203);
		tap.getColor().a = 0.0f;
		tap.setScaleX(0);
		tap.setScaleY(0);

		// ScaleTo tap_scale1 = ScaleTo.$(1.0f, 1.0f, 0.25f);
		// ScaleTo tap_scale2 = ScaleTo.$(0, 0, 0.25f);
		// FadeIn tapFadeIn1 = FadeIn.$(0.25f);
		// FadeOut tapFadeOut1 = FadeOut.$(0.25f);
		//
		// tapScaleSequence1 = Sequence.$(Delay.$(2.0f), tap_scale1,
		// /* Delay.$(2.0f), */tap_scale2/* , Delay.$(2.0f) */);
		// tapFadeSequence1 = Sequence.$(Delay.$(2.0f), tapFadeIn1,
		// /* Delay.$(2.0f), */tapFadeOut1/* , Delay.$(2.0f) */);

		//tap.action((tapScaleSequence1.copy()));
		//tap.action((tapFadeSequence1.copy()));

		GameObject hand = (GameObject) rootGroup.findActor("hand");
		hand.clearActions();
		hand.setPositionXY(328, 60);
		hand.setScaleX(1.5f);
		hand.setScaleY(1.5f);
		hand.getColor().a = 0;
		hand.setRotation(-30);

		// ScaleTo hand_scale1 = ScaleTo.$(1.5f, 1.5f, 0.25f);
		// ScaleTo hand_scale2 = ScaleTo.$(1.0f, 1.0f, 0.25f);
		// FadeOut handFadeOut1 = FadeOut.$(0.25f);
		// FadeIn handFadeIn1 = FadeIn.$(0.25f);
		// RotateTo handRotate1 = RotateTo.$(-30, 0.25f);
		// RotateTo handRotate2 = RotateTo.$(0, 0.25f);
		//
		// handScaleSequence1 = Sequence.$(Delay.$(2.0f), hand_scale2,
		// /* Delay.$(2.0f), */hand_scale1/* , Delay.$(1.75f) */);
		// handFadeSequence1 = Sequence.$(Delay.$(2.0f), handFadeIn1, /*
		// * Delay. $
		// * (2.5f ),
		// */
		// handFadeOut1/* , Delay.$(1.75f) */);
		// handRotateSequence1 = Sequence.$(Delay.$(2.0f), handRotate2,
		// /* Delay.$(2.0f), */handRotate1/* , Delay.$(1.75f) */);

		//hand.action((handScaleSequence1.copy()));
		//hand.action((handFadeSequence1.copy()));
		//hand.action((handRotateSequence1.copy()));
	}

	private void playPage5Anim() {
		GameObject taping_hand = (GameObject) rootGroup
				.findActor("taping_hand");
		taping_hand.clearActions();
		taping_hand.setY(0);

		GameObject tap = (GameObject) rootGroup.findActor("tap");
		tap.clearActions();
		tap.setPositionXY(303, 203);
		tap.getColor().a = 0.0f;
		tap.setScaleX(0);
		tap.setScaleY(0);

		GameObject hand = (GameObject) rootGroup.findActor("hand");
		hand.clearActions();
		hand.setPositionXY(328, 60);
		hand.setScaleX(1.5f);
		hand.setScaleY(1.5f);
		hand.getColor().a = 0;
	}

	public void setPage(int p) {
		if (p > 5) {
			p = 1;
		}

		else if (p < 1) {
			p = 5;
		}

		if (this.currPage != p) {
			Group g = (GameObject) rootGroup.findActor("page" + this.currPage
					+ "Group");

			if (g != null) {
				g.setVisible(false);
			}

			currPage = p;

			g = (GameObject) rootGroup.findActor("page" + this.currPage
					+ "Group");
			g.setVisible(true);

			if (p == 1) {
				deltaCounter = 5;
				arrow_l.setVisible(false);
				arrow_r.setVisible(true);
			}

			else if (p == 2) {
				deltaCounter = 5;
				arrow_l.setVisible(true);
				arrow_r.setVisible(true);
			}

			else if (p == 3) {
				deltaCounter = 5;
				arrow_l.setVisible(true);
				arrow_r.setVisible(true);
			}

			else if (p == 4) {
				deltaCounter = 5;
				arrow_l.setVisible(true);
				arrow_r.setVisible(true);
			}

			else if (p == 5) {
				deltaCounter = 5;
				arrow_l.setVisible(true);
				arrow_r.setVisible(false);
			}

		}
	}

	private void resetFanAnimations() {
		GameObject fan_egg1 = (GameObject) rootGroup.findActor("fan-egg1");
		fan_egg1.clearActions();
		fan_egg1.getColor().a = 0;
		fan_egg1.setPositionXY(220, 250);
		fan_egg1.addAction(forever(sequence(delay(0.5f), fadeIn(0f), moveTo(158, 354, 1f), delay(2.5f), fadeOut(0f), moveTo(220, 250, 0f))));

		GameObject fan_egg2 = (GameObject) rootGroup.findActor("fan-egg2");
		fan_egg2.clearActions();
		fan_egg2.getColor().a = 0;
		fan_egg2.setPositionXY(220, 250);
		fan_egg2.addAction(forever(sequence(delay(0.5f), fadeIn(0f), moveTo(292, 354, 1f), delay(2.5f), fadeOut(0f), moveTo(220, 250, 0f))));

		GameObject fan_egg3 = (GameObject) rootGroup.findActor("fan-egg3");
		fan_egg3.clearActions();
		fan_egg3.getColor().a = 0;
		fan_egg3.setPositionXY(220, 250);
		fan_egg3.addAction(forever(sequence(delay(0.5f), fadeIn(0f), moveTo(226f, 390f, 1f), delay(2.5f), fadeOut(0f), moveTo(220, 250, 0f))));

		GameObject fan = (GameObject) rootGroup.findActor("fan");
		fan.clearActions();
		fan.getColor().a = 0;
		fan.setPositionXY(172, 0);
		fan.addAction(forever(sequence(fadeIn(0f), moveTo(172, 238, 0.5f), fadeOut(0f), delay(2.5f), moveTo(172, 0, 0f), delay(1f))));

		GameObject line1 = (GameObject) rootGroup.findActor("line1");
		line1.clearActions();
		line1.getColor().a = 0;
		line1.setPositionXY(240, 60);
		line1.addAction(forever(sequence(delay(0.0f), fadeIn(0f), delay(4f), fadeOut(0f))));

		GameObject line2 = (GameObject) rootGroup.findActor("line2");
		line2.clearActions();
		line2.getColor().a = 0;
		line2.setPositionXY(240, 95);
		line2.addAction(forever(sequence(delay(0.25f), fadeIn(0f), delay(3.75f), fadeOut(0f))));

		GameObject line3 = (GameObject) rootGroup.findActor("line3");
		line3.clearActions();
		line3.getColor().a = 0;
		line3.setPositionXY(240, 130);
		line3.addAction(forever(sequence(delay(0.5f), fadeIn(0f), delay(3.5f), fadeOut(0f))));

		GameObject line4 = (GameObject) rootGroup.findActor("line4");
		line4.clearActions();
		line4.getColor().a = 0;
		line4.setPositionXY(240, 165);
		line4.addAction(forever(sequence(delay(0.75f), fadeIn(0f), delay(3.25f), fadeOut(0f))));

		GameObject line5 = (GameObject) rootGroup.findActor("line5");
		line5.clearActions();
		line5.getColor().a = 0;
		line5.setPositionXY(240, 200);
		line5.addAction(forever(sequence(delay(1.0f), fadeIn(0f), delay(3.0f), fadeOut(0f))));

		GameObject line6 = (GameObject) rootGroup.findActor("line6");
		line6.clearActions();
		line6.getColor().a = 0;
		line6.setPositionXY(240, 235);
		line6.addAction(forever(sequence(delay(1.25f), fadeIn(0f), delay(2.75f), fadeOut(0f))));
	}

	private void resetMinerAnimations() {
		GameObject miner_egg1 = (GameObject) rootGroup.findActor("miner-egg1");
		miner_egg1.clearActions();
		miner_egg1.getColor().a = 0;
		miner_egg1.addAction(forever(sequence(delay(0.75f), fadeIn(0f), delay(4.25f), fadeOut(0f))));

		GameObject miner_egg2 = (GameObject) rootGroup.findActor("miner-egg2");
		miner_egg2.clearActions();
		miner_egg2.getColor().a = 0;
		miner_egg2.addAction(forever(sequence(delay(1.5f), fadeIn(0f), delay(3.5f), fadeOut(0f))));

		GameObject miner_egg3 = (GameObject) rootGroup.findActor("miner-egg3");
		miner_egg3.clearActions();
		miner_egg3.getColor().a = 0;
		miner_egg3.addAction(forever(sequence(delay(2.25f), fadeIn(0f), delay(2.75f), fadeOut(0f))));

		GameObject minner = (GameObject) rootGroup.findActor("miner");
		minner.clearActions();
		minner.getColor().a = 0;
		minner.setPositionXY(80, 46);
		minner.addAction(forever(sequence(fadeIn(0f), moveTo(270, 348, 4f), fadeOut(0f), delay(1f), moveTo(80, 46, 0f))));

		GameObject miner_expl = (GameObject) rootGroup.findActor("explosion");
		miner_expl.clearActions();
		miner_expl.setPositionXY(254, 332);
		miner_expl.folderName = "tutorial";
		miner_expl.getColor().a = 0;

		miner_expl.addAction(forever(sequence(delay(4.0f), fadeIn(0.5f), fadeOut(0.5f))));
	}

	private void resetBoomAnimations() {

		GameObject boom = (GameObject) rootGroup.findActor("boom");
		boom.clearActions();
		boom.getColor().a = 0;
		boom.setPositionXY(100, 175);
		boom.addAction(forever(sequence(fadeIn(0f), moveTo(150, 330, 0.5f), fadeOut(0f), moveTo(74, 84, 0f), delay(1.5f))));

		GameObject boom_expl = (GameObject) rootGroup
				.findActor("explosion-boom");
		boom_expl.clearActions();
		boom_expl.setPositionXY(150, 330);
		boom_expl.folderName = "tutorial";
		boom_expl.getColor().a = 0;

		boom_expl.addAction(forever(sequence(delay(0.5f), fadeIn(0.5f), delay(0.5f), fadeOut(0.5f))));
	}

	@Override
	public void render(float delta) {
		Gdx.app.log("x:y",
				Gdx.input.getX() - 45 + " : " + (800 - Gdx.input.getY() - 45));

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glViewport(GdxGame.START_X, GdxGame.START_Y,
				GdxGame.SCREEN_WIDTH, GdxGame.SCREEN_HEIGHT);

		if (!ImageCache.getManager().update()) {
			return;
		}

		GdxGame.camera.update();
		spriteBatch.setProjectionMatrix(GdxGame.camera.combined);

		spriteBatch.begin();
		rootGroup.draw(spriteBatch, 1);
		spriteBatch.end();

		rootGroup.act(delta);

		deltaCounter += delta;

		if (oldPage != currPage) {
			GameObject taping_hand = (GameObject) rootGroup
					.findActor("taping_hand");
			taping_hand.clearActions();
			taping_hand.getColor().a = 0;
			GameObject hand = (GameObject) rootGroup.findActor("hand");
			hand.clearActions();
			hand.getColor().a = 0;

			deltaCounter = 5;
			if (currPage == 3) {
				resetMinerAnimations();
				deltaCounter = 5 / 3;
				tapCounter = 0;
			}

		}

		if (deltaCounter >= 5 && currPage == 1) {
			deltaCounter = 0;
			playPage1Anim();
		} else if (deltaCounter >= 4 && currPage == 2) {
			deltaCounter = 0;
			//resetFanAnimations();
			playPage2Anim();
		} else if (deltaCounter >= 5 / 3 && currPage == 3) {
			tapCounter++;
			deltaCounter = 0;
			if ((tapCounter) > 0 && (tapCounter < 4)) {
				playPage3Anim();
			}
			if (tapCounter == 5) {
				tapCounter = 0;
			}
		} else if (deltaCounter >= 2f && currPage == 4) {
			deltaCounter = 0;
			resetBoomAnimations();
			playPage4Anim();
		} else if (deltaCounter >= 5 && currPage == 5) {
			deltaCounter = 0;
			playPage5Anim();
		}
		oldPage = currPage;

		if (Gdx.input.isKeyPressed(Keys.BACK)) {
			AudioPlayer.playSound("tap2");
			dispose();
		}

		/*
		 * if (Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)) {
		 * AudioPlayer.playSound("tap2"); dispose(); }
		 */
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void show() {
		this.setVisible(true);

		GdxGame.camera.position.set(GdxGame.VIRTUAL_GAME_WIDTH / 2,
				GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);
		rootGroup.setTouchable(Touchable.enabled);
		// this.game.startFlurry();
		this.game.flurryAgentOnPageView();

	}

	@Override
	public void hide() {
		this.setVisible(false);
		rootGroup.setTouchable(Touchable.disabled);
		// this.game.stopFlurry();
	}

	@Override
	public void pause() {
		this.game.stopFlurry();
	}

	@Override
	public void resume() {
		Texture.setAssetManager(ImageCache.getManager());
		this.game.startFlurry();
	}

	@Override
	public void dispose() {
		GdxGame.beetleStage.unfocusAll();

		if (!game.loadingScreen.getPrevScreenName().equals("menu")
				&& game.regularTutorial && game.fanTutorial
				&& game.minerTutorial && game.boomTutorial
				&& game.karateTutorial) {
			rootGroup.clear();
			GdxGame.beetleStage.getRoot().removeActor(rootGroup);
			ImageCache.unload("tutorial");
			this.game.tutorialScreen = null;
		}

		if (game.loadingScreen.getPrevScreenName().equals("stage")) {
			game.changeScreen("loading");
		} else if (game.loadingScreen.getPrevScreenName().equals("game")) {
			game.loadingScreen.setPrevScreenName("tutorial");
			game.setScreen(game.gameScreen);
		} else {
			game.setScreen(game.mainMenuScreen);
		}
	}

}
