/*GameScreen class also including CompleteScreen, Inventory*/
package com.workshop27.ballistabreaker.screens;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenPaths;
import aurelienribon.tweenengine.equations.Linear;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.constants.Texts;
import com.workshop27.ballistabreaker.engine.*;
import com.workshop27.ballistabreaker.engine.BBInventory.AvaliableBonuses;
import com.workshop27.ballistabreaker.engine.BBInventory.InvState;
import com.workshop27.ballistabreaker.engine.ShowTutorialObject.ArrowPosEnum;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle.BeetleState;
import com.workshop27.ballistabreaker.interfaces.AbstractBonusBeetle;
import com.workshop27.ballistabreaker.interfaces.AbstractBonusBeetle.BonusBeetleState;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class GameScreen implements Screen {
	// private final FPSLogger logger = new FPSLogger();
	public static ObjectToDestroy objectToDestroy;

	private final GdxGame game;

	public static BeetleManager bManager;

	public static GameObject particle_explosion;

	public final BackgroundObject background_night;
	public final BackgroundObject background_day;

	private final GameObject night_light;
	private final GameObject day_light;

	private final GameObject particle_dust;

	private final GameObjectAccessor bee0;
	private final GameObjectAccessor bee1;
	private final GameObjectAccessor bee2;

	private boolean isButtonsInited = false;
	private Boolean firsEnterFrame = true;

	private ArrayList<ArrayList<Integer>> beetleHolder;

	private static float mShakeTimer;
	public static float mShakeCounter;

	public static int mOffsetX = 0;
	public static int mOffsetY = 0;

	public static int levelNumb = 0;

	public static Group rootGroup;

	private final SpriteBatch spriteBatch;

	public static JarObject jar;
	public static GameObject jar_flies;

	private final GameObject yes_no_back;
	public final GameObject finish_game_back;
	private final InGameShopBtn buy_btn;
	private final InGameShopBtn no_btn;
	public final InGameShopBtn finish_level_btn;

	private HdCompatibleButton restart_btn;
	private HdCompatibleButton fast_finish_btn;
	private HdCompatibleButton pause_btn;

	private HdCompatibleButton pause_resume_btn;
	private HdCompatibleButton pause_restart_btn;
	private HdCompatibleButton pause_menu_btn;

	private HdCompatibleButton menu_btn;
	private HdCompatibleButton next_btn;
	private HdCompatibleButton retry_btn;

	BeetleButton regular_btn;
	BeetleButton fan_btn;
	BeetleButton miner_btn;
	BeetleButton boom_btn;
	BeetleButton karate_btn;

	private HdCompatibleButton soundBtn;
	private HdCompatibleButton musicBtn;
	private HdCompatibleButton vibroBtn;
	private HdCompatibleButton settingsBtn;
	private HdCompatibleButton scoreLoopBtn;
	private HdCompatibleButton shopBtn;
	public static String vibro;

	private static HdCompatibleButton mInvBtn1;
	private static TextGameObject mInvBtn1Txt;

	// private static GameObject level;

	// private static GameObject progress;

	public static boolean isLevelEnd;
	private boolean isBeetlesAndStarsCalculated = false;
	public static boolean isFirstRun = true;

	public static EndScreenProgressBar endScreenProgressBar;

	private static boolean gameOnPause = false;

	public static PopupBackground backgroundComplete;
	public static PopupBackground backgroundYesNo;
	public static GameObject pauseTxt;

	public static float reloadTimer;
	public static float pointsTimer;
	public static float showCompleteTimer = Constants.RELOADING_DELAY;
	
	public static GameObject trees;
	public static GameObject castle;

	private String currLevelMusic;

	private float pixelsDestroyed;

	private ArrayList<Integer> bonusBeetleHolder;

	public boolean isEndLevel;

	public static boolean isFirstEnter;

	public static int pointsForDestruction;

	public static boolean isEarlyCompletion;

	private boolean flagIsBeetleLoaded = false;

	private final String[] mShopList = { "shop-laser", "shop-expl",
			"shop-boom", "shop-karate", "shop-range", "shop-egg1", "shop-egg2",
			"shop-bundle" };
	private int failedScreensCounter = 0;

	private int mCurrShopPage = 0;
	private static boolean flagGameAutoPauseDissabled = false;

	private final ArrayList<AnimatedTextGameObject> pointsLblArray = new ArrayList<AnimatedTextGameObject>();

	private String rotateSoundName;

	// declaration CompleteScreen GameObjects
	private final GameObject shop_txt;
	public static GameObject complete;
	public static GameObject failed;
	private static GameObject min;
	private final GameObject ingameShopContainer;

	private final GameObject bigRope;

	private HdCompatibleButton shop_left;
	private HdCompatibleButton shop_right;
	private HdCompatibleButton facebook;
	private HdCompatibleButton twitter;

	private static TextGameObject currentScore;
	private static TextGameObject levelNumber;
	private TextGameObject bestScore;

	private String message;

	Map<String, String> params = new HashMap<String, String>();

	private boolean isNeedToDrawLaser;

	public static boolean isGamePaused() {
		return gameOnPause;
	}

	public static void setGamePause(boolean b) {
		gameOnPause = b;
	}

	public void startLevel(int level) {
		try {
			vibro = GdxGame.files.getOptionsAndProgress("options", "vibro");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		GameScreen.levelNumb = level;

		if (objectToDestroy == null) {
			objectToDestroy = new ObjectToDestroy("object_to_destroy"
					+ levelNumb, bigRope);
			rootGroup.addActorBefore(particle_explosion, objectToDestroy);
		} else {
			objectToDestroy.reInit();
		}

		setGameVariablesToDefault();
	}

	private void setGameVariablesToDefault() {
		isBeetlesAndStarsCalculated = false;
		pointsForDestruction = 0;
		pixelsDestroyed = 0;
		bManager.pixelsLabelValue = 0;
		isEndLevel = false;
		BeetleManager.loadingCounter = 0;
		isEarlyCompletion = false;
		calcPoints(false);
	}

	public void initBeetles(int level2) {
		bManager.removeAllBeetles();

		if (bManager.beetleList.isEmpty()) {
			beetleHolder = new ArrayList<ArrayList<Integer>>();
			String beetles = null;
			try {
				beetles = GdxGame.files
						.getScenario("beetles", "level" + level2);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Scanner s = new Scanner(beetles).useDelimiter("-");
			int r = s.nextInt();
			if (regular_btn != null) {
				regular_btn.setBeetleTypeTotal(r);
			}
			ArrayList<Integer> beetlesbonus1Params = new ArrayList<Integer>();
			beetlesbonus1Params.add(r);
			r = s.nextInt();
			beetlesbonus1Params.add(r);
			beetleHolder.add(beetlesbonus1Params);

			r = s.nextInt();
			if (fan_btn != null) {
				fan_btn.setBeetleTypeTotal(r);
			}

			ArrayList<Integer> beetlesbonus2Params = new ArrayList<Integer>();
			beetlesbonus2Params.add(r);
			r = s.nextInt();
			beetlesbonus2Params.add(r);
			beetleHolder.add(beetlesbonus2Params);

			r = s.nextInt();
			if (miner_btn != null) {
				miner_btn.setBeetleTypeTotal(r);
			}

			ArrayList<Integer> beetlesbonus3Params = new ArrayList<Integer>();
			beetlesbonus3Params.add(r);
			r = s.nextInt();
			beetlesbonus3Params.add(r);
			beetleHolder.add(beetlesbonus3Params);

			r = s.nextInt();
			if (boom_btn != null) {
				boom_btn.setBeetleTypeTotal(r);
			}

			ArrayList<Integer> beetlesbonus4Params = new ArrayList<Integer>();
			beetlesbonus4Params.add(r);
			r = s.nextInt();
			beetlesbonus4Params.add(r);
			beetleHolder.add(beetlesbonus4Params);

			r = s.nextInt();
			if (karate_btn != null) {
				karate_btn.setBeetleTypeTotal(r);
			}

			ArrayList<Integer> beetlesbonus5Params = new ArrayList<Integer>();
			beetlesbonus5Params.add(r);
			r = s.nextInt();
			beetlesbonus5Params.add(r);
			beetleHolder.add(beetlesbonus5Params);

			HashMap<String, ArrayList<Integer>> bManageParams = new HashMap<String, ArrayList<Integer>>();

			bManageParams.put("Regular", beetleHolder.get(0));
			bManageParams.put("Fan", beetleHolder.get(1));
			bManageParams.put("Minner", beetleHolder.get(2));
			bManageParams.put("Boom", beetleHolder.get(3));
			bManageParams.put("Karate", beetleHolder.get(4));

			bManager.addBeetles(this, bManageParams);
		}
	}

	public void initBonusBeetles(int level2) {
		bManager.removeAllBonusBeetles();
		if (bManager.bonusBeetleList.isEmpty()) {
			bonusBeetleHolder = new ArrayList<Integer>();
			// String beetles = GdxGame.options.get("beetles", "level" +
			// level2);
			// Scanner s = new Scanner(beetles).useDelimiter("-");
			// int r = s.nextInt();
			bonusBeetleHolder.add(1);
			// if (regular_btn != null) {
			// regular_btn.setBeetleTypeTotal(1);
			// }
			// r = s.nextInt();
			bonusBeetleHolder.add(1);
			// if (fan_btn != null) {
			// fan_btn.setBeetleTypeTotal(1);
			// }
			// r = s.nextInt();
			bonusBeetleHolder.add(1);
			// if (miner_btn != null) {
			// miner_btn.setBeetleTypeTotal(1);
			// }
			bonusBeetleHolder.add(1);
			bonusBeetleHolder.add(1);

			HashMap<String, Integer> bManageParams = new HashMap<String, Integer>();

			bManageParams.put("Regular", bonusBeetleHolder.get(0));
			bManageParams.put("Fan", bonusBeetleHolder.get(1));
			bManageParams.put("Minner", bonusBeetleHolder.get(2));
			bManageParams.put("Boom", bonusBeetleHolder.get(3));
			bManageParams.put("Karate", bonusBeetleHolder.get(4));

			bManager.addBonusBeetles(this, bManageParams, level2);
			bManager.addBonusStars(this, level2);
		}
	}

	public void addToRootGroup(Actor a) {
		rootGroup.addActorBefore(backgroundComplete, a);
	}

	public GameScreen(GdxGame game) {
		spriteBatch = GdxGame.beetleStage.getSpriteBatch();

		this.game = game;

		rootGroup = new Group();
		rootGroup.setName("gameGroup");
		rootGroup.setTouchable(Touchable.disabled);

		background_night = new BackgroundObject("background-night", false, false);
		background_night.setPositionXY(0, 0);
		background_night.folderName = "background1";
		background_night.setVisible(false);
		rootGroup.addActor(background_night);

		background_day = new BackgroundObject("background", false, false);
		background_day.setPositionXY(0, 0);
		background_day.folderName = "main";
		//background_day.setVisible(false);
		rootGroup.addActor(background_day);

		castle = new GameObject("castle", false, true);
		castle.setPositionXY(0, 0);
		castle.folderName = "main";
		rootGroup.addActor(castle);

		trees = new GameObject("trees", false, true);
		trees.setPositionXY(0, 200);
		trees.folderName = "main";
		rootGroup.addActor(trees);
		
		GameObject cloud1 = new GameObject("cloud1", false, true);
		cloud1.setPositionXY(480, 388);
		cloud1.folderName = "main";
		rootGroup.addActor(cloud1);
		
		cloud1.clearActions();
		MoveToAction m1 = moveTo(-362, cloud1.getPositionY(), 20);		
		MoveToAction m2 = moveTo(480, cloud1.getPositionY(), 0);
		cloud1.addAction(forever(sequence(m1, m2)));
		
		GameObject cloud_shadow1 = new GameObject("cloud-shadow1", false, true);
		cloud_shadow1.setPositionXY(480, 110);
		cloud_shadow1.folderName = "main";
		rootGroup.addActor(cloud_shadow1);

		cloud_shadow1.clearActions();
		MoveToAction m3 = moveTo(-362, cloud_shadow1.getPositionY(), 20);		
		MoveToAction m4 = moveTo(480, cloud_shadow1.getPositionY(), 0);
		cloud_shadow1.addAction(forever(sequence(m3, m4)));
		
		night_light = new GameObject("light", false, true);
		night_light.setPositionXY(-26, 120);
		night_light.setOriginX(265);
		night_light.setOriginY(720);
		night_light.setWidth(532);
		night_light.setHeight(686);
		night_light.setOnCreateFilter(true);
		night_light.folderName = "background1";

		RotateToAction night_light_rotate1 = rotateTo(3, 3);
		RotateToAction night_light_rotate2 = rotateTo(-3, 3);
		night_light.addAction(forever(sequence(night_light_rotate1,
				night_light_rotate2)));

		//rootGroup.addActor(night_light);

		day_light = new GameObject("light", false, true);
		day_light.getColor().a = 0.8f;
		day_light.setPositionXY(10, 230);
		day_light.setOriginX(529);
		day_light.setOriginY(652);
		day_light.setOnCreateFilter(true);
		day_light.folderName = "background2";

		RotateToAction day_light_rotate1 = rotateTo(2, 5);
		RotateToAction day_light_rotate2 = rotateTo(-2, 5);
		day_light.addAction(forever(sequence(day_light_rotate1,
				day_light_rotate2)));

		//rootGroup.addActor(day_light);

		particle_dust = new GameObject("particle_dust", false);
		//particle_dust.setOriginX(240);
		//particle_dust.setOriginY(650);
		//particle_dust.skinName = "";
		//particle_dust.createParticle("p3", true);
		//rootGroup.addActor(particle_dust);

		jar = new JarObject("main-jar", "prog-back", "prog-jam",
				"prog-ellipse", "prog-cover", "prog-mus", "star1", "star2",
				"star3");
		jar.setPositionXY(0, 46);
		jar.folderName = "main";
		jar.resetTxtFields();
		rootGroup.addActor(jar);

		particle_explosion = new GameObject("particle_explosion", false);
		particle_explosion.skinName = "";
		particle_explosion.getColor().a = 0;
		particle_explosion.createParticle("explosion1", true);
		particle_explosion.createParticle("explosion2", true);
		particle_explosion.createParticle("explosion3", true);
		rootGroup.addActor(particle_explosion);

		/*
		 * jar_flies = new GameObject("jar_flies", false);
		 * jar_flies.setPositionXY(75, 205); jar_flies.createParticle("flies",
		 * true); rootGroup.addActor(jar_flies);
		 */
		bee0 = new GameObjectAccessor("bee0", "bee", false);
		bee0.setPositionXY(161, 793);
		bee0.folderName = "particles";
		bee0.setOriginX(12);
		bee0.setOriginY(12);
		bee0.setScaleX(0.25f * GdxGame.HD_X_RATIO);
		bee0.setScaleY(0.25f * GdxGame.HD_Y_RATIO);
		bee0.getColor().a = 0.7f;
		rootGroup.addActor(bee0);

		Tween.to(bee0, GameObjectAccessor.POSITION_XY, 2.0f).ease(Linear.INOUT)
				.waypoint(216, 765).waypoint(270, 794).waypoint(254, 747)
				.waypoint(166, 754).target(161, 793)
				.path(TweenPaths.catmullRom).repeat(Tween.INFINITY, 0)
				.start(GdxGame.tweenManager);

		bee1 = new GameObjectAccessor("bee1", "bee", false);
		bee1.setPositionXY(275, 790);
		bee1.folderName = "particles";
		bee1.setOriginX(12);
		bee1.setOriginY(12);
		bee1.setScaleX(0.25f * GdxGame.HD_X_RATIO);
		bee1.setScaleY(0.25f * GdxGame.HD_Y_RATIO);
		bee1.getColor().a = 0.7f;
		rootGroup.addActor(bee1);

		Tween.to(bee1, GameObjectAccessor.POSITION_XY, 1.5f).ease(Linear.INOUT)
				.waypoint(193, 765).waypoint(220, 790).waypoint(252, 750)
				.waypoint(289, 767).target(275, 790)
				.path(TweenPaths.catmullRom).repeat(Tween.INFINITY, 0)
				.start(GdxGame.tweenManager);

		bee2 = new GameObjectAccessor("bee2", "bee", false);
		bee2.setPositionXY(193, 762);
		bee2.folderName = "particles";
		bee2.setOriginX(12);
		bee2.setOriginY(12);
		bee2.setScaleX(0.25f * GdxGame.HD_X_RATIO);
		bee2.setScaleY(0.25f * GdxGame.HD_Y_RATIO);
		bee2.getColor().a = 0.7f;
		rootGroup.addActor(bee2);

		Tween.to(bee2, GameObjectAccessor.POSITION_XY, 1.25f)
				.ease(Linear.INOUT).waypoint(243, 752).waypoint(271, 780)
				.waypoint(229, 776).waypoint(202, 796).target(193, 762)
				.path(TweenPaths.catmullRom).repeat(Tween.INFINITY, 0)
				.start(GdxGame.tweenManager);

		bigRope = new GameObject("big-rope", "big-rope", false);
		bigRope.folderName = "main";
		bigRope.loadSkinOnDraw = true;
		bigRope.setPositionX(210);
		bigRope.setPositionY(720);
		bigRope.setOnCreateFilter(true);
		rootGroup.addActor(bigRope);

		backgroundComplete = new PopupBackground("background2", false, true);
		backgroundComplete.setPositionXY(0, 0);
		backgroundComplete.setWidth(GdxGame.VIRTUAL_GAME_WIDTH);
		backgroundComplete.setHeight(GdxGame.VIRTUAL_GAME_HEIGHT);
		backgroundComplete.folderName = "main";
		rootGroup.addActor(backgroundComplete);

		pauseTxt = new GameObject("paused-txt", false, true);
		pauseTxt.setPositionXY(94, 667);
		rootGroup.addActor(pauseTxt);

		endScreenProgressBar = new EndScreenProgressBar("endScreenProgressBar",
				new GameObject("completeStar1"),
				new GameObject("completeStar2"),
				new GameObject("completeStar3"), new GameObject(
						"completeMustache"));
		endScreenProgressBar.setPositionXY(149, 295);
		endScreenProgressBar.setScaleX(1.3f);
		endScreenProgressBar.setScaleY(1.3f);
		rootGroup.addActor(endScreenProgressBar);

		// level = new GameObject("level", false, true);
		// level.setPositionXY(157, 622);
		// level.folderName = "main";
		// level.loadSkinOnDraw = true;
		// rootGroup.addActor(level);

		// progress = new GameObject("progress", false, true);
		// progress.setPositionXY(53, 530);
		// progress.folderName = "main";
		// progress.loadSkinOnDraw = true;
		// rootGroup.addActor(progress);

		// --------------LOSE SHOP-----------------------------
		ingameShopContainer = new GameObject("ingame-shop-container", false,
				true);
		ingameShopContainer.skinName = "";
		ingameShopContainer.setPositionXY(68, 320);
		ingameShopContainer.setVisible(false);
		ingameShopContainer.folderName = "main";
		rootGroup.addActor(ingameShopContainer);

		GameObject shop_back = new GameObject("shop-back", false, true);
		shop_back.setPositionXY(0, 0);
		shop_back.folderName = "main";
		shop_back.loadSkinOnDraw = true;
		ingameShopContainer.addActor(shop_back);

		InGameShopBtn shop_laser = new InGameShopBtn("shop-laser", false, true,
				this.game, this);
		shop_laser.setPositionXY(172, 400);
		shop_laser.folderName = "main";
		shop_laser.loadSkinOnDraw = true;
		shop_laser.setTouchable(Touchable.enabled);
		shop_laser.setVisible(false);
		rootGroup.addActor(shop_laser);

		InGameShopBtn shop_boom = new InGameShopBtn("shop-boom", false, true,
				this.game, this);
		shop_boom.setPositionXY(175, 399);
		shop_boom.folderName = "main";
		shop_boom.loadSkinOnDraw = true;
		shop_boom.setTouchable(Touchable.enabled);
		shop_boom.setVisible(false);
		rootGroup.addActor(shop_boom);

		InGameShopBtn shop_karate = new InGameShopBtn("shop-karate", false,
				true, this.game, this);
		shop_karate.setPositionXY(193, 396);
		shop_karate.folderName = "main";
		shop_karate.loadSkinOnDraw = true;
		shop_karate.setTouchable(Touchable.enabled);
		shop_karate.setVisible(false);
		rootGroup.addActor(shop_karate);

		InGameShopBtn shop_expl = new InGameShopBtn("shop-expl", false, true,
				this.game, this);
		shop_expl.setPositionXY(194, 400);
		shop_expl.folderName = "main";
		shop_expl.loadSkinOnDraw = true;
		shop_expl.setVisible(false);
		shop_expl.setTouchable(Touchable.enabled);
		rootGroup.addActor(shop_expl);

		InGameShopBtn shop_range = new InGameShopBtn("shop-range", false, true,
				this.game, this);
		shop_range.setPositionXY(202, 400);
		shop_range.folderName = "main";
		shop_range.loadSkinOnDraw = true;
		shop_range.setVisible(false);
		shop_range.setTouchable(Touchable.enabled);
		rootGroup.addActor(shop_range);

		InGameShopBtn shop_egg1 = new InGameShopBtn("shop-egg1", false, true,
				this.game, this);
		shop_egg1.setPositionXY(200, 400);
		shop_egg1.folderName = "main";
		shop_egg1.loadSkinOnDraw = true;
		shop_egg1.setVisible(false);
		shop_egg1.setTouchable(Touchable.enabled);
		rootGroup.addActor(shop_egg1);

		InGameShopBtn shop_egg2 = new InGameShopBtn("shop-egg2", false, true,
				this.game, this);
		shop_egg2.setPositionXY(200, 400);
		shop_egg2.folderName = "main";
		shop_egg2.loadSkinOnDraw = true;
		shop_egg2.setVisible(false);
		shop_egg2.setTouchable(Touchable.enabled);
		rootGroup.addActor(shop_egg2);

		InGameShopBtn shop_bundle = new InGameShopBtn("shop-bundle", false,
				true, this.game, this);
		shop_bundle.setPositionXY(170, 400);
		shop_bundle.folderName = "main";
		shop_bundle.loadSkinOnDraw = true;
		shop_bundle.setVisible(false);
		shop_bundle.setTouchable(Touchable.enabled);
		rootGroup.addActor(shop_bundle);

		// ----------------------------------------------------

		// ---------------------COMPLETE SCREEN BOX-------------------------

		int yh = 50;

		shop_txt = new GameObject("shop-txt", false, true);
		shop_txt.setPositionXY(98, 310 - yh);
		shop_txt.folderName = "main";
		shop_txt.setVisible(false);
		shop_txt.loadSkinOnDraw = true;
		rootGroup.addActor(shop_txt);

		complete = new GameObject("complete", false, true);
		complete.setPositionXY(40, 677 - yh);
		complete.folderName = "main";
		complete.loadSkinOnDraw = true;
		rootGroup.addActor(complete);

		failed = new GameObject("failed", false, true);
		failed.setPositionXY(110, 707 - yh);
		failed.folderName = "main";
		failed.loadSkinOnDraw = true;
		rootGroup.addActor(failed);

		min = new GameObject("min", false, true);
		min.setPositionXY(140, 300 - yh);
		min.folderName = "main";
		min.loadSkinOnDraw = true;
		rootGroup.addActor(min);
		// ----------------------------------------------------

		// ---------------------FINISH GAME BOX-------------------------
		finish_game_back = new GameObject("yes_no_back", false, true);
		finish_game_back.skinName = "txt-back";
		finish_game_back.setPositionXY(48, 440);
		finish_game_back.folderName = "main";
		finish_game_back.loadSkinOnDraw = true;
		finish_game_back.setVisible(false);
		rootGroup.addActor(finish_game_back);

		AnimatedTextGameObject text_lose_game_help = new AnimatedTextGameObject(
				"text-lose-game-help", false, "tutorial_white_bold", 30);
		text_lose_game_help.setTextCenteredOnPosition(Texts.LOOSE_HELP_TEXT,
				200, 190, 330, false);
		text_lose_game_help.setColor(0.26f, 0.125f, 0.016f, 1.0f);
		finish_game_back.addActor(text_lose_game_help);

		finish_level_btn = new InGameShopBtn("finish-level-btn", false, true,
				this.game, this);
		finish_level_btn.skinName = "buy-btn";
		finish_level_btn.setPositionXY(195, 473);
		finish_level_btn.folderName = "main";
		finish_level_btn.setTouchable(Touchable.enabled);
		finish_level_btn.setVisible(false);
		finish_level_btn.loadSkinOnDraw = true;
		rootGroup.addActor(finish_level_btn);
		// ----------------------------------------------------

		// ------------------SHOP YES/NO BOX-------------------
		backgroundYesNo = new PopupBackground("background2", false, true);
		backgroundYesNo.setPositionXY(0, 0);
		backgroundYesNo.setWidth(GdxGame.VIRTUAL_GAME_WIDTH);
		backgroundYesNo.setHeight(GdxGame.VIRTUAL_GAME_HEIGHT);
		backgroundYesNo.folderName = "main";
		backgroundYesNo.setVisible(false);
		rootGroup.addActor(backgroundYesNo);

		yes_no_back = new GameObject("yes_no_back", false, true);
		yes_no_back.skinName = "txt-back";
		yes_no_back.setPositionXY(48, 290);
		yes_no_back.folderName = "main";
		yes_no_back.loadSkinOnDraw = true;
		yes_no_back.setVisible(false);
		rootGroup.addActor(yes_no_back);

		AnimatedTextGameObject _99price = new AnimatedTextGameObject(
				"_99price", false, "tutorial", 45);
		_99price.setTextCenteredOnPosition(Texts.SHOP_TEXT_PRICE_BLASTER, 200,
				80, 480, false);
		_99price.setColor(0.2f, 0.65f, 0.2f, 1.0f);
		yes_no_back.addActor(_99price);

		AnimatedTextGameObject text1 = new AnimatedTextGameObject(
				"text-yes-no", false, "popup", 20);
		text1.setTextCenteredOnPosition(Texts.SHOP_TEXT_BUNDLE, 200, 180, 330,
				false);
		text1.setColor(0.31f, 0.2f, 0.1f, 1.0f);
		yes_no_back.addActor(text1);

		AnimatedTextGameObject name1 = new AnimatedTextGameObject(
				"name-yes-no", false, "tutorial", 45);
		name1.setTextCenteredOnPosition(Texts.SHOP_TEXT_NAME_RADIUS, 200, 250,
				480, false);
		name1.setColor(0.75f, 0.15f, 0.15f, 1.0f);
		yes_no_back.addActor(name1);

		buy_btn = new InGameShopBtn("buy-btn", false, true, this.game, this);
		buy_btn.skinName = "buy-btn";
		buy_btn.setPositionXY(76, 323);
		buy_btn.folderName = "main";
		buy_btn.setTouchable(Touchable.enabled);
		buy_btn.setVisible(false);
		buy_btn.loadSkinOnDraw = true;
		rootGroup.addActor(buy_btn);

		no_btn = new InGameShopBtn("no-btn", false, true, this.game, this);
		no_btn.skinName = "no-btn";
		no_btn.setPositionXY(320, 330);
		no_btn.folderName = "main";
		no_btn.setTouchable(Touchable.enabled);
		no_btn.setVisible(false);
		no_btn.loadSkinOnDraw = true;
		rootGroup.addActor(no_btn);

		// -------------------------------------------------------------

		Slingshot.init(this);

		bManager = new BeetleManager();
		bManager.game = game;

		GdxGame.mInventory.setBManager(bManager);

		GdxGame.beetleStage.addActor(rootGroup);
	}

	private void InitButtons() {
		if (mInvBtn1 == null) {
			mInvBtn1 = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("laser-sight")));
			mInvBtn1.setPositionXY(125, 440);
			mInvBtn1.setVisible(false);
			rootGroup.addActor(mInvBtn1);

			mInvBtn1.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
                    AudioPlayer.playSound("tap3");
				}
			});
		} else {
			mInvBtn1.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("laser-sight"));
		}

		mInvBtn1Txt = new TextGameObject("mInvBtn1Txt", false, 60);
		mInvBtn1Txt.setPositionXY(275, 500);
		mInvBtn1Txt.setVisible(false);
		rootGroup.addActor(mInvBtn1Txt);

		levelNumber = new TextGameObject("levelNumber", false, 30);
		levelNumber.setPositionXY(70, 640);
		rootGroup.addActorBefore(backgroundYesNo, levelNumber);

		GdxGame.points.setPositionXY(180, 760);
		rootGroup.addActorBefore(backgroundComplete, GdxGame.points);

		//rootGroup.addActorBefore(backgroundComplete, GdxGame.level_numb);

		currentScore = new TextGameObject("currentScore", false, 30);
		rootGroup.addActorBefore(yes_no_back, currentScore);

		bestScore = new TextGameObject("bestScore", false, 30);
		rootGroup.addActor(bestScore);

		// -----------------PAUSE SCREEN-------------------------------
		if (pause_resume_btn == null) {
			pause_resume_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("resume-btn")));
			pause_resume_btn.setPositionXY(147, 566);
			rootGroup.addActor(pause_resume_btn);

			pause_resume_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					hidePauseScreen();
					AudioPlayer.playSound("tap3");
				}
			});
		} else {
			pause_resume_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("resume-btn"));
		}

		if (pause_restart_btn == null) {
			pause_restart_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("restart-btn")));
			pause_restart_btn.setPositionXY(147, 467);
			rootGroup.addActor(pause_restart_btn);

			pause_restart_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					if (bManager.getActiveBeetle() == null) {
						return;
					}

					if (bManager.getActiveBeetle().getState() != BeetleState.ACTIVE
							&& bManager.getActiveBeetle().getState() != BeetleState.ANIM_TO_FLY
							&& bManager.getActiveBeetle().getState() != BeetleState.BREAKING
							&& bManager.getActiveBeetle().getState() != BeetleState.INFLY) {
						return;
					}

					game.flurryAgentLogEvent("pause_restart_btn");

					AudioPlayer.playSound("tap3");
					// reloadTimer += 1;
					bManager.restart();
					objectToDestroy.restart();
					isFirstRun = true;
					jar.setProgress(0);
					hidePauseScreen();
					setGameVariablesToDefault();
				}
			});
		} else {
			pause_restart_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("restart-btn"));
		}

		if (pause_menu_btn == null) {
			pause_menu_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("menu")));
			pause_menu_btn.setPositionXY(147, 367);
			rootGroup.addActor(pause_menu_btn);

			pause_menu_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					hidePauseScreen();
					AudioPlayer.playSound("tap3");
					dispose();
					game.loadingScreen.setNextScreen("stage");
					game.loadingScreen.setPrevScreenName("game");
					game.changeScreen("loading");
				}
			});
		} else {
			pause_menu_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("menu"));
		}

		// -------------------WIN SCREEN---------------------------------------
		if (menu_btn == null) {
			menu_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("menu")));
			menu_btn.setPositionXY(290, 50);
			rootGroup.addActorBefore(backgroundYesNo, menu_btn);

			menu_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					dispose();
					game.loadingScreen.setNextScreen("stage");
					game.loadingScreen.setPrevScreenName("game");
					game.changeScreen("loading");
					hideCompleteScreen();
					AudioPlayer.playSound("tap3");
				}
			});
		} else {
			menu_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("menu"));
		}

		if (next_btn == null) {
			next_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("next")));
			next_btn.setPositionXY(160, 145);
			rootGroup.addActorBefore(backgroundYesNo, next_btn);

			next_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					final int level = levelNumb + 1;

					if (level >= 25 && level <= 48) {
						GdxGame.lastPosition = GdxGame.VIRTUAL_GAME_WIDTH
								+ GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;
						GdxGame.stageNumber = 2;
					} else if (level >= 49) {
						GdxGame.lastPosition = 2 * GdxGame.VIRTUAL_GAME_WIDTH
								+ GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;
						GdxGame.stageNumber = 3;
					}

					if (level <= Constants.LEVELS_COUNT) {
						// LeafObject leaf = (LeafObject) StageScreen.rootGroup
						// .findActor("level" + level + "-btn");
						game.loadingScreen.setNextScreen("game", level);
					} else {
						// LeafObject leaf = (LeafObject) StageScreen.rootGroup
						// .findActor("level" + 1 + "-btn");
						game.loadingScreen.setNextScreen("game", 1);
					}

					game.loadingScreen.setNextScreen("game");
					game.loadingScreen.setPrevScreenName("game");

					game.changeScreen("loading");
					hideCompleteScreen();
					AudioPlayer.playSound("tap3");

					bManager.pixelsLabelValue = 0;
				}
			});
		} else {
			next_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("next"));
		}

		if (retry_btn == null) {
			retry_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("retry")));
			retry_btn.setPositionXY(10, 50);
			rootGroup.addActorBefore(backgroundYesNo, retry_btn);

			retry_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					retryLevel();
				}
			});
		} else {
			retry_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("retry"));
		}
		// menu_btn.visible = false;
		// next_btn.visible = false;
		// retry_btn.visible = false;

		// ---------------RESTART-----------------------
		if (restart_btn == null) {
			restart_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("refresh")));
			restart_btn.setPositionXY(410, 725);
			rootGroup.addActorBefore(backgroundComplete, restart_btn);

			restart_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);

					if (bManager.getActiveBeetle() == null) {
						return;
					}

					for (final AbstractBonusBeetle bonusBeetle : bManager
							.getBonusesList()) {
						if (bonusBeetle.getActiveState() == BonusBeetleState.INFLY) {
							return;
						}
					}

					if (bManager.getActiveBeetle().getState() != BeetleState.ACTIVE
							&& bManager.getActiveBeetle().getState() != BeetleState.ANIM_TO_FLY
							&& bManager.getActiveBeetle().getState() != BeetleState.BREAKING
							&& bManager.getActiveBeetle().getState() != BeetleState.INFLY) {
						return;
					}

					game.flurryAgentLogEvent("restart_btn");

					// reloadTimer += 1;
					bManager.restart();
					objectToDestroy.restart();
					isFirstRun = true;

					jar.setProgress(0);
					// level_destruction_lbl.setText(" 0%");
					AudioPlayer.playSound("tap3");
					setGameVariablesToDefault();

					// showInventory();
				}
			});
		} else {
			restart_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("refresh"));
		}

		// ---------------FAST FINISH-----------------------
		if (fast_finish_btn == null) {
			fast_finish_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-complete")));
			fast_finish_btn.setPositionXY(387, 724);
			fast_finish_btn.setVisible(false);
			rootGroup.addActorBefore(backgroundComplete, fast_finish_btn);

			fast_finish_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					game.flurryAgentLogEvent("fast_finish_btn");
					pointsForDestruction += (JarObject.getStars() * 250);

					showCompleteScreen(JarObject.getStars());

					isLevelEnd = false;
				}
			});
		} else {
			fast_finish_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("btn-complete"));
		}

		// ---------------PAUSE-----------------------
		if (pause_btn == null) {
			pause_btn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("menu-btn")));

			pause_btn.setPositionXY(20, 735);
			rootGroup.addActorBefore(backgroundComplete, pause_btn);

			pause_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					if (showCompleteTimer == 0) {
						showPauseScreen();
						AudioPlayer.playSound("tap3");
					}

				}
			});
		} else {
			pause_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("menu-btn"));
		}

		// ---------------REGULAR BEETLE-----------------------
		if (regular_btn == null) {
			regular_btn = new BeetleButton(new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("btn-regular")), new TextureRegionDrawable (ImageCache.getManager().get("data/atlas/main/pack", TextureAtlas.class).findRegion("btn-regular")), beetleHolder.get(0).get(0));
			regular_btn.beetleName = "regular_btn";
			regular_btn.setPositionXY(400, 124);
			regular_btn.gameScreen = this;

			rootGroup.addActorBefore(backgroundComplete, regular_btn);
			bManager.regularBtn = regular_btn;

			regular_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					if (bManager.getActiveBeetle() != null) {
						bManager.onRegularButtonClicked();
					}
				}
			});
		} else {
			regular_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-regular"));
			regular_btn.getStyle().down = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-regular"));
		}
		// ---------------FAN BEETLE-----------------------
		if (fan_btn == null) {
			fan_btn = new BeetleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-fan")), new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-fan")), beetleHolder.get(1).get(0));
			fan_btn.beetleName = "Fan";
			fan_btn.setPositionXY(349, 78);
			fan_btn.gameScreen = this;

			rootGroup.addActorBefore(backgroundComplete, fan_btn);
			bManager.fanBtn = fan_btn;

			fan_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					if (bManager.getActiveBeetle() != null) {
						bManager.onFanButtonClicked();
					}
				}
			});
		} else {
			fan_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-fan"));
			fan_btn.getStyle().down = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-fan"));
		}

		// ---------------MINER BEETLE-----------------------
		if (miner_btn == null) {
			miner_btn = new BeetleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-minner")), new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-minner")), beetleHolder.get(2).get(0));
			miner_btn.beetleName = "Miner";
			miner_btn.setPositionXY(308, 2);
			miner_btn.gameScreen = this;

			rootGroup.addActorBefore(backgroundComplete, miner_btn);
			bManager.minerBtn = miner_btn;

			miner_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					if (bManager.getActiveBeetle() != null) {
						bManager.onMinerButtonClicked();
					}
				}
			});
		} else {
			miner_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-minner"));
			miner_btn.getStyle().down = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-minner"));
		}

		// ---------------BOOM BEETLE-----------------------
		if (boom_btn == null) {
			boom_btn = new BeetleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-boom")), new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-boom")), beetleHolder.get(3).get(0));
			boom_btn.beetleName = "Boom";
			boom_btn.setPositionXY(400, 10);
			boom_btn.gameScreen = this;

			rootGroup.addActorBefore(backgroundComplete, boom_btn);
			bManager.boomBtn = boom_btn;

			boom_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					if (bManager.getActiveBeetle() != null) {
						bManager.onBoomButtonClicked();
					}
				}
			});
		} else {
			boom_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-boom"));
			boom_btn.getStyle().down = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-boom"));
		}

		// ---------------KARATE BEETLE-----------------------
		if (karate_btn == null) {
			karate_btn = new BeetleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-karate")), new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-karate")), beetleHolder.get(4).get(0));
			karate_btn.beetleName = "Karate";
			karate_btn.setPositionXY(410, 60);
			karate_btn.gameScreen = this;

			rootGroup.addActorBefore(backgroundComplete, karate_btn);
			bManager.karateBtn = karate_btn;

			karate_btn.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					if (bManager.getActiveBeetle() != null) {
						bManager.onKarateButtonClicked();
					}
				}
			});
		} else {
			karate_btn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-karate"));
			karate_btn.getStyle().down = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("btn-karate"));
		}

		// ---------------SHOP LEFT-----------------------
		if (shop_left == null) {
			shop_left = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("shop-left")));

			shop_left.setPositionXY(40, 400);
			shop_left.setVisible(false);
			rootGroup.addActorBefore(backgroundYesNo, shop_left);

			shop_left.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					onShopLeft();
				}
			});
		} else {
			shop_left.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("shop-left"));
		}

		if (facebook == null) {
			facebook = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("level-complete-f")));

			facebook.setPositionXY(20, 730);
			facebook.setVisible(false);
			rootGroup.addActorBefore(backgroundYesNo, facebook);

		} else {
			facebook.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("level-complete-f"));
		}

		if (twitter == null) {
			twitter = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("level-complete-tw")));

			twitter.setPositionXY(70, 730);
			twitter.setVisible(false);
			rootGroup.addActorBefore(backgroundYesNo, twitter);

		} else {
			twitter.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("level-complete-tw"));
		}

		// ---------------SHOP RIGHT-----------------------
		if (shop_right == null) {
			shop_right = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("shop-right")));

			shop_right.setPositionXY(390, 400);
			shop_right.setVisible(false);
			rootGroup.addActorBefore(backgroundYesNo, shop_right);

			shop_right.addListener(new ClickListener() {
				@Override
				public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                    super.touchUp(event, x, y, pointer, button);
					onShopRight();
				}
			});
		} else {
			shop_right.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("shop-right"));
		}

		optionsButtonsInit();

		hidePauseScreen();
		hideCompleteScreen();
		isFirstRun = true;

		bManager.restartManager();

		rootGroup.addActorBefore(backgroundYesNo, GdxGame.mInventory);
		GdxGame.mInventory.setSkin();

		if (BBInventory.coockieCount == null) {
			GdxGame.mInventory.initTxtObjects();
		}

		if (GameScreen.levelNumb >= 5) {
			GdxGame.tutorialObject.initTutorial(Texts.TUTORIAL_MESSAGE_ARROW,
					"arrow", 240, 400, 420, ArrowPosEnum.BOTTOM, 0.5f,
					rootGroup);
		}

		GdxGame.tutorialObject.initTutorial(Texts.NINETY_PERSENT_MINIMUM,
				"90%", 240, 330, 420, ArrowPosEnum.BOTTOM, 0.0f, rootGroup);
	}

	public void retryLevel() {
		bManager.restart();
		objectToDestroy.restart();
		isFirstRun = true;
		jar.setProgress(0);
		hideCompleteScreen();
		AudioPlayer.playSound("tap3");
		setGameVariablesToDefault();
	}

	public static void explosionShake() {
		mShakeTimer = 10 / 1000.0f;

		mOffsetX += Slingshot.getRandomInt(6) - 3;
		mOffsetY += Slingshot.getRandomInt(6) - 3;

		if (mOffsetX < -3 || mOffsetX > 3) {
			mOffsetX = 0;
		}

		if (mOffsetY < -3 || mOffsetY > 3) {
			mOffsetY = 0;
		}

		GdxGame.camera.position.set(GdxGame.VIRTUAL_GAME_WIDTH / 2 - mOffsetX,
				GdxGame.VIRTUAL_GAME_HEIGHT / 2 - mOffsetY, 0);

		if (GdxGame.camera.zoom > 0.97f) {
			GdxGame.camera.zoom -= 0.005f;

		}

		mShakeCounter--;
		if (mShakeCounter == 0) {
			GdxGame.camera.position.set(GdxGame.VIRTUAL_GAME_WIDTH / 2,
					GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);
		}
	}

	@Override
	public void render(float delta) {

		// Gdx.app.log("FPS",
		// String.valueOf(Gdx.graphics.getFramesPerSecond()));

		// Gdx.app.log("FPS",
		// String.valueOf(Gdx.graphics.getFramesPerSecond()));
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		Gdx.gl.glViewport(GdxGame.START_X, GdxGame.START_Y,
				GdxGame.SCREEN_WIDTH, GdxGame.SCREEN_HEIGHT);

		if (!ImageCache.getManager().update()) {
			return;
		}

		if (!this.isButtonsInited) {
			InitButtons();

			this.isButtonsInited = true;
		}

		if (mShakeTimer > 0) {
			mShakeTimer -= delta;
		} else if (mShakeCounter > 0) {
			explosionShake();
		} else if (mShakeCounter == 0 && GdxGame.camera.zoom < 1.0f) {
			mShakeTimer = 10 / 1000.0f;
			GdxGame.camera.zoom += 0.005f;
		}

		GdxGame.camera.update();
		spriteBatch.setProjectionMatrix(GdxGame.camera.combined);

		rootGroup.act(delta);

		if (!isGamePaused()) {
			bManager.think(delta);
			Slingshot.think(delta);
			reloadTimer = reloadTimer - Gdx.graphics.getDeltaTime();
		}
		GdxGame.tweenManager.update(delta);

		spriteBatch.begin();
		rootGroup.draw(spriteBatch, 1);

		int i = 0;
		while (i < this.pointsLblArray.size()) {
			this.pointsLblArray.get(i).act(delta);

			if (!backgroundComplete.isVisible() && !backgroundYesNo.isVisible()) {
				this.pointsLblArray.get(i).draw(spriteBatch, 1);
			}

			if (!this.pointsLblArray.get(i).hasActiveActions()) {
				this.pointsLblArray.remove(i);
			} else {
				i++;
			}
		}

		spriteBatch.end();

		/*
		 * shapeRenderer.setProjectionMatrix(GdxGame.camera.combined);
		 * shapeRenderer.begin(ShapeType.FilledRectangle);
		 * shapeRenderer.setColor(0, 0, 0, 0.1f); shapeRenderer.filledRect(0, 0,
		 * 480, 800); shapeRenderer.end();
		 */

		/*
		 * if (Gdx.input.isKeyPressed(Keys.CONTROL_LEFT)) { dispose();
		 * game.loadingScreen.setNextScreen("stage");
		 * game.loadingScreen.setPrevScreenName("game");
		 * game.changeScreen("loading"); }
		 */

		// logger.log();

		if (reloadTimer < 0) {
			reloadTimer = 0;
		}

		// if (bManager.getActiveBeetle() != null
		// && bManager.getActiveBeetle().getState()
		// .equals(BeetleState.ACTIVE)) {

		// }

		if (this.flagIsBeetleLoaded && delta <= 0.1f) {
			while (objectToDestroy.currAngleIndex == objectToDestroy.prevAngleIndex) {
				if (objectToDestroy.maxAngleIndex <= 1) {
					objectToDestroy.currAngleIndex = 0;
					objectToDestroy.prevAngleIndex = 0;
					break;
				}

				objectToDestroy.currAngleIndex = Slingshot
						.getRandomInt(objectToDestroy.maxAngleIndex);
			}

			AudioPlayer.stopSound(rotateSoundName);
			rotateSoundName = "wheel" + Slingshot.getRandomInt(1);
			AudioPlayer.playSound(rotateSoundName, 0.2f);

			// Change movement SPEED and DIRECTION
			objectToDestroy.currRotationSpeed = objectToDestroy.rotationAngles[objectToDestroy.currAngleIndex];

			objectToDestroy.prevAngleIndex = objectToDestroy.currAngleIndex;

			this.flagIsBeetleLoaded = false;
		}

		if (reloadTimer == 0 && bManager.getActiveBeetle() == null
				&& !isEndLevel && !bManager.isBonusInFly()) {
			BeetleManager.loadingCounter++;
			bManager.loadNextBeetle(isFirstRun);

			if (bManager.getActiveBeetle() == null) {
				isEndLevel = true;
			}

			this.flagIsBeetleLoaded = true;

			if (jar.getProgress() > 99) {
				isLevelEnd = true;
			}
		}

		if (pointsTimer > 0) {
			pointsTimer = pointsTimer - Gdx.graphics.getDeltaTime();
			if (pointsTimer <= 0) {
				calcPoints(true);
				pointsTimer = 0;
			}
		}

		showCompleteTimer = showCompleteTimer - Gdx.graphics.getDeltaTime();
		if (showCompleteTimer < 0) {
			showCompleteTimer = 0;
		}

		// TODO isEnd conflicted
		if (showCompleteTimer == 0 && isLevelEnd && pointsTimer <= 0
				&& this.pointsLblArray.size() == 0 && !bManager.isBonusInFly()) {
			if (!isBeetlesAndStarsCalculated) {
				pointsForDestruction += (bManager.getBeetlesCount() * 500 + JarObject
						.getStars() * 250);

				GdxGame.points.setText("POINTS: " + pointsForDestruction);

				this.setPoints(10, 210, JarObject.getStars() * 250,
						JarObject.getStars());
				this.setPoints(420, 220, bManager.getBeetlesCount() * 500,
						bManager.getBeetlesCount());

				isBeetlesAndStarsCalculated = true;
			}

			else {
				if (GdxGame.isShopUsed || GdxGame.mInventory.getAutoOpenUsed()
						|| !GdxGame.mInventory.isVisible()
						|| GameScreen.jar.getProgress() >= 90) {

					if (GameScreen.jar.getProgress() < 90) {
						showCompleteScreen(0);
					}

					else

					{
						showCompleteScreen(JarObject.getStars());
					}

				} else if (!GdxGame.mInventory.getAutoOpenUsed()) {
					GdxGame.mInventory.setState(InvState.AUTO_OPEN);
					isFirstRun = true;
				}

				isLevelEnd = false;
			}
		}

		AudioPlayer.Think(delta);

		if (showCompleteTimer != 0 && Gdx.input.isKeyPressed(Keys.BACK)) {
			return;
		}

		if (!this.firsEnterFrame && Gdx.input.isKeyPressed(Keys.BACK)) {
			hidePauseScreen();
			hideCompleteScreen();
			AudioPlayer.playSound("tap2");
			dispose();
			game.loadingScreen.setNextScreen("stage");
			game.loadingScreen.setPrevScreenName("game");
			game.changeScreen("loading");
		}

		if (this.firsEnterFrame && !Gdx.input.isKeyPressed(Keys.BACK)) {
			this.firsEnterFrame = false;
			reloadTimer = 2;
		}

		// laserTimer = laserTimer - Gdx.graphics.getDeltaTime();

		// if (laserTimer <= 0) {

		if (isNeedToDrawLaser) {
			isNeedToDrawLaser = !isNeedToDrawLaser;
			Slingshot.resetLaser();
			if (GameScreen.bManager.getActiveBeetle() != null) {
				Slingshot.enableFlashlight(
						GameScreen.bManager.getActiveBeetleType(),
						Slingshot.getX1(), Slingshot.getY1(),
						Slingshot.getX2(), Slingshot.getY2());
				// laserTimer = 0.03f;
				// }

			}
		} else {
			isNeedToDrawLaser = !isNeedToDrawLaser;
		}

	}

	public void onMenuClick() {
		if (failed.isVisible() || complete.isVisible() || finish_game_back.isVisible()) {
			return;
		}

		if (!isGamePaused()) {
			showPauseScreen();
		} else {
			hidePauseScreen();
		}
	}

	public void onPause() {
		if (!GameScreen.flagGameAutoPauseDissabled && !menu_btn.isVisible()) {
			showPauseScreen();
		}
	}

	public void setPoints(float x, float y, int points, int explCounter) {
		if (points == 0) {
			return;
		}

		AnimatedTextGameObject pointsText = new AnimatedTextGameObject(
				"pointsTxt", false, 30);

		pointsText.setRotation(180);
		pointsText.setScaleX(0.7f);
		pointsText.setScaleY(0.7f);

		if (points < 1500) {
			pointsText.setColor(0.2f, 1.0f, 0.2f, 0);
		} else if (points < 7000) {
			pointsText.setColor(1.0f, 1.0f, 0.2f, 0);
		} else {
			pointsText.setColor(1.0f, 0.2f, 0.2f, 0);
		}

		pointsText.setTextCenteredOnPosition("+ " + points, x, y, true);
		pointsText.setOrigin(pointsText.getPositionX(),
				pointsText.getPositionY());

		AlphaAction pointsFadeIn = fadeIn(0.2f);
		RotateByAction pointsRotate1 = rotateBy(180, 0.5f);
		ScaleToAction pointsScale = scaleTo(1f, 1f, 0.75f);
		AlphaAction pointsFadeOut = fadeOut(0.15f);
		MoveToAction pointsMoveTo = moveTo(pointsText.getPositionX(), pointsText.getPositionY() + 100 * GdxGame.HD_Y_RATIO, 0.75f);

		pointsText.addAction(parallel(
				pointsFadeIn,
				pointsRotate1,
				sequence(
						delay(0.5f),
						parallel(pointsScale, pointsMoveTo,
								sequence(delay(0.6f), pointsFadeOut)))));

		this.pointsLblArray.add(pointsText);

		if (explCounter > 1) {
			AnimatedTextGameObject comboText = new AnimatedTextGameObject(
					"comboTxt", false, 30);

			comboText.setRotation(20);
			comboText.setScaleX(1.0f);
			comboText.setScaleY(1.0f);

			comboText.setColor(1.0f, 0.6f, 0.1f, 1);
			comboText.setTextCenteredOnPosition("COMBO X" + explCounter, x + 65
					* GdxGame.HD_X_RATIO, y - 80 * GdxGame.HD_Y_RATIO, true);
			comboText.setOrigin(pointsText.getPositionX(),
					pointsText.getPositionY());

			AlphaAction comboFadeIn = fadeIn(0.2f);
			ScaleToAction comboScale1 = scaleTo(1.3f, 1.3f, 0.5f);
			ScaleToAction comboScale2 = scaleTo(1.0f, 1.0f, 0.5f);
			ScaleToAction comboScale3 = scaleTo(1.1f, 1.1f, 0.8f);
			AlphaAction comboFadeOut = fadeOut(0.2f);
			comboText.addAction(comboFadeOut);
			comboText.addAction(parallel(comboFadeIn, sequence(comboScale1,
					comboScale2, comboScale3, comboFadeOut)));

			this.pointsLblArray.add(comboText);
		}
	}

	public void calcPoints(boolean animated) {
		float value1 = (PixmapHelper.pixelsDestroyed - pixelsDestroyed);
		float value2 = (1 + AbstractBeetle.explodeCounter / 10);
		int value3 = (int) (Math.round(value1 * value2 / 100) * 10 / 1.5f);
		pointsForDestruction += value3;

		GdxGame.points.setText("POINTS: " + pointsForDestruction);

		pixelsDestroyed = PixmapHelper.pixelsDestroyed;

		if (animated) {
			this.setPoints(AbstractBeetle.getLastExplPos().x,
					AbstractBeetle.getLastExplPos().y, value3,
					(int) AbstractBeetle.explodeCounter);
		}

		AbstractBeetle.explodeCounter = 0;

		if (fast_finish_btn != null) {
			if (jar.getProgress() >= 90) {
				fast_finish_btn.setVisible(true);
				restart_btn.setVisible(false);
			} else {
				fast_finish_btn.setVisible(false);
				restart_btn.setVisible(true);
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	public Vector2 getStageAndLevel() {
		Vector2 res = new Vector2(0, 0);
		res.x = (int) (1 + (levelNumb - 0.1f) / 24);
		res.y = 24 - (24 * res.x - levelNumb);

		return res;
	}

	@Override
	public void show() {
		GdxGame.beetleStage.unfocusAll();
		rootGroup.setTouchable(Touchable.enabled);
		this.firsEnterFrame = true;
		this.pointsLblArray.clear();

		if (!game.loadingScreen.getPrevScreenName().equals("shop")) {
			GdxGame.camera.position.set(GdxGame.VIRTUAL_GAME_WIDTH / 2,
					GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);

			if (!game.loadingScreen.getPrevScreenName().equals("tutorial")) {
				GdxGame.mInventory.setAutoOpenUsed(false);
				objectToDestroy.pixmapHelper.initTexture();
				this.isButtonsInited = false;

				if (fast_finish_btn != null) {
					fast_finish_btn.setVisible(false);
					restart_btn.setVisible(true);
				}

				jar.setProgress(0);

				Vector2 l = getStageAndLevel();
				GdxGame.level_numb.setTextCenteredOnPosition("STAGE "
						+ (int) l.x + " LEVEL " + (int) l.y, 240, 40, 150,
						false);

				isLevelEnd = false;

				GdxGame.mInventory.resetActiveBonuses();

				if (!bManager.checkEggBeetles()) {
					GdxGame.mInventory
							.activeBonusesAdd(AvaliableBonuses.EXPLOSION_RADIUS);
					GdxGame.mInventory
							.activeBonusesAdd(AvaliableBonuses.BONUS_EGG1);
					GdxGame.mInventory
							.activeBonusesAdd(AvaliableBonuses.BONUS_EGG2);
				}

				this.flagIsBeetleLoaded = false;

			} else {
				objectToDestroy.isFilterInited = false;
				objectToDestroy.pixmapHelper.reload();
			}

		} else {
			GdxGame.mInventory.setVisible(false);
			rootGroup.addActorBefore(backgroundYesNo, GdxGame.mInventory);
			objectToDestroy.isFilterInited = false;
			objectToDestroy.pixmapHelper.reload();
		}

		//if (GdxGame.dayMinutes == 0) {
			background_day.setVisible(true);
			//day_light.setVisible(true);

			particle_dust.setOriginX(340);
			particle_dust.setOriginY(600);

			bee0.setVisible(false);
			bee1.setVisible(false);
			bee2.setVisible(false);

			if (!game.loadingScreen.getPrevScreenName().equals("tutorial")) {
				currLevelMusic = "day_theme" + Slingshot.getRandomInt(5);
				AudioPlayer.stopAllMusic();
				// AudioPlayer.playMusic(currLevelMusic);
			}
		//} else {
		/*	particle_dust.setOriginX(240);
			particle_dust.setOriginY(650);

			background_night.setVisible(true);
			night_light.setVisible(true);

			bee0.setVisible(true);
			bee1.setVisible(true);
			bee2.setVisible(true);

			if (!game.loadingScreen.getPrevScreenName().equals("tutorial")) {
				currLevelMusic = "night_theme" + Slingshot.getRandomInt(5);
				AudioPlayer.stopAllMusic();

				if (currLevelMusic.equals("night_theme4")) {
					AudioPlayer.playMusic(currLevelMusic, 0.15f);
				}

				else {
					AudioPlayer.playMusic(currLevelMusic);
				}
			}
		}*/

		GdxGame.mInventory.setState(InvState.CLOSED);
		this.finish_level_btn.setVisible(false);
		this.finish_game_back.setVisible(false);

		game.loadingScreen.setPrevScreenName("game");

		// this.game.startFlurry();
		this.game.flurryAgentOnPageView();
	}

	@Override
	public void hide() {
		rootGroup.setTouchable(Touchable.disabled);
		AudioPlayer.setRandomMusic(false);

		background_night.setVisible(false);
		background_day.setVisible(false);

		day_light.setVisible(false);
		night_light.setVisible(false);

		if (!this.game.loadingScreen.getNextScreen().equals("tutorial")
				&& !this.game.loadingScreen.getNextScreen().equals("shop")
				&& bManager.getActiveBeetle() != null
				&& bManager.getActiveBeetle().getName().equals("boom-shop")) {
			this.game.updateItem("android.beetle1.purchased", -1);

			params.clear();
			params.put("purchased", "android.beetle1.purchased -1");
			params.put("level", String.valueOf(GameScreen.levelNumb));
			this.game.flurryAgentLogEvent("Purchased", params);

		}

		else if (!this.game.loadingScreen.getNextScreen().equals("tutorial")
				&& !this.game.loadingScreen.getNextScreen().equals("shop")
				&& bManager.getActiveBeetle() != null
				&& bManager.getActiveBeetle().getName().equals("karate-shop")) {
			this.game.updateItem("android.beetle2.purchased", -1);

			params.clear();
			params.put("purchased", "android.beetle2.purchased -1");
			params.put("level", String.valueOf(GameScreen.levelNumb));
			this.game.flurryAgentLogEvent("Purchased", params);
		}
		// this.game.stopFlurry();
	}

	@Override
	public void pause() {
		this.game.stopFlurry();
	}

	@Override
	public void resume() {
		Texture.setAssetManager(ImageCache.getManager());
		objectToDestroy.isFilterInited = false;
		objectToDestroy.pixmapHelper.reload();
		GdxGame.beetleStage.unfocusAll();

		if (finish_level_btn.isVisible()) {
			hidePauseScreen();
			GdxGame.mInventory.setState(InvState.AUTO_OPEN);
			isFirstRun = true;
			isLevelEnd = false;
		}
		this.game.startFlurry();
	}

	@Override
	public void dispose() {
		objectToDestroy.dispose();
	}

	private void optionsButtonsInit() {

		if (settingsBtn == null) {
			settingsBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("settings-btn")));
			settingsBtn.setPositionXY(125, 239);
		} else {
			settingsBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("settings-btn"));
		}

		rootGroup.addActor(settingsBtn);

		if (shopBtn == null) {
			shopBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("shop-btn")));
			shopBtn.setPositionXY(287, 239);
		} else {
			shopBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("shop-btn"));
		}

		rootGroup.addActor(shopBtn);

		if (scoreLoopBtn == null) {
			scoreLoopBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("board-btn")));
			scoreLoopBtn.setPositionXY(206, 239);
		} else {
			scoreLoopBtn.getStyle().up = new TextureRegionDrawable (ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("board-btn"));
		}

		rootGroup.addActor(scoreLoopBtn);

		try {
			if (GdxGame.files.getOptionsAndProgress("options", "sound").equals(
					"on")) {
				if (soundBtn == null) {
					soundBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("sound")));
					soundBtn.setPositionXY(53, 239);
				} else {
					soundBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("sound"));
				}
			} else {
				if (soundBtn == null) {
					soundBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("sound-off")));
					soundBtn.setPositionXY(53, 239);
				} else {
					soundBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("sound-off"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rootGroup.addActor(soundBtn);
		try {
			if (GdxGame.files.getOptionsAndProgress("options", "music").equals(
					"on")) {
				if (musicBtn == null) {
					musicBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("music")));
					musicBtn.setPositionXY(52, 163);
				} else {
					musicBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("music"));
				}
			} else {
				if (musicBtn == null) {
					musicBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("music-off")));
					musicBtn.setPositionXY(52, 163);
				} else {
					musicBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("music-off"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rootGroup.addActor(musicBtn);
		try {
			if (GdxGame.files.getOptionsAndProgress("options", "vibro").equals(
					"on")) {
				if (vibroBtn == null) {
					vibroBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("vibro")));
					vibroBtn.setPositionXY(125, 163);
				} else {
					vibroBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("vibro"));
				}
			} else {
				if (vibroBtn == null) {
					vibroBtn = new HdCompatibleButton(new TextureRegionDrawable (ImageCache.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("vibro-off")));
					vibroBtn.setPositionXY(125, 163);
				} else {
					vibroBtn.getStyle().up = new TextureRegionDrawable (ImageCache
							.getManager()
							.get("data/atlas/main/pack", TextureAtlas.class)
							.findRegion("vibro-off"));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		rootGroup.addActor(vibroBtn);

		soundBtn.addListener(new ClickListener() {

			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
				AudioPlayer.playSound("tap3");

				try {
					if (GdxGame.files.getOptionsAndProgress("options", "sound")
							.equals("on")) {
						GdxGame.files.setOptionsAndProgress("options", "sound",
								"off");
						soundBtn.getStyle().up = (Drawable) ImageCache
										.getManager()
										.get("data/atlas/main/pack",
												TextureAtlas.class)
										.findRegion("sound-off");
					} else {
						GdxGame.files.setOptionsAndProgress("options", "sound",
								"on");
						soundBtn.getStyle().up = (Drawable)
								ImageCache
										.getManager()
										.get("data/atlas/main/pack",
												TextureAtlas.class)
										.findRegion("sound");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		musicBtn.addListener(new ClickListener() {

			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
				AudioPlayer.playSound("tap3");

				try {
					if (GdxGame.files.getOptionsAndProgress("options", "music")
							.equals("on")) {
						AudioPlayer.pauseMusic(currLevelMusic);

						GdxGame.files.setOptionsAndProgress("options", "music",
								"off");
						musicBtn.getStyle().up = (Drawable)
								ImageCache
										.getManager()
										.get("data/atlas/main/pack",
												TextureAtlas.class)
										.findRegion("music-off");
					} else {
						GdxGame.files.setOptionsAndProgress("options", "music",
								"on");

						AudioPlayer.playMusic(currLevelMusic);

						musicBtn.getStyle().up = (Drawable)
								ImageCache
										.getManager()
										.get("data/atlas/main/pack",
												TextureAtlas.class)
										.findRegion("music");

						// AudioPlayer.playMusic("win", 0.3f);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		vibroBtn.addListener(new ClickListener() {

			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
				AudioPlayer.playSound("tap3");

				try {
					if (GdxGame.files.getOptionsAndProgress("options", "vibro")
							.equals("on")) {
						GdxGame.files.setOptionsAndProgress("options", "vibro",
								"off");
						vibroBtn.getStyle().up = (Drawable)
								ImageCache
										.getManager()
										.get("data/atlas/main/pack",
												TextureAtlas.class)
										.findRegion("vibro-off");
						vibro = GdxGame.files.getOptionsAndProgress("options",
								"vibro");
					} else {
						Gdx.input.vibrate(25);

						GdxGame.files.setOptionsAndProgress("options", "vibro",
								"on");
						vibroBtn.getStyle().up = (Drawable)
								ImageCache
										.getManager()
										.get("data/atlas/main/pack",
												TextureAtlas.class)
										.findRegion("vibro");
						vibro = GdxGame.files.getOptionsAndProgress("options",
								"vibro");
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});

		scoreLoopBtn.addListener(new ClickListener() {

			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
				AudioPlayer.playSound("tap3");
				game.showScoreloop();
			}
		});

		facebook.addListener(new ClickListener() {

			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
				AudioPlayer.playSound("tap3");
				game.startFacebook(message, 2);
				game.flurryAgentLogEvent("facebook post result");
			}
		});

		twitter.addListener(new ClickListener() {

			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
				AudioPlayer.playSound("tap3");
				game.startTwitter(message, 2);
				game.flurryAgentLogEvent("twitter post result");
			}
		});

		shopBtn.addListener(new ClickListener() {
			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
				AudioPlayer.playSound("tap3");
				game.loadingScreen.setNextScreen("shop");
				game.loadingScreen.setPrevScreenName("game");

				rootGroup.setTouchable(Touchable.disabled);
				game.changeScreen("loading");
			}
		});

		settingsBtn.addListener(new ClickListener() {
			@Override
			public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
				AudioPlayer.playSound("tap3");
				soundBtn.setVisible(!soundBtn.isVisible());
				vibroBtn.setVisible(!vibroBtn.isVisible());
				musicBtn.setVisible(!musicBtn.isVisible());
			}
		});

	}

	public void showPauseScreen() {
		AudioPlayer.setRandomMusic(false);

		// AudioPlayer.pauseAllMusic();
		// AudioPlayer.playMusic("night_theme0");

		optionsButtonsInit();
		setGamePause(true);

		GdxGame.mInventory.setVisible(false);
		GdxGame.mInventory.setState(InvState.CLOSED);
		backgroundComplete.getColor().a = 1;
		backgroundComplete.setVisible(true);
		pauseTxt.setVisible(true);

		pause_resume_btn.setVisible(true);
		pause_restart_btn.setVisible(true);
		pause_menu_btn.setVisible(true);
		// soundBtn.visible = true;
		// musicBtn.visible = true;
		// vibroBtn.visible = true;
		scoreLoopBtn.setVisible(true);
		shopBtn.setVisible(true);
		settingsBtn.setVisible(true);
	}

	public void hidePauseScreen() {
		// AudioPlayer.pauseMusic("night_theme0");
		// AudioPlayer.playMusic(currLevelMusic);

		setGamePause(false);

		GdxGame.mInventory.setVisible(true);
		backgroundComplete.getColor().a = 0;
		backgroundComplete.setVisible(false);
		pauseTxt.setVisible(false);

		pause_resume_btn.setVisible(false);
		pause_restart_btn.setVisible(false);
		pause_menu_btn.setVisible(false);
		soundBtn.setVisible(false);
		musicBtn.setVisible(false);
		vibroBtn.setVisible(false);
		scoreLoopBtn.setVisible(false);
		shopBtn.setVisible(false);
		settingsBtn.setVisible(false);
	}

	private int getShopAd() {
		failedScreensCounter++;

		if (failedScreensCounter > 3) {
			failedScreensCounter = 0;
			return Slingshot.getRandomInt(mShopList.length - 1);
		} else {
			return -1;
		}
	}

	private void onShopLeft() {
		GameObject go = (GameObject) rootGroup
				.findActor(mShopList[mCurrShopPage]);
		go.setVisible(false);

		mCurrShopPage--;

		if (mCurrShopPage < 0) {
			mCurrShopPage = mShopList.length - 1;
		}

		else if (mCurrShopPage > mShopList.length - 1) {
			mCurrShopPage = 0;
		}

		AlphaAction fade1 = fadeIn(0.15f);
		go = (GameObject) rootGroup.findActor(mShopList[mCurrShopPage]);
		go.clearActions();
		go.getColor().a = 0;
		go.addAction(fade1);
		go.setVisible(true);

		shop_left.clearActions();
		MoveToAction leftMoveTo1 = moveTo(45 * GdxGame.HD_X_RATIO,
				400 * GdxGame.HD_Y_RATIO, 0.15f);
		MoveToAction leftMoveTo2 = moveTo(40 * GdxGame.HD_X_RATIO,
				400 * GdxGame.HD_Y_RATIO, 0.15f);
		ScaleToAction scale1 = scaleTo(1.05f, 1.05f, 0.15f);
		ScaleToAction scale2 = scaleTo(1.0f, 1.0f, 0.15f);
		shop_left.addAction(parallel(sequence(leftMoveTo1, leftMoveTo2),
				sequence(scale1, scale2)));
	}

	private void onShopRight() {
		GameObject go = (GameObject) rootGroup
				.findActor(mShopList[mCurrShopPage]);
		go.setVisible(false);

		mCurrShopPage++;

		if (mCurrShopPage < 0) {
			mCurrShopPage = mShopList.length - 1;
		}

		else if (mCurrShopPage > mShopList.length - 1) {
			mCurrShopPage = 0;
		}

		AlphaAction fade1 = fadeIn(0.15f);
		go = (GameObject) rootGroup.findActor(mShopList[mCurrShopPage]);
		go.clearActions();
		go.getColor().a = 0;
		go.addAction(fade1);
		go.setVisible(true);

		shop_right.clearActions();
		MoveToAction rightMoveTo1 = moveTo(395 * GdxGame.HD_X_RATIO,
				400 * GdxGame.HD_Y_RATIO, 0.15f);
		MoveToAction rightMoveTo2 = moveTo(390 * GdxGame.HD_X_RATIO,
				400 * GdxGame.HD_Y_RATIO, 0.15f);
		ScaleToAction scale1 = scaleTo(1.05f, 1.05f, 0.15f);
		ScaleToAction scale2 = scaleTo(1.0f, 1.0f, 0.15f);
		shop_right.addAction(parallel(sequence(rightMoveTo1, rightMoveTo2),
				sequence(scale1, scale2)));
	}

	private void calcLevel() {
		int lNumber = levelNumb;
		int stage = (int) (1 + (lNumber - 0.1f) / 24);
		lNumber = 24 - (24 * stage - levelNumb);

		levelNumber.setText("STAGE " + String.valueOf(stage) + " - LEVEL "
				+ String.valueOf(lNumber));
	}

	public void showCompleteScreen(final int starsCount) {
		GdxGame.points.setVisible(false);
		GdxGame.level_numb.setVisible(false);
		GameScreen.flagGameAutoPauseDissabled = true;

		hidePauseScreen();

		setGamePause(true);
		AudioPlayer.setRandomMusic(false);
		AudioPlayer.pauseAllMusic();

		int beetles = bManager.getBeetlesCount();

		int prog = 0;
		if (GameScreen.jar.getProgress() >= 90) {
			prog = 1;
		}

		final int points = pointsForDestruction * prog;

		message = points + "-" + levelNumb + "-" + GdxGame.stageNumber;

		// sendResult();

		try {
			if (Integer.parseInt(GdxGame.files.getOptionsAndProgress("points",
					"level" + levelNumb)) < points) {
				GdxGame.files.setOptionsAndProgress("points", "level"
						+ levelNumb, String.valueOf(points));

				setGamePause(false);
				GdxGame.tutorialObject.initTutorial(
						Texts.TUTORIAL_MESSAGE_WIN_SHARE, "win_share", 240,
						550, 420, ArrowPosEnum.TOP, 0.075f,
						GameScreen.rootGroup);
				setGamePause(true);
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (GameScreen.jar.getProgress() < 90) {
			this.game.flurryAgentLogEvent("level " + levelNumb + " fail");
			endScreenProgressBar.setPositionXY(149, 320);

			AudioPlayer.playMusicFromStart("lose", 0.2f);
			if (AudioPlayer.getMusic("lose") != null) {
				AudioPlayer.getMusic("lose").getMusic().setLooping(false);
			}

			failed.setVisible(true);
			// level.visible = true;
			// progress.visible = true;
			min.setVisible(true);

			levelNumber.setVisible(true);
			currentScore.setVisible(true);
			currentScore.setPositionXY(45, 565);

			mCurrShopPage = getShopAd();
			if (mCurrShopPage >= 0) {
				GameObject go = (GameObject) rootGroup
						.findActor(mShopList[mCurrShopPage]);
				go.setVisible(true);

				ingameShopContainer.setVisible(true);
				shop_txt.setVisible(true);
				shop_left.setVisible(true);
				shop_right.setVisible(true);

				endScreenProgressBar.setVisible(false);
				min.setVisible(false);

			} else {
				ingameShopContainer.setVisible(false);
				shop_txt.setVisible(false);
				shop_left.setVisible(false);
				shop_right.setVisible(false);

				endScreenProgressBar.setVisible(true);
				endScreenProgressBar.playCompleteAnim(
						GameScreen.jar.getProgress(), starsCount);

				min.setVisible(true);
			}

			retry_btn.setPositionXY(10, 40);
			next_btn.setPositionXY(156, 140);
			menu_btn.setPositionXY(288, 40);

			calcLevel();
			currentScore.setText("LEVEL PROGRESS: " + jar.getProgress() + "%");

		} else {
			this.game.flurryAgentLogEvent("level " + levelNumb + " win");
			facebook.setVisible(true);
			twitter.setVisible(true);
			endScreenProgressBar.setPositionXY(149, 275);
			endScreenProgressBar.playCompleteAnim(GameScreen.jar.getProgress(),
					starsCount);

			AudioPlayer.playMusicFromStart("win", 0.2f);
			if (AudioPlayer.getMusic("win") != null) {
				AudioPlayer.getMusic("win").getMusic().setLooping(false);
			}

			complete.setVisible(true);
			// level.visible = true;
			bestScore.setVisible(true);

			levelNumber.setVisible(true);
			currentScore.setVisible(true);
			currentScore.setPositionXY(35, 580);
			bestScore.setPositionXY(70, 516);

			retry_btn.setPositionXY(10, 50);
			next_btn.setPositionXY(156, 160);
			menu_btn.setPositionXY(286, 50);

			calcLevel();
			currentScore.setText("CURRENT SCORE: " + points);
			try {
				bestScore.setText("BEST SCORE: "
						+ GdxGame.files.getOptionsAndProgress("points", "level"
								+ String.valueOf(levelNumb)));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		menu_btn.setVisible(true);

		Thread thread = null;
		try {
			thread = new Thread() {
				int starsNew = starsCount;
				int starsOld = Integer.parseInt(GdxGame.files
						.getOptionsAndProgress("levels",
								"level" + String.valueOf(levelNumb)));
				private int collectedPoints = 0;

				@Override
				public void run() {

					if (starsNew > starsOld) {
						GdxGame.files.setOptionsAndProgress("levels", "level"
								+ String.valueOf(levelNumb), "" + starsCount);
					}

					int levelsToOpened = 3;
					int ii = 1;
					int currLevelPoints = 0;
					int currLevelStars = 0;

					while (ii <= Constants.LEVELS_COUNT && levelsToOpened > 0) {
						try {
							currLevelPoints = Integer.parseInt(GdxGame.files
									.getOptionsAndProgress("points", "level"
											+ String.valueOf(ii)));

							if (currLevelPoints == 0) {
								currLevelStars = Integer.parseInt(GdxGame.files
										.getOptionsAndProgress("levels",
												"level" + String.valueOf(ii)));
								if (currLevelStars == 0) {
									levelsToOpened--;
								}
							}
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						if (currLevelPoints == 0 && currLevelStars == -1) {
							break;
						}

						ii++;
					}

					// if (starsNew >= 1 && starsOld == 0) {
					if (points != 0) {
						int curLevel = levelNumb + 1;
						while (levelsToOpened > 0) {
							if (curLevel > Constants.LEVELS_COUNT) {
								break;
							}
							try {
								if (Integer
										.parseInt(GdxGame.files.getOptionsAndProgress(
												"points",
												"level"
														+ String.valueOf(curLevel))) == 0
										&& Integer
												.parseInt(GdxGame.files
														.getOptionsAndProgress(
																"levels",
																"level"
																		+ String.valueOf(curLevel))) == -1) {
									GdxGame.files.setOptionsAndProgress(
											"levels",
											"level" + String.valueOf(curLevel),
											"0");
									levelsToOpened--;
								}
							} catch (NumberFormatException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							curLevel++;
						}
					}

					try {
						if (((levelNumb + 1) <= Constants.LEVELS_COUNT)
								&& Integer
										.parseInt(GdxGame.files.getOptionsAndProgress(
												"levels",
												"level"
														+ String.valueOf(levelNumb + 1))) != -1) {

							if (levelNumb == 24 || levelNumb == 48) {
								GdxGame.collectedStars = 0;

								for (int i = 1; i <= Constants.LEVELS_COUNT; i++) {
									int stars = Integer
											.parseInt(GdxGame.files.getOptionsAndProgress(
													"levels",
													"level" + String.valueOf(i)));
									if (stars != -1) {
										GdxGame.collectedStars += stars;
									}

								}
								if (levelNumb == 24
										&& menu_btn.isVisible()
										&& GdxGame.collectedStars >= Constants.STARS_TO_UNLOCK_SECOND_STAGE) {
									next_btn.setVisible(true);
								}
								if (levelNumb == 48
										&& menu_btn.isVisible()
										&& GdxGame.collectedStars >= Constants.STARS_TO_UNLOCK_THIRD_STAGE) {
									next_btn.setVisible(true);
								}
							} else if (menu_btn.isVisible()) {
								next_btn.setVisible(true);
							}
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (menu_btn.isVisible()) {
						retry_btn.setVisible(true);
					}
					for (int i = 1; i <= Constants.LEVELS_COUNT; i++) {
						try {
							collectedPoints += Integer.parseInt(GdxGame.files
									.getOptionsAndProgress("points", "level"
											+ String.valueOf(i)));
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}
					if (game.checkConnection()) {
						game.saveScore(collectedPoints);
					}
					flagGameAutoPauseDissabled = false;
				}
			};
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		thread.start();

		GdxGame.mInventory.setState(InvState.CLOSED);
		GdxGame.mInventory.setVisible(false);

		backgroundComplete.clearActions();
		backgroundComplete.getColor().a = 1;
		backgroundComplete.setVisible(true);
	}

	public void hideCompleteScreen() {
		AudioPlayer.pauseAllMusic();

		AudioPlayer.playMusic(currLevelMusic);

		GdxGame.mInventory.setVisible(true);
		backgroundComplete.getColor().a = 0;
		backgroundComplete.setVisible(false);
		menu_btn.setVisible(false);
		next_btn.setVisible(false);
		retry_btn.setVisible(false);
		shop_txt.setVisible(false);
		facebook.setVisible(false);
		twitter.setVisible(false);

		ingameShopContainer.setVisible(false);
		shop_left.setVisible(false);
		shop_right.setVisible(false);

		if (mCurrShopPage >= 0) {
			GameObject go = (GameObject) rootGroup
					.findActor(mShopList[mCurrShopPage]);
			go.setVisible(false);
		}

		failed.setVisible(false);

		// level.visible = false;
		// progress.visible = false;
		min.setVisible(false);
		complete.setVisible(false);

		levelNumber.setVisible(false);
		currentScore.setVisible(false);
		bestScore.setVisible(false);

		GdxGame.points.setVisible(true);
		GdxGame.points.setText("");
		GdxGame.level_numb.setVisible(true);

		setGamePause(false);

		endScreenProgressBar.resetCompleteAnim();
	}

	private static void changePopupMessageText(String name, String text,
			String price) {
		AnimatedTextGameObject aTgO = (AnimatedTextGameObject) rootGroup
				.findActor("name-yes-no");
		aTgO.setTextCenteredOnPosition(name, 200, 255, 480, false);

		aTgO = (AnimatedTextGameObject) rootGroup.findActor("text-yes-no");
		aTgO.setTextCenteredOnPosition(text, 200, 180, 330, false);

		aTgO = (AnimatedTextGameObject) rootGroup.findActor("_99price");
		aTgO.setTextCenteredOnPosition(price, 200, 80, 480, false);
	}

	public void showYesNoBox(int buttonId) {
		yes_no_back.setVisible(true);
		buy_btn.setVisible(true);
		no_btn.setVisible(true);
		backgroundYesNo.setVisible(true);

		switch (buttonId) {
		case 1:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_MUSTACHE,
					Texts.SHOP_TEXT_MUSTACHE, Texts.SHOP_TEXT_PRICE_MUSTACHE);
			break;

		case 2:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_LASER,
					Texts.SHOP_TEXT_LASER, Texts.SHOP_TEXT_PRICE_LASER);
			break;

		case 3:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_BREAKER,
					Texts.SHOP_TEXT_BREAKER, Texts.SHOP_TEXT_PRICE_BREAKER);
			break;

		case 4:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_BLASTER,
					Texts.SHOP_TEXT_BLASTER, Texts.SHOP_TEXT_PRICE_BLASTER);
			break;

		case 5:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_RADIUS,
					Texts.SHOP_TEXT_RADIUS, Texts.SHOP_TEXT_PRICE_RADIUS);
			break;

		case 6:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_EGG1,
					Texts.SHOP_TEXT_EGG1, Texts.SHOP_TEXT_PRICE_EGG1);
			break;

		case 7:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_EGG2,
					Texts.SHOP_TEXT_EGG2, Texts.SHOP_TEXT_PRICE_EGG2);
			break;

		case 8:
			changePopupMessageText(Texts.SHOP_TEXT_NAME_BUNDLE,
					Texts.SHOP_TEXT_BUNDLE, Texts.SHOP_TEXT_PRICE_BLASTER);
			break;

		default:
			break;
		}
	}

	public void hideYesNoBox() {
		yes_no_back.setVisible(false);
		buy_btn.setVisible(false);
		no_btn.setVisible(false);
		backgroundYesNo.setVisible(false);
		GdxGame.beetleStage.unfocusAll();
	}
}
