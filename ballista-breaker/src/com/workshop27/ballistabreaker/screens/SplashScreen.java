package com.workshop27.ballistabreaker.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.GameObject;
import com.workshop27.ballistabreaker.engine.ImageCache;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class SplashScreen implements Screen {
	private final SpriteBatch spriteBatch;
	private final GdxGame game;
	private final GameObject chillingo;
	private final Group rootGroup;
	private float timer = 6;
	private final GameObject moc;

	/**
	 * Constructor for the splash screen
	 * 
	 * @param g
	 *            Game which called this splash screen.
	 */
	public SplashScreen(GdxGame g) {
		spriteBatch = GdxGame.beetleStage.getSpriteBatch();
		this.game = g;
		rootGroup = new Group();
		rootGroup.setName("splashGroup");
		
		chillingo = new GameObject("chillingo", false, true);
		chillingo.setPositionXY(0, 0);
		chillingo.folderName = "splash";
		chillingo.loadSkinOnDraw = true;
		chillingo.getColor().a = 0;

		SequenceAction sequence = sequence(fadeIn(0.5f), delay(2f),
				fadeOut(0.5f));
		chillingo.addAction(sequence);
		rootGroup.addActor(chillingo);

		moc = new GameObject("moc", false, true);
		moc.setPositionXY(0, 0);
		moc.folderName = "splash";
		moc.loadSkinOnDraw = true;
		moc.getColor().a = 0;

		SequenceAction sequence2 = sequence(delay(3), fadeIn(0.5f),
				delay(2f), fadeOut(0.5f));
		moc.addAction(sequence2);
		rootGroup.addActor(moc);

		GdxGame.beetleStage.addActor(rootGroup);
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);

		Gdx.gl.glViewport(GdxGame.START_X, GdxGame.START_Y,
				GdxGame.SCREEN_WIDTH, GdxGame.SCREEN_HEIGHT);

		if (!ImageCache.getManager().update()) {
			return;
		}

		GdxGame.camera.update();
		spriteBatch.setProjectionMatrix(GdxGame.camera.combined);

		spriteBatch.begin();
		rootGroup.draw(spriteBatch, 1);
		spriteBatch.end();

		rootGroup.act(delta);

		timer = timer - Gdx.graphics.getDeltaTime();
		if (timer <= 0) {
			game.loadingScreen.setPrevScreenName("splash");
			game.setScreen(game.mainMenuScreen);
		}

	}

	@Override
	public void show() {

	}

	@Override
	public void resize(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		GdxGame.beetleStage.unfocusAll();

		rootGroup.clear();
		//GdxGame.beetleStage.removeActor(rootGroup);
		ImageCache.unload("splash");
		this.game.splashScreen = null;

	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {

	}

}
