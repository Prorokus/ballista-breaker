package com.workshop27.ballistabreaker.constants;

public class Constants {

	public static final int EXPLOSION_CHAIN_LENGTH_SMALL = 65;
	public static final int EXPLOSION_CHAIN_LENGTH_BIG = 90;
	public static final int EXPLOSION_CHAIN_LENGTH_BONUS = 100;

	public static final int BASE_EGGS_QUANTITY = 3;
	public static final int BONUS1_EGGS_QUANTITY = 1;
	public static final int BONUS2_EGGS_QUANTITY = 2;

	public static final int EGG_PONG_COUNTER = 4;

	public static int RELOADING_DELAY = 0;
	public static int BEETLE_SPEED = 30;

	public static final boolean IS_HD = false;
	public static final int LEVELS_COUNT = 72;
	public static final int STARS_TO_UNLOCK_SECOND_STAGE = 60;
	public static final int STARS_TO_UNLOCK_THIRD_STAGE = 120;
	public static final float KARATE_RADIUS = 90;
	public static final byte[] CRYPTO_KEY = { 1, 2, 3, 5, 4, 6, 5, 4, 1, 5, 4,
			2, 3, 5, 6, 2 };
	public static final String TWITTER_HASHTAG = "#bbreaker";
	public static final String PACKAGE_NAME = "data/data/com.masterofcode.android.bbreaker/files/";
	public static final long DAY_IN_MILLISECONDS = 86400000; // 24 hours
	public static final String FLURRY_API_KEY = "HV9278JNY65RX7487849";
	public static final String EULA_URL = "http://tos.ea.com/legalapp/mobileeula/US/en/OTHER/";
	public static final String PRIVACY_POLICY = "http://tos.ea.com/legalapp/WEBPRIVACY/US/en/PC/";
	public static final String TERMS_OF_SERVICE = "http://tos.ea.com/legalapp/WEBTERMS/US/en/PC/";
}
