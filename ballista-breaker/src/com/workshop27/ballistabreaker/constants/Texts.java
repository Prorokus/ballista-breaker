package com.workshop27.ballistabreaker.constants;

public class Texts {

	// LOOSE HELP
	public static final String LOOSE_HELP_TEXT = "Looks like the battle is going tough. Try using these extras. They will help!";

	// -----------------------------------------------------------------------------------------------------------------------------------------------------------
	// TUTORIAL MESSAGE
	// -----------------------------------------------------------------------------------------------------------------------------------------------------------
	public static final String TUTORIAL_MESSAGE_FB_AD = "You share - we thank you... by dropping awards into your inventory.";
	public static final String TUTORIAL_MESSAGE_STAGE_LEVEL = "Tap a leaf to select a level. Slide the stage to pass on to the next one.";
	public static final String TUTORIAL_MESSAGE_STAGE = "Collect stars to unlock new stage.";
	public static final String TUTORIAL_MESSAGE_ARROW = "Don't always pull to the max. Red and green colors on the arrow show draw strength.";
	public static final String TUTORIAL_MESSAGE_INV = "This is your inventory. All the good stuff your got or bought is here.";
	public static final String TUTORIAL_MESSAGE_BOMB_CONNECTION = "Eggs detonate when they are close enough to each other.";
	public static final String TUTORIAL_MESSAGE_MUSTACHE = "The longer beetles wait for their launch the stronger they will explode. Watch their moustache.";
	public static final String TUTORIAL_MESSAGE_WIN_SHARE = "Post your score on Facebook or Twitter.";
	public static final String TUTORIAL_MESSAGE_BONUS_BUG = "Get bonus beetles by exploding near them before they disappear.";
	public static final String TUTORIAL_MESSAGE_SHOP_BLASTER = "Blasters will sit in your inventory ready to be used.";
	public static final String TUTORIAL_MESSAGE_SHOP_KARATE = "Stub Text.";
	public static final String TUTORIAL_MESSAGE_SHOP_LASER = "Lazer will show exactly where you are aiming to shoot.";
	public static final String TUTORIAL_MESSAGE_SHOP_MUSTACHE = "Stub Text.";
	public static final String TUTORIAL_MESSAGE_SHOP_RADIUS = "Stub Text.";
	public static final String TUTORIAL_MESSAGE_SHOP_EGG1 = "Stub Text.";
	public static final String TUTORIAL_MESSAGE_SHOP_EGG2 = "Stub Text.";
	public static final String NINETY_PERSENT_MINIMUM = "Destroy at least 90% of the target to complete the level (watch the progress bar).";

	// BEETLE TUTORIAL
	// public static final String BEETLE_TUTORIAL_BOOMA =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";
	// public static final String BEETLE_TUTORIAL_EGGER =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";
	// public static final String BEETLE_TUTORIAL_MINER =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";
	// public static final String BEETLE_TUTORIAL_BLASTER =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";
	// public static final String BEETLE_TUTORIAL_BREAKER =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";

	// SHOP TEXT
	public static final String SHOP_TEXT_NAME_BLASTER = "Blaster";
	public static final String SHOP_TEXT_NAME_BREAKER = "Breaker";
	public static final String SHOP_TEXT_NAME_LASER = "Laser";
	public static final String SHOP_TEXT_NAME_EGG1 = "Magic Elixir";
	public static final String SHOP_TEXT_NAME_EGG2 = "Magic Elixir";
	public static final String SHOP_TEXT_NAME_MUSTACHE = "Magic Elixir";
	public static final String SHOP_TEXT_NAME_RADIUS = "Magic Elixir";
	public static final String SHOP_TEXT_NAME_BUNDLE = "Bundle";

	public static final String SHOP_TEXT_PRICE_BLASTER = "0.99$";
	public static final String SHOP_TEXT_PRICE_BREAKER = "0.99$";
	public static final String SHOP_TEXT_PRICE_LASER = "0.99$";
	public static final String SHOP_TEXT_PRICE_EGG1 = "0.99$";
	public static final String SHOP_TEXT_PRICE_EGG2 = "0.99$";
	public static final String SHOP_TEXT_PRICE_MUSTACHE = "0.99$";
	public static final String SHOP_TEXT_PRICE_RADIUS = "0.99$";
	public static final String SHOP_TEXT_PRICE_BUNDLE = "0.99$";

	public static final String SHOP_TEXT_QUANTITY_X5 = "x5";

	public static final String SHOP_TEXT_BLASTER = "Blaster is going to explode when you tap.";
	public static final String SHOP_TEXT_BREAKER = "Breaker does not stop for nothing and destroys everything on its way.";
	public static final String SHOP_TEXT_LASER = "Will help you aim exactly where you want the beetle to fly.";
	public static final String SHOP_TEXT_EGG1 = "Tasty elixir will add 1 more egg to Egger or Miner.";
	public static final String SHOP_TEXT_EGG2 = "Super tasty elixir will add 2 eggs to Egger or Miner.";
	public static final String SHOP_TEXT_MUSTACHE = "By drinking this magic juice your little guys will energize and explode 30% harder.";
	public static final String SHOP_TEXT_RADIUS = "Mines will connect at longer distances.";
	public static final String SHOP_TEXT_BUNDLE = "Our super pack! You'll find all the goodies nicely packed for you to destroy more and faster.";

	// SOCIAL TEXT
	// public static final String SOCIAL_TEXT_FB =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";
	// public static final String SOCIAL_TEXT_FB_SHARE =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";
	public static final String SOCIAL_TEXT_FB_REWARD = "You told your friends about us - we rewarded you for it! Find bonus in your inventory. It's all fair.";
	//
	// public static final String SOCIAL_TEXT_TWITTER =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";
	// public static final String SOCIAL_TEXT_TWITTER_SHARE =
	// "There are alot of good stuff in this bag. This survival bag will help you in any hard situation.";
	public static final String SOCIAL_TEXT_TWITTER_REWARD = "You told your friends about us - we rewarded you for it! Find bonus in your inventory. It's all fair.";

	// NOTIFICATION
	public static final String NOTIF_TEXT_REWARD = "You didn't forget about our game - we rewarded you for it! Find bonus in your inventory. It's all fair.";
}
