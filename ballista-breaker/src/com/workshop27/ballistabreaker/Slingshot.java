package com.workshop27.ballistabreaker;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.utils.Disposable;
import com.workshop27.ballistabreaker.engine.*;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle.BeetleState;
import com.workshop27.ballistabreaker.screens.GameScreen;

import java.util.Random;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class Slingshot implements Disposable {
	// Fields
	private static Random mGenerator = new Random();

	private static Stretch mRopeL;
	private static Stretch mRopeR;

	private static BombPlaceObject mBombPlace;

	private static GameObject mRopeBackL;
	private static GameObject mRopeBackR;

	private static DirrectionArrow mArrow;

	private static float mDragX = 0;
	private static float mDragY = 0;

	private static float mDropX = 0;
	private static float mDropY = 0;

	private static boolean mIsDragging = false;

	private static float mLaunchTimer = 0;

	private static Vector2 slingshotPower = new Vector2(0, 0);
	private static Vector2 minSlingshotPower = new Vector2(0,
			10 * GdxGame.HD_Y_RATIO);

	// private static float prevArgX = 0;
	// private static float prevArgY = 0;

	private static ArmStretch laser1;
	private static ArmStretch laser2;
	private static ArmStretch laser3;
	private static ArmStretch laser4;
	private static ArmStretch laser5;

	private static boolean mShopLaserEnabled = false;

	private static float bugWidth;

	private static float angle;

	private static float x1;
	private static float y1;
	private static float x2;
	private static float y2;

	private static float A;
	private static float B;
	private static float R;
	private static float X1;
	private static float Y1;
	private static float time;

	private static String currRopeSound;
	
	private static float treesStartY = 0;
	private static float castleStartY = 0;
	
	private static GameObject ballista_back;
	private static GameObject slingshot;

	// Methods
	public static int getRandomInt(int value) {
		return mGenerator.nextInt(value + 1);
	}

	public static float getBombX() {
		return mBombPlace.getPositionX();
	}

	public static float getBombY() {
		return mBombPlace.getPositionY();
	}

	public static float getBombAngle() {
		return mBombPlace.getRotation();
	}

	public static float getBombCenterX() {
		return mBombPlace.getCenter().x;
	}

	public static float getBombCenterY() {
		return mBombPlace.getCenter().y;
	}

	public static float getX1() {
		return x1;
	}

	public static float getY1() {
		return y1;
	}

	public static float getX2() {
		return x2;
	}

	public static float getY2() {
		return y2;
	}

	public static void init(GameScreen gs) {
		//mBombPlace = new BombPlaceObject("bombPlace", "slingshot-back", false);
		mBombPlace = new BombPlaceObject("bombPlace", "bombPlace", false);
		mBombPlace.setWidth(120);
		mBombPlace.setHeight(120);
		mBombPlace.setOriginX(60);
		mBombPlace.setOriginY(60);
		mBombPlace.folderName = "main";
		mBombPlace.setPositionXY(190, 130);
		mBombPlace.setOnCreateFilter(true);

		mRopeL = new Stretch("ropeL", "rope");
		mRopeL.setPositionXY(70, 220);
		//gs.addToRootGroup(mRopeL);

		mRopeL.setWidth(mBombPlace.getCenter().x);
		mRopeL.setHeight(mBombPlace.getCenter().y);

		mRopeR = new Stretch("ropeR", "rope");
		mRopeR.setPositionXY(415, 218);
		//gs.addToRootGroup(mRopeR);

		mRopeR.setWidth(mBombPlace.getCenter().x);
		mRopeR.setHeight(mBombPlace.getCenter().y);

		mArrow = new DirrectionArrow("arrow");
		mArrow.setPositionX(mBombPlace.getPositionX() + mBombPlace.getWidth()
				/ 8);
		mArrow.setPositionY(mBombPlace.getPositionY() + mBombPlace.getHeight()
				/ 2);
		mArrow.setVisible(false);
		mArrow.getColor().a = 0.3f;
		gs.addToRootGroup(mArrow);

		mRopeBackL = new GameObject("ropeBackL", "rope1", false);
		mRopeBackL.setPositionXY(46, 208);
		mRopeBackL.folderName = "main";
		mRopeBackL.setOnCreateFilter(true);
		mRopeBackL.setRotation(mRopeL.getRotation());
		mRopeBackL.setOriginX(25);
		mRopeBackL.setOriginY(12);
		//gs.addToRootGroup(mRopeBackL);
		
		ballista_back = new GameObject("ballista-back", "ballista-back", false);
		ballista_back.setPositionXY(128, 63);
		ballista_back.folderName = "main";
		ballista_back.setOnCreateFilter(true);
		gs.addToRootGroup(ballista_back);

		slingshot = new GameObject("slingshot", "slingshot", false);
		slingshot.setPositionXY(185, 47);
		slingshot.setOriginX(56);
		slingshot.setOriginY(180);
		slingshot.folderName = "main";
		slingshot.setOnCreateFilter(true);
		gs.addToRootGroup(slingshot);
		
		mRopeBackR = new GameObject("rope_back2", "rope1", false);
		mRopeBackR.setPositionXY(390, 206);
		mRopeBackR.folderName = "main";
		mRopeBackR.setOnCreateFilter(true);
		mRopeBackR.setRotation(mRopeR.getRotation() + 180);
		mRopeBackR.setOriginX(25);
		mRopeBackR.setOriginY(12);
		//gs.addToRootGroup(mRopeBackR);

		gs.addToRootGroup(mBombPlace);

		laser1 = new ArmStretch("laser1_stretch", "laser");
		laser2 = new ArmStretch("laser2_stretch", "laser");
		laser3 = new ArmStretch("laser3_stretch", "laser");
		laser4 = new ArmStretch("laser4_stretch", "laser");
		laser5 = new ArmStretch("laser5_stretch", "laser");

		gs.addToRootGroup(laser1);
		gs.addToRootGroup(laser2);
		gs.addToRootGroup(laser3);
		gs.addToRootGroup(laser4);
		gs.addToRootGroup(laser5);

	}

    public static void setBombPlaceTouchable(Touchable touchable){
        mBombPlace.setTouchable(touchable);
    }

	public static boolean bombTouchDown(float arg_x, float arg_y) {
		// Костыль чтобы жук не прыгал, не
		// срывался как только его взяли
		// if (Gdx.input.getY() < 600) {
		// return false;
		// }

		if (GameScreen.bManager.getActiveBeetle() != null
				&& GameScreen.bManager.getActiveBeetle().getState() == BeetleState.ACTIVE) {

			currRopeSound = "rope" + Slingshot.getRandomInt(1);
			AudioPlayer.playSound(currRopeSound, 0.25f);

			// bugWidth = GameScreen.bManager.getActiveBeetle().getWidth();
			bugWidth = 0;

			mBombPlace.clearActions();
			mBombPlace.setPositionXY(190, 130);

			mDragX = mBombPlace.getPositionX();
			mDragY = mBombPlace.getPositionY();
			
			treesStartY = 200;
			castleStartY = 0;
			
			mArrow.setVisible(true);

			mIsDragging = true;

			mArrow.setPositionXLiterally(mBombPlace.getPositionX()
					+ mBombPlace.getWidth() / 8);
			mArrow.setPositionYLiterally(mBombPlace.getPositionY()
					+ mBombPlace.getHeight() / 2);
			mArrow.setRotation(mBombPlace.getRotation());

			mArrow.setScaleX(1
					- (GdxGame.VIRTUAL_GAME_WIDTH - minSlingshotPower.len() * 3)
					/ GdxGame.VIRTUAL_GAME_WIDTH);
			mArrow.setScaleY(1
					- (GdxGame.VIRTUAL_GAME_HEIGHT - minSlingshotPower.len() * 8)
					/ GdxGame.VIRTUAL_GAME_HEIGHT);

			mArrow.updateColor();

            //Slingshot.setBombPlaceTouchable(Touchable.childrenOnly);

			return true;
		}

		return false;
	}

	public static void resetLaser() {
		laser1.setX(0);
		laser1.setY(0);
		laser1.setWidth(0);
		laser1.setHeight(0);

		laser2.setX(0);
		laser2.setY(0);
		laser2.setWidth(0);
		laser2.setHeight(0);

		laser3.setX(0);
		laser3.setY(0);
		laser3.setWidth(0);
		laser3.setHeight(0);

		laser4.setX(0);
		laser4.setY(0);
		laser4.setWidth(0);
		laser4.setHeight(0);

		laser5.setX(0);
		laser5.setY(0);
		laser5.setWidth(0);
		laser5.setHeight(0);

	}

	public static void bombTouchDragged(float arg_x, float arg_y) {
		// && (Math.abs(prevArgX - arg_x) > 2 || Math.abs(prevArgY - arg_y) > 2)
		if (mIsDragging) {
			// prevArgX = arg_x;
			// prevArgY = arg_y;

			x1 = Gdx.input.getX() * 480 * GdxGame.HD_X_RATIO
					/ Gdx.graphics.getWidth() - mBombPlace.getWidth() / 2;
			y1 = GdxGame.VIRTUAL_GAME_HEIGHT - Gdx.input.getY() * 800
					* GdxGame.HD_Y_RATIO / Gdx.graphics.getHeight()
					- mBombPlace.getHeight() / 2;

			x2 = mDragX;
			y2 = mDragY;

			// Gdx.app.log("x1:y1", x1 + " - " + y1);
			// Gdx.app.log("x2:y2", x2 + " - " + y2);

			R = 133;
			// Gdx.app.log("pifagor", (pifagor + ""));

			if (R < Math.sqrt(Math.pow(x2 - x1, 2) + Math.pow(y2 - y1, 2))) {

				A = (float) (x2 - (R * (x2 - x1) / Math.sqrt(Math.pow(
						(x2 - x1), 2) + Math.pow((y2 - y1), 2))));

				B = (y2 - y1) / (x2 - x1) * (A - x1) + y1;

				// Gdx.app.log("A:B", (A + " ; " + B));

				if ((x2 - x1) == 0) {
					B = -13;
				} else if ((A - B) == 0) {
					A = B = 0;

				}

				mBombPlace.setPositionXYLiterally(A, B);
			} else {

				mBombPlace.setPositionXYLiterally(x1, y1);
			}

			slingshotPower.x = (mDragX + mBombPlace.getWidth() / 2)
					- (mBombPlace.getPositionX() + mBombPlace.getWidth() / 2);

			slingshotPower.y = (mDragY + mBombPlace.getHeight() / 2 + 30 * GdxGame.HD_Y_RATIO)
					- (mBombPlace.getPositionY() + mBombPlace.getHeight() / 2);

			mBombPlace.setRotation(slingshotPower.angle() - 90);

			mArrow.setPositionXLiterally(mBombPlace.getPositionX() + mBombPlace.getWidth() / 8);
			mArrow.setPositionYLiterally(mBombPlace.getPositionY()
					+ mBombPlace.getHeight() / 2);

			mArrow.setRotation(mBombPlace.getRotation());

			mArrow.setScaleX(1
					- (GdxGame.VIRTUAL_GAME_WIDTH - (minSlingshotPower.len() + slingshotPower
							.len()) * 3) / GdxGame.VIRTUAL_GAME_WIDTH);
			mArrow.setScaleY(1
					- (GdxGame.VIRTUAL_GAME_HEIGHT - (minSlingshotPower.len() + slingshotPower
							.len()) * 8) / GdxGame.VIRTUAL_GAME_HEIGHT);

			mArrow.updateColor();
			
			if (mBombPlace.getPositionY() > mDragY + 30 * GdxGame.HD_Y_RATIO) {

				AudioPlayer.playSound("release_rope");
				AudioPlayer.stopSound(currRopeSound);

				mIsDragging = false;
				mDropX = mBombPlace.getPositionX();
				mDropY = mBombPlace.getPositionY();

				mArrow.setVisible(false);

				slingshotPower.x = mDragX - mDropX;
				slingshotPower.y = mDragY - mDropY;

				X1 = mDragX;
				Y1 = mDragY;

				time = 0;
				if (slingshotPower.len() > 50) {
					time = slingshotPower.len() / 350;
				} else {
					time = slingshotPower.len() / 50;
				}

				angle = (mBombPlace.getRotation() > 180) ? 360 : 0;

				mBombPlace.addAction(parallel(moveTo(X1, Y1, time), rotateTo(angle, time)));

				slingshotPower.x = 0;
				slingshotPower.y = 0;
				
				GameScreen.trees.clearActions();
				MoveToAction m1 = moveTo(GameScreen.trees.getPositionX(),treesStartY, 0.75f);
				GameScreen.trees.addAction(m1);

				GameScreen.castle.clearActions();
				MoveToAction m2 = moveTo(GameScreen.castle.getPositionX(), castleStartY, 0.75f);
				GameScreen.castle.addAction(m2);

				ballista_back.clearActions();
				MoveToAction m3 = moveTo(ballista_back.getPositionX(), 63, 0.75f);
				ballista_back.addAction(m3);
				
				slingshot.clearActions();
				MoveToAction m4 = moveTo(slingshot.getPositionX(), 47, 0.75f);
				slingshot.addAction(m4);
				
				RotateToAction r1 = rotateTo(0, 0.75f); 
				slingshot.addAction(r1); 
				
				return;
			}
			
			GameScreen.trees.clearActions();
			MoveToAction m1 = moveTo(GameScreen.trees.getPositionX(), treesStartY + (mDragY - mBombPlace.getPositionY()) * 0.1f, 0.05f);
			GameScreen.trees.addAction(m1);

			GameScreen.castle.clearActions();
			MoveToAction m2 = moveTo(GameScreen.castle.getPositionX(), castleStartY - (mDragY - mBombPlace.getPositionY()) * 0.45f, 0.05f);
			GameScreen.castle.addAction(m2);
			
			ballista_back.clearActions();
			MoveToAction m3 = moveTo(ballista_back.getPositionX(), 63 - (mDragY - mBombPlace.getPositionY()) * 0.45f, 0.05f);
			ballista_back.addAction(m3);
			
			slingshot.clearActions();
			MoveToAction m4 = moveTo(slingshot.getPositionX(), 47 - (mDragY - mBombPlace.getPositionY()) * 0.45f, 0.05f);
			slingshot.addAction(m4);
			slingshot.setRotation(mBombPlace.getRotation()); 
		}
	}

	public static void toggleShopLaser(boolean b) {
		mShopLaserEnabled = b;
	}

	public static boolean getShopLaserState() {
		return mShopLaserEnabled;
	}

	private static boolean checkLaserCollisionWithObject(Integer type,
			Vector2 vec, float x0, float y0, ArmStretch laser) {

		if (!(type != 1 && type != 3)) {
			Point p1 = intersection(x0, y0, vec.x, vec.y,
					GameScreen.objectToDestroy.getX(), GameScreen.objectToDestroy.getY(),
					GameScreen.objectToDestroy.getX()
							+ GameScreen.objectToDestroy.getWidth(),
					GameScreen.objectToDestroy.getY());

			Point p2 = intersection(x0, y0, vec.x, vec.y,
					GameScreen.objectToDestroy.getX(), GameScreen.objectToDestroy.getY(),
					GameScreen.objectToDestroy.getX(), GameScreen.objectToDestroy.getY()
							+ GameScreen.objectToDestroy.getHeight());

			Point p3 = intersection(x0, y0, vec.x, vec.y,
					GameScreen.objectToDestroy.getX(), GameScreen.objectToDestroy.getY()
							+ GameScreen.objectToDestroy.getHeight(),
					GameScreen.objectToDestroy.getX()
							+ GameScreen.objectToDestroy.getWidth(),
					GameScreen.objectToDestroy.getY()
							+ GameScreen.objectToDestroy.getHeight());

			Point p4 = intersection(x0, y0, vec.x, vec.y,
					GameScreen.objectToDestroy.getX()
							+ GameScreen.objectToDestroy.getWidth(),
					GameScreen.objectToDestroy.getY(), GameScreen.objectToDestroy.getX()
							+ GameScreen.objectToDestroy.getWidth(),
					GameScreen.objectToDestroy.getY()
							+ GameScreen.objectToDestroy.getHeight());

			if (p1 != null && p2 != null && p3 != null && p4 != null) {

				Vector2 vectorCopy = vec.cpy();
				float kof = 0.005f;
				vectorCopy = vectorCopy.mul(kof);

				while (Math.round(vectorCopy.x) != Math.round(vec.x)
						&& Math.round(vectorCopy.y) != Math.round(vec.y)) {
					if (GameScreen.objectToDestroy.checkLaserCollision(x0
							+ vectorCopy.x, y0 + vectorCopy.y, bugWidth,
							bugWidth)) {
						laser.setWidth(x0 + vectorCopy.x);
						laser.setHeight(y0 + vectorCopy.y);
						return true;
					} else {
						kof = kof + 0.005f;
						vectorCopy = vec.cpy();
						vectorCopy = vectorCopy.mul(kof);
					}
				}
			}

			return false;
		}
		return false;
	}

	public static void enableFlashlight(Integer type, float x1, float y1,
			float x2, float y2) {

		if (!mIsDragging
				|| !(mBombPlace.getPositionY() <= mDragY + 30
						* GdxGame.HD_Y_RATIO) || !mShopLaserEnabled) {

			return;
		}
		float x0 = mBombPlace.getPositionX() + mBombPlace.getWidth() / 2;
		float y0 = mBombPlace.getPositionY() + mBombPlace.getHeight() / 2;

		laser1.setX(x0);
		laser1.setY(y0);

		x1 = mDragX + mBombPlace.getWidth() / 2;
		y1 = mDragY + mBombPlace.getHeight() / 2 + 30 * GdxGame.HD_Y_RATIO;

		Vector2 vec;
		if (x1 - x0 == 0) {
			vec = new Vector2(x1 - x0 + 0.5f, y1 - y0);
		} else {
			vec = new Vector2(x1 - x0, y1 - y0);
		}
		vec = vec.mul(10);

		if (checkLaserCollisionWithObject(type, vec, x0, y0, laser1)) {
			return;
		}

		laser1.setWidth(x0 + vec.x);
		laser1.setHeight(y0 + vec.y);

		if (laser1.getWidth() <= 20) {
			Point p = intersection(laser1.getX(), laser1.getY(), laser1.getWidth(),
					laser1.getHeight(), 20.0f, 20.0f, 20.0f, 780.0f);
			x2 = p.x;
			y2 = p.y;

			// if (y2 > 781.0f) {
			// y2 = 781.0f;
			// }

			laser1.setWidth(x2);
			laser1.setHeight(y2);

			Vector2 firstLaserVector = new Vector2(x0 - x1, y0 - y1);
			Vector2 laserVector = new Vector2(x0 - x2, y0 - y2);

			if (!GameScreen.bManager.getActiveBeetleType().equals(5)) {
				addLaserFromWall(type, x2, y2, firstLaserVector,
						slingshotPower.len(), 1, laserVector.len());
			}

		} else if (laser1.getWidth() >= GdxGame.VIRTUAL_GAME_WIDTH - 20) {

			Point p = intersection(laser1.getX(), laser1.getY(), laser1.getWidth(),
					laser1.getHeight(), 460.0f, 20.0f, 460.0f, 780.0f);
			x2 = p.x;
			y2 = p.y;

			// if (y2 > 781.0f) {
			// y2 = 781.0f;
			// }

			laser1.setWidth(x2);
			laser1.setHeight(y2);

			Vector2 firstLaserVector = new Vector2(x0 - x1, y0 - y1);
			Vector2 laserVector = new Vector2(x0 - x2, y0 - y2);

			if (!GameScreen.bManager.getActiveBeetleType().equals(5)) {
				addLaserFromWall(type, x2, y2, firstLaserVector,
						slingshotPower.len(), 1, laserVector.len());
			}
		}
		if (laser1.getHeight() >= GdxGame.VIRTUAL_GAME_HEIGHT - 20) {

			Point p = intersection(laser1.getX(), laser1.getY(), laser1.getWidth(),
					laser1.getHeight(), 20.0f, 780.0f, 460.0f, 780.0f);
			x2 = p.x;
			y2 = p.y;

			// if (y2 > 781.0f) {
			// y2 = 781.0f;
			// }

			laser1.setWidth(x2);
			laser1.setHeight(y2);

			Vector2 firstLaserVector = new Vector2(x0 - x1, y0 - y1);
			Vector2 laserVector = new Vector2(x0 - x2, y0 - y2);

			if (!GameScreen.bManager.getActiveBeetleType().equals(5)) {
				addLaserFromWall(type, x2, y2, firstLaserVector,
						slingshotPower.len(), -1, laserVector.len());
			}
		} else if (laser1.getHeight() <= 20) {
			return;
		}
		// Gdx.app.log("laser1", "(" + laser1.x + "," + laser1.y + ";"
		// + laser1.width + " : " + laser1.height + ")");
	}

	private static Point calcNewVector(Vector2 laserVector, int roof,
			float power, float x, float y) {

		float dxn = MathUtils.cosDeg(-1 * laserVector.angle() - 90 * (roof))
				- MathUtils.sinDeg(-1 * laserVector.angle() - 90 * (roof))
				* power;
		float dyn = MathUtils.sinDeg(-1 * laserVector.angle() - 90 * (roof))
				+ MathUtils.cosDeg(-1 * laserVector.angle() - 90 * (roof))
				* power;

		float x1 = dxn + x;
		float y1 = dyn + y;

		if (x1 <= 20) {
			Point p = intersection(x, y, x1, y1, 20.0f, 20.0f, 20.0f, 780.0f);
			if (p == null) {
				return null;
			}
			x1 = p.x;
			y1 = p.y;
			roof = 1;

		} else if (x1 >= GdxGame.VIRTUAL_GAME_WIDTH - 20) {

			Point p = intersection(x, y, x1, y1, 460.0f, 20.0f, 460.0f, 780.0f);
			if (p == null) {
				return null;
			}
			x1 = p.x;
			y1 = p.y;
			roof = 1;

		}
		if (y1 >= GdxGame.VIRTUAL_GAME_HEIGHT - 20) {

			Point p = intersection(x, y, x1, y1, 20.0f, 780.0f, 460.0f, 780.0f);
			if (p == null) {
				return null;
			}

			x1 = p.x;
			y1 = p.y;
			roof = -1;

		} else if (y1 <= 20) {

			Point p = intersection(x, y, x1, y1, 20.0f, 20.0f, 460.0f, 20.0f);
			if (p == null) {
				return null;
			}

			x1 = p.x;
			y1 = p.y;
			roof = 0;
		}

		Vector2 way = new Vector2(x - x1, y - y1);
		return new Point(x1, y1, roof, way.len());

	}

	private static void addLaserFromWall(Integer type, float x2, float y2,
			Vector2 firstLaserVector, float power, int roof, float len) {
		power *= 10.5;

		laser2.setX(x2);
		laser2.setY(y2);

		power -= len;

		Point p = calcNewVector(firstLaserVector, roof, power, x2, y2);

		float x3 = 0;
		float y3 = 0;
		if (p == null) {
			laser2.setWidth(x2);
			laser2.setHeight(y2);
			return;

		} else {

			x3 = p.x;
			y3 = p.y;
		}

		// Gdx.app.log("p2.way", String.valueOf(p.way));

		if (power <= 0) {
			laser2.setWidth(x2);
			laser2.setHeight(y2);
			return;
		}

		power -= p.way;

		Vector2 first = new Vector2(x3 - x2, y3 - y2);

		Vector2 vectorCopy = first.cpy();

		if (checkLaserCollisionWithObject(type, vectorCopy, x2, y2, laser2)) {
			return;
		}

		laser2.setWidth(x3);
		laser2.setHeight(y3);
		// Gdx.app.log("laser2", "(" + laser2.x + "," + laser2.y + ";"
		// + laser2.width + " : " + laser2.height + ")");
		// ==============================================

		Point l21 = intersection(x2, y2, x3 * 10, y3 * 10, 0, 0, 480, 0);
		Point l22 = intersection(x2, y2, x3 * 10, y3 * 10, 0, 0, 0, 800);
		Point l23 = intersection(x2, y2, x3 * 10, y3 * 10, 480, 0, 480, 800);
		if (power <= 0
				|| p.roof == 0
				|| (l21 != null && l22 != null && l23 != null
						&& (l21.y > l22.y) && ((l21.y > l23.y)))) {

			laser3.setX(0);
			laser3.setY(0);
			laser3.setWidth(0);
			laser3.setHeight(0);

			laser4.setX(0);
			laser4.setY(0);
			laser4.setWidth(0);
			laser4.setHeight(0);

			laser5.setX(0);
			laser5.setY(0);
			laser5.setWidth(0);
			laser5.setHeight(0);

			return;
		}

		laser3.setX(x3);
		laser3.setY(y3);

		Vector2 secondLaserVector = new Vector2(x2 - x3, y2 - y3);
		p = calcNewVector(secondLaserVector, p.roof, power, x3, y3);

		float x4 = 0;
		float y4 = 0;
		if (p == null) {
			laser3.setWidth(x3);
			laser3.setHeight(y3);
			return;

		} else {

			x4 = p.x;
			y4 = p.y;
		}

		// Gdx.app.log("p3.way", String.valueOf(p.way));

		if (power <= 0) {
			laser3.setWidth(x3);
			laser3.setHeight(y3);
			return;
		}

		power -= p.way;

		Vector2 second = new Vector2(x4 - x3, y4 - y3);

		vectorCopy = second.cpy();

		if (checkLaserCollisionWithObject(type, vectorCopy, x3, y3, laser3)) {
			return;
		}

		laser3.setWidth(x4);
		laser3.setHeight(y4);
		// Gdx.app.log("laser3", "(" + laser3.x + "," + laser3.y + ";"
		// + laser3.width + " : " + laser3.height + ")");

		// =====================================================

		Point l31 = intersection(x3, y3, x4 * 10, y4 * 10, 0, 0, 480, 0);
		Point l32 = intersection(x3, y3, x4 * 10, y4 * 10, 0, 0, 0, 800);
		Point l33 = intersection(x3, y3, x4 * 10, y4 * 10, 480, 0, 480, 800);
		if (power <= 0
				|| p.roof == 0
				|| (l31 != null && l32 != null && l33 != null
						&& (l31.y > l32.y) && ((l31.y > l33.y)))) {

			laser4.setX(0);
			laser4.setY(0);
			laser4.setWidth(0);
			laser4.setHeight(0);

			laser5.setX(0);
			laser5.setY(0);
			laser5.setWidth(0);
			laser5.setHeight(0);

			return;
		}

		laser4.setX(x4);
		laser4.setY(y4);

		Vector2 thirdLaserVector = new Vector2(x3 - x4, y3 - y4);
		p = calcNewVector(thirdLaserVector, p.roof, power, x4, y4);

		float x5 = 0;
		float y5 = 0;
		if (p == null) {
			laser4.setWidth(x4);
			laser4.setHeight(y4);
			return;

		} else {

			x5 = p.x;
			y5 = p.y;
		}

		// Gdx.app.log("p4.way", String.valueOf(p.way));
		if (power <= 0) {
			laser4.setWidth(x4);
			laser4.setHeight(y4);
			return;
		}

		power -= p.way;

		Vector2 third = new Vector2(x5 - x4, y5 - y4);

		vectorCopy = third.cpy();

		if (checkLaserCollisionWithObject(type, vectorCopy, x4, y4, laser4)) {
			return;
		}

		laser4.setWidth(x5);
		laser4.setHeight(y5);

		// Gdx.app.log("laser4", "(" + laser4.x + "," + laser4.y + ";"
		// + laser4.width + " : " + laser4.height + ")");

		// Gdx.app.log("laser2", "(" + laser2.x + "," + laser2.y + ";"
		// + laser2.width + " : " + laser2.height + ")");
		// ==============================================

		Point l41 = intersection(x4, y4, x5 * 10, y5 * 10, 0, 0, 480, 0);
		Point l42 = intersection(x4, y4, x5 * 10, y5 * 10, 0, 0, 0, 800);
		Point l43 = intersection(x4, y4, x5 * 10, y5 * 10, 480, 0, 480, 800);
		if (power <= 0
				|| p.roof == 0
				|| (l41 != null && l42 != null && l43 != null
						&& (l41.y > l42.y) && ((l41.y > l43.y)))) {
			laser5.setX(0);
			laser5.setY(0);
			laser5.setWidth(0);
			laser5.setHeight(0);
			return;
		}

		laser5.setX(x5);
		laser5.setY(y5);

		Vector2 fourLaserVector = new Vector2(x4 - x5, y4 - y5);
		p = calcNewVector(fourLaserVector, p.roof, power, x5, y5);

		float x6 = 0;
		float y6 = 0;
		if (p == null) {
			laser5.setWidth(x5);
			laser5.setHeight(y5);
			return;

		} else {

			x6 = p.x;
			y6 = p.y;
		}

		// Gdx.app.log("p3.way", String.valueOf(p.way));

		if (power <= 0) {
			laser5.setWidth(x5);
			laser5.setHeight(y5);
			return;
		}

		power -= p.way;

		Vector2 four = new Vector2(x6 - x5, y6 - y5);
		vectorCopy = four.cpy();

		if (checkLaserCollisionWithObject(type, vectorCopy, x5, y5, laser5)) {
			return;
		}

		laser5.setWidth(x6);
		laser5.setHeight(y6);

		// Gdx.app.log("laser3", "(" + laser3.x + "," + laser3.y + ";"
		// + laser3.width + " : " + laser3.height + ")");

	}

	public static Vector2 getSlingshotPower() {
		return slingshotPower;
	}

	public static void bombTouchUp(float arg_x, float arg_y) {
		if (mIsDragging) {
			Gdx.app.log(
					"x:y",
					mBombPlace.getPositionX() + " : "
							+ (mBombPlace.getPositionY()));
			AudioPlayer.stopSound(currRopeSound);
			mIsDragging = false;
			mDropX = mBombPlace.getPositionX();
			mDropY = mBombPlace.getPositionY();

			// mDropX = 233;
			// mDropY = 9;

			mArrow.setVisible(false);

			mDragY += 30 * GdxGame.HD_Y_RATIO;
			Vector2 v = new Vector2(mDragX - mDropX, Math.abs(mDragY - mDropY));
			Vector2 v_small = new Vector2(v.x / 10 * GdxGame.HD_X_RATIO, v.y
					/ 10 * GdxGame.HD_Y_RATIO);
			float X1 = mDragX;
			float Y1 = mDragY;

			mLaunchTimer = v.len() / 1250 * GdxGame.HD_X_RATIO;

			GameScreen.trees.clearActions();
			MoveToAction m1 = moveTo(GameScreen.trees.getPositionX(),treesStartY, 0.75f);
			GameScreen.trees.addAction(m1);

			GameScreen.castle.clearActions();
			MoveToAction m2 = moveTo(GameScreen.castle.getPositionX(), castleStartY, 0.75f);
			GameScreen.castle.addAction(m2);
			
			ballista_back.clearActions();
			MoveToAction m3 = moveTo(ballista_back.getPositionX(), 63, 0.75f);
			ballista_back.addAction(m3);
			
			slingshot.clearActions();
			MoveToAction m4 = moveTo(slingshot.getPositionX(), 47, 0.75f);
			slingshot.addAction(m4);
			
			RotateToAction r1 = rotateTo(0, 0.75f); 
			slingshot.addAction(r1);
			
			if (v.len() < 15 * GdxGame.HD_Y_RATIO) {
				AudioPlayer.playSound("release_rope");

				mIsDragging = false;
				mDropX = mBombPlace.getPositionX();
				mDropY = mBombPlace.getPositionY();

				mArrow.setVisible(false);

				slingshotPower.x = mDragX - mDropX;
				slingshotPower.y = mDragY - mDropY;

				float xx1 = mDragX;
				float yy1 = mDragY;

				float time = 0;
				if (slingshotPower.len() > 50) {
					time = slingshotPower.len() / 350;
				} else {
					time = slingshotPower.len() / 50;
				}

				MoveToAction move_slingshot1 = moveTo(xx1, yy1, time);
				mBombPlace.addAction(parallel(move_slingshot1, rotateTo(0, time)));
			}

			else {
				resetLaser();

				AudioPlayer.playSound("shoot" + Slingshot.getRandomInt(11));

				moveBombBackAnim(v, v_small, X1, Y1);
				GameScreen.bManager.getActiveBeetle().setState(
						BeetleState.ANIM_TO_FLY);

				slingshotPower.x = 0;
				slingshotPower.y = 0;
			}
		}
	}

	private static void moveBombBackAnim(Vector2 v, Vector2 v_small, float X1,
			float Y1) {

		GameScreen.bManager.getActiveBeetle().beetleSpeedX = v.x
				* GameScreen.bManager.getActiveBeetle().speedCoeficient;

		GameScreen.bManager.getActiveBeetle().beetleSpeedY = v.y
				* GameScreen.bManager.getActiveBeetle().speedCoeficient;

		MoveToAction move_slingshot1 = moveTo(X1 + v_small.x, Y1, mLaunchTimer);

		MoveToAction move_slingshot2 = moveTo(X1 - v_small.x, Y1 - v_small.y - 30 * GdxGame.HD_Y_RATIO, v_small.len() / 200);

		MoveToAction move_slingshot3 = moveTo(X1, Y1 - 30 * GdxGame.HD_Y_RATIO, v_small.len() / 200);

		mBombPlace.addAction(parallel(sequence(move_slingshot1, move_slingshot2, move_slingshot3), rotateTo(0, mLaunchTimer)));
	}

	public static void animateArmed() {
		MoveToAction move_slingshot1 = moveTo(mBombPlace.getPositionX(), 130 * GdxGame.HD_Y_RATIO - 10 * GdxGame.HD_Y_RATIO, 0.3f);

		MoveToAction move_slingshot2 = moveTo(mBombPlace.getPositionX(), 130 * GdxGame.HD_Y_RATIO, 0.3f);

		mBombPlace.addAction(sequence(move_slingshot1, move_slingshot2));
	}

	public static void think(float delta) {
		mRopeL.setWidth(mBombPlace.getCenter().x);
		mRopeL.setHeight(mBombPlace.getCenter().y - 5 * GdxGame.HD_X_RATIO);

		mRopeR.setWidth(mBombPlace.getCenter().x);
		mRopeR.setHeight(mBombPlace.getCenter().y - 5 * GdxGame.HD_Y_RATIO);

		mRopeBackL.setRotation(mRopeL.getRotation());
		mRopeBackR.setRotation(mRopeR.getRotation());

		if (mLaunchTimer > 0) {
			mLaunchTimer -= delta;
		} else if (GameScreen.bManager.getActiveBeetle() != null
				&& GameScreen.bManager.getActiveBeetle().getState() == BeetleState.ANIM_TO_FLY) {
			GameScreen.bManager.getActiveBeetle().setState(BeetleState.INFLY);
			GameScreen.bManager.getActiveBeetle().createParticle("p4", true);

			AudioPlayer.PlayRandomLaunchBeetleSound(GameScreen.bManager
					.getActiveBeetle().getType());
		}
	}

	@Override
	public void dispose() {

	}

	public static Point intersection(float x1, float y1, float x2, float y2,
			float x3, float y3, float x4, float y4) {
		float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4);
		if (d == 0) {
			return null;
		}

		float xi = ((x3 - x4) * (x1 * y2 - y1 * x2) - (x1 - x2)
				* (x3 * y4 - y3 * x4))
				/ d;
		float yi = ((y3 - y4) * (x1 * y2 - y1 * x2) - (y1 - y2)
				* (x3 * y4 - y3 * x4))
				/ d;

		return new Point(xi, yi, 0, 0);
	}

	private static class Point {

		private final float x;
		private final float y;
		private final int roof;
		private final float way;

		public Point(float x1, float y1, int roof, float way) {
			this.x = x1;
			this.y = y1;
			this.roof = roof;
			this.way = way;
		}

	}
}
