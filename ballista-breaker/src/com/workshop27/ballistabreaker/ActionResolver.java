package com.workshop27.ballistabreaker;

import java.util.Map;

public interface ActionResolver {
	public void bootstrap();

	public void showScoreloop();

	public void submitScore(int score);

	public void refreshScores();

	public boolean checkInetConnection();

	public void startFacebook(String message, int action);

	public void startTwitter(String message, int action);

	public void startFlurry();

	public void stopFlurry();

	public void flurryAgentLogEvent(String eventId);

	public void flurryAgentLogEvent(String eventId, boolean timed);

	public void flurryAgentLogEvent(String eventId,
			Map<String, String> parameters);

	public void flurryAgentLogEvent(String eventId,
			Map<String, String> parameters, boolean timed);

	public void flurryAgentEndTimedEvent(String eventId);

	public void flurryAgentOnPageView();

	public void startBrowser(String url);

}