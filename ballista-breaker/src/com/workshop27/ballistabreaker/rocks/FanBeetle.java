package com.workshop27.ballistabreaker.rocks;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class FanBeetle extends AbstractBeetle {
	private final ArrayList<String> names;

	private Vector2 legsXY;
	private Vector2 musXY;
	private Vector2 eyesXY;
	private Vector2 closedXY;
	private Vector2 leftBackXY;
	private Vector2 rightBackXY;
	private Vector2 leftWingXY;
	private Vector2 rightWingXY;
	private Vector2 eyesClosedXY;

	private Vector2 legsWH;
	private Vector2 musWH;
	private Vector2 eyesWH;
	private Vector2 closedWH;
	private Vector2 leftBackWH;
	private Vector2 rightBackWH;
	private Vector2 leftWingWH;
	private Vector2 rightWingWH;

	private Vector2 legsOrXY;
	private Vector2 musOrXY;
	private Vector2 eyesOrXY;
	private Vector2 closedOrXY;
	private Vector2 leftBackOrXY;
	private Vector2 rightBackOrXY;
	private Vector2 leftWingOrXY;
	private Vector2 rightWingOrXY;

	private TextureRegion legsRegion;
	private TextureRegion musRegion;
	private TextureRegion eyesRegion;
	private TextureRegion closedRegion;
	private TextureRegion leftBackRegion;
	private TextureRegion rightBackRegion;
	private TextureRegion leftWingRegion;
	private TextureRegion rightWingRegion;

	private boolean drawClosed = true;
	private float powerLen;
	private float eyesScale;
	private float eyesScaleX;
	private float eyesScaleY;

	private final float musScaleXY = 1;

	@Override
	public ArrayList<String> getNames() {
		return names;
	}

	public FanBeetle(String name, String skinName,
			HashMap<String, Integer> params, ArrayList<String> names) {
		super(name, skinName);
		super.init(params);
		this.names = names;

		super.skinName = "";

		setWidth(54);
		setHeight(39);

		this.activeState = BeetleState.SLEEP;
	}

	@Override
	public void onStateChanged(BeetleState newState) {
		if (newState == BeetleState.ACTIVE) {
			eyesScaleX = 1;
			eyesScaleY = 1;
		}

		if (newState == BeetleState.ANIM_TO_FLY) {
			eyesScaleX += Slingshot.getSlingshotPower().len() / 500;
			eyesScaleY = eyesScaleX;
		}

		if (newState == BeetleState.INFLY) {
			this.drawClosed = false;
		} else {
			this.drawClosed = true;
		}

		super.onStateChanged(newState);
	}

	@Override
	public void clearSkin() {
		this.legsRegion = null;
		this.musRegion = null;
		this.eyesRegion = null;
		this.closedRegion = null;
		this.leftBackRegion = null;
		this.rightBackRegion = null;
		this.leftWingRegion = null;
		this.rightWingRegion = null;

	}

	@Override
	public void setSkin() {
		this.legsRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("fan-legs");

		this.musRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("fan-mus");

		this.eyesRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("fan-eyes");

		this.closedRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("fan-closed");

		this.leftBackRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("fan-left-back");

		this.rightBackRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("fan-right-back");

		this.leftWingRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("fan-left-wing");

		this.rightWingRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("fan-right-wing");

		legsXY = new Vector2(0, 0);
		musXY = new Vector2(11 * GdxGame.HD_X_RATIO, 17 * GdxGame.HD_Y_RATIO);
		eyesXY = new Vector2(10 * GdxGame.HD_X_RATIO, 24 * GdxGame.HD_Y_RATIO);
		eyesClosedXY = new Vector2(10 * GdxGame.HD_X_RATIO,
				24 * GdxGame.HD_Y_RATIO);
		closedXY = new Vector2(6 * GdxGame.HD_X_RATIO, -8 * GdxGame.HD_Y_RATIO);
		leftBackXY = new Vector2(-18 * GdxGame.HD_X_RATIO,
				10 * GdxGame.HD_Y_RATIO);
		rightBackXY = new Vector2(30 * GdxGame.HD_X_RATIO,
				10 * GdxGame.HD_Y_RATIO);
		leftWingXY = new Vector2(-18 * GdxGame.HD_X_RATIO,
				9 * GdxGame.HD_Y_RATIO);
		rightWingXY = new Vector2(30 * GdxGame.HD_X_RATIO,
				9 * GdxGame.HD_Y_RATIO);

		legsWH = new Vector2(legsRegion.getRegionWidth(),
				legsRegion.getRegionHeight());
		musWH = new Vector2(musRegion.getRegionWidth(),
				musRegion.getRegionHeight());
		eyesWH = new Vector2(eyesRegion.getRegionWidth(),
				eyesRegion.getRegionHeight());
		closedWH = new Vector2(closedRegion.getRegionWidth(),
				closedRegion.getRegionHeight());
		leftBackWH = new Vector2(leftBackRegion.getRegionWidth(),
				leftBackRegion.getRegionHeight());
		rightBackWH = new Vector2(rightBackRegion.getRegionWidth(),
				rightBackRegion.getRegionHeight());
		leftWingWH = new Vector2(leftWingRegion.getRegionWidth(),
				leftWingRegion.getRegionHeight());
		rightWingWH = new Vector2(rightWingRegion.getRegionWidth(),
				rightWingRegion.getRegionHeight());

		legsOrXY = new Vector2(legsWH.x / 2, legsWH.y / 2);
		musOrXY = new Vector2(legsOrXY.x - musXY.x, legsOrXY.y - musXY.y);
		eyesOrXY = new Vector2(legsOrXY.x - eyesXY.x, legsOrXY.y - eyesXY.y);
		closedOrXY = new Vector2(legsOrXY.x - closedXY.x, legsOrXY.y
				- closedXY.y);

		leftBackOrXY = new Vector2(legsOrXY.x - leftBackXY.x, legsOrXY.y
				- leftBackXY.y);
		rightBackOrXY = new Vector2(legsOrXY.x - rightBackXY.x, legsOrXY.y
				- rightBackXY.y);
		leftWingOrXY = new Vector2(legsOrXY.x - leftWingXY.x, legsOrXY.y
				- leftWingXY.y);
		rightWingOrXY = new Vector2(legsOrXY.x - rightWingXY.x, legsOrXY.y
				- rightWingXY.y);
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(this.getColor());

		batch.draw(this.legsRegion, getX() + legsXY.x, getY() + legsXY.y, legsOrXY.x,
				legsOrXY.y, legsWH.x, legsWH.y, getScaleX(), getScaleY(), getRotation());

		if (this.drawClosed) {
			batch.draw(this.closedRegion, getX() + closedXY.x, getY() + closedXY.y,
					closedOrXY.x, closedOrXY.y, closedWH.x, closedWH.y, getScaleX(),
					getScaleY(), getRotation());
		} else {
			batch.draw(this.leftWingRegion, getX() + leftWingXY.x, getY() + leftWingXY.y,
					leftWingOrXY.x, leftWingOrXY.y, leftWingWH.x, leftWingWH.y,
					getScaleX(), getScaleY(), getRotation());

			batch.draw(this.rightWingRegion, getX() + rightWingXY.x, getY()
					+ rightWingXY.y, rightWingOrXY.x, rightWingOrXY.y,
					rightWingWH.x, rightWingWH.y, getScaleX(), getScaleY(), getRotation());

			batch.draw(this.leftBackRegion, getX() + leftBackXY.x, getY() + leftBackXY.y,
					leftBackOrXY.x, leftBackOrXY.y, leftBackWH.x, leftBackWH.y,
					getScaleX(), getScaleY(), getRotation());

			batch.draw(this.rightBackRegion, getX() + rightBackXY.x, getY()
					+ rightBackXY.y, rightBackOrXY.x, rightBackOrXY.y,
					rightBackWH.x, rightBackWH.y, getScaleX(), getScaleY(), getRotation());
		}

		if (this.inRage) {
			batch.draw(this.musRegion, getX() + musXY.x, getY() + musXY.y, musOrXY.x,
					musOrXY.y, musWH.x, musWH.y, musScaleXY, musScaleXY,
					getRotation());
		}

		powerLen = Slingshot.getSlingshotPower().len();
		if (powerLen >= 0 && this.drawClosed
				&& getState() != BeetleState.ANIM_TO_FLY) {
			eyesScale = getScaleX() + powerLen / 500;
			// if (getState() == BeetleState.BONUS) {
			// eyesScale = scaleX;
			// }

			batch.draw(this.eyesRegion, getX() + eyesXY.x, getY() + eyesXY.y, eyesOrXY.x,
					eyesOrXY.y, eyesWH.x, eyesWH.y, eyesScale, eyesScale,
					getRotation());
		} else {
			if (eyesScaleX > 1) {
				eyesScaleX -= 0.25f * Gdx.graphics.getDeltaTime();
				eyesScaleY -= 0.25f * Gdx.graphics.getDeltaTime();
			}

			batch.draw(this.eyesRegion, getX() + eyesXY.x, getY() + eyesXY.y, eyesOrXY.x,
					eyesOrXY.y, eyesWH.x, eyesWH.y, eyesScaleX, eyesScaleY,
					getRotation());
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.legsRegion != null && this.musRegion != null
				&& this.eyesRegion != null && this.closedRegion != null
				&& this.leftBackRegion != null && this.rightBackRegion != null
				&& this.leftWingRegion != null && this.rightWingRegion != null) {

			if (getState() == BeetleState.BREAKING
					&& !GameScreen.bManager.isEggsInFly(this)) {
				setState(BeetleState.EXPLODE);
			}

			if (getState() == BeetleState.SLEEP
					|| getState() == BeetleState.EXPLODE
					|| getState() == BeetleState.BREAKING
					|| getState() == BeetleState.HIDDEN) {
				drawParticles(batch, parentAlpha);
				return;
			}

			drawParticles(batch, parentAlpha);
			// super.draw(batch, parentAlpha);
			this.drawSkin(batch, parentAlpha);

			super.fly();
		}
	}

}
