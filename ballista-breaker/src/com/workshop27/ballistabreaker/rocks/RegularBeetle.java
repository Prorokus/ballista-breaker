package com.workshop27.ballistabreaker.rocks;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class RegularBeetle extends AbstractBeetle {
	private Vector2 legsXY;
	private Vector2 musXY;
	private Vector2 eyesXY;
	private Vector2 openedXY;
	private Vector2 closedXY;
	private Vector2 backXY;

	private Vector2 legsWH;
	private Vector2 musWH;
	private Vector2 eyesWH;
	private Vector2 openedWH;
	private Vector2 closedWH;
	private Vector2 backWH;

	private Vector2 legsOrXY;
	private Vector2 musOrXY;
	private Vector2 eyesOrXY;
	private Vector2 openedOrXY;
	private Vector2 closedOrXY;
	private Vector2 backOrXY;

	private TextureRegion legsRegion;
	private TextureRegion musRegion;
	private TextureRegion eyesRegion;
	private TextureRegion openedRegion;
	private TextureRegion closedRegion;
	private TextureRegion backRegion;

	private boolean drawClosed = true;
	private float powerLen;
	private float eyesScale;
	private float eyesScaleX;
	private float eyesScaleY;

	public float musScaleXY = 1;

	public RegularBeetle(String name, String skinName,
			HashMap<String, Integer> params) {
		super(name, skinName);
		super.init(params);

		super.skinName = "";

		setWidth(58);
		setHeight(41);

		this.activeState = BeetleState.SLEEP;
	}

	@Override
	public void onStateChanged(BeetleState newState) {
		if (newState == BeetleState.ACTIVE) {
			eyesScaleX = 1;
			eyesScaleY = 1;
		}

		if (newState == BeetleState.ANIM_TO_FLY) {
			eyesScaleX += Slingshot.getSlingshotPower().len() / 500;
			eyesScaleY = eyesScaleX;
		}

		if (newState == BeetleState.INFLY) {
			this.drawClosed = false;
		} else {
			this.drawClosed = true;
		}

		super.onStateChanged(newState);
	}

	@Override
	public void clearSkin() {
		this.legsRegion = null;
		this.musRegion = null;
		this.eyesRegion = null;
		this.openedRegion = null;
		this.closedRegion = null;
		this.backRegion = null;
	}

	@Override
	public void setSkin() {
		this.legsRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("bomb1");

		this.musRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("regular-mus");

		this.eyesRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("regular-eyes");

		this.openedRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("regular-opened");

		this.closedRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("regular-closed");
		this.backRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("regular-back");

		legsXY = new Vector2(0, 0);
		musXY = new Vector2(10 * GdxGame.HD_X_RATIO, 5 * GdxGame.HD_Y_RATIO);
		eyesXY = new Vector2(13 * GdxGame.HD_X_RATIO, 29 * GdxGame.HD_Y_RATIO);
		openedXY = new Vector2(6 * GdxGame.HD_X_RATIO, -2 * GdxGame.HD_Y_RATIO);
		closedXY = new Vector2(7 * GdxGame.HD_X_RATIO, -2 * GdxGame.HD_Y_RATIO);
		backXY = new Vector2(15 * GdxGame.HD_X_RATIO, 28 * GdxGame.HD_Y_RATIO);

		legsWH = new Vector2(legsRegion.getRegionWidth(),
				legsRegion.getRegionHeight());
		musWH = new Vector2(musRegion.getRegionWidth(),
				musRegion.getRegionHeight());
		eyesWH = new Vector2(eyesRegion.getRegionWidth(),
				eyesRegion.getRegionHeight());
		openedWH = new Vector2(openedRegion.getRegionWidth(),
				openedRegion.getRegionHeight());
		closedWH = new Vector2(closedRegion.getRegionWidth(),
				closedRegion.getRegionHeight());
		backWH = new Vector2(backRegion.getRegionWidth(),
				backRegion.getRegionHeight());

		legsOrXY = new Vector2(legsWH.x / 2, legsWH.y / 2);
		musOrXY = new Vector2(legsOrXY.x - musXY.x, legsOrXY.y - musXY.y);
		eyesOrXY = new Vector2(legsOrXY.x - eyesXY.x, legsOrXY.y - eyesXY.y);
		openedOrXY = new Vector2(legsOrXY.x - openedXY.x, legsOrXY.y
				- openedXY.y);
		closedOrXY = new Vector2(legsOrXY.x - closedXY.x, legsOrXY.y
				- closedXY.y);
		backOrXY = new Vector2(legsOrXY.x - backXY.x, legsOrXY.y - backXY.y);
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(this.getColor());

		batch.draw(this.legsRegion, getX() + legsXY.x, getY() + legsXY.y, legsOrXY.x,
				legsOrXY.y, legsWH.x, legsWH.y, getScaleX(), getScaleY(), getRotation());

		/*batch.draw(this.backRegion, getX() + backXY.x, getY() + backXY.y, backOrXY.x,
				backOrXY.y, backWH.x, backWH.y, getScaleX(), getScaleY(), getRotation());

		if (this.drawClosed) {
			batch.draw(this.closedRegion, getX() + closedXY.x, getY() + closedXY.y,
					closedOrXY.x, closedOrXY.y, closedWH.x, closedWH.y, getScaleX(),
					getScaleY(), getRotation());
		} else {
			batch.draw(this.openedRegion, getX() + openedXY.x, getY() + openedXY.y,
					openedOrXY.x, openedOrXY.y, openedWH.x, openedWH.y, getScaleX(),
					getScaleY(), getRotation());
		}

		if (this.inRage) {
			batch.draw(this.musRegion, getX() + musXY.x, getY() + musXY.y, musOrXY.x,
					musOrXY.y, musWH.x, musWH.y, musScaleXY, musScaleXY,
					getRotation());
		}

		powerLen = Slingshot.getSlingshotPower().len();
		if (powerLen >= 0 && this.drawClosed
				&& getState() != BeetleState.ANIM_TO_FLY) {
			eyesScale = getScaleX() + powerLen / 500;

			// if (getState() == BeetleState.BONUS) {
			// eyesScale = scaleX;
			// }

			batch.draw(this.eyesRegion, getX() + eyesXY.x, getY() + eyesXY.y, eyesOrXY.x,
					eyesOrXY.y, eyesWH.x, eyesWH.y, eyesScale, eyesScale,
					getRotation());
		} else {
			if (eyesScaleX > 1) {
				eyesScaleX -= 0.25f * Gdx.graphics.getDeltaTime();
				eyesScaleY -= 0.25f * Gdx.graphics.getDeltaTime();
			}

			batch.draw(this.eyesRegion, getX() + eyesXY.x, getY() + eyesXY.y, eyesOrXY.x,
					eyesOrXY.y, eyesWH.x, eyesWH.y, eyesScaleX, eyesScaleY,
					getRotation());
		}*/
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.legsRegion != null && this.musRegion != null
				&& this.eyesRegion != null && this.openedRegion != null
				&& this.closedRegion != null && this.backRegion != null) {

			if (getState() == BeetleState.BREAKING
					&& !GameScreen.bManager.isEggsInFly(this)) {
				setState(BeetleState.EXPLODE);
			}

			if (getState() == BeetleState.SLEEP
					|| getState() == BeetleState.EXPLODE
					|| getState() == BeetleState.BREAKING
					|| getState() == BeetleState.HIDDEN) {
				drawParticles(batch, parentAlpha);
				return;
			}

			drawParticles(batch, parentAlpha);
			// super.draw(batch, parentAlpha);
			this.drawSkin(batch, parentAlpha);

			super.fly();
		}
	}

	@Override
	public ArrayList<String> getNames() {
		// TODO Auto-generated method stub
		return null;
	}

}
