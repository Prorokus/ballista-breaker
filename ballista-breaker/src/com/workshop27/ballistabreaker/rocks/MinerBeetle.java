package com.workshop27.ballistabreaker.rocks;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class MinerBeetle extends AbstractBeetle {
	private final ArrayList<String> name2;

	private Vector2 musXY;
	private Vector2 eyesXY;
	private Vector2 closedXY;

	private Vector2 musWH;
	private Vector2 eyesWH;
	private Vector2 closedWH;

	private Vector2 musOrXY;
	private Vector2 eyesOrXY;
	private Vector2 closedOrXY;

	private TextureRegion musRegion;
	private TextureRegion eyesRegion;
	private TextureRegion closedRegion;

	private float powerLen;
	private float eyesScale;
	private float eyesScaleX;
	private float eyesScaleY;

	private boolean drawClosed = true;

	private final float musScaleXY = 1;

	public MinerBeetle(String name, String skinName,
			HashMap<String, Integer> params, ArrayList<String> names) {
		super(name, skinName);
		super.init(params);
		this.name2 = names;

		super.skinName = "";

		setWidth(48);
		setHeight(58);

		this.activeState = BeetleState.SLEEP;
	}

	public ArrayList<String> getName2() {
		return this.name2;
	}

	@Override
	public void clearSkin() {
		this.musRegion = null;
		this.eyesRegion = null;
		this.closedRegion = null;
	}

	@Override
	public void setSkin() {
		this.musRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("miner-mus");

		this.eyesRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("miner-eyes");

		this.closedRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("miner-closed");

		this.musXY = new Vector2(1 * GdxGame.HD_X_RATIO,
				35 * GdxGame.HD_Y_RATIO);
		this.eyesXY = new Vector2(2 * GdxGame.HD_X_RATIO,
				39 * GdxGame.HD_Y_RATIO);
		this.closedXY = new Vector2(0, 0);

		this.musWH = new Vector2(this.musRegion.getRegionWidth(),
				this.musRegion.getRegionHeight());
		this.eyesWH = new Vector2(this.eyesRegion.getRegionWidth(),
				this.eyesRegion.getRegionHeight());
		this.closedWH = new Vector2(this.closedRegion.getRegionWidth(),
				this.closedRegion.getRegionHeight());

		this.closedOrXY = new Vector2(closedWH.x / 2, closedWH.y / 2);
		this.musOrXY = new Vector2(closedOrXY.x - musXY.x, closedOrXY.y
				- musXY.y);
		this.eyesOrXY = new Vector2(closedOrXY.x - eyesXY.x, closedOrXY.y
				- eyesXY.y);
	}

	@Override
	public void onStateChanged(BeetleState newState) {
		if (newState == BeetleState.ACTIVE) {
			this.eyesScaleX = 1;
			this.eyesScaleY = 1;
		}

		if (newState == BeetleState.ANIM_TO_FLY) {
			this.eyesScaleX += Slingshot.getSlingshotPower().len() / 500;
			this.eyesScaleY = this.eyesScaleX;
		}

		if (newState == BeetleState.INFLY) {
			this.drawClosed = false;
		} else {
			this.drawClosed = true;
		}

		super.onStateChanged(newState);
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(this.getColor());

		batch.draw(this.closedRegion, this.getX() + closedXY.x, this.getY() + closedXY.y,
				closedOrXY.x, closedOrXY.y, closedWH.x, closedWH.y,
				this.getScaleX(), this.getScaleY(), this.getRotation());

		this.powerLen = Slingshot.getSlingshotPower().len();
		if (this.powerLen >= 0 && this.drawClosed
				&& getState() != BeetleState.ANIM_TO_FLY) {

			this.eyesScale = this.getScaleX() + this.powerLen / 500;
			// if (getState() == BeetleState.BONUS) {
			// eyesScale = scaleX;
			// }

			batch.draw(this.eyesRegion, this.getX() + eyesXY.x, this.getY() + eyesXY.y,
					eyesOrXY.x, eyesOrXY.y, eyesWH.x, eyesWH.y, this.eyesScale,
					this.eyesScale, this.getRotation());
		} else {
			if (this.eyesScaleX > 1) {
				this.eyesScaleX -= 0.25f * Gdx.graphics.getDeltaTime();
				this.eyesScaleY -= 0.25f * Gdx.graphics.getDeltaTime();
			}

			batch.draw(this.eyesRegion, this.getX() + eyesXY.x, this.getY() + eyesXY.y,
					eyesOrXY.x, eyesOrXY.y, eyesWH.x, eyesWH.y,
					this.eyesScaleX, this.eyesScaleY, this.getRotation());
		}

		if (this.inRage) {
			batch.draw(this.musRegion, getX() + musXY.x, getY() + musXY.y, musOrXY.x,
					musOrXY.y, musWH.x, musWH.y, musScaleXY, musScaleXY,
					getRotation());
		}

	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.musRegion != null && this.eyesRegion != null
				&& this.closedRegion != null) {

			if (getState() == BeetleState.BREAKING
					&& !GameScreen.bManager.isEggsInFly(this)) {
				setState(BeetleState.EXPLODE);
			}

			if (getState() == BeetleState.SLEEP
					|| getState() == BeetleState.EXPLODE
					|| getState() == BeetleState.BREAKING
					|| getState() == BeetleState.HIDDEN) {
				drawParticles(batch, parentAlpha);
				return;
			}

			drawParticles(batch, parentAlpha);
			// super.draw(batch, parentAlpha);
			this.drawSkin(batch, parentAlpha);

			super.fly();
		}
	}

	@Override
	public ArrayList<String> getNames() {
		return name2;
	}
}
