package com.workshop27.ballistabreaker.rocks;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class KarateBeetle extends AbstractBeetle {

	private final ArrayList<String> name2;

	private Vector2 eyesXY;
	private Vector2 closedXY;
	private Vector2 openXY;
	private Vector2 legsXY;
	private Vector2 backXY;

	private Vector2 eyesWH;
	private Vector2 closedWH;
	private Vector2 openWH;
	private Vector2 legsWH;
	private Vector2 backWH;

	private Vector2 eyesOrXY;
	private Vector2 closedOrXY;
	private Vector2 openOrXY;
	private Vector2 legsOrXY;
	private Vector2 backOrXY;

	private TextureRegion eyesRegion;
	private TextureRegion closedRegion;
	private TextureRegion openRegion;
	private TextureRegion legsRegion;
	private TextureRegion backRegion;

	private float powerLen;
	private float eyesScale;
	private float eyesScaleX;
	private float eyesScaleY;

	private boolean drawClosed = true;

	public KarateBeetle(String name, String skinName,
			HashMap<String, Integer> params, ArrayList<String> names) {
		super(name, skinName);
		super.init(params);
		this.name2 = names;

		// super.skinName = "";

		setWidth(58);
		setHeight(41);

		this.activeState = BeetleState.SLEEP;
	}

	public ArrayList<String> getName2() {
		return name2;
	}

	@Override
	public void clearSkin() {
		this.eyesRegion = null;
		this.closedRegion = null;
		this.openRegion = null;
		this.legsRegion = null;
		this.backRegion = null;
	}

	@Override
	public void setSkin() {

		this.eyesRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("karate-eyes");

		this.closedRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("karate-closed");

		this.openRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("karate-opened");

		this.legsRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("karate-legs");

		this.backRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("karate-back");

		backXY = new Vector2(14 * GdxGame.HD_X_RATIO, 26 * GdxGame.HD_Y_RATIO);
		eyesXY = new Vector2(12 * GdxGame.HD_X_RATIO, 25 * GdxGame.HD_Y_RATIO);
		closedXY = new Vector2(10 * GdxGame.HD_X_RATIO, 0);
		openXY = new Vector2(-7 * GdxGame.HD_X_RATIO, 0);
		legsXY = new Vector2(4 * GdxGame.HD_X_RATIO, 0);

		eyesWH = new Vector2(eyesRegion.getRegionWidth(),
				eyesRegion.getRegionHeight());
		closedWH = new Vector2(closedRegion.getRegionWidth(),
				closedRegion.getRegionHeight());
		openWH = new Vector2(openRegion.getRegionWidth(),
				openRegion.getRegionHeight());
		backWH = new Vector2(backRegion.getRegionWidth(),
				backRegion.getRegionHeight());
		legsWH = new Vector2(legsRegion.getRegionWidth(),
				legsRegion.getRegionHeight());

		legsOrXY = new Vector2(legsWH.x / 2, legsWH.y / 2);
		closedOrXY = new Vector2(legsOrXY.x - closedXY.x, legsOrXY.y
				- closedXY.y);
		openOrXY = new Vector2(legsOrXY.x - openXY.x, legsOrXY.y - openXY.y);
		eyesOrXY = new Vector2(legsOrXY.x - eyesXY.x, legsOrXY.y - eyesXY.y);
		backOrXY = new Vector2(legsOrXY.x - backXY.x, legsOrXY.y - backXY.y);
	}

	@Override
	public void onStateChanged(BeetleState newState) {
		if (newState == BeetleState.ACTIVE) {
			eyesScaleX = 1;
			eyesScaleY = 1;
		}

		if (newState == BeetleState.ANIM_TO_FLY) {
			eyesScaleX += Slingshot.getSlingshotPower().len() / 500;
			eyesScaleY = eyesScaleX;
		}

		if (newState == BeetleState.INFLY) {
			this.drawClosed = false;
		} else {
			this.drawClosed = true;
		}

		super.onStateChanged(newState);
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(this.getColor());

		batch.draw(this.legsRegion, getX() + legsXY.x, getY() + legsXY.y, legsOrXY.x,
				legsOrXY.y, legsWH.x, legsWH.y, getScaleX(), getScaleY(), getRotation());

		if (this.drawClosed) {

			batch.draw(this.closedRegion, getX() + closedXY.x, getY() + closedXY.y,
					closedOrXY.x, closedOrXY.y, closedWH.x, closedWH.y, getScaleX(),
					getScaleY(), getRotation());
		} else {
			batch.draw(this.closedRegion, getX() + closedXY.x, getY() + closedXY.y,
					closedOrXY.x, closedOrXY.y, closedWH.x, closedWH.y, getScaleX(),
					getScaleY(), getRotation());

			batch.draw(this.openRegion, getX() + openXY.x, getY() + openXY.y, openOrXY.x,
					openOrXY.y, openWH.x, openWH.y, getScaleX(), getScaleY(), getRotation());

			batch.draw(this.backRegion, getX() + backXY.x, getY() + backXY.y, backOrXY.x,
					backOrXY.y, backWH.x, backWH.y, getScaleX(), getScaleY(), getRotation());
		}

		powerLen = Slingshot.getSlingshotPower().len();
		if (powerLen >= 0 && this.drawClosed
				&& getState() != BeetleState.ANIM_TO_FLY) {

			eyesScale = getScaleX() + powerLen / 500;
			// if (getState() == BeetleState.BONUS) {
			// eyesScale = scaleX;
			// }

			batch.draw(this.eyesRegion, getX() + eyesXY.x, getY() + eyesXY.y, eyesOrXY.x,
					eyesOrXY.y, eyesWH.x, eyesWH.y, eyesScale, eyesScale,
					getRotation());
		} else {
			if (eyesScaleX > 1) {
				eyesScaleX -= 0.25f * Gdx.graphics.getDeltaTime();
				eyesScaleY -= 0.25f * Gdx.graphics.getDeltaTime();
			}

			batch.draw(this.eyesRegion, getX() + eyesXY.x, getY() + eyesXY.y, eyesOrXY.x,
					eyesOrXY.y, eyesWH.x, eyesWH.y, eyesScaleX, eyesScaleY,
					getRotation());
		}

	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.eyesRegion != null && this.closedRegion != null) {

			if (getState() == BeetleState.BREAKING
					&& !GameScreen.bManager.isEggsInFly(this)) {
				setState(BeetleState.EXPLODE);
			}

			if (getState() == BeetleState.SLEEP
					|| getState() == BeetleState.EXPLODE
					|| getState() == BeetleState.BREAKING
					|| getState() == BeetleState.HIDDEN) {
				drawParticles(batch, parentAlpha);
				return;
			}

			drawParticles(batch, parentAlpha);
			// super.draw(batch, parentAlpha);
			this.drawSkin(batch, parentAlpha);

			super.fly();
		}
	}

	@Override
	public ArrayList<String> getNames() {
		return name2;
	}
}
