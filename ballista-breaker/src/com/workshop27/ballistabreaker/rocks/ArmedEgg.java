package com.workshop27.ballistabreaker.rocks;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;

public class ArmedEgg extends AbstractBeetle {

	public ArmedEgg(String name, String skinName,
			HashMap<String, Integer> params) {
		super(name, skinName);
		super.init(params);

		this.activeState = BeetleState.SLEEP;
	}

	@Override
	public void explode(float centerX, float centerY) {
		super.explode(centerX, centerY);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if ((getState() == BeetleState.SLEEP
				|| getState() == BeetleState.EXPLODE || getState() == BeetleState.BREAKING)) {
			drawParticles(batch, parentAlpha);
			return;
		}

		super.draw(batch, parentAlpha);
		super.fly();
	}

	@Override
	public ArrayList<String> getNames() {
		// TODO Auto-generated method stub
		return null;
	}

}
