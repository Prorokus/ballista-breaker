package com.workshop27.ballistabreaker.rocks;

import java.util.ArrayList;
import java.util.HashMap;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class ArmedEggMovable extends AbstractBeetle {

	private float xRotated;
	private float yRotated;

	private float sin;
	private float cos;

	private float originXRotated;
	private float originYRotated;

	private float baseRotation;

	public ArmedEggMovable(String name, String skinName,
			HashMap<String, Integer> params) {
		super(name, skinName);
		super.init(params);

		this.activeState = BeetleState.SLEEP;
	}

	@Override
	public void explode(float centerX, float centerY) {
		super.explode(centerX, centerY);
	}

	@Override
	public void onStateChanged(BeetleState newState) {

		if (newState == BeetleState.ARMED) {
			this.originXRotated = 0;
			this.originYRotated = 0;
		}

		super.onStateChanged(newState);
	}

	@Override
	public Vector2 getCenter() {
		if (getState() != BeetleState.ARMED) {
			return new Vector2(this.getX() + this.getWidth() / 2, this.getY() + this.getHeight()
					/ 2);
		}

		else {
			return new Vector2(this.xRotated + this.getWidth() / 2, this.yRotated
					+ this.getHeight() / 2);
		}
	}

	/*
	 * @Override public float getPositionX() { if (getState() !=
	 * BeetleState.ARMED) { return this.x; } else { return 0;// this.xRotated; }
	 * }
	 * 
	 * @Override public float getPositionY() { if (getState() !=
	 * BeetleState.ARMED) { return this.y; } else { return this.yRotated; } }
	 */

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		if (this.filter) {
			this.skinRegion.getTexture().setFilter(TextureFilter.Linear,
					TextureFilter.Linear);
			this.filter = false;
		}

		batch.setColor(this.getColor());

		if ((getState() == BeetleState.ARMED || getState() == BeetleState.EXPLODING))//GameScreen.objectToDestroy.rotation != 0 &&  
		{
			if (originXRotated == 0 && originYRotated == 0) {
				originXRotated = GameScreen.objectToDestroy.getPositionX()
						+ GameScreen.objectToDestroy.getOriginX()
						- this.getWidth() / 2;
				originYRotated = GameScreen.objectToDestroy.getPositionY()
						+ GameScreen.objectToDestroy.getOriginY()
						- this.getHeight() / 2;
				this.baseRotation = GameScreen.objectToDestroy.getRotation();
				RotateToAction object_rotateTo0 = rotateTo(0, 0.05f);
				this.addAction(object_rotateTo0);
			}

			this.sin = MathUtils.sinDeg(GameScreen.objectToDestroy.getRotation()
					- this.baseRotation);
			this.cos = MathUtils.cosDeg(GameScreen.objectToDestroy.getRotation()
					- this.baseRotation);

			this.xRotated = originXRotated + (getX() - originXRotated) * this.cos
					- (getY() - originYRotated) * this.sin;
			this.yRotated = originYRotated + (getY() - originYRotated) * this.cos
					+ (getX() - originXRotated) * this.sin;
		}

		else {
			this.xRotated = getX();
			this.yRotated = getY();
		}

		batch.draw(this.skinRegion, this.xRotated, this.yRotated, getOriginX(),
				getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if ((getState() == BeetleState.SLEEP
				|| getState() == BeetleState.EXPLODE || getState() == BeetleState.BREAKING)) {
			drawParticles(batch, parentAlpha);
			return;
		}

		super.draw(batch, parentAlpha);
		super.fly();
	}

	@Override
	public ArrayList<String> getNames() {
		// TODO Auto-generated method stub
		return null;
	}

}
