package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;

public class DirrectionArrow extends GameObject {
	private Vector2 arrow0XY;
	private Vector2 arrow1XY;
	private Vector2 arrow2XY;
	private Vector2 arrow3XY;
	private Vector2 arrow4XY;
	private Vector2 arrow5XY;
	private Vector2 arrow6XY;
	private Vector2 arrow7XY;

	private TextureRegion arrow0Region;
	private TextureRegion arrow1Region;
	private TextureRegion arrow2Region;
	private TextureRegion arrow3Region;
	private TextureRegion arrow4Region;
	private TextureRegion arrow5Region;
	private TextureRegion arrow6Region;
	private TextureRegion arrow7Region;

	private Vector2 arrow0WH;
	private Vector2 arrow1WH;
	private Vector2 arrow2WH;
	private Vector2 arrow3WH;
	private Vector2 arrow4WH;
	private Vector2 arrow5WH;
	private Vector2 arrow6WH;
	private Vector2 arrow7WH;

	private Vector2 arrow0OrXY;
	private Vector2 arrow1OrXY;
	private Vector2 arrow2OrXY;
	private Vector2 arrow3OrXY;
	private Vector2 arrow4OrXY;
	private Vector2 arrow5OrXY;
	private Vector2 arrow6OrXY;
	private Vector2 arrow7OrXY;

	private float powerLen;

	private final Color colorGreen = new Color(0, 1, 0, 0.5f);
	private final Color colorYellow = new Color(1, 1, 0, 0.5f);
	private final Color colorRed = new Color(1, 0, 0, 0.5f);

	private Color colorArrow0 = colorGreen;
	private Color colorArrow1 = colorGreen;
	private Color colorArrow2 = colorGreen;
	private Color colorArrow3 = colorGreen;
	private Color colorArrow4 = colorGreen;
	private Color colorArrow5 = colorGreen;
	private Color colorArrow6 = colorGreen;
	private Color colorArrow7 = colorGreen;

	private final float maxPower = 163;
	private final float oneStepPower = maxPower / 9;
	private int currPower = 0;

	public DirrectionArrow(String name) {
		super(name, false, true);

		super.skinName = "";

		setWidth(76);
		setHeight(211);
	}

	@Override
	public void clearSkin() {
		this.arrow0Region = null;
		this.arrow1Region = null;
		this.arrow2Region = null;
		this.arrow3Region = null;
		this.arrow4Region = null;
		this.arrow5Region = null;
		this.arrow6Region = null;
		this.arrow7Region = null;
	}

	@Override
	public void setSkin() {
		this.arrow0Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("arrow0");
		this.arrow1Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("arrow1");
		this.arrow2Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("arrow2");
		this.arrow3Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("arrow3");
		this.arrow4Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("arrow4");
		this.arrow5Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("arrow5");
		this.arrow6Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("arrow6");
		this.arrow7Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("arrow7");

		arrow0XY = new Vector2(0, 0);
		arrow1XY = new Vector2(0 * GdxGame.HD_X_RATIO, 26 * GdxGame.HD_Y_RATIO);
		arrow2XY = new Vector2(7 * GdxGame.HD_X_RATIO, 52 * GdxGame.HD_Y_RATIO);
		arrow3XY = new Vector2(11 * GdxGame.HD_X_RATIO, 78 * GdxGame.HD_Y_RATIO);
		arrow4XY = new Vector2(15 * GdxGame.HD_X_RATIO,
				103 * GdxGame.HD_Y_RATIO);
		arrow5XY = new Vector2(19 * GdxGame.HD_X_RATIO,
				129 * GdxGame.HD_Y_RATIO);
		arrow6XY = new Vector2(24 * GdxGame.HD_X_RATIO,
				155 * GdxGame.HD_Y_RATIO);
		arrow7XY = new Vector2(27 * GdxGame.HD_X_RATIO,
				179 * GdxGame.HD_Y_RATIO);

		arrow0WH = new Vector2(arrow0Region.getRegionWidth(),
				arrow0Region.getRegionHeight());
		arrow1WH = new Vector2(arrow1Region.getRegionWidth(),
				arrow1Region.getRegionHeight());
		arrow2WH = new Vector2(arrow2Region.getRegionWidth(),
				arrow2Region.getRegionHeight());
		arrow3WH = new Vector2(arrow3Region.getRegionWidth(),
				arrow3Region.getRegionHeight());
		arrow4WH = new Vector2(arrow4Region.getRegionWidth(),
				arrow4Region.getRegionHeight());
		arrow5WH = new Vector2(arrow5Region.getRegionWidth(),
				arrow5Region.getRegionHeight());
		arrow6WH = new Vector2(arrow6Region.getRegionWidth(),
				arrow6Region.getRegionHeight());
		arrow7WH = new Vector2(arrow7Region.getRegionWidth(),
				arrow7Region.getRegionHeight());

		arrow0OrXY = new Vector2(arrow0WH.x / 2, 0);
		arrow1OrXY = new Vector2(arrow1WH.x / 2, -arrow1XY.y);
		arrow2OrXY = new Vector2(arrow2WH.x / 2, -arrow2XY.y);
		arrow3OrXY = new Vector2(arrow3WH.x / 2, -arrow3XY.y);
		arrow4OrXY = new Vector2(arrow4WH.x / 2, -arrow4XY.y);
		arrow5OrXY = new Vector2(arrow5WH.x / 2, -arrow5XY.y);
		arrow6OrXY = new Vector2(arrow6WH.x / 2, -arrow6XY.y);
		arrow7OrXY = new Vector2(arrow7WH.x / 2, -arrow7XY.y);
	}

	public void updateColor() {
		this.currPower = (int) (Slingshot.getSlingshotPower().len() / this.oneStepPower);

		switch (this.currPower) {
		case 0:
			colorArrow0 = colorGreen;
			colorArrow1 = colorGreen;
			colorArrow2 = colorGreen;
			colorArrow3 = colorGreen;
			colorArrow4 = colorGreen;
			colorArrow5 = colorGreen;
			colorArrow6 = colorGreen;
			colorArrow7 = colorGreen;

			break;

		case 1:
			colorArrow0 = colorRed;
			colorArrow1 = colorGreen;
			colorArrow2 = colorGreen;
			colorArrow3 = colorGreen;
			colorArrow4 = colorGreen;
			colorArrow5 = colorGreen;
			colorArrow6 = colorGreen;
			colorArrow7 = colorGreen;
			break;

		case 2:
			colorArrow0 = colorRed;
			colorArrow1 = colorRed;
			colorArrow2 = colorGreen;
			colorArrow3 = colorGreen;
			colorArrow4 = colorGreen;
			colorArrow5 = colorGreen;
			colorArrow6 = colorGreen;
			colorArrow7 = colorGreen;
			break;

		case 3:
			colorArrow0 = colorRed;
			colorArrow1 = colorRed;
			colorArrow2 = colorRed;
			colorArrow3 = colorGreen;
			colorArrow4 = colorGreen;
			colorArrow5 = colorGreen;
			colorArrow6 = colorGreen;
			colorArrow7 = colorGreen;
			break;

		case 4:
			colorArrow0 = colorRed;
			colorArrow1 = colorRed;
			colorArrow2 = colorRed;
			colorArrow3 = colorRed;
			colorArrow4 = colorGreen;
			colorArrow5 = colorGreen;
			colorArrow6 = colorGreen;
			colorArrow7 = colorGreen;
			break;

		case 5:
			colorArrow0 = colorRed;
			colorArrow1 = colorRed;
			colorArrow2 = colorRed;
			colorArrow3 = colorRed;
			colorArrow4 = colorRed;
			colorArrow5 = colorGreen;
			colorArrow6 = colorGreen;
			colorArrow7 = colorGreen;
			break;

		case 6:
			colorArrow0 = colorRed;
			colorArrow1 = colorRed;
			colorArrow2 = colorRed;
			colorArrow3 = colorRed;
			colorArrow4 = colorRed;
			colorArrow5 = colorRed;
			colorArrow6 = colorGreen;
			colorArrow7 = colorGreen;
			break;

		case 7:
			colorArrow0 = colorRed;
			colorArrow1 = colorRed;
			colorArrow2 = colorRed;
			colorArrow3 = colorRed;
			colorArrow4 = colorRed;
			colorArrow5 = colorRed;
			colorArrow6 = colorRed;
			colorArrow7 = colorGreen;
			break;

		case 8:
			colorArrow0 = colorRed;
			colorArrow1 = colorRed;
			colorArrow2 = colorRed;
			colorArrow3 = colorRed;
			colorArrow4 = colorRed;
			colorArrow5 = colorRed;
			colorArrow6 = colorRed;
			colorArrow7 = colorRed;
			break;

		default:
			break;
		}
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(colorArrow7);
		batch.draw(this.arrow0Region, getX() + arrow0XY.x, getY() + arrow0XY.y,
				arrow0OrXY.x, arrow0OrXY.y, arrow0WH.x, arrow0WH.y, getScaleX(),
				getScaleY(), getRotation());

		batch.setColor(colorArrow6);
		batch.draw(this.arrow1Region, getX() + arrow1XY.x, getY() + arrow1XY.y,
				arrow1OrXY.x, arrow1OrXY.y, arrow1WH.x, arrow1WH.y, getScaleX(),
				getScaleY(), getRotation());

		batch.setColor(colorArrow5);
		batch.draw(this.arrow2Region, getX() + arrow2XY.x, getY() + arrow2XY.y,
				arrow2OrXY.x, arrow2OrXY.y, arrow2WH.x, arrow2WH.y, getScaleX(),
				getScaleY(), getRotation());

		batch.setColor(colorArrow4);
		batch.draw(this.arrow3Region, getX() + arrow3XY.x, getY() + arrow3XY.y,
				arrow3OrXY.x, arrow3OrXY.y, arrow3WH.x, arrow3WH.y, getScaleX(),
				getScaleY(), getRotation());

		batch.setColor(colorArrow3);
		batch.draw(this.arrow4Region, getX() + arrow4XY.x, getY() + arrow4XY.y,
				arrow4OrXY.x, arrow4OrXY.y, arrow4WH.x, arrow4WH.y, getScaleX(),
				getScaleY(), getRotation());

		batch.setColor(colorArrow2);
		batch.draw(this.arrow5Region, getX() + arrow5XY.x, getY() + arrow5XY.y,
				arrow5OrXY.x, arrow5OrXY.y, arrow5WH.x, arrow5WH.y, getScaleX(),
				getScaleY(), getRotation());

		batch.setColor(colorArrow1);
		batch.draw(this.arrow6Region, getX() + arrow6XY.x, getY() + arrow6XY.y,
				arrow6OrXY.x, arrow6OrXY.y, arrow6WH.x, arrow6WH.y, getScaleX(),
				getScaleY(), getRotation());

		batch.setColor(colorArrow0);
		batch.draw(this.arrow7Region, getX() + arrow7XY.x, getY() + arrow7XY.y,
				arrow7OrXY.x, arrow7OrXY.y, arrow7WH.x, arrow7WH.y, getScaleX(),
				getScaleY(), getRotation());
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.arrow0Region != null && this.arrow1Region != null
				&& this.arrow2Region != null && this.arrow3Region != null
				&& this.arrow4Region != null && this.arrow5Region != null
				&& this.arrow6Region != null && this.arrow7Region != null) {

			this.drawSkin(batch, parentAlpha);
		}
	}
}
