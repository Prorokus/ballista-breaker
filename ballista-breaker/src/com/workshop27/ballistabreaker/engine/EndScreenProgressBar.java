package com.workshop27.ballistabreaker.engine;

import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.GameScreen;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class EndScreenProgressBar extends GameObject {
	private Vector2 prog_jamXY;
	private Vector2 prog_ellipseXY;
	private Vector2 prog_coverXY;

	private Vector2 prog_jamWH;
	private Vector2 prog_ellipseWH;
	private Vector2 prog_coverWH;

	private TextureRegion prog_jamRegion;
	private TextureRegion prog_ellipseRegion;
	private TextureRegion prog_coverRegion;

	private final GameObject completeStar1;
	private final GameObject completeStar2;
	private final GameObject completeStar3;
	private final GameObject completeMustache;

	private float jamScaleX = 1.0f;
	private float jamScaleY = 1.0f;

	private float finalProgress = 0;
	private float currProgressToAnim = 0;

	private final float animationDelay = 2;

	private final Timer endScreenTimer = new Timer();

	private int starsCount;

	public EndScreenProgressBar(String name, GameObject star1,
			GameObject star2, GameObject star3, GameObject mustache) {
		super(name, false, true);
		super.skinName = "";

		this.completeStar1 = star1;
		this.completeStar2 = star2;
		this.completeStar3 = star3;
		this.completeMustache = mustache;

		initObjects();
	}

	@Override
	public void setPositionXY(float x, float y) {
		this.setX(x * GdxGame.HD_X_RATIO);
		this.setY(y * GdxGame.HD_Y_RATIO);

		this.completeStar1.setPositionXY(this.getX() + 34, this.getY() + 160);
		this.completeStar2.setPositionXY(this.getX() + 42 + 30, this.getY() + 162);
		this.completeStar3.setPositionXY(this.getX() + 45 + 35 + 40, this.getY() + 160);
		this.completeMustache.setPositionXY(this.getX() + 50, this.getY() + 70);
	}

	private void initObjects() {
		this.completeStar1.skinName = "small-star";
		this.completeStar1.folderName = "main";
		this.completeStar1.loadSkinOnDraw = true;
		this.completeStar1.setOriginX(17);
		this.completeStar1.setOriginY(19);
		this.completeStar1.getColor().a = 0;

		this.completeStar2.skinName = "big-star";
		this.completeStar2.folderName = "main";
		this.completeStar2.loadSkinOnDraw = true;
		this.completeStar2.setOriginX(22);
		this.completeStar2.setOriginY(23);
		this.completeStar2.getColor().a = 0;

		this.completeStar3.skinName = "small-star";
		this.completeStar3.folderName = "main";
		this.completeStar3.loadSkinOnDraw = true;
		this.completeStar3.setOriginX(17);
		this.completeStar3.setOriginY(19);
		this.completeStar3.getColor().a = 0;

		this.completeMustache.skinName = "prog-mus";
		this.completeMustache.folderName = "main";
		this.completeMustache.loadSkinOnDraw = true;
		this.completeMustache.setOriginX(41);
		this.completeMustache.setOriginY(22);
		this.completeMustache.setScaleX(1.3f);
		this.completeMustache.setScaleY(1.3f);
		this.completeMustache.getColor().a = 0;
	}

	public void setProgress(final float pixelsLabelValue) {
		if (pixelsLabelValue < 15 || pixelsLabelValue > 90) {
			this.jamScaleY = 0;
		} else {
			this.jamScaleY = pixelsLabelValue / 75 + 0.25f;
		}

		if (pixelsLabelValue > 70) {
			jamScaleX = 1 - (100 - pixelsLabelValue) / 100;
		} else {
			jamScaleX = 1.0f;
		}

		float a = pixelsLabelValue;
		if (a > 90) {
			a = 90;
		}

		this.prog_jamRegion
				.setV(JarObject.getJarJamV2()
						- ((JarObject.getJarJamV2() - JarObject.getJarJamV1()) / 90 * a));

		prog_jamWH = new Vector2(prog_jamRegion.getRegionWidth(),
				prog_jamRegion.getRegionHeight());
	}

	@Override
	public void clearSkin() {
		this.prog_jamRegion = null;
		this.prog_coverRegion = null;
		this.prog_ellipseRegion = null;

		this.completeStar1.clearSkin();
		this.completeStar2.clearSkin();
		this.completeStar3.clearSkin();
		this.completeMustache.clearSkin();
	}

	@Override
	public void setSkin() {
		this.completeStar1.setSkin();
		this.completeStar2.setSkin();
		this.completeStar3.setSkin();
		this.completeMustache.setSkin();

		this.prog_jamRegion = new TextureRegion(ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("prog-jam"));

		this.prog_jamRegion.setV(JarObject.getJarJamV2());

		this.prog_ellipseRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("prog-ellipse");

		this.prog_coverRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("prog-cover");

		prog_jamXY = new Vector2(51 * GdxGame.HD_X_RATIO,
				32 * GdxGame.HD_Y_RATIO);
		prog_ellipseXY = new Vector2(62 * GdxGame.HD_X_RATIO,
				35 * GdxGame.HD_Y_RATIO);

		prog_coverXY = new Vector2(0, 0 * GdxGame.HD_Y_RATIO);

		prog_jamWH = new Vector2(prog_jamRegion.getRegionWidth(),
				prog_jamRegion.getRegionHeight());
		prog_coverWH = new Vector2(prog_coverRegion.getRegionWidth(),
				prog_coverRegion.getRegionHeight());

		prog_ellipseWH = new Vector2(prog_ellipseRegion.getRegionWidth(),
				prog_ellipseRegion.getRegionHeight());
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(this.getColor());

		batch.draw(this.prog_jamRegion, this.getX() + prog_jamXY.x, this.getY()
				+ prog_jamXY.y, getOriginX(), getOriginY(), prog_jamWH.x, prog_jamWH.y,
				getScaleX(), getScaleY(), getRotation());

		batch.draw(this.prog_ellipseRegion, this.getX() + prog_ellipseXY.x,
				this.getY()
						+ prog_jamXY.y
						+ (prog_jamWH.y - prog_ellipseWH.y
								+ (1.0f - this.jamScaleY) * prog_ellipseWH.y
								/ 2 + 4 * GdxGame.HD_Y_RATIO) * 1.3f, 31,
				0 * GdxGame.HD_Y_RATIO, prog_ellipseWH.x, prog_ellipseWH.y,
				this.jamScaleX * 1.3f, this.jamScaleY * 1.3f, getRotation());

		batch.draw(this.prog_coverRegion, this.getX() + prog_coverXY.x, this.getY()
				+ prog_coverXY.y, getOriginX(), getOriginY(), prog_coverWH.x,
				prog_coverWH.y, getScaleX(), getScaleY(), getRotation());
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!isVisible()) {
			return;
		}

		if (this.currProgressToAnim <= this.finalProgress
				&& this.finalProgress != 0) {

			this.currProgressToAnim += this.finalProgress
					* Gdx.graphics.getDeltaTime() / this.animationDelay;

			if (this.currProgressToAnim >= this.finalProgress) {
				this.currProgressToAnim = this.finalProgress;
			}

			setProgress(this.currProgressToAnim);
		}

		if (this.prog_jamRegion != null && this.prog_coverRegion != null
				&& this.prog_ellipseRegion != null) {
			this.drawSkin(batch, parentAlpha);
		}

		this.completeMustache.act(Gdx.graphics.getDeltaTime());
		this.completeMustache.draw(batch, parentAlpha);

		this.completeStar1.drawParticles(batch, parentAlpha);
		this.completeStar2.drawParticles(batch, parentAlpha);
		this.completeStar3.drawParticles(batch, parentAlpha);

		this.completeStar1.act(Gdx.graphics.getDeltaTime());
		this.completeStar2.act(Gdx.graphics.getDeltaTime());
		this.completeStar3.act(Gdx.graphics.getDeltaTime());

		this.completeStar1.drawSkin(batch, parentAlpha);
		this.completeStar2.drawSkin(batch, parentAlpha);
		this.completeStar3.drawSkin(batch, parentAlpha);
	}

	public void playCompleteAnim(float progress, int starsCount) {
		this.setVisible(true);

		this.finalProgress = progress;
		this.currProgressToAnim = 0;
		this.starsCount = starsCount;

		if (this.starsCount > 0) {
			this.completeStar1.setScaleX(10);
			this.completeStar1.setScaleY(10);
			this.completeStar1.setRotation(240);

			AlphaAction starFadeIn1 = fadeIn(0.3f);

			completeStar1.setPositionXY(this.completeStar1.getX() - 250, this.completeStar1.getY() + 250);
			MoveToAction starMoveTo = moveTo(this.completeStar1.getX() + 250, this.completeStar1.getY() - 250, 0.3f);
			this.completeStar1.setPositionX(this.completeStar1.getX() - 250);
			this.completeStar1.setPositionY(this.completeStar1.getY() + 250);

			ScaleToAction starScale1 = scaleTo(1.0f, 1.0f, 0.3f);
			RotateToAction starRotate1 = rotateTo(0, 0.3f);

			RotateToAction starRotate2 = rotateTo(-5, 0.5f);
			RotateToAction starRotate3 = rotateTo(5, 0.5f);

			completeStar1.addAction(sequence(delay(3.2f), parallel(
					starFadeIn1, starMoveTo, starScale1, starRotate1)));
			completeStar1.addAction(sequence(delay(3.5f), forever(sequence(starRotate2, starRotate3))));

			if (completeStar1.particleStack == null) {
				completeStar1.createParticle("magicVictoryBoom", true);
				completeStar1.loadParticleData("magicVictoryBoom");
			}

			completeStar1.createParticle("magicVictory", true);

			endScreenTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					if (GameScreen.isGamePaused()) {
						completeStar1.particleStack.get("magicVictoryBoom")
								.start();

						GameScreen.mShakeCounter = 5;
						GameScreen.explosionShake();
					}
				}
			}, 3500);
		}

		if (this.starsCount > 1) {
			this.completeStar2.setScaleX(10);
			this.completeStar2.setScaleY(10);

			AlphaAction starFadeIn1 = fadeIn(0.3f);

			completeStar2.setPositionY(this.completeStar2.getY() + 250);
			MoveToAction starMoveTo = moveTo(this.completeStar2.getX(), this.completeStar2.getY() - 250, 0.3f);

			ScaleToAction starScale1 = scaleTo(1.0f, 1.0f, 0.3f);

			RotateToAction starRotate2 = rotateTo(-5, 0.5f);
			RotateToAction starRotate3 = rotateTo(5, 0.5f);

			completeStar2.addAction(sequence(delay(3.7f),
					parallel(starFadeIn1, starMoveTo, starScale1)));
			completeStar2.addAction(sequence(delay(4.0f),
					forever(sequence(starRotate2, starRotate3))));

			if (completeStar2.particleStack == null) {
				completeStar2.createParticle("magicVictoryBoom", true);
				completeStar2.loadParticleData("magicVictoryBoom");
			}

			completeStar2.createParticle("magicVictory", true);

			endScreenTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					if (GameScreen.isGamePaused()) {
						completeStar2.particleStack.get("magicVictoryBoom")
								.start();

						GameScreen.mShakeCounter = 5;
						GameScreen.explosionShake();
					}
				}
			}, 4000);
		}

		if (this.starsCount > 2) {
			this.completeStar3.setScaleX(10);
			this.completeStar3.setScaleY(10);
			this.completeStar3.setRotation(-240);

			AlphaAction starFadeIn1 = fadeIn(0.3f);

			MoveToAction starMoveTo = moveTo(this.completeStar3.getX() - 250, this.completeStar3.getY() - 250, 0.3f);
			this.completeStar3.setPositionX(this.completeStar3.getX() + 250);
			this.completeStar3.setPositionY(this.completeStar3.getY() + 250);

			ScaleToAction starScale1 = scaleTo(1.0f, 1.0f, 0.3f);
			RotateToAction starRotate1 = rotateTo(0, 0.3f);

			RotateToAction starRotate2 = rotateTo(-5, 0.5f);
			RotateToAction starRotate3 = rotateTo(5, 0.5f);

			completeStar3.addAction(sequence(delay(4.2f), parallel(
					starFadeIn1, starMoveTo, starScale1, starRotate1)));
			completeStar3.addAction(sequence(delay(4.5f),
					forever(sequence(starRotate2, starRotate3))));

			if (completeStar3.particleStack == null) {
				completeStar3.createParticle("magicVictoryBoom", true);
				completeStar3.loadParticleData("magicVictoryBoom");
			}

			completeStar3.createParticle("magicVictory", true);

			endScreenTimer.schedule(new TimerTask() {
				@Override
				public void run() {
					if (GameScreen.isGamePaused()) {
						completeStar3.particleStack.get("magicVictoryBoom")
								.start();

						GameScreen.mShakeCounter = 5;
						GameScreen.explosionShake();
					}
				}
			}, 4500);
		}

		if (this.finalProgress >= 90) {
			this.completeMustache.setScaleX(0);
			this.completeMustache.setScaleY(0);

			ScaleToAction musScale1 = scaleTo(1.8f, 1.8f, 0.25f);
			ScaleToAction musScale2 = scaleTo(1.3f, 1.3f, 0.25f);

			RotateToAction musRotate1 = rotateTo(-10, 0.1f);

			RotateToAction musRotate2 = rotateTo(10, 0.1f);

			RotateToAction musRotate3 = rotateTo(0, 0.1f);

			AlphaAction musFadeIn1 = fadeIn(0.25f);

			this.completeMustache.addAction(sequence(
					delay(this.animationDelay),
					parallel(musFadeIn1, musScale1)));
			this.completeMustache.addAction(sequence(
					delay(this.animationDelay + 0.25f), musScale2));

			this.completeMustache.addAction(sequence(delay(this.animationDelay + 0.5f), forever(sequence(
					musRotate1, musRotate2, musRotate1, musRotate2, musRotate1,
					musRotate2, musRotate3, delay(4)))));
		}
	}

	public void resetCompleteAnim() {
		this.setVisible(false);
		this.finalProgress = 0;
		this.currProgressToAnim = 0;

		this.completeStar1.clearActions();
		this.completeStar1.getColor().a = 0;
		this.completeStar1.clearParticle("magicVictory");

		this.completeStar2.clearActions();
		this.completeStar2.getColor().a = 0;
		this.completeStar2.clearParticle("magicVictory");

		this.completeStar3.clearActions();
		this.completeStar3.getColor().a = 0;
		this.completeStar3.clearParticle("magicVictory");

		this.completeMustache.clearActions();
		this.completeMustache.setRotation(0);
		this.completeMustache.getColor().a = 0;
	}

}
