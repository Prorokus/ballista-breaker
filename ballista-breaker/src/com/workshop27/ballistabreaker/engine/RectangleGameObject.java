package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.workshop27.ballistabreaker.GdxGame;

public class RectangleGameObject extends GameObject {

	private final ShapeRenderer shapeRenderer = new ShapeRenderer();;

	public RectangleGameObject(String name, boolean addToRoot) {
		super(name, addToRoot);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!isVisible() || this.getColor().a <= 0.1f) {
			return;
		}

		Gdx.graphics.getGL10().glEnable(GL10.GL_BLEND);
		Gdx.graphics.getGL10().glBlendFunc(GL10.GL_SRC_ALPHA,
				GL10.GL_ONE_MINUS_SRC_ALPHA);

		shapeRenderer.setProjectionMatrix(GdxGame.camera.combined);
		shapeRenderer.begin(ShapeType.FilledRectangle);
		shapeRenderer.setColor(this.getColor());
		shapeRenderer.filledRect(this.getPositionX(), this.getPositionY(),
				this.getWidth(), this.getHeight());
		shapeRenderer.end();
	}
}
