package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.TutorialScreen;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class TutorialButton extends GameObject {
	private final String type;
	private final TutorialScreen tutorial;

	public TutorialButton(String skinName, boolean addToRoot,
			boolean enableBlending, String type, TutorialScreen tutorial) {
		super(skinName, addToRoot, enableBlending);

		this.tutorial = tutorial;
		this.type = type;

        setTouchable(Touchable.enabled);
		
		this.addListener(new ClickListener() {
	        public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
	                return touchDownOld(x, y, pointer);
	        }
		});		
	}
	
	public boolean touchDownOld(float arg_x, float arg_y, int arg2) {
		AudioPlayer.playSound("tap1");
		this.clearActions();

		if (type.equals("right")) {
			MoveToAction rightMoveTo1 = moveTo(365 * GdxGame.HD_X_RATIO, 350 * GdxGame.HD_Y_RATIO, 0.15f);
			MoveToAction rightMoveTo2 = moveTo(350 * GdxGame.HD_X_RATIO, 350 * GdxGame.HD_Y_RATIO, 0.15f);
			ScaleToAction scale1 = scaleTo(-1.05f, 1.05f, 0.15f);
			ScaleToAction scale2 = scaleTo(-1.0f, 1.0f, 0.15f);
			this.addAction(parallel(sequence(rightMoveTo1, rightMoveTo2), sequence(scale1, scale2)));

			this.tutorial.setPage(this.tutorial.currPage + 1);
		} else if (type.equals("left")) {
			MoveToAction rightMoveTo1 = moveTo(5 * GdxGame.HD_X_RATIO, 350 * GdxGame.HD_Y_RATIO, 0.15f);
			MoveToAction rightMoveTo2 = moveTo(20 * GdxGame.HD_X_RATIO, 350 * GdxGame.HD_Y_RATIO, 0.15f);
			ScaleToAction scale1 = scaleTo(1.05f, 1.05f, 0.15f);
			ScaleToAction scale2 = scaleTo(1.0f, 1.0f, 0.15f);
			this.addAction(parallel(sequence(rightMoveTo1, rightMoveTo2), sequence(scale1, scale2)));

			this.tutorial.setPage(this.tutorial.currPage - 1);
		} else {
			AudioPlayer.playSound("tap2");
			this.tutorial.dispose();
		}

		return true;
	}
}
