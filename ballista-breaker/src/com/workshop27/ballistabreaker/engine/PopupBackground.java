package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.workshop27.ballistabreaker.GdxGame;

public class PopupBackground extends GameObject {
	public PopupBackground(String string, boolean b, boolean c) {
		super(string, b, c);

        setTouchable(Touchable.enabled);

        this.addListener(new InputListener() {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return touchDownOld(x, y, pointer);
            }
        });
    }

	public boolean touchDownOld(float arg_x, float arg_y, int arg2) {
		if (this.getName().equals("background-tutorial")) {
			GdxGame.tutorialObject.closeTutorial();
		}

		return true;
	}
}
