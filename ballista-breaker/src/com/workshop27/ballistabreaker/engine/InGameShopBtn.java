package com.workshop27.ballistabreaker.engine;

import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.GameScreen;
import com.workshop27.ballistabreaker.screens.MainMenuScreen;
import com.workshop27.ballistabreaker.screens.ShopScreen;

public class InGameShopBtn extends GameObject {

	private int mShopId = 0;
	private final GdxGame game;
	private GameScreen gs;
	private MainMenuScreen ms;

	public InGameShopBtn(String name, boolean addToRoot,
			boolean enableBlending, GdxGame game, GameScreen gs) {
		super(name, addToRoot, enableBlending);

		this.gs = gs;
		this.game = game;

		if (name.equals("shop-expl")) {
			mShopId = 1;
		}

		else if (name.equals("shop-laser")) {
			mShopId = 2;
		}

		else if (name.equals("shop-karate")) {
			mShopId = 3;
		}

		else if (name.equals("shop-boom")) {
			mShopId = 4;
		}

		if (name.equals("shop-range")) {
			mShopId = 5;
		}

		if (name.equals("shop-egg1")) {
			mShopId = 6;
		}

		if (name.equals("shop-egg2")) {
			mShopId = 7;
		}

		if (name.equals("shop-bundle")) {
			mShopId = 8;
		}

		else if (name.equals("buy-btn")) {
			mShopId = 98;
		}

		else if (name.equals("no-btn")) {
			mShopId = 99;
		}

		else if (name.equals("finish-level-btn")) {
			mShopId = 97;
		}
	}

	public InGameShopBtn(String name, boolean addToRoot,
			boolean enableBlending, GdxGame game, MainMenuScreen ms) {
		super(name, addToRoot, enableBlending);

		this.ms = ms;
		this.game = game;

		if (name.equals("ok-btn")) {
			mShopId = 89;
		}
	}

	/*@Override
	public boolean touchDown(float arg_x, float arg_y, int arg2) {
		AudioPlayer.playSound("tap3");

		if (mShopId == 99) {
			gs.hideYesNoBox();
		}

		else if (mShopId == 98) {
			gs.hideYesNoBox();
			this.game.buyItem(ShopScreen.mBtnClickedId);
		}

		else if (mShopId == 97) {
			GdxGame.mInventory.initAutoClose();
			GameScreen.isLevelEnd = true;
		}

		else if (mShopId == 89) {
			ms.hideYesNoBox();
		}

		else if (mShopId < 97) {
			ShopScreen.mBtnClickedId = mShopId;
			gs.showYesNoBox(mShopId);
		}

		return true;
	}*/
}
