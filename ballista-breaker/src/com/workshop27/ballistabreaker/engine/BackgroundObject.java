package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle.BeetleState;
import com.workshop27.ballistabreaker.rocks.FanBeetle;
import com.workshop27.ballistabreaker.rocks.MinerBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class BackgroundObject extends GameObject {
	public BackgroundObject(String string, boolean b, boolean c) {
		super(string, b, c);

        setTouchable(Touchable.enabled);

        initClickListener();
	}

	private void activateBeetleType2(AbstractBeetle beetle) {
		if (beetle.getState() == BeetleState.INFLY
				&& beetle.mineCount < Constants.BASE_EGGS_QUANTITY
						+ GdxGame.mInventory.getBonusEggs()) {

			AudioPlayer.playSound("bomb_plant");
			GameScreen.bManager.addMinnerEgg(((MinerBeetle) beetle).getName2()
					.get(beetle.mineCount), beetle.getRotation());
		}
	}

	private void activateBeetleType3(AbstractBeetle beetle) {
		if ((beetle.getState() == BeetleState.INFLY || beetle.getState() == BeetleState.ANIM_TO_FLY)
				&& beetle.mineCount < 1) {
			beetle.clearParticle("p4");

			GameScreen.bManager.shotFanEggs(Constants.BASE_EGGS_QUANTITY
					+ GdxGame.mInventory.getBonusEggs(),
					((FanBeetle) beetle).getNames(), beetle.getNewAngle());

			beetle.setState(BeetleState.BREAKING);
		}
	}

	private void activateBeetleType4(AbstractBeetle beetle) {
		if ((beetle.getState() == BeetleState.INFLY || beetle.getState() == BeetleState.ANIM_TO_FLY)) {
			beetle.clearParticle("p4");

			beetle.beetleSpeedX = 0;
			beetle.beetleSpeedY = 0;
			// beetle.explodeOnTouch = true;
		}
	}

    private void initClickListener()
    {
        ClickListener clickListener = new ClickListener(){
            @Override
            public boolean touchDown(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y, int pointer, int button)
            {
                return touchDownOld(x, y, pointer);
            }
        };

        addListener(clickListener);
    }

	public boolean touchDownOld(float arg_x, float arg_y, int arg2) {
		if (!this.isVisible() || arg2 != 0) {
			return false;
		}

		AbstractBeetle beetle = GameScreen.bManager.getActiveBeetle();
		if (beetle != null) {
			Integer type = beetle.getType();

			switch (type) {
			case 2:
				activateBeetleType2(beetle);
				break;
			case 3:
				activateBeetleType3(beetle);
				break;
			case 4:
				activateBeetleType4(beetle);
				break;
			default:
				break;
			}

            Slingshot.setBombPlaceTouchable(Touchable.enabled);
		}
		return true;
	}
}
