package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Files.FileType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

public class MusicEvent {
	private final String musicName;
	private final Music music;

	public MusicEvent(String name) {
		this.musicName = name;
		this.music = Gdx.audio.newMusic(Gdx.files.getFileHandle("data/music/"
				+ name + ".ogg", FileType.Internal));
		this.music.setVolume(1.0f);
		this.music.setLooping(true);
	}

	public void play() {
		this.music.play();
	}

	public String getName() {
		return this.musicName;
	}

	public Music getMusic() {
		return this.music;
	}

}
