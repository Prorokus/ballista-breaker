package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.SnapshotArray;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.GameObjectAnimation.AnimKey;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class GameObject extends Group {

	public TextureRegion skinRegion;
	public String skinName = "";
	public String folderName = "main";

	public ObjectMap<String, ParticleEffect> particleStack;
	private boolean effectsInheritPos;
	private boolean enableBlending = true;

	private float parallaxX = 1.0f;
	// private float parallaxY = 1.0f;

	public Animation animCache;
	public boolean haveAnimation = false;
	public boolean pongAnimation = false;
	public boolean animPaused = false;
	public float pauseAnim = 0;
	public float animWaitTimer = 0;
	private float stateTime = 0;

	private final ParallaxCamera camera = (ParallaxCamera) GdxGame.beetleStage
			.getCamera();

	private boolean useParallax = false;
	private float parallaxCameraOffsetX;

	private boolean needChangePos = false;
	private boolean changeParticlesParallaxPos = true;
	protected boolean filter = false;
	private boolean customizedFilter = false;

	public boolean loadSkinOnDraw = false;

	private AnimKey animParam;

	public boolean mDrawParticlesFirst = false;

	public boolean isParticlesPaused = false;

	public GameObject(String name, boolean addToRoot, float parallaxX,
			float parallaxY) {
		super();
		this.setName(name);
		
		this.parallaxX = parallaxX;
		if (addToRoot) {
			GdxGame.beetleStage.addActor(this);
		}

		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;

        setTouchable(Touchable.childrenOnly);
	}

	public GameObject(boolean addToRoot, float parallaxX, float parallaxY) {
		super();

		this.parallaxX = parallaxX;
		if (addToRoot) {
			GdxGame.beetleStage.addActor(this);
		}

		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;

        setTouchable(Touchable.childrenOnly);
	}

	public GameObject() {
		super();

		GdxGame.beetleStage.addActor(this);

		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;

        setTouchable(Touchable.childrenOnly);
	}

	public GameObject(String name) {
		this(name, name, true);
	}

	public GameObject(String name, boolean addToRoot) {
		this(name, name, addToRoot);
	}

	public GameObject(String name, boolean addToRoot, boolean enableBlending,
			float parallaxX, float parallaxY) {
		this(name, name, addToRoot);
		this.enableBlending = enableBlending;
		this.parallaxX = parallaxX;
		// this.parallaxY = parallaxY;
		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;
	}

	public GameObject(String name, String skinName, boolean addToRoot,
			boolean enableBlending, float parallaxX, float parallaxY) {
		this(name, skinName, addToRoot);
		this.enableBlending = enableBlending;
		this.parallaxX = parallaxX;
		// this.parallaxY = parallaxY;
		this.useParallax = this.parallaxX <= 0.99f || this.parallaxX >= 1.01f;
	}

	public GameObject(String name, boolean addToRoot, boolean enableBlending) {
		this(name, name, addToRoot);
		this.enableBlending = enableBlending;
	}

	public GameObject(String name, String skinName, boolean addToRoot) {
		super();
		this.setName(name);

		this.skinName = skinName;

		if (addToRoot) {
			GdxGame.beetleStage.addActor(this);
		}

        setTouchable(Touchable.childrenOnly);
	}

	public void setOnCreateFilter(Boolean filter) {
		this.filter = filter;
		this.customizedFilter = filter;
	}

	public void setSkin() {
		this.filter = this.customizedFilter;

		if (this.skinName.equals("")) {
			return;
		}

		this.skinRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(skinName);

		if (this.skinRegion == null) {
			return;
		}

		if (getWidth() == 0) {
			setWidth(this.skinRegion.getRegionWidth());
		}

		if (getHeight() == 0) {
			setHeight(this.skinRegion.getRegionHeight());
		}
	}

	public void loadAnimationFrames() {
		this.animCache = ImageCache.animations.get(this.animParam.animTexName);

		if (this.animCache == null) {
			Array<TextureRegion> anim_tex_regions = new Array<TextureRegion>();
			TextureRegion currFrame;

			while (true) {
				currFrame = ImageCache
						.getManager()
						.get("data/atlas/" + this.folderName + "/pack",
								TextureAtlas.class)
						.findRegion(
								this.animParam.animTexName + "-f0"
										+ this.animParam.framesCount);

				if (currFrame != null) {
					anim_tex_regions.add(currFrame);
				} else {
					break;
				}

				this.animParam.framesCount++;
			}
			this.animParam.framesCount--;
			int pong_frames = this.animParam.framesCount;

			if (this.pongAnimation) {
				this.animParam.framesCount = this.animParam.framesCount
						+ this.animParam.framesCount - 1;

				while (pong_frames > 2) {
					pong_frames--;
					currFrame = ImageCache
							.getManager()
							.get("data/atlas/" + this.folderName + "/pack",
									TextureAtlas.class)
							.findRegion(
									this.animParam.animTexName + "-f0"
											+ pong_frames);
					anim_tex_regions.add(currFrame);
				}

			}

			this.animCache = new Animation(this.animParam.duration, anim_tex_regions);
			ImageCache.animations.put(this.animParam.animTexName,
					this.animCache);
		}

		TextureRegion frame = this.animCache.getKeyFrame(0, false);
		this.skinRegion = frame;

		if (this.getWidth() == 0) {
			this.setWidth(frame.getRegionWidth());
		}

		if (this.getHeight() == 0) {
			this.setHeight(frame.getRegionHeight());
		}
	}

	public boolean hasActiveActions() {
		return getActions().size > 0;
	}

	public void AddAnim(AnimKey animParam) {
		this.haveAnimation = true;
		this.animParam = animParam;
	}

	public void loadParticleData(String s) {
		ParticleEffect effect = particleStack.get(s);

		effect.load(
				Gdx.files.internal("data/" + "/particles/" + s + ".txt"),
				ImageCache.getManager().get("data/atlas/particles/pack",
						TextureAtlas.class));

		effect.setPosition(this.getPositionX() + this.getOriginX(),
				this.getPositionY() + this.getOriginY());
	}

	public void createParticle(String name, Boolean inherit_pos) {
		if (this.particleStack == null) {
			this.particleStack = new ObjectMap<String, ParticleEffect>();
			this.effectsInheritPos = inherit_pos;
		}

		ParticleEffect effect = new ParticleEffect();
		this.particleStack.put(name, effect);
	}

	public int getParticlesCount() {
		return particleStack.size;
	}

	public void clearParticles() {
		if (particleStack != null && particleStack.size > 0) {
			particleStack.clear();
		}
	}

	public void clearParticle(String name) {
		if (particleStack != null && particleStack.size > 0) {
			particleStack.remove(name);
		}
	}

	public void clearParticlesAndRemove() {
		clearParticles();
		// this.markToRemove(true);
	}

	public void setOriginX(float x) {
		super.setOriginX(x * GdxGame.HD_X_RATIO);
	}

	public void setOriginXLiterally(float x) {
		super.setOriginX(x);
	}

	public void setOriginY(float y) {
		super.setOriginY(y * GdxGame.HD_Y_RATIO);
	}

	public void setOriginYLiterally(float y) {
		super.setOriginY(y);
	}

	public void setWidth(float width) {
		super.setWidth(width * GdxGame.HD_X_RATIO);
	}

	public void setHeight(float height) {
		super.setHeight(height * GdxGame.HD_Y_RATIO);
	}

	public void setScaleX(float scaleX) {
		super.setScaleX(scaleX);
	}

	public void setScaleY(float scaleY) {
		super.setScaleY(scaleY);
	}

	public float getOriginX() {
		return super.getOriginX();
	}

	public float getWidth() {
		return super.getWidth();
	}

	public float getHeight() {
		return super.getHeight();
	}

	public float getOriginY() {
		return super.getOriginY();
	}

	public float getPositionX() {
		return super.getX();
	}

	public Vector2 getCenter() {
		return new Vector2(this.getX() + this.getWidth() / 2, this.getY() + this.getHeight() / 2);
	}

	public float getPositionY() {
		return this.getY();
	}

	public void setPositionX(float x) {
		this.setX(x * GdxGame.HD_X_RATIO);
	}

	public void setPositionY(float y) {
		this.setY(y * GdxGame.HD_Y_RATIO);
	}

	public void setPositionXY(float x, float y) {
		this.setX(x * GdxGame.HD_X_RATIO);
		this.setY(y * GdxGame.HD_Y_RATIO);
	}

	public void setPositionXYLiterally(float x, float y) {
		this.setX(x);
		this.setY(y);
	}

	public void setPositionXLiterally(float x) {
		this.setX(x);
	}

	public void setPositionYLiterally(float y) {
		this.setY(y);
	}

	public void clearFilter() {
		this.skinRegion.getTexture().setFilter(TextureFilter.Nearest,
				TextureFilter.Nearest);
	}

	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		if (filter) {
			this.skinRegion.getTexture().setFilter(TextureFilter.Linear,
					TextureFilter.Linear);
			filter = false;
		}

		if (!GdxGame.enableParallax) {
			batch.setColor(this.getColor());

			batch.draw(this.skinRegion, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(),
					getScaleX(), getScaleY(), getRotation());
		} else {
			this.parallaxCameraOffsetX = getX();

			if (this.useParallax) {
				if (this.camera.recalculateParallax(parallaxX)) {
					batch.setProjectionMatrix(this.camera
							.calculateParallaxMatrix(parallaxX));
				}

				this.parallaxCameraOffsetX -= this.camera.viewportWidth
						* (1 - parallaxX);
			} else {
				if (this.camera.recalculateParallax(1)) {
					batch.setProjectionMatrix(this.camera
							.calculateParallaxMatrix(1.0f));
				}
			}

			batch.setColor(this.getColor());
			batch.draw(this.skinRegion, this.parallaxCameraOffsetX, getY(), getOriginX(),
					getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());

		}

	}

	private void calcNextFrame() {
		this.stateTime += Gdx.graphics.getDeltaTime();

		if (this.animPaused) {
			if (this.skinRegion == null) {
				this.skinRegion = animCache.getKeyFrame(0, false);
			} else {
				return;
			}
		}

		if (this.pauseAnim <= 0) {
			this.skinRegion = animCache.getKeyFrame(this.stateTime, true);
		}

		else if (this.pauseAnim > 0
				&& this.stateTime > this.animParam.framesCount
						* animCache.frameDuration) {
			this.animWaitTimer += Gdx.graphics.getDeltaTime();
			if (this.animWaitTimer >= this.pauseAnim) {
				this.stateTime = 0;
				this.animWaitTimer = 0;
			}
		}

		else {
			this.skinRegion = animCache.getKeyFrame(this.stateTime, true);
		}
	}

	public void drawParticles(SpriteBatch batch, float parentAlpha) {
		if (particleStack != null && particleStack.size > 0) {
			for (String s : particleStack.keys()) {
				if (!GdxGame.enableParallax) {
					if (this.effectsInheritPos) {
						particleStack.get(s).setPosition(
								this.getPositionX() + this.getOriginX(),
								this.getPositionY() + this.getOriginY());
					}

					if (this.useParallax && this.needChangePos) {
						this.needChangePos = false;
						this.changeParticlesParallaxPos = true;
						particleStack.get(s).setPosition(
								this.getPositionX() + this.getOriginX(),
								this.getPositionY() + this.getOriginY());
					}

					if (particleStack.get(s).getEmitters().size == 0) {
						loadParticleData(s);
						return;
					}

					if (this.getColor().a > 0.1f && !this.isParticlesPaused) {
						particleStack.get(s).draw(batch,
								Gdx.graphics.getDeltaTime());
					}
				} else {
					// ------------------PARTICLE
					// DRAW--------------------------------------
					if (particleStack.get(s).getEmitters().size == 0) {
						loadParticleData(s);
					}

					if (this.effectsInheritPos) {
						if (!this.useParallax && !GdxGame.enableParallax) {
							particleStack.get(s).setPosition(
									this.getPositionX() + this.getOriginX(),
									this.getPositionY() + this.getOriginY());
						} else {
							this.changeParticlesParallaxPos = true;
						}
					}

					if (this.useParallax && this.changeParticlesParallaxPos) {
						this.changeParticlesParallaxPos = false;
						this.needChangePos = true;

						this.parallaxCameraOffsetX = this.getPositionX();

						this.parallaxCameraOffsetX -= this.camera.viewportWidth
								* (1 - parallaxX);

						particleStack.get(s).setPosition(
								this.parallaxCameraOffsetX + this.getOriginX(),
								this.getPositionY() + this.getOriginY());
					}

					if (this.useParallax) {
						if (this.camera.recalculateParallax(parallaxX)) {
							batch.setProjectionMatrix(this.camera
									.calculateParallaxMatrix(parallaxX));
						}
					} else {
						if (this.camera.recalculateParallax(1)) {
							batch.setProjectionMatrix(this.camera
									.calculateParallaxMatrix(1));
						}
					}
					particleStack.get(s).draw(batch,
							Gdx.graphics.getDeltaTime());
					// ---------------------------------------------------------------------

				}
			}
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!isVisible()) {
			return;
		}

		if (mDrawParticlesFirst && !GameScreen.isGamePaused()) {
			drawParticles(batch, parentAlpha);
		}

		if (this.skinRegion != null) {
			if (this.haveAnimation) {
				calcNextFrame();
			}

			if (!this.enableBlending) {
				batch.disableBlending();
				this.drawSkin(batch, parentAlpha);
				batch.enableBlending();
			}

			else {
				this.drawSkin(batch, parentAlpha);
			}

		} else if (this.loadSkinOnDraw && !this.skinName.equals("")) {
			setSkin();
		}

		else if (this.haveAnimation) {
			loadAnimationFrames();
		}

		if (this.getChildren().size > 0) {
			super.draw(batch, parentAlpha);
		}

		if (!mDrawParticlesFirst && !GameScreen.isGamePaused()) {
			drawParticles(batch, parentAlpha);
		}
	}

	public void clearAnimCache() {
		if (animParam != null) {
			ImageCache.animations.remove(this.animParam.animTexName);
		}
	}

	public void clearSkinAndName() {
		this.skinName = "";
		this.skinRegion = null;
	}

	public void clearSkin() {
		this.skinRegion = null;
	}

	@Override
	public void clear() {
		SnapshotArray<Actor> objects = this.getChildren();
		for (int i = 0; i < objects.size; i++) {
			GameObject obj = (GameObject) objects.get(i);
			obj.clear();
		}

		this.clearAnimCache();
		this.clearSkinAndName();
	}
}