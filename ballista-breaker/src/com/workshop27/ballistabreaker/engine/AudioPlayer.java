package com.workshop27.ballistabreaker.engine;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;

import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class AudioPlayer {
	private static final HashMap<String, SoundEvent> PLAYING_SOUND = new HashMap<String, SoundEvent>();
	private static final HashMap<String, MusicEvent> PLAYING_MUSICS = new HashMap<String, MusicEvent>();

	private static String randomBeetleSound;

	public static void PlayRandomBeetleSound(int type) {
		if (GameScreen.isGamePaused()) {
			return;
		}

		if (type == 1) {
			randomBeetleSound = "booma_name";
		} else if (type == 2) {
			randomBeetleSound = "miner_name";
		} else if (type == 3) {
			randomBeetleSound = "egger_name";
		} else if (type == 4) {
			randomBeetleSound = "blaster_name";
		} else {
			randomBeetleSound = "breaker_name";
		}

		playSound(randomBeetleSound, 0.2f);
	}

	public static void PlayRandomLaunchBeetleSound(int type) {
		if (GameScreen.isGamePaused()) {
			return;
		}

		if (type == 1) {
			randomBeetleSound = "booma" + (Slingshot.getRandomInt(2) + 1);
		} else if (type == 2) {
			randomBeetleSound = "miner" + (Slingshot.getRandomInt(2) + 1);
		} else if (type == 3) {
			randomBeetleSound = "egger" + (Slingshot.getRandomInt(2) + 1);
		} else if (type == 4) {
			randomBeetleSound = "blaster" + (Slingshot.getRandomInt(2) + 1);
		} else {
			randomBeetleSound = "breaker" + (Slingshot.getRandomInt(2) + 1);
		}

		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				playSound(randomBeetleSound, 0.2f);
			}
		}, 250);
	}

	public static void setRandomMusic(boolean m) {
	}

	public static void Think(float delta) {
		/*
		 * if (randomMusicTimer <= 0 && playRandomMusic) { randomMusicTimer =
		 * Slingshot.getRandomInt(25) + 1 + 25;
		 * 
		 * PlayRandomMusic(); } else { randomMusicTimer -= delta; }
		 */
	}

	public static SoundEvent getSound(String s) {
		return PLAYING_SOUND.get(s);
	}

	public static MusicEvent getMusic(String s) {
		return PLAYING_MUSICS.get(s);
	}

	public static void removeSound(String s) {
		PLAYING_SOUND.remove(s);
	}

	public static void playSound(String s, float volume, boolean loop) {
		try {
			if (GdxGame.files != null
					&& GdxGame.files.getOptionsAndProgress("options", "sound")
							.equals("off")) {
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (PLAYING_SOUND.containsKey(s)) {
			PLAYING_SOUND.get(s).play(volume, loop);
		} else {
			PLAYING_SOUND.put(s, new SoundEvent(s, volume, loop));
		}
	}

	public static void playSound(String s, float volume) {
		try {
			if (GdxGame.files != null
					&& GdxGame.files.getOptionsAndProgress("options", "sound")
							.equals("off")) {
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (PLAYING_SOUND.containsKey(s)) {
			PLAYING_SOUND.get(s).play(volume);
		} else {
			PLAYING_SOUND.put(s, new SoundEvent(s, volume));
		}
	}

	public static void playSound(String s) {
		try {
			if (GdxGame.files != null
					&& GdxGame.files.getOptionsAndProgress("options", "sound")
							.equals("off")) {
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (PLAYING_SOUND.containsKey(s)) {
			PLAYING_SOUND.get(s).play();
		} else {
			PLAYING_SOUND.put(s, new SoundEvent(s));
		}
	}

	public static void stopSound(String s) {
		if (PLAYING_SOUND.containsKey(s)) {
			PLAYING_SOUND.get(s).getSound().stop();
		}
	}

	public static void stopAllSound() {
		Iterator<Entry<String, SoundEvent>> it1 = PLAYING_SOUND.entrySet()
				.iterator();

		while (it1.hasNext()) {
			Entry<String, SoundEvent> pairs = it1.next();
			pairs.getValue().getSound().stop();
		}
	}

	public static void playMusic(String s) {
		try {
			if (GdxGame.files.getOptionsAndProgress("options", "music").equals(
					"off")) {
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pauseAllMusic();

		if (PLAYING_MUSICS.containsKey(s)
				&& !PLAYING_MUSICS.get(s).getMusic().isPlaying()) {
			PLAYING_MUSICS.get(s).getMusic().play();
		}

		else if (!PLAYING_MUSICS.containsKey(s)) {
			MusicEvent musicEvent = new MusicEvent(s);
			PLAYING_MUSICS.put(s, musicEvent);
			musicEvent.getMusic().play();
		}
	}

	public static void playMusic(String s, float volume) {
		try {
			if (GdxGame.files.getOptionsAndProgress("options", "music").equals(
					"off")) {
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pauseAllMusic();

		if (PLAYING_MUSICS.containsKey(s)
				&& !PLAYING_MUSICS.get(s).getMusic().isPlaying()) {
			PLAYING_MUSICS.get(s).getMusic().setVolume(volume);
			PLAYING_MUSICS.get(s).getMusic().play();
		}

		else if (!PLAYING_MUSICS.containsKey(s)) {
			MusicEvent musicEvent = new MusicEvent(s);
			PLAYING_MUSICS.put(s, musicEvent);
			musicEvent.getMusic().setVolume(volume);
			musicEvent.getMusic().play();
		}
	}

	public static void playMusicFromStart(String s, float volume) {
		try {
			if (GdxGame.files.getOptionsAndProgress("options", "music").equals(
					"off")) {
				return;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		pauseAllMusic();

		if (PLAYING_MUSICS.containsKey(s)
				&& !PLAYING_MUSICS.get(s).getMusic().isPlaying()) {
			PLAYING_MUSICS.get(s).getMusic().setVolume(volume);

			PLAYING_MUSICS.get(s).getMusic().stop();
			PLAYING_MUSICS.get(s).getMusic().play();
		}

		else if (!PLAYING_MUSICS.containsKey(s)) {
			MusicEvent musicEvent = new MusicEvent(s);
			PLAYING_MUSICS.put(s, musicEvent);
			musicEvent.getMusic().setVolume(volume);

			musicEvent.getMusic().stop();
			musicEvent.getMusic().play();
		}
	}

	public static void pauseMusic(String s) {
		if (PLAYING_MUSICS.containsKey(s)) {
			PLAYING_MUSICS.get(s).getMusic().pause();
		}
	}

	public static void stopMusic(String s) {
		if (PLAYING_MUSICS.containsKey(s)) {
			PLAYING_MUSICS.get(s).getMusic().stop();
		}
	}

	public static void pauseAllMusic() {
		Iterator<Entry<String, MusicEvent>> it1 = PLAYING_MUSICS.entrySet()
				.iterator();

		while (it1.hasNext()) {
			Entry<String, MusicEvent> pairs = it1.next();
			pairs.getValue().getMusic().pause();
		}
	}

	public static void stopAllMusic() {
		Iterator<Entry<String, MusicEvent>> it1 = PLAYING_MUSICS.entrySet()
				.iterator();

		while (it1.hasNext()) {
			Entry<String, MusicEvent> pairs = it1.next();
			pairs.getValue().getMusic().stop();
		}
	}

	public static void clearAllSounds() {
		Iterator<Entry<String, SoundEvent>> it1 = PLAYING_SOUND.entrySet()
				.iterator();

		while (it1.hasNext()) {
			Entry<String, SoundEvent> pairs = it1.next();
			pairs.getValue().getSound().stop();
			pairs.getValue().getSound().dispose();
			it1.remove();
		}
	}

	public static void clearAllMusic() {
		Iterator<Entry<String, MusicEvent>> it1 = PLAYING_MUSICS.entrySet()
				.iterator();

		while (it1.hasNext()) {
			Entry<String, MusicEvent> pairs = it1.next();
			pairs.getValue().getMusic().stop();
			pairs.getValue().getMusic().dispose();
			it1.remove();
		}
	}
}
