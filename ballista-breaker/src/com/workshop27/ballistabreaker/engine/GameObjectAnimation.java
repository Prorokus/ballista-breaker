package com.workshop27.ballistabreaker.engine;

public class GameObjectAnimation {
	public static class AnimKey {
		public String animTexName;

		public float anim_width;
		public float anim_height;
		public int framesCount = 1;

		public float duration;

		public AnimKey(String name, float duration) {
			this.animTexName = name;
			this.duration = duration;
		}

		public AnimKey(String name, float duration, float anim_width,
				float anim_height) {
			this.animTexName = name;
			this.anim_width = anim_width;
			this.anim_height = anim_height;
			this.duration = duration;
		}
	}
}
