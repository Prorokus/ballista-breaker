package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.StageScreen;

public class LeafObject extends GameObject {

	private Vector2 star1XY;
	private Vector2 star2XY;
	private Vector2 star3XY;

	private Vector2 star1WH;
	private Vector2 star2WH;
	private Vector2 star3WH;

	private Vector2 star1OrXY;
	private Vector2 star2OrXY;
	private Vector2 star3OrXY;

	private TextureRegion star1Region;
	private TextureRegion star2Region;
	private TextureRegion star3Region;

	private TextureRegion leaf;
	private TextureRegion bud;

	private final GdxGame game;
	private final int level;

	private String stars;
	private final String angle;
	private Vector2 budWH;
	private Vector2 leafWH;
	private final String skinName2;
	private final int budOrX;
	private final int budOrY;
	private final int budX;
	private final int budY;

	private boolean filtersSet = false;
	private float touchx;
	private float touchy;

	private final Vector2 mDrag;

	public LeafObject(String skinName, String actorName, boolean b,
			GdxGame game, int level, String levelName, String angle,
			int budOrX, int budOrY, int budX, int budY) {
		super(actorName, "", b);
		this.skinName2 = skinName;
		this.angle = angle;
		this.game = game;
		this.level = level;
		this.budOrX = (int) (budOrX * GdxGame.HD_X_RATIO);
		this.budOrY = (int) (budOrY * GdxGame.HD_Y_RATIO);
		this.budX = (int) (budX * GdxGame.HD_X_RATIO);
		this.budY = (int) (budY * GdxGame.HD_Y_RATIO);
		try {
			stars = GdxGame.files.getOptionsAndProgress("levels", levelName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		mDrag = new Vector2(0, 0);

        setTouchable(Touchable.enabled);

        initClickListener();
	}

	@Override
	public void setPositionXY(float x, float y) {
		this.setX(x * GdxGame.HD_X_RATIO);
		this.setY(y * GdxGame.HD_Y_RATIO);
	}

	public void setStars(String stars) {
		this.stars = stars;

	}

	public String getStars() {
		return stars;
	}

	/*@Override
	public boolean touchDown(float arg_x, float arg_y, int arg2) {
		StageScreen.background1.touchDown(Gdx.input.getX(), Gdx.input.getY(),
				arg2);
		GdxGame.isStageDragged = false;
		touchx = Gdx.input.getX();
		touchy = Gdx.input.getY();
		return true;
	}*/

	/*@Override
	public void touchDragged(float arg_x, float arg_y, int arg2) {
		mDrag.x = Gdx.input.getX() - touchx;
		mDrag.y = Gdx.input.getY() - touchy;

		if (mDrag.len() > 10) {
			GdxGame.isStageDragged = true;
			StageScreen.background1.touchDragged(Gdx.input.getX(),
					Gdx.input.getY(), arg2);
		}
		// super.touchDragged(arg_x, arg_y, arg2);
	}*/

	/*@Override
	public void touchUp(float arg_x, float arg_y, int arg2) {
		super.touchUp(arg_x, arg_y, arg2);
		if (!GdxGame.isStageDragged) {
			if (GdxGame.isStageMoving || GdxGame.isStageDragged) {
				return;
			}
			// float stage = (float) (level / 24.0);
			if (StageScreen.lock2.isVisible() && GdxGame.stageNumber > 1) {
				AudioPlayer.playSound("tap1");
				return;
			}

			if (StageScreen.lock3.isVisible() && GdxGame.stageNumber > 2) {
				AudioPlayer.playSound("tap1");
				return;
			}

			if (StageScreen.lock4.isVisible() && GdxGame.stageNumber > 3) {
				AudioPlayer.playSound("tap1");
				return;
			}

			if (stars.equals("-1")) {
				AudioPlayer.playSound("tap1");

				return;
			} else {
				// LeafObject leaf = (LeafObject) StageScreen.rootGroup
				// .findActor("level" + level + "-btn");

				game.loadingScreen.setPrevScreenName("stage");
				game.loadingScreen.setNextScreen("game", level);

				if (!game.regularTutorial) {
					// GdxGame.camera.position.set(GdxGame.VIRTUAL_GAME_WIDTH /
					// 2,
					// GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);
					GdxGame.files.setOptionsAndProgress("tutorial", "regular",
							"yes");
					game.regularTutorial = true;
					game.tutorialScreen.setPage(1);
					game.setScreen(game.tutorialScreen);
				} else {
					game.changeScreen("loading");
				}

				AudioPlayer.playSound("tap3");
				return;
			}
		} else {
			StageScreen.background1.touchUp(Gdx.input.getX(), Gdx.input.getY(),
					arg2);
		}

	}*/

	@Override
	public void clearSkin() {
		super.clearSkin();
		this.star1Region = null;
		this.star2Region = null;
		this.star3Region = null;
		this.bud = null;
		this.leaf = null;
	}

	@Override
	public void setSkin() {
		super.setSkin();
		this.star1Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("star-level1");

		this.star2Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("star-level2");

		this.star3Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("star-level3");
		this.bud = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(skinName2 + "-closed");
		this.leaf = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(skinName2);

		this.filtersSet = false;

		this.setWidth(leaf.getRegionWidth());
		this.setHeight(leaf.getRegionHeight());

		if (angle.equals("left")) {

			star1XY = new Vector2(this.getWidth() - 70 * this.getWidth() / 110,
					this.getHeight() - 20);
			star2XY = new Vector2(this.getWidth() - 90 * this.getWidth() / 110,
					this.getHeight() - 40);
			star3XY = new Vector2(this.getWidth() - 100 * this.getWidth() / 110,
					this.getHeight() - 60);

			// star1XY = new Vector2(this.originX - this.originX / 1.8f,
			// this.originY - this.originY / 4);
			// star2XY = new Vector2(this.originX - this.originX, this.originY
			// - this.originY / 2);
			// star3XY = new Vector2(this.originX - this.originX * 1.1f,
			// this.originY - this.originY / 1.2f);
		} else {
			star1XY = new Vector2(this.getWidth() - 60 * this.getWidth() / 100,
					this.getHeight() - 30);
			star2XY = new Vector2(this.getWidth() - 40 * this.getWidth() / 100,
					this.getHeight() - 50);
			star3XY = new Vector2(this.getWidth() - 30 * this.getWidth() / 100,
					this.getHeight() - 70);
			// star1XY = new Vector2(this.originX + this.originX / 1.8f,
			// this.originY - this.originY / 4);
			// star2XY = new Vector2(this.originX + this.originX * 2f,
			// this.originY - this.originY / 2);
			// star3XY = new Vector2(this.originX + this.originX * 3f,
			// this.originY - this.originY / 1.2f);
		}

		star1WH = new Vector2(star1Region.getRegionWidth(),
				star1Region.getRegionHeight());

		star2WH = new Vector2(star2Region.getRegionWidth(),
				star2Region.getRegionHeight());

		star3WH = new Vector2(star3Region.getRegionWidth(),
				star3Region.getRegionHeight());

		budWH = new Vector2(bud.getRegionWidth(), bud.getRegionHeight());
		leafWH = new Vector2(leaf.getRegionWidth(), leaf.getRegionHeight());

		star1OrXY = new Vector2(this.getOriginX() - star1XY.x, this.getOriginY()
				- star1XY.y);
		star2OrXY = new Vector2(this.getOriginX() - star2XY.x, this.getOriginY()
				- star2XY.y);
		star3OrXY = new Vector2(this.getOriginX() - star3XY.x, this.getOriginY()
				- star3XY.y);

	}

	private void drawSkin(SpriteBatch batch, float parentAlpha, String stars) {
		batch.setColor(this.getColor());
		if (stars.equals("-1")) {
			batch.draw(this.bud, budX, budY, budOrX, budOrY, budWH.x, budWH.y,
					getScaleX(), getScaleY(), getRotation());
		} else if (stars.equals("0")) {
			batch.draw(this.leaf, getX(), getY(), getOriginX(), getOriginY(), leafWH.x, leafWH.y,
					getScaleX(), getScaleY(), getRotation());
		} else if (stars.equals("1")) {
			batch.draw(this.leaf, getX(), getY(), getOriginX(), getOriginY(), leafWH.x, leafWH.y,
					getScaleX(), getScaleY(), getRotation());
			batch.draw(this.star1Region, getX() + star1XY.x, getY() + star1XY.y,
					star1OrXY.x, star1OrXY.y, star1WH.x, star1WH.y, getScaleX(),
					getScaleY(), getRotation());
		} else if (stars.equals("2")) {
			batch.draw(this.leaf, getX(), getY(), getOriginX(), getOriginY(), leafWH.x, leafWH.y,
					getScaleX(), getScaleY(), getRotation());
			batch.draw(this.star1Region, getX() + star1XY.x, getY() + star1XY.y,
					star1OrXY.x, star1OrXY.y, star1WH.x, star1WH.y, getScaleX(),
					getScaleY(), getRotation());

			batch.draw(this.star2Region, getX() + star2XY.x, getY() + star2XY.y,
					star2OrXY.x, star2OrXY.y, star2WH.x, star2WH.y, getScaleX(),
					getScaleY(), getRotation());
		} else if (stars.equals("3")) {
			batch.draw(this.leaf, getX(), getY(), getOriginX(), getOriginY(), leafWH.x, leafWH.y,
					getScaleX(), getScaleY(), getRotation());
			batch.draw(this.star1Region, getX() + star1XY.x, getY() + star1XY.y,
					star1OrXY.x, star1OrXY.y, star1WH.x, star1WH.y, getScaleX(),
					getScaleY(), getRotation());

			batch.draw(this.star2Region, getX() + star2XY.x, getY() + star2XY.y,
					star2OrXY.x, star2OrXY.y, star2WH.x, star2WH.y, getScaleX(),
					getScaleY(), getRotation());

			batch.draw(this.star3Region, getX() + star3XY.x, getY() + star3XY.y,
					star3OrXY.x, star3OrXY.y, star3WH.x, star3WH.y, getScaleX(),
					getScaleY(), getRotation());
		}

	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		// super.draw(batch, parentAlpha);

		if (!this.filtersSet) {
			this.leaf.getTexture().setFilter(TextureFilter.Linear,
					TextureFilter.Linear);
			this.bud.getTexture().setFilter(TextureFilter.Linear,
					TextureFilter.Linear);
			this.filtersSet = true;
		}

		this.drawSkin(batch, parentAlpha, stars);
	}

    private void initClickListener(){
        ClickListener clickListener = new ClickListener(){
            @Override
            public boolean touchDown(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y, int pointer, int button) {
                super.touchDown(event, x, y, pointer, button);
                //StageScreen.background1.touchDown(Gdx.input.getX(), Gdx.input.getY(),arg2);
                GdxGame.isStageDragged = false;
                touchx = Gdx.input.getX();
                touchy = Gdx.input.getY();
                return true;
            }

            @Override
            public void touchDragged(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y, int pointer) {
                super.touchDragged(event, x, y, pointer);

                mDrag.x = Gdx.input.getX() - touchx;
                mDrag.y = Gdx.input.getY() - touchy;

                if (mDrag.len() > 10) {
                    GdxGame.isStageDragged = true;
//                    StageScreen.background1.touchDragged(Gdx.input.getX(),Gdx.input.getY(), arg2);
                }
                // super.touchDragged(arg_x, arg_y, arg2);
            }

            @Override
            public void touchUp(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                if (!GdxGame.isStageDragged) {
                    if (GdxGame.isStageMoving || GdxGame.isStageDragged) {
                        return;
                    }
                    // float stage = (float) (level / 24.0);
                    if (StageScreen.lock2.isVisible() && GdxGame.stageNumber > 1) {
                        AudioPlayer.playSound("tap1");
                        return;
                    }

                    if (StageScreen.lock3.isVisible() && GdxGame.stageNumber > 2) {
                        AudioPlayer.playSound("tap1");
                        return;
                    }

                    if (StageScreen.lock4.isVisible() && GdxGame.stageNumber > 3) {
                        AudioPlayer.playSound("tap1");
                        return;
                    }

                    if (stars.equals("-1")) {
                        AudioPlayer.playSound("tap1");

                        return;
                    } else {
                        // LeafObject leaf = (LeafObject) StageScreen.rootGroup
                        // .findActor("level" + level + "-btn");

                        game.loadingScreen.setPrevScreenName("stage");
                        game.loadingScreen.setNextScreen("game", level);

                        if (!game.regularTutorial) {
                            // GdxGame.camera.position.set(GdxGame.VIRTUAL_GAME_WIDTH /
                            // 2,
                            // GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);
                            GdxGame.files.setOptionsAndProgress("tutorial", "regular",
                                    "yes");
                            game.regularTutorial = true;
                            game.tutorialScreen.setPage(1);
                            game.setScreen(game.tutorialScreen);
                        } else {
                            game.changeScreen("loading");
                        }

                        AudioPlayer.playSound("tap3");
                        return;
                    }
                } else {
//                    StageScreen.background1.touchUp(Gdx.input.getX(), Gdx.input.getY(),arg2);
                }

            }
        };

        addListener(clickListener);
    }
}
