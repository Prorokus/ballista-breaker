package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class ArmBlickMovable extends ArmBlick {
	private float xRotated;
	private float yRotated;

	private float sin;
	private float cos;

	private float originXRotated;
	private float originYRotated;

	private float baseRotation;

	public ArmBlickMovable(String name, String skinName, boolean addToRoot) {
		super(name, skinName, addToRoot);
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		if (this.filter) {
			this.skinRegion.getTexture().setFilter(TextureFilter.Linear,
					TextureFilter.Linear);
			this.filter = false;
		}

		batch.setColor(this.getColor());

		if (GameScreen.objectToDestroy.getRotation() != 0) {
			if (originXRotated == 0 && originYRotated == 0) {
				originXRotated = GameScreen.objectToDestroy.getPositionX()
						+ GameScreen.objectToDestroy.getOriginX()
						- this.getWidth() / 2;
				originYRotated = GameScreen.objectToDestroy.getPositionY()
						+ GameScreen.objectToDestroy.getOriginY()
						- this.getHeight() / 2;
				this.baseRotation = GameScreen.objectToDestroy.getRotation();
			}

			this.sin = MathUtils.sinDeg(GameScreen.objectToDestroy.getRotation()
					- this.baseRotation);
			this.cos = MathUtils.cosDeg(GameScreen.objectToDestroy.getRotation()
					- this.baseRotation);

			this.xRotated = originXRotated + (getX() - originXRotated) * this.cos
					- (getY() - originYRotated) * this.sin;
			this.yRotated = originYRotated + (getY() - originYRotated) * this.cos
					+ (getX() - originXRotated) * this.sin;
		}

		else {
			this.xRotated = getX();
			this.yRotated = getY();
		}

		batch.draw(this.skinRegion, this.xRotated, this.yRotated, getOriginX(),
				getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
	}
}
