package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.audio.Sound;

public class SoundEvent {
	private final String soundName;
	private final Sound sound;
	private long soundId = 0;

	public SoundEvent(String name) {
		this.soundName = name;

		this.sound = ImageCache.getManager().get(
				"data/sounds/" + name + ".ogg", Sound.class);

		this.soundId = this.sound.play(1.0f);
	}

	public SoundEvent(String name, float volume) {
		this.soundName = name;

		this.sound = ImageCache.getManager().get(
				"data/sounds/" + name + ".ogg", Sound.class);

		this.soundId = this.sound.play(volume);
	}

	public SoundEvent(String name, float volume, boolean loop) {
		this.soundName = name;

		this.sound = ImageCache.getManager().get(
				"data/sounds/" + name + ".ogg", Sound.class);

		if (loop) {
			this.soundId = this.sound.loop(volume);
		} else {
			this.soundId = this.sound.play(volume);
		}
	}

	public void play() {
		this.soundId = this.sound.play(1.0f);
	}

	public void play(float volume) {
		this.soundId = this.sound.play(volume);
	}

	public void play(float volume, boolean loop) {
		this.soundId = this.sound.loop(volume);
	}

	public long getSoundId() {
		return this.soundId;
	}

	public String getName() {
		return this.soundName;
	}

	public Sound getSound() {
		return this.sound;
	}

}
