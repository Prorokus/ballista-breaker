package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.workshop27.ballistabreaker.GdxGame;

public class HdCompatibleButton extends Button {
	public String skinName = "";

	public HdCompatibleButton(TextureRegionDrawable regionUp) {
		super(regionUp);
	}

	public HdCompatibleButton(TextureRegionDrawable regionUp, TextureRegionDrawable regionDown) {
		super(regionUp, regionDown);
	}

	public void setPositionXY(float x, float y) {
		this.setX(x * GdxGame.HD_X_RATIO);
		this.setY(y * GdxGame.HD_Y_RATIO);
	}

}
