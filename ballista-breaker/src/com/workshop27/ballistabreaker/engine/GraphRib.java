package com.workshop27.ballistabreaker.engine;

public class GraphRib {

	String vertex1;
	String vertex2;
	String ribName;

	public GraphRib(String ribName, String vertex1, String vertex2) {
		this.vertex1 = vertex1;
		this.vertex2 = vertex2;
		this.ribName = ribName;
	}

	public String getVertex1() {
		return this.vertex1;
	}

	public void setVertex1(String vertex1) {
		this.vertex1 = vertex1;
	}

	public String getRibName() {
		return this.ribName;
	}

	public void setRibName(String ribName) {
		this.ribName = ribName;
	}

	public String getVertex2() {
		return this.vertex2;
	}

	public void setVertex2(String vertex2) {
		this.vertex2 = vertex2;
	}

}
