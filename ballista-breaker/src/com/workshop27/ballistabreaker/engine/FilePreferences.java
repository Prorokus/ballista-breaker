package com.workshop27.ballistabreaker.engine;

import java.io.File;
import java.io.IOException;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

import com.workshop27.ballistabreaker.constants.Constants;

public class FilePreferences {

	private File scenarioFile;
	private File gameFile;
	private String version;

	public String getOptionsAndProgress(String section, String name)
			throws Exception {
		Wini wini = null;
		try {
			wini = new Wini(gameFile);
		} catch (InvalidFileFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "null";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "null";
		}

		return Crypto.decrypt(wini.get(Crypto.encrypt(section),
				Crypto.encrypt(name)));
	}

	public void setOptionsAndProgress(String section, String name, String value) {
		Wini wini = null;
		try {
			wini = new Wini(gameFile);
		} catch (InvalidFileFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			wini.put(Crypto.encrypt(section), Crypto.encrypt(name),
					Crypto.encrypt(value));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			wini.store();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getScenario(String section, String name) throws Exception {
		Wini wini = null;
		try {
			wini = new Wini(scenarioFile);
		} catch (InvalidFileFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Crypto.decrypt(wini.get(Crypto.encrypt(section),
				Crypto.encrypt(name)));
	}

	public void setScenario(String section, String name, String value) {
		Wini wini = null;
		try {
			wini = new Wini(scenarioFile);
		} catch (InvalidFileFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			wini.put(Crypto.encrypt(section), Crypto.encrypt(name),
					Crypto.encrypt(value));
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			wini.store();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void init() {
		version = "0.2";
		scenarioFile = new File(Constants.PACKAGE_NAME + "scenario.data");
		if (!scenarioFile.exists()) {
			try {
				scenarioFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			generateScenarioFile(version);

		} else {
			try {
				if (!getScenario("scenario", "version").equals(version)) {
					scenarioFile.delete();
					try {
						scenarioFile.createNewFile();
					} catch (IOException e) {

						e.printStackTrace();
					}
					generateScenarioFile(version);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		gameFile = new File(Constants.PACKAGE_NAME + "game.data");
		if (!gameFile.exists()) {
			try {
				gameFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			generateGameFile(version);
		}

	}

	private void generateGameFile(String version2) {

		setOptionsAndProgress("options", "vibro", "off");
		setOptionsAndProgress("options", "sound", "on");
		setOptionsAndProgress("options", "music", "on");

		setOptionsAndProgress("shop", "shop", "not-used");
		setOptionsAndProgress("shop", "return-bonus", "false");

		for (int i = 1; i <= 3; i++) {
			setOptionsAndProgress("levels", "level" + i, "0");
		}

		// TESTS
		// for (int i = 4; i <= 72; i++) {
		// setOptionsAndProgress("levels", "level" + i, "0");
		// }
		// TESTS

		for (int i = 4; i <= Constants.LEVELS_COUNT; i++) {
			setOptionsAndProgress("levels", "level" + i, "-1");// -1!!!!!!!!!!!!!!
		}

		for (int i = 1; i <= Constants.LEVELS_COUNT; i++) {
			setOptionsAndProgress("points", "level" + i, "0");// 0!!!!!!!!!!!!!!!
		}

		setOptionsAndProgress("tutorial", "regular", "no");
		setOptionsAndProgress("tutorial", "miner", "no");
		setOptionsAndProgress("tutorial", "fan", "no");
		setOptionsAndProgress("tutorial", "boom", "no");
		setOptionsAndProgress("tutorial", "karate", "no");

		setOptionsAndProgress("tutorial", "fb_ad", "no");
		setOptionsAndProgress("tutorial", "stage_level", "no");
		setOptionsAndProgress("tutorial", "stage_stage", "no");
		setOptionsAndProgress("tutorial", "arrow", "no");
		setOptionsAndProgress("tutorial", "inv", "no");
		setOptionsAndProgress("tutorial", "bomb_connection", "no");
		setOptionsAndProgress("tutorial", "mustache", "no");
		setOptionsAndProgress("tutorial", "win_share", "no");
		setOptionsAndProgress("tutorial", "bonus_bug", "no");
		setOptionsAndProgress("tutorial", "shop_blaster", "no");
		setOptionsAndProgress("tutorial", "shop_karate", "no");
		setOptionsAndProgress("tutorial", "shop_laser", "no");
		setOptionsAndProgress("tutorial", "shop_mustache", "no");
		setOptionsAndProgress("tutorial", "shop_radius", "no");
		setOptionsAndProgress("tutorial", "shop_egg1", "no");
		setOptionsAndProgress("tutorial", "shop_egg2", "no");
		setOptionsAndProgress("tutorial", "90%", "no");
	}

	private void generateScenarioFile(String version2) {

		setScenario("scenario", "version", version2);

		// Только вращение, 4 - 6 скоростей симетричных
		setScenario("speed", "level1", "0 -40 -20 20 40");
		setScenario("speed", "level2", "0 -40 -20 20 40");
		setScenario("speed", "level3", "0 -40 -20 20 40");
		setScenario("speed", "level4", "0 -30 -20 20 30");
		setScenario("speed", "level5", "0 -10 10");
		setScenario("speed", "level6", "0 -20 -10 10 20");
		setScenario("speed", "level7", "0 -30 -20 -10 10 20 30");
		setScenario("speed", "level8", "0 -30 -20 -10 10 20 30");
		setScenario("speed", "level9", "0 -20 -10 10 20");
		setScenario("speed", "level10", "0 -40 -30 -20 20 30 40");
		setScenario("speed", "level11", "0 -40 -30 30 40");
		setScenario("speed", "level12", "0 -20 -30 -40 40 30 20");
		setScenario("speed", "level13", "0 -30 -20 20 30");
		setScenario("speed", "level14", "0 -40 -20 20 40");
		setScenario("speed", "level15", "0 -60 -50 -40 40 50 60");
		setScenario("speed", "level16", "0 -60 -40 40 60");
		setScenario("speed", "level17", "0 -50 -40 -30 30 40 50");
		setScenario("speed", "level18", "0 -80 -60 60 80");
		setScenario("speed", "level19", "0 -80 80");
		setScenario("speed", "level20", "0 -60 -40 40 60");
		setScenario("speed", "level21", "0 -50 -40 -30 30 40 50");
		setScenario("speed", "level22", "0 -60 -40 -30 30 40 60");
		setScenario("speed", "level23", "0 -80 -60 -30 30 60 80");
		setScenario("speed", "level24", "0 -40 -30 -20 20 30 40");

		// Только шатание, 6 - 8, скоростей симетричных
		setScenario("speed", "level25", "1 683 20 40 60");
		setScenario("speed", "level26", "1 680 20 40 60 80");
		setScenario("speed", "level27", "1 683 30 40 50 60 70");
		setScenario("speed", "level28", "1 699 30 40 50 60 70");
		setScenario("speed", "level29", "1 713 30 40 50 60 70");
		setScenario("speed", "level30", "1 710 60 70 80");
		setScenario("speed", "level31", "1 713 20 30 40 50");
		setScenario("speed", "level32", "1 708 30 50 70");
		setScenario("speed", "level33", "1 714 40 50 60");
		setScenario("speed", "level34", "1 681 40 60");
		setScenario("speed", "level35", "1 708 30 40 50 60");
		setScenario("speed", "level36", "1 710 20 30 40 50");
		setScenario("speed", "level37", "1 683 30 40 50 60");
		setScenario("speed", "level38", "1 670 30 40 60 80");
		setScenario("speed", "level39", "1 717 30 40 50 60");
		setScenario("speed", "level40", "1 658 30 60 70");
		setScenario("speed", "level41", "1 655 30 60");
		setScenario("speed", "level42", "1 705 40 50 60 70");
		setScenario("speed", "level43", "1 710 70");
		setScenario("speed", "level44", "1 701 30 40 50 60");
		setScenario("speed", "level45", "1 695 40 60 80");
		setScenario("speed", "level46", "1 709 20 30 40");
		setScenario("speed", "level47", "1 607 50 60 70 80");
		setScenario("speed", "level48", "1 660 30 40 50 60");

		// Вращение и шатание, 6 - 8, скоростей асиметричных
		setScenario("speed", "level49", "1 710 30 100 50 80 60 20 10");
		setScenario("speed", "level50", "0 -50 30 -10 90 -80 -60 80");
		setScenario("speed", "level51", "1 710 40 70 100 10 50 20 90");
		setScenario("speed", "level52", "0 -60 -40 -10 70 50 -90 -70");
		setScenario("speed", "level53", "1 690 60 70 10 50 30 40 20");
		setScenario("speed", "level54", "1 680 10 30 40 60 50 70 20");
		setScenario("speed", "level55", "1 690 30 20 90 60 40 70 100");
		setScenario("speed", "level56", "0 -100 -40 -70 -50 80 -60 70");
		setScenario("speed", "level57", "1 690 90 10 50 40 70 80 60");
		setScenario("speed", "level58", "0 -50 70 -80 90 -60 80 50");
		setScenario("speed", "level59", "0 -70 -90 -60 90 -100 70 100");
		setScenario("speed", "level60", "1 710 70 10 60 80 90 20 30");
		setScenario("speed", "level61", "1 710 10 70 20 80 60 40 100");
		setScenario("speed", "level62", "0 10 50 -40 20 -80 70 -30 -10");
		setScenario("speed", "level63", "0 -10 100 50 -50 90 -100 40");
		setScenario("speed", "level64", "1 710 10 20 40 70 50 80 100");
		setScenario("speed", "level65", "0 100 10 60 80 -60 -90 50");
		setScenario("speed", "level66", "1 710 20 80 90 70 30 60 50");
		setScenario("speed", "level67", "1 710 90 20 10 100 50 70 40");
		setScenario("speed", "level68", "1 710 60 90 80 50 40 30 20");
		setScenario("speed", "level69", "0 -10 -90 30 60 -40 90 -70");
		setScenario("speed", "level70", "0 -100 60 80 -60 -50 -80 10");
		setScenario("speed", "level71", "0 -100 -70 40 -10 20 10 100");
		setScenario("speed", "level72", "1 670 50 100 90 70 40 30 20");

		// Просто звезды 1
		setScenario("stars", "level1", "200-500-1-1-192-597-3-1-289-625-5-1");
		setScenario("stars", "level2", "180-400-1-1-180-500-4-1-180-600-6-1");
		setScenario("stars", "level3", "122-493-1-1-205-456-2-1-300-480-3-1");
		setScenario("stars", "level4", "133-489-2-1-213-681-4-1-360-566-6-1");
		setScenario("stars", "level5", "220-450-1-1-220-550-3-1-220-650-4-1");
		setScenario("stars", "level6", "130-600-1-1-325-605-3-1-250-440-5-1");
		setScenario("stars", "level7", "314-635-1-1-202-650-2-1-120-650-3-1");
		setScenario("stars", "level8", "145-460-2-1-317-455-2-1-260-580-2-1");
		setScenario("stars", "level9", "153-556-1-1-288-587-2-1-219-670-3-1");
		setScenario("stars", "level10", "158-504-1-1-298-560-2-1-158-662-3-1");
		setScenario("stars", "level11", "157-531-3-1-230-550-3-1-320-580-4-1");
		setScenario("stars", "level12", "119-533-1-1-226-659-2-1-316-572-3-1");
		setScenario("stars", "level13", "143-572-3-1-322-576-5-1-209-660-2-1");
		setScenario("stars", "level14", "143-487-1-1-239-571-2-1-360-670-3-1");
		setScenario("stars", "level15", "230-541-1-1-262-629-2-1-174-656-2-1");
		setScenario("stars", "level16", "198-489-1-1-317-456-1-1-134-672-4-1");
		setScenario("stars", "level17", "200-500-4-1-200-600-3-1-200-700-2-1");
		setScenario("stars", "level18", "154-650-3-1-234-580-3-1-325-536-3-1");
		setScenario("stars", "level19", "225-601-1-1-298-542-1-1-150-677-1-1");
		setScenario("stars", "level20", "192-515-3-1-154-665-3-1-254-617-3-1");
		setScenario("stars", "level21", "96-667-2-1-331-622-3-1-222-453-1-1");
		setScenario("stars", "level22", "227-480-3-1-231-566-3-1-233-652-3-1");
		setScenario("stars", "level23", "243-565-2-1-193-681-3-1-290-471-5-1");
		setScenario("stars", "level24", "203-670-3-1-150-496-2-1-284-488-3-1");

		// Просто звезды 2
		setScenario("stars", "level25", "238-573-3-1-281-417-3-1-134-437-3-1");
		setScenario("stars", "level26", "277-513-2-1-157-609-4-1-246-646-3-1");
		setScenario("stars", "level27", "253-629-2-1-148-554-2-1-281-514-4-1");
		setScenario("stars", "level28", "203-546-3-1-146-453-4-1-270-626-2-1");
		setScenario("stars", "level29", "104-564-2-1-306-560-2-1-206-651-3-1");
		setScenario("stars", "level30", "129-578-4-1-236-415-3-1-262-688-1-1");
		setScenario("stars", "level31", "284-581-3-1-202-537-2-1-125-583-1-1");
		setScenario("stars", "level32", "178-654-2-1-305-509-2-1-97-530-4-1");
		setScenario("stars", "level33", "104-534-2-1-223-631-3-1-333-525-2-1");
		setScenario("stars", "level34", "223-637-3-1-172-547-3-1-282-539-2-1");
		setScenario("stars", "level35", "291-684-1-1-155-476-3-1-214-580-3-1");
		setScenario("stars", "level36", "120-578-4-1-196-492-3-1-217-650-3-1");
		setScenario("stars", "level37", "160-587-1-1-234-500-1-1-281-604-4-1");
		setScenario("stars", "level38", "349-667-2-1-91-650-3-1-219-523-4-1");
		setScenario("stars", "level39", "142-601-1-1-286-604-2-1-222-570-3-1");
		setScenario("stars", "level40", "291-495-2-1-218-573-2-1-124-644-2-1");
		setScenario("stars", "level41", "126-492-1-1-287-610-1-1-177-583-1-1");
		setScenario("stars", "level42", "272-495-3-1-185-610-1-1-205-417-3-1");
		setScenario("stars", "level43", "166-491-2-1-221-583-4-1-262-424-1-1");
		setScenario("stars", "level44", "269-493-1-1-110-605-2-1-178-470-3-1");
		setScenario("stars", "level45", "244-632-1-1-149-548-3-1-295-538-3-1");
		setScenario("stars", "level46", "217-631-1-1-203-505-2-1-203-419-3-1");
		setScenario("stars", "level47", "67-637-2-1-254-519-4-1-358-667-6-1");
		setScenario("stars", "level48", "215-573-1-1-345-488-3-1-61-672-3-1");

		// Просто звезды 3
		setScenario("stars", "level49", "99-529-1-1-338-544-3-1-216-681-5-1");
		setScenario("stars", "level50", "200-500-1-1-305-577-3-1-128-584-5-1");
		setScenario("stars", "level51", "200-500-1-1-216-566-3-1-233-640-5-1");
		setScenario("stars", "level52", "117-680-1-1-259-537-3-1-305-625-5-1");
		setScenario("stars", "level53", "200-500-1-1-233-578-3-1-299-669-5-1");
		setScenario("stars", "level54", "255-545-1-1-150-621-3-1-189-678-5-1");
		setScenario("stars", "level55", "147-481-1-1-250-668-3-1-230-549-5-1");
		setScenario("stars", "level56", "138-524-1-1-331-585-3-1-214-659-5-1");
		setScenario("stars", "level57", "115-559-1-1-359-463-3-1-221-641-5-1");
		setScenario("stars", "level58", "165-584-1-1-246-598-3-1-320-651-5-1");
		setScenario("stars", "level59", "145-537-1-1-300-619-3-1-210-667-5-1");
		setScenario("stars", "level60", "200-500-1-1-185-597-3-1-237-671-5-1");
		setScenario("stars", "level61", "175-618-1-1-252-464-3-1-322-644-5-1");
		setScenario("stars", "level62", "185-660-1-1-139-592-3-1-276-505-5-1");
		setScenario("stars", "level63", "152-640-1-1-290-515-3-1-214-576-5-1");
		setScenario("stars", "level64", "200-500-1-1-262-505-3-1-222-616-5-1");
		setScenario("stars", "level65", "264-483-1-1-161-633-3-1-201-537-5-1");
		setScenario("stars", "level66", "167-466-1-1-268-584-3-1-215-653-5-1");
		setScenario("stars", "level67", "231-493-1-1-186-621-3-1-256-649-5-1");
		setScenario("stars", "level68", "200-500-1-1-270-575-3-1-211-646-5-1");
		setScenario("stars", "level69", "108-555-1-1-216-599-3-1-288-680-5-1");
		setScenario("stars", "level70", "200-500-1-1-298-552-3-1-185-645-5-1");
		setScenario("stars", "level71", "159-536-1-1-202-607-3-1-242-578-5-1");
		setScenario("stars", "level72", "244-642-1-1-268-684-3-1-300-633-5-1");

		// ------------------------------------------------------
		// 1 - 5 Видов жуков на левел (чисто ради ознакомления)
		setScenario("beetles", "level1", "7-0-0-0-0-0-0-0-0-0");
		setScenario("beetles", "level2", "7-0-0-0-0-0-0-0-0-0");
		setScenario("beetles", "level3", "0-0-9-0-0-0-0-0-0-0");
		setScenario("beetles", "level4", "6-0-4-0-0-0-0-0-0-0");
		setScenario("beetles", "level5", "1-0-0-0-5-0-0-0-0-0");
		setScenario("beetles", "level6", "5-0-3-0-3-0-0-0-0-0");
		setScenario("beetles", "level7", "0-0-0-0-0-0-7-0-0-0");
		setScenario("beetles", "level8", "2-0-5-0-0-0-3-0-0-0");
		setScenario("beetles", "level9", "0-0-0-0-0-0-0-0-4-0");
		setScenario("beetles", "level10", "5-0-0-0-2-0-0-0-2-0");
		setScenario("beetles", "level11", "5-0-5-0-0-0-0-0-0-0");
		setScenario("beetles", "level12", "0-0-0-0-0-0-2-1-2-1");
		setScenario("beetles", "level13", "0-0-3-2-2-1-0-0-0-0");
		setScenario("beetles", "level14", "4-0-0-0-0-0-2-1-0-0");
		setScenario("beetles", "level15", "1-0-1-0-1-0-1-0-0-0");
		setScenario("beetles", "level16", "0-2-0-0-2-0-2-0-1-0");
		setScenario("beetles", "level17", "0-0-2-0-1-1-1-0-1-0");
		setScenario("beetles", "level18", "1-0-1-0-1-0-1-0-1-0");
		setScenario("beetles", "level19", "1-0-0-0-0-0-0-0-1-0");
		setScenario("beetles", "level20", "2-2-2-2-1-0-0-0-0-0");
		setScenario("beetles", "level21", "3-0-0-0-2-1-0-0-1-0");
		setScenario("beetles", "level22", "0-0-2-1-0-0-2-0-0-1");
		setScenario("beetles", "level23", "1-0-0-0-2-0-1-2-0-0");
		setScenario("beetles", "level24", "4-0-2-3-0-0-1-0-0-0");

		// Минимум 3 вида жука на левел
		setScenario("beetles", "level25", "1-0-0-3-0-0-1-0-1-0");
		setScenario("beetles", "level26", "1-2-0-0-0-0-1-0-1-2");
		setScenario("beetles", "level27", "0-0-2-0-2-0-1-1-0-0");
		setScenario("beetles", "level28", "2-0-1-0-1-0-0-0-1-0");
		setScenario("beetles", "level29", "2-0-2-0-1-0-1-2-0-0");
		setScenario("beetles", "level30", "1-0-3-0-0-0-0-0-1-1");
		setScenario("beetles", "level31", "2-0-2-2-0-0-0-0-1-0");
		setScenario("beetles", "level32", "3-0-0-0-1-0-1-0-1-0");
		setScenario("beetles", "level33", "1-3-2-0-2-0-0-0-0-0");
		setScenario("beetles", "level34", "2-0-2-0-1-0-0-0-0-1");
		setScenario("beetles", "level35", "1-0-1-0-1-0-1-0-1-0");
		setScenario("beetles", "level36", "1-3-0-0-2-0-0-0-1-0");
		setScenario("beetles", "level37", "3-0-2-0-0-0-1-1-0-0");
		setScenario("beetles", "level38", "0-0-2-0-2-0-0-0-1-0");
		setScenario("beetles", "level39", "1-2-1-2-1-0-1-0-0-0");
		setScenario("beetles", "level40", "3-0-0-0-1-1-1-0-0-0");
		setScenario("beetles", "level41", "0-2-2-0-2-0-1-0-0-0");
		setScenario("beetles", "level42", "0-0-2-0-2-0-0-0-1-0");
		setScenario("beetles", "level43", "2-2-2-0-1-0-0-0-0-0");
		setScenario("beetles", "level44", "1-0-2-0-1-0-1-2-0-0");
		setScenario("beetles", "level45", "2-0-1-0-1-2-1-0-0-0");
		setScenario("beetles", "level46", "2-0-1-2-2-0-0-0-0-0");
		setScenario("beetles", "level47", "2-1-0-1-0-1-0-1-0-1");
		setScenario("beetles", "level48", "2-0-0-0-1-0-1-0-1-0");

		// Минимум 4 вида жуков на левел
		setScenario("beetles", "level49", "0-4-1-0-1-0-1-0-1-0");
		setScenario("beetles", "level50", "1-0-0-3-1-0-1-0-1-0");
		setScenario("beetles", "level51", "0-2-1-0-1-0-1-0-1-0");
		setScenario("beetles", "level52", "1-0-0-3-1-0-1-0-1-0");
		setScenario("beetles", "level53", "1-0-1-0-0-2-1-0-1-0");
		setScenario("beetles", "level54", "1-0-1-0-1-0-1-0-0-2");
		setScenario("beetles", "level55", "1-0-1-0-1-0-1-0-0-2");
		setScenario("beetles", "level56", "1-0-1-0-1-0-1-0-0-2");
		setScenario("beetles", "level57", "1-0-0-2-1-0-1-0-1-0");
		setScenario("beetles", "level58", "1-0-1-0-1-0-1-0-0-2");
		setScenario("beetles", "level59", "1-0-1-0-1-0-0-2-1-0");
		setScenario("beetles", "level60", "1-0-1-0-1-0-0-2-1-0");
		setScenario("beetles", "level61", "1-0-1-0-0-2-1-0-1-0");
		setScenario("beetles", "level62", "1-0-1-0-1-0-1-0-0-2");
		setScenario("beetles", "level63", "1-0-1-0-1-0-1-0-0-2");
		setScenario("beetles", "level64", "1-0-1-0-1-0-0-1-1-0");
		setScenario("beetles", "level65", "1-0-0-1-1-0-3-0-1-0");
		setScenario("beetles", "level66", "0-2-1-0-1-0-1-0-1-0");
		setScenario("beetles", "level67", "1-0-1-0-0-2-1-0-1-0");
		setScenario("beetles", "level68", "1-0-0-2-1-0-1-0-1-0");
		setScenario("beetles", "level69", "1-0-1-0-1-0-0-2-1-0");
		setScenario("beetles", "level70", "0-3-1-0-1-0-1-0-1-0");
		setScenario("beetles", "level71", "1-0-0-3-1-0-1-0-1-0");
		setScenario("beetles", "level72", "0-1-1-0-1-0-1-0-1-0");

		// Обычные бонусы
		setScenario("bonuses", "level1",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level2",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level3",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level4",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level5",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level6",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level7",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level8",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level9",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level10",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level11",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level12",
				"0-0-0-0-0-0-0-0-0-0-0-0-355-562-1-3-120-649-4-6");
		setScenario("bonuses", "level13",
				"0-0-0-0-147-448-2-4-338-685-3-5-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level14",
				"0-0-0-0-0-0-0-0-0-0-0-0-313-485-3-4-0-0-0-0");
		setScenario("bonuses", "level15",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level16",
				"327-439-2-4-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level17",
				"0-0-0-0-0-0-0-0-95-610-1-2-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level18",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level19",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level20",
				"127-571-2-4-335-600-1-2-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level21",
				"0-0-0-0-0-0-0-0-245-688-1-4-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level22",
				"0-0-0-0-132-462-2-3-0-0-0-0-0-0-0-0-335-591-2-4");
		setScenario("bonuses", "level23",
				"0-0-0-0-0-0-0-0-0-0-0-0-371-643-4-5-0-0-0-0");
		setScenario("bonuses", "level24",
				"0-0-0-0-159-626-2-4-0-0-0-0-0-0-0-0-0-0-0-0");

		// Колличество бонусов уменьшается с каждым ходом
		setScenario("bonuses", "level25",
				"0-0-0-0-314-652-1-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level26",
				"80-640-3-0-0-0-0-0-0-0-0-0-0-0-0-0-359-429-2-0");
		setScenario("bonuses", "level27",
				"0-0-0-0-0-0-0-0-0-0-0-0-134-651-4-0-0-0-0-0");
		setScenario("bonuses", "level28",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level29",
				"0-0-0-0-0-0-0-0-0-0-0-0-350-550-4-0-0-0-0-0");
		setScenario("bonuses", "level30",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-99-651-2-0");
		setScenario("bonuses", "level31",
				"0-0-0-0-50-517-3-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level32",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level33",
				"22-540-1-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level34",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-370-480-3-0");
		setScenario("bonuses", "level35",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level36",
				"360-682-3-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level37",
				"0-0-0-0-0-0-0-0-0-0-0-0-100-664-3-4-0-0-0-0");
		setScenario("bonuses", "level38",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level39",
				"68-603-3-0-374-553-2-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level40",
				"0-0-0-0-0-0-0-0-102-499-4-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level41",
				"87-608-5-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level42",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level43",
				"338-459-2-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level44",
				"0-0-0-0-0-0-0-0-0-0-0-0-320-675-1-0-0-0-0-0");
		setScenario("bonuses", "level45",
				"0-0-0-0-0-0-0-0-218-484-4-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level46",
				"0-0-0-0-358-512-2-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level47",
				"14-571-2-0-319-490-1-0-93-476-3-0-381-576-4-0-194-433-5-0");
		setScenario("bonuses", "level48",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");

		// Колличество бонусов уменьшается с каждым ходом
		// 1-2-3-1-1-5-5-2-4-3-5-2-5-5-2-5-1-2-4-3-2-4-1-4
		setScenario("bonuses", "level49",
				"216-577-1-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level50",
				"0-0-0-0-203-616-2-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level51",
				"307-670-2-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level52",
				"0-0-0-0-193-577-2-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level53",
				"0-0-0-0-0-0-0-0-230-579-1-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level54",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-320-563-2-0");
		setScenario("bonuses", "level55",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-225-549-3-0");
		setScenario("bonuses", "level56",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-217-661-2-0");
		setScenario("bonuses", "level57",
				"0-0-0-0-131-672-1-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level58",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-240-400-2-0");
		setScenario("bonuses", "level59",
				"0-0-0-0-0-0-0-0-0-0-0-0-155-596-2-0-0-0-0-0");
		setScenario("bonuses", "level60",
				"0-0-0-0-0-0-0-0-0-0-0-0-153-662-1-0-0-0-0-0");
		setScenario("bonuses", "level61",
				"0-0-0-0-0-0-0-0-303-638-3-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level62",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-277-628-3-0");
		setScenario("bonuses", "level63",
				"0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-164-526-1-0");
		setScenario("bonuses", "level64",
				"0-0-0-0-0-0-0-0-0-0-0-0-222-587-1-0-0-0-0-0");
		setScenario("bonuses", "level65",
				"0-0-0-0-316-654-3-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level66",
				"243-507-2-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level67",
				"0-0-0-0-0-0-0-0-227-618-1-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level68",
				"0-0-0-0-174-577-3-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level69",
				"0-0-0-0-0-0-0-0-0-0-0-0-240-559-2-0-0-0-0-0");
		setScenario("bonuses", "level70",
				"222-584-1-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level71",
				"0-0-0-0-306-596-3-0-0-0-0-0-0-0-0-0-0-0-0-0");
		setScenario("bonuses", "level72",
				"209-507-1-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0-0");
	}
}
