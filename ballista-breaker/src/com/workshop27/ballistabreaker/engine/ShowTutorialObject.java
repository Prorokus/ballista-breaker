package com.workshop27.ballistabreaker.engine;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.GameScreen;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class ShowTutorialObject {
	private String text;
	private String flag;

	private float centerPosX;
	private float centerPosY;
	private float arrowWeight;

	private int wrapWidth;
	private ArrowPosEnum arrowPos;

	public boolean visible = false;

	private final float backSideWidth = 18;
	private final float backSideHeight = 18;

	// OBJECTS
	private final PopupBackground backgroundTutorial;
	// private final RectangleGameObject tutorialBack;
	// private final RectangleGameObject tutorialBackSideLeft;
	// private final RectangleGameObject tutorialBackSideRight;

	// private final GameObject tutorialBackLeftUp;
	// private final GameObject tutorialBackLeftDown;
	// private final GameObject tutorialBackRightUp;
	// private final GameObject tutorialBackRightDown;
	private final GameObject tutorialBack;

	private final AnimatedTextGameObject tutorialText;

	private final GameObject tutorialArrow;

	private final GdxGame game;

	private final Timer balloonTutorialTimer = new Timer();
	private final ArrayList<TimerTask> tutorialTimerTasks = new ArrayList<TimerTask>();

	// -------

	public enum ArrowPosEnum {
		TOP, BOTTOM, LEFT, RIGHT;
	}

	public ShowTutorialObject(GdxGame g) {
		this.game = g;

		backgroundTutorial = new PopupBackground("background-tutorial", false,
				true);
		backgroundTutorial.setVisible(false);

		// tutorialBack = new RectangleGameObject("tutorial-back", false);
		// tutorialBack.color.r = 0;
		// tutorialBack.color.g = 0;
		// tutorialBack.color.b = 0;
		// tutorialBack.color.a = 0;

		// tutorialBackSideLeft = new
		// RectangleGameObject("tutorial-back-side-left", false);
		// tutorialBackSideLeft.color.r = 0;
		// tutorialBackSideLeft.color.g = 0;
		// tutorialBackSideLeft.color.b = 0;
		// tutorialBackSideLeft.color.a = 0;

		// tutorialBackSideRight = new
		// RectangleGameObject("tutorial-back-side-right", false);
		// tutorialBackSideRight.color.r = 0;
		// tutorialBackSideRight.color.g = 0;
		// tutorialBackSideRight.color.b = 0;
		// tutorialBackSideRight.color.a = 0;

		tutorialBack = new GameObject("tutorial-back", "tutorial-back", false);
		tutorialBack.getColor().a = 0;

		// tutorialBackLeftUp = new GameObject("tutorial-back-left-up",
		// "tutorial-back-left-up", false);
		// tutorialBackLeftUp.color.a = 0;

		// tutorialBackLeftDown = new GameObject("tutorial-back-left-down",
		// "tutorial-back-left-down", false);
		// tutorialBackLeftDown.color.a = 0;

		// tutorialBackRightUp = new GameObject("tutorial-back-right-up",
		// "tutorial-back-right-up", false);
		// tutorialBackRightUp.color.a = 0;

		// tutorialBackRightDown = new GameObject("tutorial-back-right-down",
		// "tutorial-back-right-down", false);
		// tutorialBackRightDown.color.a = 0;

		tutorialText = new AnimatedTextGameObject("tutorialText", false,
				"tutorial_white_bold", 30);

		tutorialArrow = new GameObject("tutorial-arrow", "tutorial-arrow",
				false);
		tutorialArrow.setOnCreateFilter(true);

		clearTimers();
	}

	public void closeTutorialFast() {
		backgroundTutorial.setVisible(false);

		tutorialText.clearActions();
		tutorialText.getColor().a = 0;

		tutorialArrow.clearActions();
		tutorialArrow.getColor().a = 0;

		visible = false;
	}

	public void clearTimers() {
		for (int i = 0; i < tutorialTimerTasks.size(); i++) {
			tutorialTimerTasks.get(i).cancel();
		}

		tutorialTimerTasks.clear();
	}

	public void closeTutorial() {
		if (backgroundTutorial.isVisible()) {
			backgroundTutorial.setVisible(false);

			tutorialText.clearActions();
			tutorialText.addAction(fadeOut(0.5f));

			final AlphaAction arrowFadeOut = fadeOut(0.5f);
			final ScaleToAction arrowScale1 = scaleTo(1.4f, 1.4f, 0.5f);
			final MoveToAction arrowMove1;

			if (this.arrowPos == ArrowPosEnum.BOTTOM) {
				arrowMove1 = moveTo(tutorialArrow.getPositionX(),
						tutorialArrow.getPositionY() - 50, 0.5f);
			}

			else if (this.arrowPos == ArrowPosEnum.TOP) {
				arrowMove1 = moveTo(tutorialArrow.getPositionX(),
						tutorialArrow.getPositionY() + 50, 0.5f);
			}

			else if (this.arrowPos == ArrowPosEnum.LEFT) {
				arrowMove1 = moveTo(tutorialArrow.getPositionX() - 50,
						tutorialArrow.getPositionY(), 0.5f);
			}

			else {
				arrowMove1 = moveTo(tutorialArrow.getPositionX() + 50,
						tutorialArrow.getPositionY(), 0.5f);
			}

			tutorialArrow.clearActions();
			tutorialArrow.addAction(arrowFadeOut);
			tutorialArrow.addAction(arrowScale1);
			tutorialArrow.addAction(arrowMove1);

			TimerTask tS = new TimerTask() {
				@Override
				public void run() {
					visible = false;
				}
			};

			balloonTutorialTimer.schedule(tS, 500);
		}
	}

	public boolean isTutorialUsed(String flag) {

		if (!flag.equals("")) {
			try {
				if (GdxGame.files.getOptionsAndProgress("tutorial", flag)
						.equals("yes")) {
					return true;
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return false;
	}

	public void initTutorial(final String text, final String flag,
			final int centerPosX, final int centerPosY, final int wrapWidth,
			final ArrowPosEnum arrowPos, final float arrowWeight,
			final Group rootGroup) {

		if (!flag.equals("")) {
			try {
				if (GdxGame.files.getOptionsAndProgress("tutorial", flag)
						.equals("yes")) {
					return;
				}
			} catch (Exception e) { // TODO
				e.printStackTrace();
			}
		}

		if ((this.game.tutorialScreen != null && this.game.tutorialScreen
				.getVisible())) {
			TimerTask tS = new TimerTask() {
				@Override
				public void run() {
					initTutorial(text, flag, centerPosX, centerPosY, wrapWidth,
							arrowPos, arrowWeight, rootGroup);
				}
			};

			balloonTutorialTimer.schedule(tS, 500);
			tutorialTimerTasks.add(tS);
			return;
		}

		if (this.visible || GameScreen.isGamePaused()) {
			return;
		}

		GdxGame.files.setOptionsAndProgress("tutorial", flag, "yes");

		this.visible = true;

		this.text = text;
		this.centerPosX = centerPosX;
		this.centerPosY = centerPosY;
		this.wrapWidth = wrapWidth;
		this.arrowPos = arrowPos;
		this.arrowWeight = arrowWeight;

		backgroundTutorial.setPositionXY(0, 0);
		backgroundTutorial.setVisible(true);
		backgroundTutorial.setWidth(GdxGame.VIRTUAL_GAME_WIDTH);
		backgroundTutorial.setHeight(GdxGame.VIRTUAL_GAME_HEIGHT);

		if (GdxGame.beetleStage.getRoot().findActor("tutorial-back") == null) {
			GdxGame.beetleStage.addActor(backgroundTutorial);
			// rootGroup.addActor(backgroundTutorial);
		}

		tutorialText.setTextCenteredOnPosition(this.text, this.centerPosX,
				this.centerPosY, this.wrapWidth, false);
		tutorialText.clearActions();
		tutorialText.setColor(1.0f, 0.97f, 0.8f, 0);
		tutorialText.addAction(fadeIn(0.5f));

		tutorialArrow.folderName = "inventory";
		tutorialArrow.setSkin();
		tutorialArrow.setOriginX(tutorialArrow.getWidth() / 2);
		tutorialArrow.setOriginY(tutorialArrow.getHeight() / 2);
		tutorialArrow.setScaleX(1.0f);
		tutorialArrow.setScaleY(1.0f);
		tutorialArrow.getColor().a = 0;

		final AlphaAction arrowFadeIn = fadeIn(0.5f);
		final ScaleToAction arrowScale1 = scaleTo(0.95f, 0.95f, 0.5f);
		final ScaleToAction arrowScale2 = scaleTo(1.05f, 1.05f, 0.5f);
		final MoveToAction arrowMove1;
		final MoveToAction arrowMove2;

		if (this.arrowPos == ArrowPosEnum.BOTTOM) {
			tutorialArrow.setPositionX(tutorialText.getTextCachePositionX()
					+ tutorialText.getTextBounds().width * arrowWeight
					- tutorialArrow.getWidth() / 2);
			tutorialArrow.setPositionY(tutorialText.getTextCachePositionY()
					- tutorialText.getTextBounds().height
					- tutorialArrow.getWidth() / 2 - 40 * GdxGame.HD_Y_RATIO);
			tutorialArrow.setRotation(90);

			arrowMove1 = moveTo(tutorialArrow.getPositionX(),
					tutorialArrow.getPositionY() - 5, 0.5f);
			arrowMove2 = moveTo(tutorialArrow.getPositionX(),
					tutorialArrow.getPositionY() + 5, 0.5f);
		}

		else if (this.arrowPos == ArrowPosEnum.TOP) {
			tutorialArrow.setPositionX(tutorialText.getTextCachePositionX()
					+ tutorialText.getTextBounds().width * arrowWeight
					- tutorialArrow.getWidth() / 2);
			tutorialArrow.setPositionY(tutorialText.getTextCachePositionY()
					+ 40 * GdxGame.HD_Y_RATIO);
			tutorialArrow.setRotation(-90);

			arrowMove1 = moveTo(tutorialArrow.getPositionX(),
					tutorialArrow.getPositionY() - 5, 0.5f);
			arrowMove2 = moveTo(tutorialArrow.getPositionX(),
					tutorialArrow.getPositionY() + 5, 0.5f);
		}

		else if (this.arrowPos == ArrowPosEnum.LEFT) {
			tutorialArrow.setPositionX(tutorialText.getTextCachePositionX()
					- tutorialArrow.getWidth() - 30 * GdxGame.HD_X_RATIO);
			tutorialArrow.setPositionY(tutorialText.getTextCachePositionY()
					- tutorialText.getTextBounds().height * this.arrowWeight
					- tutorialArrow.getHeight() / 2);

			arrowMove1 = moveTo(tutorialArrow.getPositionX() - 5,
					tutorialArrow.getPositionY(), 0.5f);
			arrowMove2 = moveTo(tutorialArrow.getPositionX() + 5,
					tutorialArrow.getPositionY(), 0.5f);
		}

		else {
			tutorialArrow.setPositionX(tutorialText.getTextCachePositionX()
					+ tutorialText.getTextBounds().width + 30
					* GdxGame.HD_X_RATIO);
			tutorialArrow.setPositionY(tutorialText.getTextCachePositionY()
					- tutorialText.getTextBounds().height * this.arrowWeight
					- tutorialArrow.getHeight() / 2);

			tutorialArrow.setRotation(180);

			arrowMove1 = moveTo(tutorialArrow.getPositionX() - 5,
					tutorialArrow.getPositionY(), 0.5f);
			arrowMove2 = moveTo(tutorialArrow.getPositionX() + 5,
					tutorialArrow.getPositionY(), 0.5f);
		}

		tutorialArrow.addAction(arrowFadeIn);
		tutorialArrow.addAction(forever(sequence(arrowScale1, arrowScale2)));
		tutorialArrow.addAction(forever(sequence(arrowMove1, arrowMove2)));

		tutorialBack.setPositionXY(
				tutorialText.getTextCachePositionX() - 5 * GdxGame.HD_X_RATIO
						- backSideWidth,
				tutorialText.getTextCachePositionY()
						- tutorialText.getTextBounds().height - 15
						* GdxGame.HD_Y_RATIO - backSideHeight);
		tutorialBack.setWidth(tutorialText.getTextBounds().width + 10
				* GdxGame.HD_X_RATIO + backSideWidth * 2);
		tutorialBack.setHeight(tutorialText.getTextBounds().height + 20
				* GdxGame.HD_Y_RATIO + backSideHeight * 2);
		tutorialBack.folderName = "inventory";
		tutorialBack.setSkin();

		// ----------------------
		// tutorialBack.setPositionXY(tutorialText.getTextCachePositionX() - 5 *
		// GdxGame.HD_X_RATIO, tutorialText.getTextCachePositionY() -
		// tutorialText.getTextBounds().height - 15 * GdxGame.HD_Y_RATIO);
		// tutorialBack.setWidth(tutorialText.getTextBounds().width + 10 *
		// GdxGame.HD_X_RATIO);
		// tutorialBack.setHeight(tutorialText.getTextBounds().height + 20 *
		// GdxGame.HD_Y_RATIO);

		// tutorialBackSideLeft.setPositionXY(tutorialBack.getPositionX() -
		// backSideWidth, tutorialBack.getPositionY() + backSideHeight);
		// tutorialBackSideLeft.setWidth(backSideWidth);
		// tutorialBackSideLeft.setHeight((int) (tutorialBack.getHeight() -
		// backSideHeight * 2));

		// tutorialBackSideRight.setPositionXY(tutorialBack.getPositionX() +
		// tutorialText.getTextBounds().width + backSideWidth / 2 + 1,
		// tutorialBack.getPositionY() + backSideHeight);
		// tutorialBackSideRight.setWidth(backSideWidth);
		// tutorialBackSideRight.setHeight((int) (tutorialBack.getHeight() -
		// backSideHeight * 2));

		// -----------------------CORNERS---------------------------
		// tutorialBackLeftUp.folderName = "inventory";
		// tutorialBackLeftUp.setSkin();
		// tutorialBackLeftUp.setPositionXY(tutorialBack.getPositionX() -
		// backSideWidth, (int) (tutorialBack.getPositionY() + backSideHeight +
		// tutorialBackSideLeft.getHeight()));

		// tutorialBackLeftDown.folderName = "inventory";
		// tutorialBackLeftDown.setSkin();
		// tutorialBackLeftDown.setPositionXY(tutorialBack.getPositionX() -
		// backSideWidth, tutorialBack.getPositionY());

		// tutorialBackRightUp.folderName = "inventory";
		// tutorialBackRightUp.setSkin();
		// tutorialBackRightUp.setPositionXY(tutorialBack.getPositionX() +
		// tutorialText.getTextBounds().width + backSideWidth / 2 + 1, (int)
		// (tutorialBack.getPositionY() + backSideHeight +
		// tutorialBackSideRight.getHeight()));

		// tutorialBackRightDown.folderName = "inventory";
		// tutorialBackRightDown.setSkin();
		// tutorialBackRightDown.setPositionXY(tutorialBack.getPositionX() +
		// tutorialText.getTextBounds().width + backSideWidth / 2 + 1,
		// tutorialBack.getPositionY());
		// ---------------------------------------------------------
	}

	public void render() {
		if (!visible) {
			return;
		}

		if (!ImageCache.getManager().update()) {
			return;
		}

		tutorialBack.getColor().a = tutorialText.getColor().a * 0.75f;
		// tutorialBackSideLeft.color.a = tutorialText.color.a * 0.8f;
		// tutorialBackSideRight.color.a = tutorialText.color.a * 0.8f;

		// tutorialBackLeftUp.color.a = tutorialText.color.a * 0.8f;
		// tutorialBackLeftDown.color.a = tutorialText.color.a * 0.8f;
		// tutorialBackRightUp.color.a = tutorialText.color.a * 0.8f;
		// tutorialBackRightDown.color.a = tutorialText.color.a * 0.8f;

		// tutorialBackSideLeft.draw(null, 0);
		// tutorialBackSideRight.draw(null, 0);

		GdxGame.beetleStage.getSpriteBatch().begin();
		tutorialBack.draw(GdxGame.beetleStage.getSpriteBatch(), 1);

		// tutorialBackLeftUp.draw(GdxGame.beetleStage.getSpriteBatch(), 1);
		// tutorialBackLeftDown.draw(GdxGame.beetleStage.getSpriteBatch(), 1);
		// tutorialBackRightUp.draw(GdxGame.beetleStage.getSpriteBatch(), 1);
		// tutorialBackRightDown.draw(GdxGame.beetleStage.getSpriteBatch(), 1);

		tutorialText.act(Gdx.graphics.getDeltaTime());
		tutorialText.draw(GdxGame.beetleStage.getSpriteBatch(), 1);

		tutorialArrow.act(Gdx.graphics.getDeltaTime());
		tutorialArrow.draw(GdxGame.beetleStage.getSpriteBatch(), 1);

		GdxGame.beetleStage.getSpriteBatch().end();
	}
}
