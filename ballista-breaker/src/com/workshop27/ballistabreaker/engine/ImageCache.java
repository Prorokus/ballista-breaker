package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.ObjectMap;

public class ImageCache {
	public static AssetManager manager;

	public static ObjectMap<String, Animation> animations;

	public static void create() {
		manager = new AssetManager();

		manager.setLoader(TextureAtlas.class, new TextureAtlasLoader(
				new InternalFileHandleResolver()));
		manager.setLoader(Sound.class, new SoundLoader(
				new InternalFileHandleResolver()));

		animations = new ObjectMap<String, Animation>();
	}

	public static AssetManager getManager() {
		return manager;
	}

	public static void load(String name) {
		manager.load("data/atlas/" + name + "/pack", TextureAtlas.class);
	}

	public static void loadSounds() {
		manager.load("data/sounds/bomb_plant.ogg", Sound.class);

		manager.load("data/sounds/blaster_name.ogg", Sound.class);
		manager.load("data/sounds/blaster1.ogg", Sound.class);
		manager.load("data/sounds/blaster2.ogg", Sound.class);
		manager.load("data/sounds/blaster3.ogg", Sound.class);

		manager.load("data/sounds/booma_name.ogg", Sound.class);
		manager.load("data/sounds/booma1.ogg", Sound.class);
		manager.load("data/sounds/booma2.ogg", Sound.class);
		manager.load("data/sounds/booma3.ogg", Sound.class);

		manager.load("data/sounds/egger_name.ogg", Sound.class);
		manager.load("data/sounds/egger1.ogg", Sound.class);
		manager.load("data/sounds/egger2.ogg", Sound.class);
		manager.load("data/sounds/egger3.ogg", Sound.class);

		manager.load("data/sounds/miner_name.ogg", Sound.class);
		manager.load("data/sounds/miner1.ogg", Sound.class);
		manager.load("data/sounds/miner2.ogg", Sound.class);
		manager.load("data/sounds/miner3.ogg", Sound.class);

		manager.load("data/sounds/breaker_name.ogg", Sound.class);
		manager.load("data/sounds/breaker1.ogg", Sound.class);
		manager.load("data/sounds/breaker2.ogg", Sound.class);
		manager.load("data/sounds/breaker3.ogg", Sound.class);

		manager.load("data/sounds/explode1.ogg", Sound.class);
		manager.load("data/sounds/explode2.ogg", Sound.class);
		manager.load("data/sounds/hit.ogg", Sound.class);
		manager.load("data/sounds/motherbomb_life.ogg", Sound.class);
		manager.load("data/sounds/release_rope.ogg", Sound.class);
		manager.load("data/sounds/return_back.ogg", Sound.class);
		manager.load("data/sounds/rope0.ogg", Sound.class);
		manager.load("data/sounds/rope1.ogg", Sound.class);
		manager.load("data/sounds/tap1.ogg", Sound.class);
		manager.load("data/sounds/tap2.ogg", Sound.class);
		manager.load("data/sounds/tap3.ogg", Sound.class);

		manager.load("data/sounds/get_star0.ogg", Sound.class);
		manager.load("data/sounds/get_star1.ogg", Sound.class);

		manager.load("data/sounds/wheel0.ogg", Sound.class);
		manager.load("data/sounds/wheel1.ogg", Sound.class);
		// manager.load("data/sounds/wind0.ogg", Sound.class);
		// manager.load("data/sounds/wind1.ogg", Sound.class);

		manager.load("data/sounds/shoot0.ogg", Sound.class);
		manager.load("data/sounds/shoot1.ogg", Sound.class);
		manager.load("data/sounds/shoot2.ogg", Sound.class);
		manager.load("data/sounds/shoot3.ogg", Sound.class);
		manager.load("data/sounds/shoot4.ogg", Sound.class);
		manager.load("data/sounds/shoot5.ogg", Sound.class);
		manager.load("data/sounds/shoot6.ogg", Sound.class);
		manager.load("data/sounds/shoot7.ogg", Sound.class);
		manager.load("data/sounds/shoot8.ogg", Sound.class);
		manager.load("data/sounds/shoot9.ogg", Sound.class);
		manager.load("data/sounds/shoot10.ogg", Sound.class);
		manager.load("data/sounds/shoot11.ogg", Sound.class);
	}

	public static void unload(String name) {
		final String fileName = "data/atlas/" + name + "/pack";
		if (ImageCache.manager.isLoaded(fileName)) {
			manager.unload(fileName);
		}
	}
}
