package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class BeetleButton extends HdCompatibleButton {
	public int beetleTypeTotal = 0;
	public int beetleTypeCount = 0;
	public String beetleName = "";

	private TextureRegion dotRegion = null;

	private float dotX = 0;
	private float dotY = 0;
	private float dotX0 = 0;
	private float dotY0 = 0;

	private Vector2 musXY;
	private Vector2 musWH;
	private Vector2 musOrXY;
	private TextureRegion musRegion;

	private float startMusRotation;
	private float musRotation;
	private float musAngle = 0;
	private boolean musRotationFlag = false;

	private boolean rotationStarted = false;
	private int rotationCycles = 0;
	private double rotationStartRandom;

	private float deltaTimeSum = 0;
	private float deltaTimeEyesSum = 0;

	public boolean inRage = false;

	private float dotAngle = 0;

	public GameScreen gameScreen;
	private TextureRegion eyesClosedRegion;
	private Vector2 eyesClosedXY;
	private Vector2 eyesClosedWH;
	private Vector2 eyesClosedOrXY;

	public void restart() {
		this.beetleTypeCount = beetleTypeTotal;
		this.dotRegion = null;
		this.musRegion = null;
		this.inRage = false;
	}

	public BeetleButton(TextureRegionDrawable regionUp, TextureRegionDrawable regionDown, int c) {
		super(regionUp, regionDown);

		this.beetleTypeTotal = c;
		this.beetleTypeCount = c;
	}

	public void setBeetleTypeTotal(int beetleTypeTotal) {
		this.beetleTypeTotal = beetleTypeTotal;
		this.beetleTypeCount = beetleTypeTotal;
	}

	private void rotateMustache() {
		if (!rotationStarted && deltaTimeSum > 0.3f) {
			deltaTimeSum = 0;
			rotationStartRandom = Math.random();

			if (rotationStartRandom >= 0.65f) {
				rotationStarted = true;
				rotationCycles = 0;
			}

		} else if (!rotationStarted && deltaTimeSum <= 0.3f) {
			deltaTimeSum += Gdx.graphics.getDeltaTime();
		}

		if (rotationStarted) {
			if (musAngle > 0.35f) {
				musRotationFlag = true;
				musAngle = 0.35f;
				rotationCycles++;
			} else if (musAngle < -0.35f) {
				musRotationFlag = false;
				musAngle = -0.35f;
				rotationCycles++;
			}

			if (!musRotationFlag) {
				musAngle += Gdx.graphics.getDeltaTime() * 12;
			} else {
				musAngle -= Gdx.graphics.getDeltaTime() * 12;
			}

			musRotation = (float) (Math.asin(musAngle) * 180 / Math.PI)
					+ startMusRotation;

			if (rotationCycles >= 4 && musAngle > 0) {
				rotationStarted = false;
				musRotation = startMusRotation;
			}
		}
	}

	public void setDotRegion() {
		this.dotRegion = ImageCache.getManager()
				.get("data/atlas/main/pack", TextureAtlas.class)
				.findRegion("dot");

		if (this.dotRegion == null) {
			return;
		}

		if (getWidth() == 0) {
			setWidth(this.dotRegion.getRegionWidth());
		}

		if (getHeight() == 0) {
			setHeight(this.dotRegion.getRegionHeight());
		}

		this.dotX = this.getX() + this.getWidth() / 2;
		this.dotY = this.getY() + this.getHeight() / 2;

		if (beetleName.equals("regular_btn")) {
			this.eyesClosedRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("regular-eyes-closed");
			eyesClosedXY = new Vector2(27 * GdxGame.HD_X_RATIO,
					44 * GdxGame.HD_Y_RATIO);
			eyesClosedWH = new Vector2(eyesClosedRegion.getRegionWidth(),
					eyesClosedRegion.getRegionHeight());

			eyesClosedOrXY = new Vector2(eyesClosedWH.x / 2, eyesClosedWH.y / 2);

			this.musRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("regular-mus");

			musXY = new Vector2(26 * GdxGame.HD_X_RATIO,
					20 * GdxGame.HD_Y_RATIO);
			startMusRotation = 25;
			musWH = new Vector2(musRegion.getRegionWidth(),
					musRegion.getRegionHeight());
			musOrXY = new Vector2(17 * GdxGame.HD_X_RATIO,
					22 * GdxGame.HD_Y_RATIO);

		}

		else if (beetleName.equals("Miner")) {

			this.eyesClosedRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("miner-eyes-closed");
			eyesClosedXY = new Vector2(23 * GdxGame.HD_X_RATIO,
					55 * GdxGame.HD_Y_RATIO);
			eyesClosedWH = new Vector2(eyesClosedRegion.getRegionWidth(),
					eyesClosedRegion.getRegionHeight());

			eyesClosedOrXY = new Vector2(eyesClosedWH.x / 2, eyesClosedWH.y / 2);

			this.musRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("miner-mus");

			musXY = new Vector2(16 * GdxGame.HD_X_RATIO,
					50 * GdxGame.HD_Y_RATIO);

			startMusRotation = -32;
			musWH = new Vector2(musRegion.getRegionWidth(),
					musRegion.getRegionHeight());

			musOrXY = new Vector2(musWH.x / 2, musWH.y / 2);

		}

		else if (beetleName.equals("Fan")) {
			this.eyesClosedRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("fan-eyes-closed");
			eyesClosedXY = new Vector2(34 * GdxGame.HD_X_RATIO,
					48 * GdxGame.HD_Y_RATIO);
			eyesClosedWH = new Vector2(eyesClosedRegion.getRegionWidth(),
					eyesClosedRegion.getRegionHeight());

			eyesClosedOrXY = new Vector2(eyesClosedWH.x / 2, eyesClosedWH.y / 2);

			this.musRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("fan-mus");

			musXY = new Vector2(32 * GdxGame.HD_X_RATIO,
					40 * GdxGame.HD_Y_RATIO);

			startMusRotation = -32;
			musWH = new Vector2(musRegion.getRegionWidth(),
					musRegion.getRegionHeight());

			musOrXY = new Vector2(15 * GdxGame.HD_X_RATIO,
					9 * GdxGame.HD_Y_RATIO);

		}

		else if (beetleName.equals("Boom")) {

			this.eyesClosedRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("boom-eyes-closed");
			eyesClosedXY = new Vector2(22 * GdxGame.HD_X_RATIO,
					33 * GdxGame.HD_Y_RATIO);
			eyesClosedWH = new Vector2(eyesClosedRegion.getRegionWidth(),
					eyesClosedRegion.getRegionHeight());

			eyesClosedOrXY = new Vector2(eyesClosedWH.x / 2, eyesClosedWH.y / 2);

			this.musRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("boom-mus");

			musXY = new Vector2(23 * GdxGame.HD_X_RATIO,
					28 * GdxGame.HD_Y_RATIO);
			startMusRotation = 32;
			musWH = new Vector2(musRegion.getRegionWidth(),
					musRegion.getRegionHeight());
			musOrXY = new Vector2(15 * GdxGame.HD_X_RATIO,
					9 * GdxGame.HD_Y_RATIO);

		} else if (beetleName.equals("Karate")) {
			startMusRotation = 10;
			this.eyesClosedRegion = ImageCache.getManager()
					.get("data/atlas/main/pack", TextureAtlas.class)
					.findRegion("karate-eyes-closed");
			eyesClosedXY = new Vector2(21 * GdxGame.HD_X_RATIO,
					43 * GdxGame.HD_Y_RATIO);
			eyesClosedWH = new Vector2(eyesClosedRegion.getRegionWidth(),
					eyesClosedRegion.getRegionHeight());

			eyesClosedOrXY = new Vector2(eyesClosedWH.x / 2, eyesClosedWH.y / 2);
			return;
		}

		musRotation = startMusRotation;
		this.musRegion.getTexture().setFilter(TextureFilter.Linear,
				TextureFilter.Linear);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!GameScreen.isGamePaused()) {
			deltaTimeEyesSum += Gdx.graphics.getDeltaTime();
			rotateMustache();
		}

		if (beetleTypeCount > 0) {
			super.draw(batch, parentAlpha);

			if (this.dotRegion != null) {

				if (deltaTimeEyesSum > 2) {
					batch.draw(this.eyesClosedRegion, getX() + eyesClosedXY.x, getY()
							+ eyesClosedXY.y, eyesClosedOrXY.x,
							eyesClosedOrXY.y, eyesClosedWH.x, eyesClosedWH.y,
							1, 1, startMusRotation);
					if (deltaTimeEyesSum > 2.3) {
						deltaTimeEyesSum = (float) Math.random() * 2;
					}
				}

				if (this.inRage && !beetleName.equals("Karate")) {
					batch.draw(this.musRegion, getX() + musXY.x, getY() + musXY.y,
							musOrXY.x, musOrXY.y, musWH.x, musWH.y, getScaleX(),
							getScaleY(), musRotation);
				}

				dotAngle = 120.0f / beetleTypeCount;
				if (dotAngle > 14) {
					dotAngle = 14;
				}

				for (int i = 1; i <= beetleTypeCount; i++) {
					dotX0 = this.dotX + 37
							* MathUtils.cosDeg(i * dotAngle + 30);
					dotY0 = this.dotY + 37
							* MathUtils.sinDeg(i * dotAngle + 30);

					batch.draw(this.dotRegion, dotX0 - 7 / GdxGame.HD_X_RATIO,
							dotY0 - 10 / GdxGame.HD_Y_RATIO);
				}

			} else {
				setDotRegion();

				dotAngle = 120.0f / beetleTypeCount;
				if (dotAngle > 14) {
					dotAngle = 14;
				}

				for (int i = 1; i <= beetleTypeCount; i++) {
					dotX0 = this.dotX + 37
							* MathUtils.cosDeg(i * dotAngle + 30);
					dotY0 = this.dotY + 37
							* MathUtils.sinDeg(i * dotAngle + 30);

					batch.draw(this.dotRegion, dotX0 - 7 / GdxGame.HD_X_RATIO,
							dotY0 - 10 / GdxGame.HD_Y_RATIO);
				}

			}
		}
	}
}
