package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class ArmStretchMovable extends ArmStretch {
	// variables
	private final float[] ropeVertices = new float[20];

	private Texture tex;

	private final String skinName;

	private final Color col = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	private final float colFloat = this.col.toFloatBits();

	private float x1;
	private float y1;
	private float x2;
	private float y2;
	private float x3;
	private float y3;
	private float x4;
	private float y4;

	private float xRotated1;
	private float yRotated1;
	private float xRotated2;
	private float yRotated2;
	private float xRotated3;
	private float yRotated3;
	private float xRotated4;
	private float yRotated4;

	private float cos;
	private float sin;
	private final Vector2 v = new Vector2(0, 0);

	private float baseRotation;

	// constructors
	public ArmStretchMovable(String name, String skinName) {
		super(name, skinName);

		this.skinName = skinName;
	}

	@Override
	public void clearSkin() {
		this.tex = null;
	}

	@Override
	public void setSkin(String skinName) {
		if (!this.isVisible()) {
			return;
		}

		this.tex = ImageCache.getManager()
				.get("data/atlas/main/pack", TextureAtlas.class)
				.findRegion(skinName).getTexture();

		if (this.tex == null) {
			return;
		}

		this.tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}

	@Override
	public void calcRopeVertices() {
		this.v.x = getWidth() - getX();
		this.v.y = getHeight() - getY();

		setRotation(v.angle());
		this.cos = MathUtils.cosDeg(v.angle());
		this.sin = MathUtils.sinDeg(v.angle());

		this.x1 = this.getX() + cos - sin * 8 * GdxGame.HD_X_RATIO;
		this.y1 = this.getY() + sin + cos * 8 * GdxGame.HD_Y_RATIO;

		this.x2 = this.getX() + cos - sin * (-56) * GdxGame.HD_X_RATIO;
		this.y2 = this.getY() + sin + cos * (-56) * GdxGame.HD_Y_RATIO;

		this.x3 = this.getWidth() + cos - sin * 8 * GdxGame.HD_X_RATIO;
		this.y3 = this.getHeight() + sin + cos * 8 * GdxGame.HD_Y_RATIO;

		this.x4 = this.getWidth() + cos - sin * (-56) * GdxGame.HD_X_RATIO;
		this.y4 = this.getHeight() + sin + cos * (-56) * GdxGame.HD_Y_RATIO;

		// rotate
		if (true) {// GameScreen.objectToDestroy.rotation != 0
			if (this.getOriginX() == 0 && this.getOriginY() == 0) {
				this.setOriginX(GameScreen.objectToDestroy.getPositionX()
						+ GameScreen.objectToDestroy.getOriginX());
				this.setOriginY(GameScreen.objectToDestroy.getPositionY()
						+ GameScreen.objectToDestroy.getOriginY());
				this.baseRotation = GameScreen.objectToDestroy.getRotation();
			}

			this.cos = MathUtils.cosDeg(GameScreen.objectToDestroy.getRotation()
					- this.baseRotation);
			this.sin = MathUtils.sinDeg(GameScreen.objectToDestroy.getRotation()
					- this.baseRotation);

			xRotated1 = this.getOriginX() + (this.x1 - this.getOriginX())
					* this.cos - (this.y1 - this.getOriginY()) * this.sin;
			yRotated1 = this.getOriginY() + (this.y1 - this.getOriginY())
					* this.cos + (this.x1 - this.getOriginX()) * this.sin;

			xRotated2 = this.getOriginX() + (this.x2 - this.getOriginX())
					* this.cos - (this.y2 - this.getOriginY()) * this.sin;
			yRotated2 = this.getOriginY() + (this.y2 - this.getOriginY())
					* this.cos + (this.x2 - this.getOriginX()) * this.sin;

			xRotated3 = this.getOriginX() + (this.x3 - this.getOriginX())
					* this.cos - (this.y3 - this.getOriginY()) * this.sin;
			yRotated3 = this.getOriginY() + (this.y3 - this.getOriginY())
					* this.cos + (this.x3 - this.getOriginX()) * this.sin;

			xRotated4 = this.getOriginX() + (this.x4 - this.getOriginX())
					* this.cos - (this.y4 - this.getOriginY()) * this.sin;
			yRotated4 = this.getOriginY() + (this.y4 - this.getOriginY())
					* this.cos + (this.x4 - this.getOriginX()) * this.sin;
		}

		else {
			xRotated1 = this.x1;
			yRotated1 = this.y1;

			xRotated2 = this.x2;
			yRotated2 = this.y2;

			xRotated3 = this.x3;
			yRotated3 = this.y3;

			xRotated4 = this.x4;
			yRotated4 = this.y4;
		}

		this.ropeVertices[0] = xRotated1;
		this.ropeVertices[1] = yRotated1;
		this.ropeVertices[2] = this.colFloat;
		this.ropeVertices[3] = 0;
		this.ropeVertices[4] = 0;

		this.ropeVertices[5] = xRotated2;
		this.ropeVertices[6] = yRotated2;
		this.ropeVertices[7] = this.colFloat;
		this.ropeVertices[8] = 0;
		this.ropeVertices[9] = 1;

		this.ropeVertices[10] = xRotated3;
		this.ropeVertices[11] = yRotated3;
		this.ropeVertices[12] = this.colFloat;
		this.ropeVertices[13] = 1;
		this.ropeVertices[14] = 0;

		this.ropeVertices[15] = xRotated4;
		this.ropeVertices[16] = yRotated4;
		this.ropeVertices[17] = this.colFloat;
		this.ropeVertices[18] = 1;
		this.ropeVertices[19] = 1;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		calcRopeVertices();

		if (this.tex == null) {
			setSkin(this.skinName);
		} else {
			batch.setColor(this.getColor());
			batch.draw(this.tex, this.ropeVertices, 0, 20);
		}
	}
}
