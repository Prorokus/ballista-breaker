package com.workshop27.ballistabreaker.engine;

import java.util.Timer;
import java.util.TimerTask;

import aurelienribon.tweenengine.Tween;

import com.badlogic.gdx.Gdx;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.StageScreen;

public class StageBackgroundObject extends GameObject {
	private float startX;
	private float X;
	private float newPosition;
	private int moveTo;
	private int getDot1;
	private int getDot2;
	private int getDot3;
	private int getDot4;
	private boolean isDragged;

	public StageBackgroundObject(String string, boolean b, boolean c) {
		super(string, b, c);
	}

	/*@Override
	public boolean touchDown(float arg_x, float arg_y, int arg2) {
		if (GdxGame.isStageMoving) {
			return true;
		}

		if (!this.isVisible()) {
			return false;
		}

		startX = Gdx.input.getX();
		isDragged = false;
		return true;
	}*/

	/*@Override
	public void touchDragged(float arg_x, float arg_y, int arg2) {
		isDragged = true;
		if (GdxGame.isStageMoving) {
			return;
		}
		GdxGame.isStageDragged = true;
		// Gdx.app.log("arg_x", String.valueOf(arg_x));
		X = -(Gdx.input.getX() - startX); // shift camera on X pixels
		// Gdx.app.log("X", String.valueOf(X));

		newPosition = GdxGame.lastPosition + X;
		if (newPosition < GdxGame.VIRTUAL_GAME_WIDTH / 2) {
			newPosition = GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;
		}
		if (newPosition > 3 * GdxGame.VIRTUAL_GAME_WIDTH
				+ GdxGame.VIRTUAL_GAME_WIDTH / 2) {
			newPosition = 3 * GdxGame.VIRTUAL_GAME_WIDTH
					+ GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;
		}
		// Gdx.app.log("newPosition", String.valueOf(newPosition));

		switchDots(GdxGame.stageNumber);

		GdxGame.camera.position.set(newPosition,
				GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);
		StageScreen.dot1.setPositionXY(newPosition + getDot1, 120);
		StageScreen.dot2.setPositionXY(newPosition + getDot2, 120);
		StageScreen.dot3.setPositionXY(newPosition + getDot3, 120);
		StageScreen.dot4.setPositionXY(newPosition + getDot4, 120);

		if (((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) >= 0)
				&& ((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) <= 1)) {
			moveTo = GdxGame.VIRTUAL_GAME_WIDTH / 2;
			GdxGame.stageNumber = 1;
		}
		if ((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) > 1
				&& (newPosition / GdxGame.VIRTUAL_GAME_WIDTH) <= 2) {
			moveTo = GdxGame.VIRTUAL_GAME_WIDTH + GdxGame.VIRTUAL_GAME_WIDTH
					/ 2;
			GdxGame.stageNumber = 2;
		}
		if ((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) > 2
				&& (newPosition / GdxGame.VIRTUAL_GAME_WIDTH) <= 3) {
			moveTo = 2 * GdxGame.VIRTUAL_GAME_WIDTH
					+ GdxGame.VIRTUAL_GAME_WIDTH / 2;
			GdxGame.stageNumber = 3;
		}
		if ((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) > 3
				&& (newPosition / GdxGame.VIRTUAL_GAME_WIDTH) <= 4) {
			moveTo = 3 * GdxGame.VIRTUAL_GAME_WIDTH
					+ GdxGame.VIRTUAL_GAME_WIDTH / 2;
			GdxGame.stageNumber = 4;
		}

	}*/

	/*@Override
	public void touchUp(float arg_x, float arg_y, int arg2) {
		if (!isDragged) {
			return;
		}
		GdxGame.isStageDragged = false;
		if (GdxGame.isStageMoving) {
			return;
		}
		GdxGame.lastPosition = newPosition;
		GdxGame.isStageMoving = true;

		Tween.to(GdxGame.camera, CameraAccessor.POSITION_XY, 0.2f)
				.target(moveTo, GdxGame.VIRTUAL_GAME_HEIGHT / 2)
				.start(GdxGame.tweenManager);
		Tween.to(StageScreen.dot1, GameObjectAccessor.POSITION_X, 0.2f)
				.target(moveTo + getDot1, 120).start(GdxGame.tweenManager);
		Tween.to(StageScreen.dot2, GameObjectAccessor.POSITION_X, 0.2f)
				.target(moveTo + getDot2, 120).start(GdxGame.tweenManager);
		Tween.to(StageScreen.dot3, GameObjectAccessor.POSITION_X, 0.2f)
				.target(moveTo + getDot3, 120).start(GdxGame.tweenManager);
		Tween.to(StageScreen.dot4, GameObjectAccessor.POSITION_X, 0.2f)
				.target(moveTo + getDot4, 120).start(GdxGame.tweenManager);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				GdxGame.isStageMoving = false;

				if (((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) >= 0)
						&& ((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) <= 1)) {
					GdxGame.lastPosition = GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;
					GdxGame.stageNumber = 1;
				}
				if ((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) > 1
						&& (newPosition / GdxGame.VIRTUAL_GAME_WIDTH) <= 2) {
					GdxGame.lastPosition = GdxGame.VIRTUAL_GAME_WIDTH
							+ GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;
					GdxGame.stageNumber = 2;
				}
				if ((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) > 2
						&& (newPosition / GdxGame.VIRTUAL_GAME_WIDTH) <= 3) {
					GdxGame.lastPosition = 2 * GdxGame.VIRTUAL_GAME_WIDTH
							+ GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;
					GdxGame.stageNumber = 3;
				}
				if ((newPosition / GdxGame.VIRTUAL_GAME_WIDTH) > 3
						&& (newPosition / GdxGame.VIRTUAL_GAME_WIDTH) <= 4) {
					GdxGame.lastPosition = 3 * GdxGame.VIRTUAL_GAME_WIDTH
							+ GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;
					GdxGame.stageNumber = 4;
				}

				switchDots(GdxGame.stageNumber);

				StageScreen.dot1.setPositionXY(moveTo + getDot1, 120);
				StageScreen.dot2.setPositionXY(moveTo + getDot2, 120);
				StageScreen.dot3.setPositionXY(moveTo + getDot3, 120);
				StageScreen.dot4.setPositionXY(moveTo + getDot4, 120);

			}

		}, 200);

	}*/

	private void switchDots(int stageNumber) {
		switch (GdxGame.stageNumber) {
		case 1:
			getDot1 = -25;
			getDot2 = 25;
			getDot3 = -75;
			getDot4 = 75;
			break;
		case 2:
			getDot1 = -75;
			getDot2 = 25;
			getDot3 = -25;
			getDot4 = 75;
			break;
		case 3:
			getDot1 = -75;
			getDot2 = -25;
			getDot3 = 25;
			getDot4 = 75;
			break;
		case 4:
			getDot1 = -75;
			getDot2 = -25;
			getDot3 = 75;
			getDot4 = 25;
			break;

		default:
			break;
		}
	}
}
