package com.workshop27.ballistabreaker.engine;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenPaths;
import aurelienribon.tweenengine.equations.Linear;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateByAction;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.badlogic.gdx.scenes.scene2d.actions.ScaleToAction;
import com.badlogic.gdx.utils.Logger;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.bonus_rocks.BoomBonusBeetle;
import com.workshop27.ballistabreaker.bonus_rocks.FanBonusBeetle;
import com.workshop27.ballistabreaker.bonus_rocks.GreenBonusStar;
import com.workshop27.ballistabreaker.bonus_rocks.KarateBonusBeetle;
import com.workshop27.ballistabreaker.bonus_rocks.MinerBonusBeetle;
import com.workshop27.ballistabreaker.bonus_rocks.RedBonusStar;
import com.workshop27.ballistabreaker.bonus_rocks.RegularBonusBeetle;
import com.workshop27.ballistabreaker.bonus_rocks.YellowBonusStar;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.constants.Texts;
import com.workshop27.ballistabreaker.engine.BBInventory.AvaliableBonuses;
import com.workshop27.ballistabreaker.engine.ShowTutorialObject.ArrowPosEnum;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle;
import com.workshop27.ballistabreaker.interfaces.AbstractBonusBeetle;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle.BeetleState;
import com.workshop27.ballistabreaker.interfaces.AbstractBonusBeetle.BonusBeetleState;
import com.workshop27.ballistabreaker.rocks.ArmedEgg;
import com.workshop27.ballistabreaker.rocks.ArmedEggMovable;
import com.workshop27.ballistabreaker.rocks.BoomBeetle;
import com.workshop27.ballistabreaker.rocks.FanBeetle;
import com.workshop27.ballistabreaker.rocks.KarateBeetle;
import com.workshop27.ballistabreaker.rocks.MinerBeetle;
import com.workshop27.ballistabreaker.rocks.RegularBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class BeetleManager {
	private AbstractBeetle beetle;

	public final ArrayList<AbstractBeetle> beetleList = new ArrayList<AbstractBeetle>();
	public final ArrayList<AbstractBonusBeetle> bonusBeetleList = new ArrayList<AbstractBonusBeetle>();

	public BeetleButton regularBtn;
	public BeetleButton minerBtn;
	public BeetleButton fanBtn;
	public BeetleButton boomBtn;
	public BeetleButton karateBtn;

	private AbstractBeetle aciveBeetle;

	public float pixelsLabelValue;

	private final HashMap<String, AbstractBeetle> eggsVertex = new HashMap<String, AbstractBeetle>();
	private final ArrayList<GraphRib> eggsRibs = new ArrayList<GraphRib>();
	private final ArrayList<String> scheduledExplosions = new ArrayList<String>();
	private final ArrayList<String> visitedVertices = new ArrayList<String>();

	private float scheduleExplosionTimer = 0;

	private boolean uniqueKey = true;

	public final DecimalFormat dec = new DecimalFormat("##");

	public GdxGame game;

	private int beetleEnrageTimer = 0;

	private int explRadius = 0;

	private AbstractBonusBeetle bonusBeetle;

	public static int loadingCounter = 1;

	private int scheduledExplosionsSize;
	private Iterator<Entry<String, AbstractBeetle>> eggVertexIterator;

	private String starCollectSoundName;

	private final Timer bManagerTimer = new Timer();
	private final ArrayList<TimerTask> bManagerTimerTasks = new ArrayList<TimerTask>();
	
	private Vector2 localPoint = new Vector2(0, 0);
	private final Vector2 v = new Vector2(0, 0);

	Map<String, String> params = new HashMap<String, String>();

	Logger log = new Logger("BeetleManager");

	// private GameObject wings;

	private ArrayList<String> getNodes(String node) {
		ArrayList<String> nodesList = new ArrayList<String>();

		for (int i = 0; i < eggsRibs.size(); i++) {
			if (eggsRibs.get(i).vertex1.equals(node)) {
				nodesList.add(eggsRibs.get(i).vertex2);
			} else if (eggsRibs.get(i).vertex2.equals(node)) {
				nodesList.add(eggsRibs.get(i).vertex1);
			}
		}

		return nodesList;
	}

	public void setBeetleEnrageTimer(int i) {
		this.beetleEnrageTimer = i;
	}

	public int getBeetleEnrageTimer() {
		return this.beetleEnrageTimer;
	}

	public boolean isBeetlesInRage() {
		if (GameScreen.levelNumb == 1 || GameScreen.levelNumb == 2) {
			return true;
		}

		return beetleEnrageTimer - loadingCounter < 0;
	}

	// public int getBeetleShotNumb() {
	// int n = regularBtn.beetleTypeTotal - regularBtn.beetleTypeCount
	// + minerBtn.beetleTypeTotal - minerBtn.beetleTypeCount
	// + fanBtn.beetleTypeTotal - fanBtn.beetleTypeCount
	// + boomBtn.beetleTypeTotal - boomBtn.beetleTypeCount
	// + karateBtn.beetleTypeTotal - karateBtn.beetleTypeCount;
	// return n;
	// }

	public boolean isEggsInFly(AbstractBeetle b) {
		for (int j = 0; j < b.getNames().size(); j++) {
			AbstractBeetle egg = (AbstractBeetle) GameScreen.rootGroup
					.findActor(b.getNames().get(j));

			if (egg.getState() == BeetleState.INFLY) {
				return true;
			}
		}

		return false;
	}

	public void removeAllBeetles() {
		for (int i = 0; i < beetleList.size(); i++) {
			AbstractBeetle aBeettle = (AbstractBeetle) GameScreen.rootGroup
					.findActor(beetleList.get(i).getName());

			if (beetleList.get(i).getNames() != null) {
				for (int j = 0; j < beetleList.get(i).getNames().size(); j++) {
					AbstractBeetle egg = (AbstractBeetle) GameScreen.rootGroup
							.findActor(beetleList.get(i).getNames().get(j));
					egg.remove();
					egg = null;
				}
			}

			if (aBeettle != null) {
				aBeettle.remove();
				aBeettle = null;
			}

		}

		beetleList.clear();
	}

	public ArrayList<AbstractBonusBeetle> getBonusesList() {
		return bonusBeetleList;
	}

	public void removeAllBonusBeetles() {
		for (int i = 0; i < bonusBeetleList.size(); i++) {
			AbstractBonusBeetle aBeettle = (AbstractBonusBeetle) GameScreen.rootGroup
					.findActor(bonusBeetleList.get(i).getName());

			aBeettle.remove();
			aBeettle = null;
		}
		loadingCounter = 0;
		bonusBeetleList.clear();

	}

	private void restartBeetlesAndEggs() {
		GameScreen.particle_explosion.clearParticle("explosion"
				+ GdxGame.stageNumber);
		GameScreen.particle_explosion.createParticle("explosion"
				+ GdxGame.stageNumber, true);

		for (int i = 0; i < beetleList.size(); i++) {
			if (beetleList.get(i).isBonusBeetle()) {
				beetleList.get(i).setState(BeetleState.HIDDEN);
			} else {
				beetleList.get(i).setState(BeetleState.SLEEP);

			}
			beetleList.get(i).clearParticles();
			beetleList.get(i).mineCount = 0;
			beetleList.get(i).deceleration = beetleList.get(i).start_deceleration;
			beetleList.get(i).inRage = false;

			if (beetleList.get(i).getType() == 3
					&& beetleList.get(i).getNames() != null) {
				for (int j = 0; j < beetleList.get(i).getNames().size(); j++) {
					AbstractBeetle egg = (AbstractBeetle) GameScreen.rootGroup
							.findActor(beetleList.get(i).getNames().get(j));
					egg.setState(BeetleState.SLEEP);
					egg.getColor().a = 1.0f;
					egg.deceleration = Integer.MAX_VALUE;
					egg.isFadeOut = false;
					egg.inRage = false;
					egg.explodeOnTouch = true;
					egg.clearActions();
					egg.clearParticle("p4");
					egg.resetPontCounter();

					// egg.clearParticle("explosion" + GdxGame.stageNumber);
					// egg.createParticle("explosion" + GdxGame.stageNumber,
					// true);
				}
			}
			if (beetleList.get(i).getType() == 5) {
				beetleList.get(i).setLastExplosionX(0);
				beetleList.get(i).setLastExplosionY(0);
			}
		}
	}

	public void restartManager() {
		regularBtn.restart();
		minerBtn.restart();
		fanBtn.restart();
		boomBtn.restart();
		karateBtn.restart();

		scheduledExplosions.clear();
		setActiveBeetle(null);

		if (GameScreen.reloadTimer <= 0) {
			GameScreen.reloadTimer = Constants.RELOADING_DELAY;
		}

		while (eggsRibs.size() > 0) {
			GameObject pSt = (GameObject) GameScreen.rootGroup
					.findActor(eggsRibs.get(0).getRibName());
			if (pSt != null) {
				GameScreen.rootGroup.removeActor(pSt);
			}

			GameObject gSt = (GameObject) GameScreen.rootGroup
					.findActor(eggsRibs.get(0).getRibName() + "spark");
			if (gSt != null) {
				GameScreen.rootGroup.removeActor(gSt);
			}

			eggsRibs.remove(0);
		}

		Iterator<Entry<String, AbstractBeetle>> it1 = this.eggsVertex.entrySet().iterator();

		while (it1.hasNext()) {
			Entry<String, AbstractBeetle> pairs = it1.next();

			pairs.getValue().setState(BeetleState.SLEEP);
			it1.remove();
		}
	}

	public void restart() {
		for (int i = 0; i < bManagerTimerTasks.size(); i++) {
			bManagerTimerTasks.get(i).cancel();
		}
		bManagerTimerTasks.clear();

		restartManager();
		restartBeetlesAndEggs();
		restartBonuses();
	}

	private void restartBonuses() {
		for (int i = 0; i < bonusBeetleList.size(); i++) {
			// bonusBeetleList.get(i).clearParticles();
			bonusBeetleList.get(i).clearActions();
			bonusBeetleList.get(i).getColor().a = 1.0f;
			bonusBeetleList.get(i).setVisible(false);
			bonusBeetleList.get(i).setRotation(0);
			bonusBeetleList.get(i).setActiveState(BonusBeetleState.BONUS);
			bonusBeetleList.get(i).setX(bonusBeetleList.get(i).getX());
			bonusBeetleList.get(i).setY(bonusBeetleList.get(i).getY());
			bonusBeetleList.get(i).resetConsumableBonusValue();
		}
	}

	public boolean checkEggBeetles() {
		for (int i = 0; i < beetleList.size(); i++) {
			if (beetleList.get(i).getType() == 2
					|| beetleList.get(i).getType() == 3) {
				return true;
			}
		}

		return false;
	}

	public void addBeetles(GameScreen gs,
			HashMap<String, ArrayList<Integer>> params) {
		ArrayList<Integer> param = new ArrayList<Integer>();

		Integer a;
		// Regular
		try {
			param = params.get("Regular");
			a = param.get(0);
			if (a > 0) {
				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));
					beetleParams.put("type", 1);

					beetle = new RegularBeetle("regular_bomb"
							+ Integer.toString(a), "regular", beetleParams);

					beetle.setOriginX(23);
					beetle.setOriginY(25);

					beetle.explodeOnTouch = true;
					beetle.setBonusBeetle(false);

					beetle.setOnCreateFilter(true);

					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}
			}
			a = param.get(1);
			if (a > 0) {
				for (int i = 0; i < bonusBeetleList.size(); i++) {
					if (bonusBeetleList.get(i).getType() == 1) {
						bonusBeetleList.get(i).setInitialBonusValueCount(a);
						break;
					}
				}

				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));
					beetleParams.put("type", 1);

					beetle = new RegularBeetle("regular_bomb"
							+ Integer.toString(100 + a), "regular",
							beetleParams);

					beetle.setOriginX(23);
					beetle.setOriginY(25);

					beetle.explodeOnTouch = true;
					beetle.setBonusBeetle(true);

					beetle.setOnCreateFilter(true);

					beetle.setState(BeetleState.HIDDEN);

					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}
			}
		} catch (Exception e) {
			log.error("Regular beetle creation failed");
		}

		// Fan
		try {
			param = params.get("Fan");
			a = param.get(0);
			if (a > 0) {
				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration", Integer.MAX_VALUE);
					beetleParams.put("type", 11);

					ArrayList<String> names = new ArrayList<String>(
							Constants.BASE_EGGS_QUANTITY
									+ Constants.BONUS2_EGGS_QUANTITY);
					names = createFanEggs(Constants.BASE_EGGS_QUANTITY
							+ Constants.BONUS2_EGGS_QUANTITY, beetleParams);

					beetleParams.clear();
					// beetleParams.put("speedCoeficient", 30);
					// beetleParams.put("deceleration", 415);
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));
					beetleParams.put("type", 3);
					beetle = new FanBeetle("fan_bomb" + Integer.toString(a),
							"fan", beetleParams, names);

					beetle.setOriginX(25);
					beetle.setOriginY(31);

					beetle.explodeOnTouch = true;
					beetle.setBonusBeetle(false);

					beetle.setOnCreateFilter(true);

					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}
			}
			a = param.get(1);
			if (a > 0) {
				for (int i = 0; i < bonusBeetleList.size(); i++) {
					if (bonusBeetleList.get(i).getType() == 3) {
						bonusBeetleList.get(i).setInitialBonusValueCount(a);
						break;
					}
				}

				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration", Integer.MAX_VALUE);
					beetleParams.put("type", 11);

					ArrayList<String> names = new ArrayList<String>(
							Constants.BASE_EGGS_QUANTITY
									+ Constants.BONUS2_EGGS_QUANTITY);
					names = createFanEggs(Constants.BASE_EGGS_QUANTITY
							+ Constants.BONUS2_EGGS_QUANTITY, beetleParams);

					beetleParams.clear();
					// beetleParams.put("speedCoeficient", 30);
					// beetleParams.put("deceleration", 415);
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));
					beetleParams.put("type", 3);
					beetle = new FanBeetle("fan_bomb"
							+ Integer.toString(100 + a), "fan", beetleParams,
							names);

					beetle.setOriginX(25);
					beetle.setOriginY(31);

					beetle.explodeOnTouch = true;
					beetle.setBonusBeetle(true);

					beetle.setOnCreateFilter(true);

					beetle.setState(BeetleState.HIDDEN);

					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}
			}
		} catch (Exception e) {
			log.error("Fan beetle creation failed");

		}

		// Minner
		try {
			param = params.get("Minner");
			a = param.get(0);
			if (a > 0) {
				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", 0);
					beetleParams.put("deceleration", 0);
					beetleParams.put("type", 10);
					ArrayList<String> names = new ArrayList<String>();

					for (int ii = 1; ii <= Constants.BASE_EGGS_QUANTITY
							+ Constants.BONUS2_EGGS_QUANTITY; ii++) {
						names.add(createMinnerEggs(beetleParams));
					}

					beetleParams.clear();
					// beetleParams.put("speedCoeficient", 30);
					// beetleParams.put("deceleration", 415);
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));

					beetleParams.put("type", 2);

					beetle = new MinerBeetle("minner_bomb"
							+ Integer.toString(100 + a), "miner", beetleParams,
							names);

					beetle.setOriginX(27);
					beetle.setOriginY(23);

					beetle.explodeOnTouch = false;
					beetle.setBonusBeetle(false);

					beetle.setOnCreateFilter(true);
					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}
			}
			a = param.get(1);
			if (a > 0) {
				for (int i = 0; i < bonusBeetleList.size(); i++) {
					if (bonusBeetleList.get(i).getType() == 2) {
						bonusBeetleList.get(i).setInitialBonusValueCount(a);
						break;
					}
				}

				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", 0);
					beetleParams.put("deceleration", 0);
					beetleParams.put("type", 10);
					ArrayList<String> names = new ArrayList<String>();

					for (int ii = 1; ii <= Constants.BASE_EGGS_QUANTITY
							+ Constants.BONUS2_EGGS_QUANTITY; ii++) {
						names.add(createMinnerEggs(beetleParams));
					}

					beetleParams.clear();
					// beetleParams.put("speedCoeficient", 30);
					// beetleParams.put("deceleration", 415);
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));

					beetleParams.put("type", 2);

					beetle = new MinerBeetle("minner_bomb"
							+ Integer.toString(a), "miner", beetleParams, names);

					beetle.setOriginX(27);
					beetle.setOriginY(23);

					beetle.explodeOnTouch = false;
					beetle.setBonusBeetle(true);

					beetle.setOnCreateFilter(true);

					beetle.setState(BeetleState.HIDDEN);
					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}

			}
		} catch (Exception e) {
			log.error("Miner beetle creation failed");

		}
		try {
			param = params.get("Boom");
			a = param.get(0);
			if (a > 0) {
				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));
					beetleParams.put("type", 4);

					beetle = new BoomBeetle("boom" + Integer.toString(a),
							"boom", beetleParams);

					beetle.setOriginX(23);
					beetle.setOriginY(25);

					beetle.explodeOnTouch = false;
					beetle.setBonusBeetle(false);

					beetle.setOnCreateFilter(true);
					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}
			}
			a = param.get(1);
			if (a > 0) {
				for (int i = 0; i < bonusBeetleList.size(); i++) {
					if (bonusBeetleList.get(i).getType() == 4) {
						bonusBeetleList.get(i).setInitialBonusValueCount(a);
						break;
					}
				}

				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));
					beetleParams.put("type", 4);

					beetle = new BoomBeetle("boom" + Integer.toString(100 + a),
							"boom", beetleParams);

					beetle.setOriginX(23);
					beetle.setOriginY(25);

					beetle.explodeOnTouch = false;
					beetle.setBonusBeetle(true);

					beetle.setOnCreateFilter(true);

					beetle.setState(BeetleState.HIDDEN);

					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}

			}
		} catch (Exception e) {
			log.error("Boom beetle creation failed");

		}

		try {
			param = params.get("Karate");
			a = param.get(0);
			if (a > 0) {
				while (a > 0) {

					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", 0);
					beetleParams.put("deceleration", 0);
					beetleParams.put("type", 10);
					ArrayList<String> names = new ArrayList<String>();
					names.add(createMinnerEggs(beetleParams));

					beetleParams.clear();
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));
					beetleParams.put("type", 5);

					beetle = new KarateBeetle("karate" + Integer.toString(a),
							"karate", beetleParams, names);

					beetle.setOriginX(23);
					beetle.setOriginY(25);

					beetle.explodeOnTouch = false;
					beetle.setBonusBeetle(false);

					beetle.setOnCreateFilter(true);
					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}
			}
			a = param.get(1);
			if (a > 0) {
				for (int i = 0; i < bonusBeetleList.size(); i++) {
					if (bonusBeetleList.get(i).getType() == 5) {
						bonusBeetleList.get(i).setInitialBonusValueCount(a);
						break;
					}
				}

				while (a > 0) {
					HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
					beetleParams.put("speedCoeficient", 0);
					beetleParams.put("deceleration", 0);
					beetleParams.put("type", 10);
					ArrayList<String> names = new ArrayList<String>();
					names.add(createMinnerEggs(beetleParams));
					names.add(createMinnerEggs(beetleParams));
					names.add(createMinnerEggs(beetleParams));
					names.add(createMinnerEggs(beetleParams));
					names.add(createMinnerEggs(beetleParams));

					beetleParams.clear();
					beetleParams.put("speedCoeficient", Constants.BEETLE_SPEED);
					beetleParams.put("deceleration",
							(int) (3000 * GdxGame.HD_X_RATIO));
					beetleParams.put("type", 5);

					beetle = new KarateBeetle("karate"
							+ Integer.toString(100 + a), "karate",
							beetleParams, names);

					beetle.setOriginX(23);
					beetle.setOriginY(25);

					beetle.explodeOnTouch = false;
					beetle.setBonusBeetle(true);

					beetle.setOnCreateFilter(true);

					beetle.setState(BeetleState.HIDDEN);
					addToBeetleList(beetle);

					gs.addToRootGroup(beetle);

					a--;
				}
			}
		} catch (Exception e) {
			log.error("Karate beetle creation failed");

		}

		addShopBeetles(gs);
	}

	private void addShopBeetles(GameScreen gs) {
		HashMap<String, Integer> beetleParams = new HashMap<String, Integer>();
		beetleParams.put("speedCoeficient", 0);
		beetleParams.put("deceleration", 0);
		beetleParams.put("type", 10);
		ArrayList<String> names = new ArrayList<String>();
		names.add(createMinnerEggs(beetleParams));
		names.add(createMinnerEggs(beetleParams));
		names.add(createMinnerEggs(beetleParams));
		names.add(createMinnerEggs(beetleParams));
		names.add(createMinnerEggs(beetleParams));

		beetleParams.clear();
		beetleParams.put("speedCoeficient", 35);
		beetleParams.put("deceleration", (int) (3000 * GdxGame.HD_X_RATIO));
		beetleParams.put("type", 5);

		beetle = new KarateBeetle("karate-shop", "karate", beetleParams, names);

		beetle.setOriginX(23);
		beetle.setOriginY(25);

		beetle.explodeOnTouch = false;
		beetle.setBonusBeetle(false);

		beetle.setOnCreateFilter(true);
		addToBeetleList(beetle);

		gs.addToRootGroup(beetle);

		beetleParams.clear();
		beetleParams.put("speedCoeficient", 35);
		beetleParams.put("deceleration", (int) (3000 * GdxGame.HD_X_RATIO));
		beetleParams.put("type", 4);

		beetle = new BoomBeetle("boom-shop", "boom", beetleParams);

		beetle.setOriginX(23);
		beetle.setOriginY(25);

		beetle.explodeOnTouch = false;
		beetle.setBonusBeetle(false);

		beetle.setOnCreateFilter(true);
		addToBeetleList(beetle);

		gs.addToRootGroup(beetle);
	}

	public void addBonusStars(GameScreen gameScreen, int level2) {
		String stars = null;
		try {
			stars = GdxGame.files.getScenario("stars", "level" + level2);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Scanner s = new Scanner(stars).useDelimiter("-");

		// Green
		bonusBeetle = new GreenBonusStar("green-star", "green-star");

		bonusBeetle.setOriginX(18);
		bonusBeetle.setOriginY(17.5f);
		bonusBeetle.setX(s.nextInt() * GdxGame.HD_X_RATIO);
		bonusBeetle.setY(s.nextInt() * GdxGame.HD_Y_RATIO);
		bonusBeetle.setTurnsVisibleBegin(s.nextInt());
		bonusBeetle.setStatic(s.nextInt());
		bonusBeetle.setTurnsVisibleEnd(bonusBeetle.getTurnsVisibleBegin() + 3);
		bonusBeetle.setType(97);
		bonusBeetle.setActiveState(BonusBeetleState.BONUS);
		bonusBeetle.setVisible(false);

		addToBonusBeetleList(bonusBeetle);

		bonusBeetle.setOnCreateFilter(true);
		gameScreen.addToRootGroup(bonusBeetle);

		// Yellow
		bonusBeetle = new YellowBonusStar("yellow-star", "yellow-star");

		bonusBeetle.setOriginX(17.5f);
		bonusBeetle.setOriginY(17.5f);
		bonusBeetle.setX(s.nextInt() * GdxGame.HD_X_RATIO);
		bonusBeetle.setY(s.nextInt() * GdxGame.HD_Y_RATIO);
		bonusBeetle.setTurnsVisibleBegin(s.nextInt());
		bonusBeetle.setStatic(s.nextInt());
		bonusBeetle.setTurnsVisibleEnd(bonusBeetle.getTurnsVisibleBegin() + 2);
		bonusBeetle.setType(98);
		bonusBeetle.setActiveState(BonusBeetleState.BONUS);
		bonusBeetle.setVisible(false);

		addToBonusBeetleList(bonusBeetle);

		bonusBeetle.setOnCreateFilter(true);
		gameScreen.addToRootGroup(bonusBeetle);

		// Red
		bonusBeetle = new RedBonusStar("red-star", "red-star");

		bonusBeetle.setOriginX(18);
		bonusBeetle.setOriginY(17.5f);
		bonusBeetle.setX(s.nextInt() * GdxGame.HD_X_RATIO);
		bonusBeetle.setY(s.nextInt() * GdxGame.HD_Y_RATIO);
		bonusBeetle.setTurnsVisibleBegin(s.nextInt());
		bonusBeetle.setStatic(s.nextInt());
		bonusBeetle.setTurnsVisibleEnd(bonusBeetle.getTurnsVisibleBegin() + 1);
		bonusBeetle.setType(99);
		bonusBeetle.setActiveState(BonusBeetleState.BONUS);
		bonusBeetle.setVisible(false);

		addToBonusBeetleList(bonusBeetle);

		bonusBeetle.setOnCreateFilter(true);
		gameScreen.addToRootGroup(bonusBeetle);
	}

	public void addBonusBeetles(GameScreen gameScreen,
			HashMap<String, Integer> bManageParams, int level2) {
		int a = 0;
		// Random randomGenerator = new Random();
		String bonuses = null;
		try {
			bonuses = GdxGame.files.getScenario("bonuses", "level" + level2);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Scanner s = new Scanner(bonuses).useDelimiter("-");

		// Regular
		try {
			a = bManageParams.get("Regular");
			if (a > 0) {
				while (a > 0) {
					bonusBeetle = new RegularBonusBeetle("regular_bomb_bonus",
							"regular-bonus");

					bonusBeetle.setOriginX(23);
					bonusBeetle.setOriginY(25);
					bonusBeetle.setX(s.nextInt() * GdxGame.HD_X_RATIO);
					bonusBeetle.setY(s.nextInt() * GdxGame.HD_Y_RATIO);
					bonusBeetle.setTurnsVisibleBegin(s.nextInt());
					bonusBeetle.setTurnsVisibleEnd(s.nextInt());
					bonusBeetle.setType(1);
					bonusBeetle.setActiveState(BonusBeetleState.BONUS);
					bonusBeetle.setVisible(false);
					bonusBeetle.startAnimation();
					// bonusBeetle.createParticle("magicDust", true);

					addToBonusBeetleList(bonusBeetle);

					bonusBeetle.setOnCreateFilter(true);
					gameScreen.addToRootGroup(bonusBeetle);
					a--;
				}
			}
		} catch (Exception e) {
			log.error("Regular bonus creation failed");

		}

		// Fan
		try {
			a = bManageParams.get("Fan");
			if (a > 0) {
				while (a > 0) {

					bonusBeetle = new FanBonusBeetle("fan_bomb_bonus",
							"fan-bonus");

					bonusBeetle.setOriginX(23);
					bonusBeetle.setOriginY(25);
					bonusBeetle.setX(s.nextInt() * GdxGame.HD_X_RATIO);
					bonusBeetle.setY(s.nextInt() * GdxGame.HD_Y_RATIO);
					bonusBeetle.setTurnsVisibleBegin(s.nextInt());
					bonusBeetle.setTurnsVisibleEnd(s.nextInt());
					bonusBeetle.setType(3);
					bonusBeetle.setActiveState(BonusBeetleState.BONUS);
					bonusBeetle.setVisible(false);
					bonusBeetle.startAnimation();
					// bonusBeetle.createParticle("magicDust", true);

					addToBonusBeetleList(bonusBeetle);

					bonusBeetle.setOnCreateFilter(true);
					gameScreen.addToRootGroup(bonusBeetle);
					a--;
				}
			}
		} catch (Exception e) {
			log.error("Fan bonus creation failed");

		}

		// Minner
		try {
			a = bManageParams.get("Minner");
			if (a > 0) {
				while (a > 0) {
					bonusBeetle = new MinerBonusBeetle("minner_bomb_bonus",
							"minner-bonus");

					bonusBeetle.setOriginX(23);
					bonusBeetle.setOriginY(25);
					bonusBeetle.setX(s.nextInt() * GdxGame.HD_X_RATIO);
					bonusBeetle.setY(s.nextInt() * GdxGame.HD_Y_RATIO);
					bonusBeetle.setTurnsVisibleBegin(s.nextInt());
					bonusBeetle.setTurnsVisibleEnd(s.nextInt());
					bonusBeetle.setType(2);
					bonusBeetle.setActiveState(BonusBeetleState.BONUS);
					bonusBeetle.setVisible(false);
					bonusBeetle.startAnimation();
					// bonusBeetle.createParticle("magicDust", true);

					addToBonusBeetleList(bonusBeetle);

					bonusBeetle.setOnCreateFilter(true);
					gameScreen.addToRootGroup(bonusBeetle);
					a--;
				}
			}
		} catch (Exception e) {
			log.error("Miner bonus creation failed");
		}

		// Boom
		try {
			a = bManageParams.get("Boom");
			if (a > 0) {
				while (a > 0) {
					bonusBeetle = new BoomBonusBeetle("boom_bomb_bonus",
							"boom-bonus");

					bonusBeetle.setOriginX(23);
					bonusBeetle.setOriginY(25);
					bonusBeetle.setX(s.nextInt() * GdxGame.HD_X_RATIO);
					bonusBeetle.setY(s.nextInt() * GdxGame.HD_Y_RATIO);
					bonusBeetle.setTurnsVisibleBegin(s.nextInt());
					bonusBeetle.setTurnsVisibleEnd(s.nextInt());
					bonusBeetle.setType(4);
					bonusBeetle.setActiveState(BonusBeetleState.BONUS);
					bonusBeetle.setVisible(false);
					bonusBeetle.startAnimation();
					// bonusBeetle.createParticle("magicDust", true);

					addToBonusBeetleList(bonusBeetle);

					bonusBeetle.setOnCreateFilter(true);
					gameScreen.addToRootGroup(bonusBeetle);
					a--;
				}
			}
		} catch (Exception e) {
			log.error("Boom bonus creation failed");

		}

		// Karate
		try {
			a = bManageParams.get("Karate");
			if (a > 0) {
				while (a > 0) {
					bonusBeetle = new KarateBonusBeetle("karate_bomb_bonus",
							"karate-bonus");

					bonusBeetle.setOriginX(23);
					bonusBeetle.setOriginY(25);
					bonusBeetle.setX(s.nextInt() * GdxGame.HD_X_RATIO);
					bonusBeetle.setY(s.nextInt() * GdxGame.HD_Y_RATIO);
					bonusBeetle.setTurnsVisibleBegin(s.nextInt());
					bonusBeetle.setTurnsVisibleEnd(s.nextInt());
					bonusBeetle.setType(5);
					bonusBeetle.setActiveState(BonusBeetleState.BONUS);
					bonusBeetle.setVisible(false);
					bonusBeetle.startAnimation();
					// bonusBeetle.createParticle("magicDust", true);

					addToBonusBeetleList(bonusBeetle);

					bonusBeetle.setOnCreateFilter(true);
					gameScreen.addToRootGroup(bonusBeetle);
					a--;
				}
			}
		} catch (Exception e) {
			log.error("Karate bonus creation failed");

		}

	}

	private void addToBonusBeetleList(AbstractBonusBeetle beetle2) {
		bonusBeetleList.add(beetle2);

	}

	private void addToBeetleList(AbstractBeetle b) {
		beetleList.add(b);
	}

	private AbstractBeetle getBeetleByType(int t) {
		for (int i = 0; i < beetleList.size(); i++) {
			if (beetleList.get(i).getType() == t
					&& beetleList.get(i).getState() == BeetleState.SLEEP) {
				return beetleList.get(i);
			}
		}

		return null;
	}

	private AbstractBeetle getBonusBeetleByName(String s) {
		AbstractBeetle aBeetle = null;

		for (int i = 0; i < beetleList.size(); i++) {
			if (beetleList.get(i).getName().equals(s + "-shop")) {
				aBeetle = beetleList.get(i);
			}
		}

		if (aBeetle != null) {
			aBeetle.setState(BeetleState.SLEEP);

			aBeetle.clearParticles();
			aBeetle.mineCount = 0;
			aBeetle.deceleration = aBeetle.start_deceleration;
		}

		return aBeetle;
	}

	public int getBeetlesCount() {
		if (getActiveBeetle() != null
				&& getActiveBeetle().getState().equals(BeetleState.ACTIVE)) {
			return 1 + regularBtn.beetleTypeCount + fanBtn.beetleTypeCount
					+ minerBtn.beetleTypeCount + boomBtn.beetleTypeCount
					+ karateBtn.beetleTypeCount;
		} else {
			return regularBtn.beetleTypeCount + fanBtn.beetleTypeCount
					+ minerBtn.beetleTypeCount + boomBtn.beetleTypeCount
					+ karateBtn.beetleTypeCount;
		}
	}

	public int getRegularBeetlesCount() {
		return regularBtn.beetleTypeCount;
	}

	public int getFanBeetlesCount() {
		return fanBtn.beetleTypeCount;
	}

	public int getMinerBeetlesCount() {
		return minerBtn.beetleTypeCount;
	}

	public void onRegularButtonClicked() {
		if ((getActiveBeetle() != null && getActiveBeetle().getState() != BeetleState.ACTIVE)
				|| GameScreen.reloadTimer != 0) {
			return;
		}

		if (regularBtn.beetleTypeCount > 0) {
			if (getActiveBeetle() != null) {
				int t = getActiveBeetle().getType();

				if (t == 1) {
					regularBtn.beetleTypeCount++;
				}

				else if (t == 2) {
					minerBtn.beetleTypeCount++;
				}

				else if (t == 3) {
					fanBtn.beetleTypeCount++;
				}

				else if (t == 4) {
					if (!getActiveBeetle().getName().equals("boom-shop")) {
						boomBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle1.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle1.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.BOOM_BUG);
					}

				} else if (t == 5) {
					if (!getActiveBeetle().getName().equals("karate-shop")) {
						karateBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle2.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle2.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.KARATE_BUG);
					}
				}
			}

			regularBtn.beetleTypeCount--;

			tryActivateRage();
			getBeetleByType(1).abstractTouchDown();
			AudioPlayer.PlayRandomBeetleSound(getActiveBeetle().getType());
		}
	}

	public void onFanButtonClicked() {
		if ((getActiveBeetle() != null && getActiveBeetle().getState() != BeetleState.ACTIVE)
				|| GameScreen.reloadTimer != 0) {
			return;
		}

		if (fanBtn.beetleTypeCount > 0) {
			if (getActiveBeetle() != null) {
				int t = getActiveBeetle().getType();
				if (t == 1) {
					regularBtn.beetleTypeCount++;
				}

				else if (t == 2) {
					minerBtn.beetleTypeCount++;
				}

				else if (t == 3) {
					fanBtn.beetleTypeCount++;
				}

				else if (t == 4) {
					if (!getActiveBeetle().getName().equals("boom-shop")) {
						boomBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle1.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle1.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.BOOM_BUG);
					}

				} else if (t == 5) {
					if (!getActiveBeetle().getName().equals("karate-shop")) {
						karateBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle2.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle2.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.KARATE_BUG);
					}
				}
			}

			fanBtn.beetleTypeCount--;

			tryActivateRage();
			getBeetleByType(3).abstractTouchDown();
			AudioPlayer.PlayRandomBeetleSound(getActiveBeetle().getType());
		}
	}

	public void onMinerButtonClicked() {
		if ((getActiveBeetle() != null && getActiveBeetle().getState() != BeetleState.ACTIVE)
				|| GameScreen.reloadTimer != 0) {
			return;
		}

		if (minerBtn.beetleTypeCount > 0) {
			if (getActiveBeetle() != null) {
				int t = getActiveBeetle().getType();
				if (t == 1) {
					regularBtn.beetleTypeCount++;
				}

				else if (t == 2) {
					minerBtn.beetleTypeCount++;
				}

				else if (t == 3) {
					fanBtn.beetleTypeCount++;
				}

				else if (t == 4) {
					if (!getActiveBeetle().getName().equals("boom-shop")) {
						boomBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle1.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle1.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.BOOM_BUG);
					}

				} else if (t == 5) {
					if (!getActiveBeetle().getName().equals("karate-shop")) {
						karateBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle2.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle2.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.KARATE_BUG);
					}
				}

			}

			minerBtn.beetleTypeCount--;

			tryActivateRage();
			getBeetleByType(2).abstractTouchDown();
			AudioPlayer.PlayRandomBeetleSound(getActiveBeetle().getType());
		}
	}

	public void onBoomButtonClicked() {
		if ((getActiveBeetle() != null && getActiveBeetle().getState() != BeetleState.ACTIVE)
				|| GameScreen.reloadTimer != 0) {
			return;
		}

		if (boomBtn.beetleTypeCount > 0) {
			if (getActiveBeetle() != null) {
				int t = getActiveBeetle().getType();
				if (t == 1) {
					regularBtn.beetleTypeCount++;
				}

				else if (t == 2) {
					minerBtn.beetleTypeCount++;
				}

				else if (t == 3) {
					fanBtn.beetleTypeCount++;
				}

				else if (t == 4) {
					if (!getActiveBeetle().getName().equals("boom-shop")) {
						boomBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle1.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle1.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.BOOM_BUG);
					}

				} else if (t == 5) {
					if (!getActiveBeetle().getName().equals("karate-shop")) {
						karateBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle2.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle2.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.KARATE_BUG);
					}
				}

			}

			boomBtn.beetleTypeCount--;

			tryActivateRage();
			getBeetleByType(4).abstractTouchDown();
			AudioPlayer.PlayRandomBeetleSound(getActiveBeetle().getType());
		}

	}

	public void onKarateButtonClicked() {
		if ((getActiveBeetle() != null && getActiveBeetle().getState() != BeetleState.ACTIVE)
				|| GameScreen.reloadTimer != 0) {
			return;
		}

		if (karateBtn.beetleTypeCount > 0) {
			if (getActiveBeetle() != null) {
				int t = getActiveBeetle().getType();
				if (t == 1) {
					regularBtn.beetleTypeCount++;
				}

				else if (t == 2) {
					minerBtn.beetleTypeCount++;
				}

				else if (t == 3) {
					fanBtn.beetleTypeCount++;
				}

				else if (t == 4) {
					if (!getActiveBeetle().getName().equals("boom-shop")) {
						boomBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle1.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle1.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.BOOM_BUG);
					}
				}

				else if (t == 5) {
					if (!getActiveBeetle().getName().equals("karate-shop")) {
						karateBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle2.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle2.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.KARATE_BUG);
					}
				}
			}

			karateBtn.beetleTypeCount--;

			tryActivateRage();
			getBeetleByType(5).abstractTouchDown();
			AudioPlayer.PlayRandomBeetleSound(getActiveBeetle().getType());
		}

	}

	public void onShopBoomButtonClicked() {
		if ((getActiveBeetle() != null && getActiveBeetle().getState() != BeetleState.ACTIVE)
				|| GameScreen.reloadTimer != 0) {
			return;
		}

		if (true) {
			if (getActiveBeetle() != null) {
				int t = getActiveBeetle().getType();

				if (t == 1) {
					regularBtn.beetleTypeCount++;
				}

				else if (t == 2) {
					minerBtn.beetleTypeCount++;
				}

				else if (t == 3) {
					fanBtn.beetleTypeCount++;
				}

				else if (t == 4) {
					if (!getActiveBeetle().getName().equals("boom-shop")) {
						boomBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle1.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle1.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.BOOM_BUG);
					}

				} else if (t == 5) {
					if (!getActiveBeetle().getName().equals("karate-shop")) {
						karateBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle2.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle2.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.KARATE_BUG);
					}
				}
			}

			tryActivateRage();
			getBonusBeetleByName("boom").abstractTouchDown();
			AudioPlayer.PlayRandomBeetleSound(getActiveBeetle().getType());
		}
	}

	public void onShopKarateButtonClicked() {
		if ((getActiveBeetle() != null && getActiveBeetle().getState() != BeetleState.ACTIVE)
				|| GameScreen.reloadTimer != 0) {
			return;
		}

		if (true) {
			if (getActiveBeetle() != null) {
				int t = getActiveBeetle().getType();

				if (t == 1) {
					regularBtn.beetleTypeCount++;
				}

				else if (t == 2) {
					minerBtn.beetleTypeCount++;
				}

				else if (t == 3) {
					fanBtn.beetleTypeCount++;
				}

				else if (t == 4) {
					if (!getActiveBeetle().getName().equals("boom-shop")) {
						boomBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle1.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle1.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.BOOM_BUG);
					}
				}

				else if (t == 5) {
					if (!getActiveBeetle().getName().equals("karate-shop")) {
						karateBtn.beetleTypeCount++;
					} else {
						this.game.updateItem("android.beetle2.purchased", -1);

						params.clear();
						params.put("purchased", "android.beetle2.purchased -1");
						params.put("level",
								String.valueOf(GameScreen.levelNumb));
						this.game.flurryAgentLogEvent("Purchased", params);

						GdxGame.mInventory
								.activeBonusesRemove(AvaliableBonuses.KARATE_BUG);
					}
				}
			}

			tryActivateRage();
			getBonusBeetleByName("karate").abstractTouchDown();
			AudioPlayer.PlayRandomBeetleSound(getActiveBeetle().getType());
		}
	}

	public boolean isBonusInFly() {
		for (final AbstractBonusBeetle bonusBeetle : bonusBeetleList) {
			if (bonusBeetle.getActiveState() == BonusBeetleState.INFLY) {
				return true;
			}
		}

		return false;
	}

	public void calcChainExplosion(float explX, float explY, String explName) {

		Iterator<Entry<String, AbstractBeetle>> it1 = this.eggsVertex
				.entrySet().iterator();

		while (it1.hasNext()) {
			Entry<String, AbstractBeetle> pairs = it1.next();

			Vector2 v = new Vector2(pairs.getValue().getCenter().x - explX,
					pairs.getValue().getCenter().y - explY);

			if (isBeetlesInRage()) {
				explRadius = (int) ((Constants.EXPLOSION_CHAIN_LENGTH_BIG + GdxGame.mInventory
						.getBonusRadius()) * GdxGame.HD_X_RATIO);
			} else {
				explRadius = (int) ((Constants.EXPLOSION_CHAIN_LENGTH_SMALL + GdxGame.mInventory
						.getBonusRadius()) * GdxGame.HD_X_RATIO);
			}

			if (v.len() < explRadius
					&& !scheduledExplosions.contains(pairs.getKey())
					&& pairs.getValue().getState() == BeetleState.ARMED) {
				scheduledExplosions.add(pairs.getKey());
			}

			else if (!scheduledExplosions.contains(pairs.getKey())
					&& pairs.getValue().getState() == BeetleState.ARMED) {
				for (int i = 0; i < eggsRibs.size(); i++) {
					if ((this.eggsRibs.get(i).vertex1.equals(explName) && this.eggsRibs
							.get(i).vertex2.equals(pairs.getValue().getName()))
							|| (this.eggsRibs.get(i).vertex1.equals(explName) && this.eggsRibs
									.get(i).vertex2
									.equals(pairs.getValue().getName()))) {
						scheduledExplosions.add(pairs.getKey());
					}
				}
			}
		}

		for (final AbstractBonusBeetle bonusBeetle : bonusBeetleList) {
			Vector2 v = new Vector2(bonusBeetle.getCenter().x - explX,
					bonusBeetle.getCenter().y - explY);

			if (isBeetlesInRage()) {
				explRadius = (int) ((Constants.EXPLOSION_CHAIN_LENGTH_BIG + GdxGame.mInventory
						.getBonusRadius()) * GdxGame.HD_X_RATIO);
			} else {
				explRadius = (int) ((Constants.EXPLOSION_CHAIN_LENGTH_SMALL + GdxGame.mInventory
						.getBonusRadius()) * GdxGame.HD_X_RATIO);
			}

			if (bonusBeetle.getType() == 97 || bonusBeetle.getType() == 98
					|| bonusBeetle.getType() == 99) {
				if (bonusBeetle.getActiveState() == BonusBeetleState.ACTIVATING) {
					explRadius = 0;
				} else {
					explRadius -= bonusBeetle.getWidth() / 2;
				}
			}

			if (v.len() < explRadius && bonusBeetle.isVisible()
					&& bonusBeetle.getActiveState() != BonusBeetleState.INFLY) {
				bonusBeetle.setActiveState(BonusBeetleState.INFLY);
				bonusBeetle.stopAnimation();
				float targetX;
				float targetY;

				final float oldX = bonusBeetle.getPositionX();
				final float oldY = bonusBeetle.getPositionY();

				switch (bonusBeetle.getType()) {
				case 1:
					targetX = 400;
					targetY = 124;
					break;
				case 2:
					targetX = 308;
					targetY = 2;
					break;
				case 3:
					targetX = 349;
					targetY = 78;
					break;
				case 4:
					targetX = 400;
					targetY = 2;
					break;
				case 5:
					targetX = 400;
					targetY = 78;
					break;
				case 97:
					GameScreen.jar.collectStar1(true);

					targetX = 23;
					targetY = 153;

					starCollectSoundName = "get_star"
							+ Slingshot.getRandomInt(1);
					AudioPlayer.playSound(starCollectSoundName, 0.5f);

					break;
				case 98:
					GameScreen.jar.collectStar2(true);

					targetX = 46;
					targetY = 145;

					starCollectSoundName = "get_star"
							+ Slingshot.getRandomInt(1);
					AudioPlayer.playSound(starCollectSoundName, 0.5f);

					break;
				case 99:
					GameScreen.jar.collectStar3(true);

					targetX = 71;
					targetY = 135;

					starCollectSoundName = "get_star"
							+ Slingshot.getRandomInt(1);
					AudioPlayer.playSound(starCollectSoundName, 0.5f);

					break;
				default:
					targetX = 480;
					targetY = 0;
					break;
				}

				long duration = 0;

				if (bonusBeetle.getType() == 97 || bonusBeetle.getType() == 98
						|| bonusBeetle.getType() == 99) {
					bonusBeetle.rotationAngleOffset = 90;

					duration = (long) Tween
							.to(bonusBeetle, GameObjectAccessor.POSITION_XY,
									2.0f).ease(Linear.INOUT)
							.waypoint(oldX - 100, oldY + 100)
							.waypoint(targetX + 100, targetY - 100)
							.target(targetX, targetY)
							.path(TweenPaths.catmullRom)
							.start(GdxGame.tweenManager).getFullDuration();

					bonusBeetle.addAction(scaleTo(0.5f, 0.5f, 2));
				}

				else {
					bonusBeetle.rotationAngleOffset = 90;

					duration = (long) Tween
							.to(bonusBeetle, GameObjectAccessor.POSITION_XY,
									2.0f).ease(Linear.INOUT)
							.waypoint(oldX + 50, oldY + 50)
							.waypoint(targetX - 50, targetY - 50)
							.target(targetX + 20, targetY + 20)
							.target(targetX, targetY)
							.path(TweenPaths.catmullRom)
							.start(GdxGame.tweenManager).getFullDuration();
				}

				GameScreen.showCompleteTimer = 1.5f;

				TimerTask tS = new TimerTask() {
					@Override
					public void run() {
						bonusBeetle.setActiveState(BonusBeetleState.TAKEN);
						unlockBonusBeetles(bonusBeetle.getType());
						bonusBeetle.setVisible(false);
						bonusBeetle.setX(bonusBeetle.getX());
						bonusBeetle.setY(bonusBeetle.getY());

						if (bonusBeetle.getType() == 97) {
							GameScreen.jar.showStar1(true);
						}

						if (bonusBeetle.getType() == 98) {
							GameScreen.jar.showStar2(true);
						}

						if (bonusBeetle.getType() == 99) {
							GameScreen.jar.showStar3(true);
						}
					}
				};

				bManagerTimerTasks.add(tS);
				bManagerTimer.schedule(tS, duration * 1000);

			}
		}

		this.scheduleExplosionTimer = 0.2f;
	}

	private void unlockBonusBeetles(Integer type) {
		if (type == 97 || type == 98 || type == 99) {
			return;
		}

		int beetlesToAdd = 0;

		for (int i = 0; i < bonusBeetleList.size(); i++) {
			if (bonusBeetleList.get(i).getType() == type) {
				beetlesToAdd = bonusBeetleList.get(i).getBonusValueCount();
				break;
			}
		}

		for (AbstractBeetle beetle : beetleList) {
			if (beetle.getType().equals(type)
					&& beetle.getState().equals(BeetleState.HIDDEN)
					&& beetlesToAdd > 0) {
				beetle.setState(BeetleState.SLEEP);

				beetle.getColor().a = 1.0f;
				if (beetle.getType() == 1) {
					regularBtn.beetleTypeCount++;
				} else if (beetle.getType() == 3) {
					fanBtn.beetleTypeCount++;
				} else if (beetle.getType() == 2) {
					minerBtn.beetleTypeCount++;
				} else if (beetle.getType() == 4) {
					boomBtn.beetleTypeCount++;
				} else if (beetle.getType() == 5) {
					karateBtn.beetleTypeCount++;
				}

				if (GameScreen.levelNumb > 24) {
					beetlesToAdd--;
				}
			}
		}

		GameScreen.isFirstRun = true;
		GameScreen.isLevelEnd = false;
		this.game.gameScreen.isEndLevel = false;
	}

	private void ExplodeScheduledBeetles() {

		this.scheduledExplosionsSize = scheduledExplosions.size();

		if (this.scheduledExplosionsSize > 0) {
			GameScreen.reloadTimer += 0.5f;
		}

		while (this.scheduledExplosionsSize > 0) {
			beetle = eggsVertex.get(scheduledExplosions.get(0));

			v.x = beetle.getCenter().x;
			v.y = beetle.getCenter().y;
			localPoint = GameScreen.objectToDestroy.parentToLocalCoordinates(v);			
			
			// Remove Scheduled explosion
			scheduledExplosions.remove(0);
			this.scheduledExplosionsSize--;

			beetle.explode(localPoint.x, GameScreen.objectToDestroy.getHeight()
					- localPoint.y);
		}

	}

	public void checkConnections(AbstractBeetle egg, String name) {
		this.eggsVertex.put(egg.getName(), egg);

		Iterator<Entry<String, AbstractBeetle>> it1 = this.eggsVertex.entrySet().iterator();

		while (it1.hasNext()) {
			Entry<String, AbstractBeetle> pairs = it1.next();

			Vector2 v = new Vector2(pairs.getValue().getCenter().x
					- egg.getCenter().x, pairs.getValue().getCenter().y
					- egg.getCenter().y);

			uniqueKey = false;

			if (isBeetlesInRage()) {
				explRadius = (int) ((Constants.EXPLOSION_CHAIN_LENGTH_BIG + GdxGame.mInventory
						.getBonusRadius()) * GdxGame.HD_X_RATIO);
			} else {
				explRadius = (int) ((Constants.EXPLOSION_CHAIN_LENGTH_SMALL + GdxGame.mInventory
						.getBonusRadius()) * GdxGame.HD_X_RATIO);
			}

			// if (v.len() != 0) {
			// Gdx.app.log("v.len() = ", v.len() + "  explRadius = " +
			// explRadius);
			// }

			if (pairs.getValue().getType() == egg.getType()
					&& v.len() < explRadius
					&& !visitedVertices.contains(pairs.getKey())) {
				uniqueKey = isValidRib(pairs.getKey(), egg.getName());
			}

			if (uniqueKey) {
				GdxGame.tutorialObject.initTutorial(
						Texts.TUTORIAL_MESSAGE_BOMB_CONNECTION,
						"bomb_connection", 240, 250, 420, ArrowPosEnum.TOP,
						0.5f, GameScreen.rootGroup);

				float x1 = pairs.getValue().getCenter().x;
				float y1 = pairs.getValue().getCenter().y;
				float x2 = egg.getCenter().x;
				float y2 = egg.getCenter().y;

				UUID idOne = UUID.randomUUID();
				this.eggsRibs.add(new GraphRib("stretch" + idOne, (pairs
						.getKey()), egg.getName()));

				GameObject s = null;

				if (egg.getType() == 11) {
					s = new ArmStretchMovable("stretch" + idOne, "connector");
				} else {
					s = new ArmStretch("stretch" + idOne, "connector");
				}

				s.setX(x1);
				s.setY(y1);
				s.setWidth(x2);
				s.setHeight(y2);
				game.gameScreen.addToRootGroup(s);

				GameObject g = null;
				if (egg.getType() == 11) {
					g = new ArmBlickMovable("stretch" + idOne + "spark",
							"spark", false);
				} else {
					g = new ArmBlick("stretch" + idOne + "spark", "spark",
							false);
				}

				g.setPositionXYLiterally(x2 - 10 * GdxGame.HD_X_RATIO, y2 - 10
						* GdxGame.HD_Y_RATIO);
				g.setSkin();
				g.setOriginX(10);
				g.setOriginY(10);
				g.addAction(forever(sequence(
						moveTo(x1 - 10 * GdxGame.HD_X_RATIO, y1 - 10
								* GdxGame.HD_Y_RATIO, 1.0f),
						moveTo(x2 - 10 * GdxGame.HD_X_RATIO, y2 - 10
								* GdxGame.HD_Y_RATIO, 1.0f))));
				g.addAction(forever(sequence(rotateTo(359, 1.0f),
						rotateTo(0, 0))));

				game.gameScreen.addToRootGroup(g);
			}

		}
		visitedVertices.clear();
	}

	public boolean isValidRib(String vertex1, String vertex2) {
		if (vertex1.equals(vertex2)) {
			return false;
		} else {
			for (int i = 0; i < this.getNodes(vertex1).size(); i++) // Вызываем
																	// этот
																	// же
																	// метод
																	// для
																	// каждой
																	// смежной
																	// вершины
			{
				if (!visitedVertices.contains(this.getNodes(vertex1).get(i))) // Проверяем,
																				// вызывали
																				// ли
																				// мы
																				// этот
																				// метод
																				// для
																				// вершины
																				// node.getNode().get(i)
				{
					visitedVertices.add(this.getNodes(vertex1).get(i));
					isValidRib(this.getNodes(vertex1).get(i), vertex2);
				}
			}
		}

		return true;
	}

	public ArrayList<AbstractBeetle> getArmedBeetles() {
		ArrayList<AbstractBeetle> eggList = new ArrayList<AbstractBeetle>(
				eggsVertex.values());
		return eggList;
	}

	private void showBeetleTutorial(final String bName, final int bTypePage) {
		if (GdxGame.tutorialObject.visible
				|| !this.game.getScreen().equals(this.game.gameScreen)) {
			TimerTask tS = new TimerTask() {
				@Override
				public void run() {
					showBeetleTutorial(bName, bTypePage);
				}
			};

			bManagerTimer.schedule(tS, 500);
			return;
		}

		GdxGame.files.setOptionsAndProgress("tutorial", bName, "yes");

		switch (bTypePage) {
		case 2:
			game.fanTutorial = true;
			break;
		case 3:
			game.minerTutorial = true;
			break;
		case 4:
			game.boomTutorial = true;
			break;
		case 5:
			game.karateTutorial = true;
			break;

		default:
			break;
		}

		game.tutorialScreen.setPage(bTypePage);
		game.setScreen(game.tutorialScreen);
	}

	public void setActiveBeetle(AbstractBeetle beetle) {
		this.aciveBeetle = beetle;

		if (this.aciveBeetle != null && this.aciveBeetle.getType() == 2
				&& !game.minerTutorial) {
			this.showBeetleTutorial("miner", 3);
		}

		else if (this.aciveBeetle != null && this.aciveBeetle.getType() == 3
				&& !game.fanTutorial) {
			this.showBeetleTutorial("fan", 2);
		}

		else if (this.aciveBeetle != null && this.aciveBeetle.getType() == 4
				&& !game.boomTutorial) {
			this.showBeetleTutorial("boom", 4);
		}

		else if (this.aciveBeetle != null && this.aciveBeetle.getType() == 5
				&& !game.karateTutorial) {
			this.showBeetleTutorial("karate", 5);
		}

	}

	public AbstractBeetle getActiveBeetle() {
		return this.aciveBeetle;
	}

	public Integer getActiveBeetleType() {
		return this.aciveBeetle.getType();
	}

	private void cleanExplodeBeetles() {
		// Remove Graph vertex for Armed Beetles
		this.eggVertexIterator = this.eggsVertex.entrySet().iterator();

		while (this.eggVertexIterator.hasNext()) {
			Entry<String, AbstractBeetle> pairs = this.eggVertexIterator.next();

			if (pairs.getValue().getState() == BeetleState.EXPLODE) {
				beetle = pairs.getValue();

				// Remove Ribs and Ribs Animation
				int i = 0;
				while (i < eggsRibs.size()) {
					if (eggsRibs.get(i).getVertex1().equals(beetle.getName())
							|| eggsRibs.get(i).getVertex2().equals(beetle.getName())) {

						GameObject pSt = (GameObject) GameScreen.rootGroup
								.findActor(eggsRibs.get(i).getRibName());
						GameObject gSt = (GameObject) GameScreen.rootGroup
								.findActor(eggsRibs.get(i).getRibName()
										+ "spark");

						if (pSt != null && gSt != null) {
							GameScreen.rootGroup.removeActor(pSt);
							GameScreen.rootGroup.removeActor(gSt);

						}

						eggsRibs.remove(i);
					} else {
						i++;
					}
				}

				this.eggVertexIterator.remove();
			}
		}

	}

	public void addMinnerEgg(String name, float angle) {
		getActiveBeetle().mineCount++;

		ArmedEgg egg = (ArmedEgg) GameScreen.rootGroup.findActor(name);
		egg.setPositionXYLiterally(getActiveBeetle().getX(), getActiveBeetle().getY());

		egg.setOriginXLiterally(egg.getWidth() / 2);
		egg.setOriginYLiterally(egg.getHeight() / 2);

		egg.setRotation(angle);

		egg.setState(BeetleState.ARMED);
		// egg.explode(
		// getActiveBeetle().x - GameScreen.objectToDestroy.getPositionX(),
		// GameScreen.objectToDestroy.getHeight()
		// - (getActiveBeetle().y - GameScreen.objectToDestroy
		// .getPositionY()));

		checkConnections(egg, name);
	}

	public void shotKarateEgg(String name, float angle) {
		// getActiveBeetle().mineCount++;

		ArmedEgg egg = (ArmedEgg) GameScreen.rootGroup.findActor(name);
		egg.setPositionXYLiterally(getActiveBeetle().getX(), getActiveBeetle().getY());

		egg.setOriginXLiterally(egg.getWidth() / 2);
		egg.setOriginYLiterally(egg.getHeight() / 2);

		egg.setRotation(angle);

		egg.setState(BeetleState.ARMED);

		v.x = egg.getCenter().x;
		v.y = egg.getCenter().y;
		localPoint = GameScreen.objectToDestroy.parentToLocalCoordinates(v);			

		egg.explode(localPoint.x, GameScreen.objectToDestroy.getHeight()
				- localPoint.y);

		checkConnections(egg, name);
	}

	// TODO optimised
	public String createMinnerEggs(HashMap<String, Integer> beetleParams) {
		UUID name = UUID.randomUUID();
		ArmedEgg egg = new ArmedEgg("egg" + name, "minner-egg", beetleParams);
		egg.setState(BeetleState.SLEEP);

		egg.isParticlesPaused = true;
		egg.createParticle("explosion" + GdxGame.stageNumber, true);
		egg.resetPontCounter();

		egg.setOnCreateFilter(true);
		game.gameScreen.addToRootGroup(egg);
		return "egg" + name;
	}

	public void shotFanEggs(int count, ArrayList<String> names, float angle) {
		getActiveBeetle().mineCount++;

		for (int i = 0; i < count; i++) {
			ArmedEggMovable egg = (ArmedEggMovable) GameScreen.rootGroup
					.findActor(names.get(i));
			egg.setState(BeetleState.INFLY);

			egg.beetleSpeedX = getActiveBeetle().beetleSpeedX;
			egg.beetleSpeedY = getActiveBeetle().beetleSpeedY;

			egg.setX(getActiveBeetle().getX());
			egg.setY(getActiveBeetle().getY());
			Vector2 v = new Vector2(Math.abs(getActiveBeetle().getX()
					+ egg.beetleSpeedX - getActiveBeetle().getX()),
					Math.abs(getActiveBeetle().getY() + egg.beetleSpeedY
							- getActiveBeetle().getY()));

			egg.setOriginXLiterally(egg.getWidth() / 2);
			egg.setOriginYLiterally(egg.getHeight() / 2);

			egg.setRotation(angle + v.angle() * (-1f) * Math.signum(i - 1));

			egg.beetleSpeedX += 125
					* GdxGame.HD_X_RATIO
					* Math.cos(Math.PI / 180 * angle)
					* (i - (Constants.BASE_EGGS_QUANTITY
							+ GdxGame.mInventory.getBonusEggs() - 1) / 2);
			egg.beetleSpeedY += 125
					* GdxGame.HD_Y_RATIO
					* Math.sin(Math.PI / 180 * angle)
					* (i - (Constants.BASE_EGGS_QUANTITY
							+ GdxGame.mInventory.getBonusEggs() - 1) / 2);

			egg.createParticle("p4", true);
			egg.isParticlesPaused = false;
		}

	}

	public ArrayList<String> createFanEggs(int count,
			HashMap<String, Integer> beetleParams) {
		ArrayList<String> names = new ArrayList<String>(count);

		for (int i = 0; i < count; i++) {
			UUID name = UUID.randomUUID();
			ArmedEggMovable egg = new ArmedEggMovable("egg" + i + name,
					"fan-egg", beetleParams);
			egg.setState(BeetleState.SLEEP);

			egg.isParticlesPaused = true;
			egg.createParticle("explosion" + GdxGame.stageNumber, true);
			egg.setOnCreateFilter(true);
			egg.resetPontCounter();

			game.gameScreen.addToRootGroup(egg);
			names.add("egg" + i + name);
		}
		return names;

	}

	public void think(float delta) {
		GameScreen.objectToDestroy.pixmapHelper.update();

		if (this.scheduleExplosionTimer > 0) {
			this.scheduleExplosionTimer -= delta;

			if (this.scheduleExplosionTimer <= 0) {
				this.scheduleExplosionTimer = 0;
				ExplodeScheduledBeetles();
				cleanExplodeBeetles();
			}
		}
	}

	public void tryActivateRage() {
		if (!regularBtn.inRage && isBeetlesInRage()) {
			GdxGame.mInventory.activeBonusesAdd(AvaliableBonuses.COOCKIE);

			regularBtn.inRage = true;
			minerBtn.inRage = true;
			fanBtn.inRage = true;
			boomBtn.inRage = true;
			karateBtn.inRage = true;

			for (int i = 0; i < beetleList.size(); i++) {
				beetleList.get(i).inRage = true;

				if (beetleList.get(i).getType() == 3
						&& beetleList.get(i).getNames() != null) {
					for (int j = 0; j < beetleList.get(i).getNames().size(); j++) {
						AbstractBeetle egg = (AbstractBeetle) GameScreen.rootGroup
								.findActor(beetleList.get(i).getNames().get(j));
						egg.inRage = true;
					}
				}
			}

			if (GameScreen.levelNumb > 2) {
				GdxGame.tutorialObject.initTutorial(
						Texts.TUTORIAL_MESSAGE_MUSTACHE, "mustache", 240, 400,
						420, ArrowPosEnum.BOTTOM, 0.5f, GameScreen.rootGroup);
			}
		} else if (!isBeetlesInRage()) {
			GdxGame.mInventory.activeBonusesRemove(AvaliableBonuses.COOCKIE);
		}

	}

	public void updateBonuses() {
		for (int i = 0; i < bonusBeetleList.size(); i++) {

			final AbstractBonusBeetle aBeettle = bonusBeetleList.get(i);

			if (GameScreen.levelNumb > 24
					&& aBeettle.getTurnsVisibleBegin() < loadingCounter
					&& aBeettle.getBonusValueCount() > 0 && aBeettle.isVisible()
					&& aBeettle.getActiveState() != BonusBeetleState.INFLY) {
				aBeettle.setBonusValueCount(aBeettle.getBonusValueCount() - 1);
			}

			if ((aBeettle.getTurnsVisibleBegin() == loadingCounter && !aBeettle.isVisible())) {
				if (!this.game.isBonusBeetleTutorialShown
						&& aBeettle.getType() < 97) {
					GdxGame.tutorialObject
							.initTutorial(
									Texts.TUTORIAL_MESSAGE_BONUS_BUG,
									"bonus_bug",
									240,
									(int) (aBeettle.getPositionY() - 175 * GdxGame.HD_Y_RATIO),
									420,
									ArrowPosEnum.TOP,
									(aBeettle.getPositionX()
											+ aBeettle.getWidth() / 2 - 5 * GdxGame.HD_X_RATIO) / 440,
									GameScreen.rootGroup);
				}

				aBeettle.setVisible(true);
				aBeettle.setScaleX(1);
				aBeettle.setScaleY(1);

				if (aBeettle.getType() == 97 || aBeettle.getType() == 98
						|| aBeettle.getType() == 99) {

					aBeettle.setActiveState(BonusBeetleState.ACTIVATING);

					aBeettle.setScaleX(0.5f);
					aBeettle.setScaleY(0.5f);
					aBeettle.getColor().a = 1.0f;

					if (!aBeettle.isStatic()) {
						ScaleToAction starScale1 = scaleTo(1.5f, 1.5f, 0.3f);
						ScaleToAction starScale2 = scaleTo(1f, 1f, 0.2f);
						aBeettle.addAction(sequence(starScale1, starScale2));

						TimerTask tS = new TimerTask() {
							@Override
							public void run() {
								aBeettle.setActiveState(BonusBeetleState.BONUS);
								aBeettle.recalcCoordinates();

								v.x = aBeettle.getCenter().x;
								v.y = aBeettle.getCenter().y;
								localPoint = GameScreen.objectToDestroy.parentToLocalCoordinates(v);			

								GameScreen.objectToDestroy.pixmapHelper
										.breakStarCircle(
												(int) localPoint.x,
												(int) (GameScreen.objectToDestroy
														.getHeight() - localPoint.y));

								game.gameScreen.calcPoints(false);

								if (!aBeettle.isBlickTimerActive) {
									aBeettle.playBlick();
								}
							}
						};

						bManagerTimerTasks.add(tS);
						bManagerTimer.schedule(tS, 500);
					}

					else {
						ScaleToAction starScale1 = scaleTo(1f, 1f, 0.5f);
						aBeettle.addAction(starScale1);

						TimerTask tS = new TimerTask() {
							@Override
							public void run() {
								aBeettle.setActiveState(BonusBeetleState.BONUS);

								RotateToAction starRotate1 = rotateTo(5f, 1f);
								RotateToAction starRotate2 = rotateTo(-5f, 1f);
								ScaleToAction starScale2 = scaleTo(1.1f, 1.1f, 0.5f);
								ScaleToAction starScale3 = scaleTo(0.9f, 0.9f, 0.5f);

								aBeettle.addAction(forever(sequence(starRotate1, starRotate2)));
								aBeettle.addAction(forever(sequence(starScale2, starScale3)));

								if (!aBeettle.isBlickTimerActive) {
									aBeettle.playBlick();
								}
							}
						};

						bManagerTimerTasks.add(tS);
						bManagerTimer.schedule(tS, 500);
					}
				}
			}

			if ((isTimeToHideBonusOld(aBeettle) || isTimeToHideBonusNew(aBeettle))
					&& !isStar(aBeettle)) {
				aBeettle.setVisible(false);
			}

			else if (isStar(aBeettle)
					&& aBeettle.getTurnsVisibleEnd() == loadingCounter
					&& aBeettle.getActiveState() != BonusBeetleState.INFLY) {
				aBeettle.setActiveState(BonusBeetleState.DEACTIVATING);
				aBeettle.clearActions();

				ScaleToAction starScale1 = scaleTo(0.25f, 0.25f, 0.5f);
				AlphaAction starFadeOut = fadeOut(0.5f);
				RotateByAction starRotate1 = rotateBy(180, 0.5f);

				aBeettle.addAction(parallel(starScale1, starFadeOut, starRotate1));

				TimerTask tS = new TimerTask() {
					@Override
					public void run() {
						aBeettle.setVisible(false);
					}
				};

				bManagerTimerTasks.add(tS);
				bManagerTimer.schedule(tS, 500);
			}

			if ((aBeettle.getTurnsVisibleBegin() < loadingCounter)
					&& (aBeettle.getTurnsVisibleEnd() > loadingCounter)
					&& aBeettle.getActiveState() != BonusBeetleState.INFLY
					&& aBeettle.getType() != 97 && aBeettle.getType() != 98
					&& aBeettle.getType() != 99) {
				aBeettle.getColor().a -= 1.0f / (aBeettle.getTurnsVisibleEnd()
						- aBeettle.getTurnsVisibleBegin() + 1);
			}

		}
	}

	private boolean isStar(AbstractBonusBeetle aBeettle) {
		return aBeettle.getType() == 97 || aBeettle.getType() == 98
				|| aBeettle.getType() == 99;
	}

	private boolean isTimeToHideBonusOld(AbstractBonusBeetle aBeettle) {
		return GameScreen.levelNumb <= 24
				&& aBeettle.getTurnsVisibleEnd() == loadingCounter
				&& aBeettle.getActiveState() != BonusBeetleState.INFLY;
	}

	private boolean isTimeToHideBonusNew(AbstractBonusBeetle aBeettle) {
		return GameScreen.levelNumb > 24 && aBeettle.getBonusValueCount() == 0
				&& aBeettle.getActiveState() != BonusBeetleState.INFLY;
	}

	public boolean loadNextBeetle(boolean isFirstRun) {
		updateBonuses();

		if (getBeetlesCount() != 0 && getActiveBeetle() == null) {
			if (regularBtn.beetleTypeCount > 0) {
				onRegularButtonClicked();
				return true;
			} else if (fanBtn.beetleTypeCount > 0) {
				onFanButtonClicked();
				return true;
			} else if (minerBtn.beetleTypeCount > 0) {
				onMinerButtonClicked();
				return true;
			} else if (boomBtn.beetleTypeCount > 0) {
				onBoomButtonClicked();
				return true;
			} else if (karateBtn.beetleTypeCount > 0) {
				onKarateButtonClicked();
				return true;
			}
		}

		if (isFirstRun) {
			if (!GameScreen.isEarlyCompletion) {
				GameScreen.isLevelEnd = true;
				GameScreen.showCompleteTimer = Constants.RELOADING_DELAY;
			}
			GameScreen.isFirstRun = false;
		}
		return false;
	}
}