package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.workshop27.ballistabreaker.GdxGame;

public class ArmStretch extends GameObject {
	// variables
	private final float[] ropeVertices = new float[20];

	private Texture tex;

	private final String skinName;

	private final Color col = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	private final float colFloat = this.col.toFloatBits();

	private float x1;
	private float y1;
	private float x2;
	private float y2;
	private float x3;
	private float y3;
	private float x4;
	private float y4;

	private float cos;
	private float sin;
	private final Vector2 v = new Vector2(0, 0);

	// constructors
	public ArmStretch(String name, String skinName) {
		super(name);

		this.skinName = skinName;
	}

	@Override
	public void clearSkin() {
		this.tex = null;
	}

	public void setSkin(String skinName) {
		if (!this.isVisible()) {
			return;
		}

		this.tex = ImageCache.getManager()
				.get("data/atlas/main/pack", TextureAtlas.class)
				.findRegion(skinName).getTexture();

		if (this.tex == null) {
			return;
		}

		this.tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}

	public void calcRopeVertices() {
		this.v.x = getWidth() - getX();
		this.v.y = getHeight() - getY();

		setRotation(v.angle());
		this.cos = MathUtils.cosDeg(v.angle());
		this.sin = MathUtils.sinDeg(v.angle());

		this.x1 = this.getX() + cos - sin * 8 * GdxGame.HD_X_RATIO;
		this.y1 = this.getY() + sin + cos * 8 * GdxGame.HD_Y_RATIO;

		this.x2 = this.getX() + cos - sin * (-56) * GdxGame.HD_X_RATIO;
		this.y2 = this.getY() + sin + cos * (-56) * GdxGame.HD_Y_RATIO;

		this.x3 = this.getWidth() + cos - sin * 8 * GdxGame.HD_X_RATIO;
		this.y3 = this.getHeight() + sin + cos * 8 * GdxGame.HD_Y_RATIO;

		this.x4 = this.getWidth() + cos - sin * (-56) * GdxGame.HD_X_RATIO;
		this.y4 = this.getHeight() + sin + cos * (-56) * GdxGame.HD_Y_RATIO;

		this.ropeVertices[0] = this.x1;
		this.ropeVertices[1] = this.y1;
		this.ropeVertices[2] = this.colFloat;
		this.ropeVertices[3] = 0;
		this.ropeVertices[4] = 0;

		this.ropeVertices[5] = this.x2;
		this.ropeVertices[6] = this.y2;
		this.ropeVertices[7] = this.colFloat;
		this.ropeVertices[8] = 0;
		this.ropeVertices[9] = 1;

		this.ropeVertices[10] = this.x3;
		this.ropeVertices[11] = this.y3;
		this.ropeVertices[12] = this.colFloat;
		this.ropeVertices[13] = 1;
		this.ropeVertices[14] = 0;

		this.ropeVertices[15] = this.x4;
		this.ropeVertices[16] = this.y4;
		this.ropeVertices[17] = this.colFloat;
		this.ropeVertices[18] = 1;
		this.ropeVertices[19] = 1;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		calcRopeVertices();

		if (this.tex == null) {
			setSkin(this.skinName);
		} else {
			batch.setColor(this.getColor());
			batch.draw(this.tex, this.ropeVertices, 0, 20);
		}
	}
}
