package com.workshop27.ballistabreaker.engine;

import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.ShopScreen;

public class ShopBtn extends GameObject {

	private int mShopId = 0;
	private final GdxGame game;

	public ShopBtn(String name, boolean addToRoot, boolean enableBlending,
			GdxGame game) {
		super(name, addToRoot, enableBlending);

		this.game = game;

		if (name.equals("coockies_container")) {
			mShopId = 1;
		}

		else if (name.equals("sight_container")) {
			mShopId = 2;
		}

		else if (name.equals("karate_container")) {
			mShopId = 3;
		}

		else if (name.equals("boom_container")) {
			mShopId = 4;
		}

		if (name.equals("expl_container")) {
			mShopId = 5;
		}

		if (name.equals("egg1_container")) {
			mShopId = 6;
		}

		if (name.equals("egg2_container")) {
			mShopId = 7;
		}

		if (name.equals("bundle_container")) {
			mShopId = 8;
		}

		else if (name.equals("buy-btn")) {
			mShopId = 98;
		}

		else if (name.equals("no-btn")) {
			mShopId = 99;
		}
	}

	/*@Override
	public boolean touchDown(float arg_x, float arg_y, int arg2) {
		this.game.flurryAgentLogEvent(getName());
		AudioPlayer.playSound("tap3");

		if (mShopId < 98) {
			ShopScreen.mBtnClickedId = mShopId;
			ShopScreen.showYesNoBox(mShopId);
		}

		else if (mShopId == 99) {
			ShopScreen.hideYesNoBox();
		}

		else if (mShopId == 98) {
			ShopScreen.hideYesNoBox();
			this.game.buyItem(ShopScreen.mBtnClickedId);
		}

		return true;

	}*/
}
