package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.PixmapTextureData;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.Logger;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class PixmapHelper implements Disposable {

	public class PixmapChange {

		int x, y;
		int width;

		void set(int x, int y, int width) {
			this.x = x;
			this.y = y;
			this.width = width;
		}
	}

	private final Logger log = new Logger("PixmapHelper");

	public Pixmap pixmap;
	public Sprite sprite;
	public Texture texture;

	private Pixmap explosionStarMask;
	private Pixmap explosionMask;
	private Pixmap explosionMaskBig;

	private byte[][] explosionStarAlphaMap;
	private byte[][] explosionAlphaMap;
	private byte[][] explosionBigAlphaMap;
	public static byte[][] imageAMap;

	private final Color color1 = new Color();

	public final Color color = new Color();

	// only allow 20 modifications
	public final PixmapChange[] modifications = new PixmapChange[20];
	public final PixmapChange[] starModifications = new PixmapChange[5];
	public int lastModification = 0;
	public int lastStarModification = 0;

	public static float pixelsDestroyed;

	private final Pixmap renderPixmap32;
	private final Pixmap renderPixmap64;
	private final Pixmap renderPixmap128;
	private final Pixmap renderPixmap256;
	private final Pixmap renderPixmap512;

	int objToDestroyW;
	int objToDestroyH;
	private int curentMask;

	public PixmapHelper(Pixmap pixmap, Sprite sprite, Texture texture) {
		pixelsDestroyed = 0;
		this.pixmap = pixmap;
		this.sprite = sprite;
		this.texture = texture;

		for (int i = 0; i < modifications.length; i++) {
			modifications[i] = new PixmapChange();
		}

		for (int i = 0; i < starModifications.length; i++) {
			starModifications[i] = new PixmapChange();
		}

		this.renderPixmap32 = new Pixmap(32, 32, Format.RGBA8888);
		this.renderPixmap64 = new Pixmap(64, 64, Format.RGBA8888);
		this.renderPixmap128 = new Pixmap(128, 128, Format.RGBA8888);
		this.renderPixmap256 = new Pixmap(256, 256, Format.RGBA8888);
		this.renderPixmap512 = new Pixmap(512, 512, Format.RGBA8888);

	}

	public int getCurentMask() {
		return curentMask;
	}

	public void setCurentMask(int curentMask) {
		this.curentMask = curentMask;
	}

	private void initExplosionMask() {
		setCurentMask(GdxGame.stageNumber);

		// -------------------------------------------
		this.explosionStarMask = new Pixmap(
				Gdx.files.internal("data/atlas/star-explosion"
						+ GdxGame.stageNumber + "/star-crack.png"));
		int explStarW = explosionStarMask.getWidth();
		int explStarH = explosionStarMask.getHeight();
		explosionStarAlphaMap = new byte[explStarW][explStarH];

		for (int a = 0; a < explStarW; a++) {
			for (int b = 0; b < explStarH; b++) {
				Color.rgba8888ToColor(this.color1,
						explosionStarMask.getPixel(a, b));

				if (this.color1.r >= 0.01f && this.color1.g >= 0.01f
						&& this.color1.b >= 0.01f && this.color1.a >= 0.95f) {
					explosionStarAlphaMap[a][b] = 2;
				}

				else if (this.color1.r >= 0.01f && this.color1.g >= 0.01f
						&& this.color1.b >= 0.01f && this.color1.a < 0.95f
						&& this.color1.a >= 0.01f) {
					explosionStarAlphaMap[a][b] = 3;
				}

				else if (this.color1.a >= 0.01f) {
					explosionStarAlphaMap[a][b] = 1;
				}
			}
		}

		// -------------------------------------------------------------------
		this.explosionMask = new Pixmap(
				Gdx.files.internal("data/atlas/explosion" + GdxGame.stageNumber
						+ "/boom0.png"));
		int explW = explosionMask.getWidth();
		int explH = explosionMask.getHeight();
		explosionAlphaMap = new byte[explW][explH];

		for (int a = 0; a < explW; a++) {
			for (int b = 0; b < explH; b++) {
				Color.rgba8888ToColor(this.color1, explosionMask.getPixel(a, b));

				if ((this.color1.r >= 0.01f || this.color1.g >= 0.01f || this.color1.b >= 0.01f)
						&& this.color1.a >= 0.95f) {
					explosionAlphaMap[a][b] = 2;
				}

				else if ((this.color1.r >= 0.01f || this.color1.g >= 0.01f || this.color1.b >= 0.01f)
						&& this.color1.a < 0.95f && this.color1.a >= 0.01f) {
					explosionAlphaMap[a][b] = 3;
				}

				else if (this.color1.a >= 0.1f) {
					explosionAlphaMap[a][b] = 1;
				}
			}
		}

		// --------------------------------------------------------------------------
		this.explosionMaskBig = new Pixmap(
				Gdx.files.internal("data/atlas/explosion" + GdxGame.stageNumber
						+ "/boom1.png"));
		explW = explosionMaskBig.getWidth();
		explH = explosionMaskBig.getHeight();
		explosionBigAlphaMap = new byte[explW][explH];

		for (int a = 0; a < explW; a++) {
			for (int b = 0; b < explH; b++) {
				Color.rgba8888ToColor(this.color1,
						explosionMaskBig.getPixel(a, b));

				if ((this.color1.r >= 0.01f || this.color1.g >= 0.01f || this.color1.b >= 0.01f)
						&& this.color1.a >= 0.95f) {
					explosionBigAlphaMap[a][b] = 2;
				}

				else if ((this.color1.r >= 0.01f || this.color1.g >= 0.01f || this.color1.b >= 0.01f)
						&& this.color1.a < 0.95f && this.color1.a >= 0.01f) {
					explosionBigAlphaMap[a][b] = 3;
				}

				else if (this.color1.a >= 0.1f) {
					explosionBigAlphaMap[a][b] = 1;
				}
			}
		}

	}

	public PixmapHelper(float w, float h, Pixmap pixmap) {
		initExplosionMask();

		this.objToDestroyW = (int) w;
		this.objToDestroyH = (int) h;

		this.pixmap = new Pixmap(pixmap.getWidth(), pixmap.getHeight(),
				Format.RGBA8888);
		this.pixmap.drawPixmap(pixmap, 0, 0);

		for (int i = 0; i < modifications.length; i++) {
			modifications[i] = new PixmapChange();
		}

		for (int i = 0; i < starModifications.length; i++) {
			starModifications[i] = new PixmapChange();
		}

		this.renderPixmap32 = new Pixmap(32, 32, Format.RGBA8888);
		this.renderPixmap64 = new Pixmap(64, 64, Format.RGBA8888);
		this.renderPixmap128 = new Pixmap(128, 128, Format.RGBA8888);
		this.renderPixmap256 = new Pixmap(256, 256, Format.RGBA8888);
		this.renderPixmap512 = new Pixmap(512, 512, Format.RGBA8888);
	}

	public void initTexture() {
		this.texture = new Texture(new PixmapTextureData(pixmap,
				Format.RGBA8888, false, false));
		// this.texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		this.sprite = new Sprite(texture);
	}

	public void restartOnEnter(Pixmap pixmap, float w, float h) {
		pixelsDestroyed = 0;

		this.objToDestroyW = (int) w;
		this.objToDestroyH = (int) h;

		this.pixmap = new Pixmap(pixmap.getWidth(), pixmap.getHeight(),
				Format.RGBA8888);
		this.pixmap.drawPixmap(pixmap, 0, 0);

		for (int i = 0; i < modifications.length; i++) {
			modifications[i] = new PixmapChange();
		}

		for (int i = 0; i < starModifications.length; i++) {
			starModifications[i] = new PixmapChange();
		}
	}

	public void restart(Pixmap pixmap) {
		pixelsDestroyed = 0;

		/*
		 * try { this.pixmap.dispose(); } catch (final Exception ex) {
		 * Log.e("dispose error", "pixmap dispose failed"); }
		 * 
		 * try { this.texture.dispose(); } catch (final Exception ex) {
		 * Log.e("dispose error", "texture dispose failed"); }
		 */

		// this.pixmap = new Pixmap(pixmap.getWidth(), pixmap.getHeight(),
		// Format.RGBA8888);
		this.pixmap.drawPixmap(pixmap, 0, 0);
		// this.texture = new Texture(new PixmapTextureData(pixmap,
		// Format.RGBA8888, false, false));
		this.texture.draw(this.pixmap, 0, 0);
		// this.texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		// this.sprite = new Sprite(texture);

		for (int i = 0; i < modifications.length; i++) {
			modifications[i] = new PixmapChange();
		}

		for (int i = 0; i < starModifications.length; i++) {
			starModifications[i] = new PixmapChange();
		}
	}

	/**
	 * Projects the coordinates (x, y) to the Pixmap coordinates system and
	 * store the result in the specified Vector2.
	 * 
	 * @param position
	 *            The Vector2 to store the transformed coordinates.
	 * @param x
	 *            The x coordinate to be projected.
	 * @param y
	 *            The y coordinate to be prjected
	 */
	public void project(Vector2 position, float x, float y) {
		position.set(x, y);

		float centerX = sprite.getX() + sprite.getOriginX();
		float centerY = sprite.getY() + sprite.getOriginY();

		position.add(-centerX, -centerY);

		position.rotate(-sprite.getRotation());

		float scaleX = pixmap.getWidth() / sprite.getWidth();
		float scaleY = pixmap.getHeight() / sprite.getHeight();

		position.x *= scaleX;
		position.y *= scaleY;

		position.add( //
				pixmap.getWidth() * 0.5f, //
				-pixmap.getHeight() * 0.5f //
		);

		position.y *= -1f;
	}

	public int getPixel(Vector2 position) {
		return getPixel(position.x, position.y);
	}

	public int getPixel(float x, float y) {
		return pixmap.getPixel((int) x, (int) y);
	}

	public void setPixel(int x, int y, int value) {
		pixmap.drawPixel(x, y, value);
	}

	public void eraseCircle(float x, float y, float radius) {
		if (lastModification == modifications.length) {
			return;
		}

		float scaleX = pixmap.getWidth() / sprite.getWidth();

		int newRadius = Math.round(radius * scaleX);

		if (x + newRadius < 0 || y + newRadius < 0) {
			return;
		}

		if (x - newRadius > pixmap.getWidth()
				|| y - newRadius > pixmap.getHeight()) {
			return;
		}

		pixmap.setColor(1f, 1f, 1f, 1f);

		int newX = Math.round(x);
		int newY = Math.round(y);

		pixmap.fillCircle(newX, newY, newRadius);

		modifications[lastModification++].set(newX, newY, newRadius * 2);
	}

	public void breakStarCircle(final int x, final int y) {

		if (lastStarModification == starModifications.length) {
			return;
		}
		int explStarW = explosionStarMask.getWidth();
		int explStarH = explosionStarMask.getHeight();

		int explosionX = 0;
		int explosionY = 0;

		pixmap.setColor(0f, 0f, 0f, 0f);

		int halfExplW = explStarW / 2;
		int halfExplH = explStarH / 2;

		for (int a = x - halfExplW; a < x + halfExplW; a++) {
			for (int b = y - halfExplH; b < y + halfExplH; b++) {
				if (a >= 0 && b >= 0 && a < objToDestroyW && b < objToDestroyH) {
					explosionX = a - x + halfExplW;
					explosionY = b - y + halfExplH;

					// Set empty pixels
					if (explosionStarAlphaMap[explosionX][explosionY] == 1
							&& PixmapHelper.imageAMap[a][b] >= 1) {
						PixmapHelper.imageAMap[a][b] = 0;
						pixelsDestroyed++;

						pixmap.drawPixel(a, b);
					}

					// Set crack image pixels (white)
					if (explosionStarAlphaMap[explosionX][explosionY] == 2
							&& PixmapHelper.imageAMap[a][b] == 1) {
						pixmap.drawPixmap(explosionStarMask, a, b, explosionX,
								explosionY, 1, 1);
					}

					// Set transparent fire pixels (slows down perfomance!!!)
					if (GdxGame.stageNumber != 2
							&& explosionStarAlphaMap[explosionX][explosionY] == 3
							&& PixmapHelper.imageAMap[a][b] == 1) {
						Pixmap.setBlending(Blending.SourceOver);
						pixmap.drawPixmap(explosionStarMask, a, b, explosionX,
								explosionY, 1, 1);
						Pixmap.setBlending(Blending.None);
					}
					if (GdxGame.stageNumber == 2
							&& explosionStarAlphaMap[explosionX][explosionY] == 3
							&& PixmapHelper.imageAMap[a][b] >= 1) {
						Pixmap.setBlending(Blending.SourceOver);
						pixmap.drawPixmap(explosionStarMask, a, b, explosionX,
								explosionY, 1, 1);
						Pixmap.setBlending(Blending.None);
					}
				}
			}
		}

		starModifications[lastStarModification++].set((x), (y), explStarW);
	}

	public synchronized void breakCircle(final int x, final int y) {

		if (lastModification == modifications.length) {
			return;
		}
		int explW = explosionMask.getWidth();
		int explH = explosionMask.getHeight();

		int explosionX = 0;
		int explosionY = 0;

		pixmap.setColor(0f, 0f, 0f, 0f);

		int halfExplW = explW / 2;
		int halfExplH = explH / 2;

		for (int a = x - halfExplW; a < x + halfExplW; a++) {
			for (int b = y - halfExplH; b < y + halfExplH; b++) {
				if (a >= 0 && b >= 0 && a < objToDestroyW && b < objToDestroyH) {
					explosionX = a - x + halfExplW;
					explosionY = b - y + halfExplH;

					// Set empty pixels
					if (explosionAlphaMap[explosionX][explosionY] == 1
							&& PixmapHelper.imageAMap[a][b] >= 1) {
						PixmapHelper.imageAMap[a][b] = 0;
						pixelsDestroyed++;

						pixmap.drawPixel(a, b);
					}

					// Set crack image pixels (white)

					if (explosionAlphaMap[explosionX][explosionY] == 2
							&& PixmapHelper.imageAMap[a][b] == 1) {
						pixmap.drawPixmap(explosionMask, a, b, explosionX,
								explosionY, 1, 1);
					}

					// Set transparent fire pixels (slows down perfomance!!!)
					if (GdxGame.stageNumber != 2
							&& explosionAlphaMap[explosionX][explosionY] == 3
							&& PixmapHelper.imageAMap[a][b] == 1) {
						Pixmap.setBlending(Blending.SourceOver);
						pixmap.drawPixmap(explosionMask, a, b, explosionX,
								explosionY, 1, 1);
						Pixmap.setBlending(Blending.None);
					}
					if (GdxGame.stageNumber == 2
							&& explosionAlphaMap[explosionX][explosionY] == 3
							&& PixmapHelper.imageAMap[a][b] >= 1) {
						Pixmap.setBlending(Blending.SourceOver);
						pixmap.drawPixmap(explosionMask, a, b, explosionX,
								explosionY, 1, 1);
						Pixmap.setBlending(Blending.None);
					}

				}
			}
		}

		modifications[lastModification++].set((x), (y), explW);
	}

	public synchronized void breakBigCircle(final int x, final int y) {
		if (lastModification == modifications.length) {
			return;
		}
		int explW = explosionMaskBig.getWidth();
		int explH = explosionMaskBig.getHeight();

		int explosionX = 0;
		int explosionY = 0;

		pixmap.setColor(0f, 0f, 0f, 0f);

		int halfExplW = explW / 2;
		int halfExplH = explH / 2;

		for (int a = x - halfExplW; a < x + halfExplW; a++) {
			for (int b = y - halfExplH; b < y + halfExplH; b++) {
				if (a >= 0 && b >= 0 && a < objToDestroyW && b < objToDestroyH) {
					explosionX = a - x + halfExplW;
					explosionY = b - y + halfExplH;

					// Set empty pixels
					if (explosionBigAlphaMap[explosionX][explosionY] == 1
							&& PixmapHelper.imageAMap[a][b] >= 1) {
						PixmapHelper.imageAMap[a][b] = 0;
						pixelsDestroyed++;

						pixmap.drawPixel(a, b);
					}

					// Set crack image pixels (white)
					if (explosionBigAlphaMap[explosionX][explosionY] == 2
							&& PixmapHelper.imageAMap[a][b] == 1) {
						pixmap.drawPixmap(explosionMaskBig, a, b, explosionX,
								explosionY, 1, 1);
					}

					// Set transparent fire pixels (slows down perfomance!!!)
					if (GdxGame.stageNumber != 2
							&& explosionBigAlphaMap[explosionX][explosionY] == 3
							&& PixmapHelper.imageAMap[a][b] == 1) {
						Pixmap.setBlending(Blending.SourceOver);
						pixmap.drawPixmap(explosionMaskBig, a, b, explosionX,
								explosionY, 1, 1);
						Pixmap.setBlending(Blending.None);
					}
					if (GdxGame.stageNumber == 2
							&& explosionBigAlphaMap[explosionX][explosionY] == 3
							&& PixmapHelper.imageAMap[a][b] >= 1) {
						Pixmap.setBlending(Blending.SourceOver);
						pixmap.drawPixmap(explosionMaskBig, a, b, explosionX,
								explosionY, 1, 1);
						Pixmap.setBlending(Blending.None);
					}
				}
			}
		}

		modifications[lastModification++].set((x), (y), explW);
	}

	private Pixmap getPixmapForRadius(int width) {
		if (width <= 32) {
			return renderPixmap32;
		}
		if (width <= 64) {
			return renderPixmap64;
		}
		if (width <= 128) {
			return renderPixmap128;
		}
		if (width <= 256) {
			return renderPixmap256;
		}

		return renderPixmap512;
	}

	/**
	 * Updates the opengl texture with all the pixmap modifications.
	 */
	public void update() {
		if (lastModification == 0 && lastStarModification == 0) {
			return;
		}

		Gdx.gl.glBindTexture(GL10.GL_TEXTURE_2D,
				this.texture.getTextureObjectHandle());

		int width = pixmap.getWidth();
		int height = pixmap.getHeight();

		if (lastModification != 0) {
			for (int i = 0; i < lastModification; i++) {

				PixmapChange pixmapChange = modifications[i];

				Pixmap renderPixmap = getPixmapForRadius(pixmapChange.width);

				int dstWidth = renderPixmap.getWidth();
				int dstHeight = renderPixmap.getHeight();

				int x = Math.round(pixmapChange.x) - dstWidth / 2;
				int y = Math.round(pixmapChange.y) - dstHeight / 2;

				if (x + dstWidth > width) {
					x = width - dstWidth;
				} else if (x < 0) {
					x = 0;
				}

				if (y + dstHeight > height) {
					y = height - dstHeight;
				} else if (y < 0) {
					y = 0;
				}

				renderPixmap
						.drawPixmap(pixmap, 0, 0, x, y, dstWidth, dstHeight);

				Gdx.gl.glTexSubImage2D(GL10.GL_TEXTURE_2D, 0, x, y, dstWidth,
						dstHeight, GL10.GL_RGBA, renderPixmap.getGLType(),
						renderPixmap.getPixels());
			}

			if (GameScreen.vibro.equals("on")) {
				Gdx.input.vibrate(25);
			}
			GameScreen.mShakeCounter = 35;
			GameScreen.explosionShake();

			lastModification = 0;
		}

		// ------------------------------------------------------------------------
		else if (lastStarModification != 0) {
			for (int i = 0; i < lastStarModification; i++) {
				PixmapChange pixmapChange = starModifications[i];

				Pixmap renderPixmap = getPixmapForRadius(pixmapChange.width);

				int dstWidth = renderPixmap.getWidth();
				int dstHeight = renderPixmap.getHeight();

				int x = Math.round(pixmapChange.x) - dstWidth / 2;
				int y = Math.round(pixmapChange.y) - dstHeight / 2;

				if (x + dstWidth > width) {
					x = width - dstWidth;
				} else if (x < 0) {
					x = 0;
				}

				if (y + dstHeight > height) {
					y = height - dstHeight;
				} else if (y < 0) {
					y = 0;
				}

				renderPixmap
						.drawPixmap(pixmap, 0, 0, x, y, dstWidth, dstHeight);

				Gdx.gl.glTexSubImage2D(GL10.GL_TEXTURE_2D, 0, x, y, dstWidth,
						dstHeight, GL10.GL_RGBA, renderPixmap.getGLType(),
						renderPixmap.getPixels());
			}

			lastStarModification = 0;
		}
		// ------------------------------------------------------------------------

		GameScreen.bManager.pixelsLabelValue = pixelsDestroyed;
		GameScreen.bManager.pixelsLabelValue = GameScreen.bManager.pixelsLabelValue
				/ GameScreen.objectToDestroy.pixelsToDestroy * 100;

		GameScreen.jar.setProgress(GameScreen.bManager.pixelsLabelValue);
	}

	/**
	 * Reload all the pixmap data to the opengl texture, to be used after the
	 * game was resumed.
	 */
	public void reload() {
		this.texture = new Texture(new PixmapTextureData(pixmap,
				Format.RGBA8888, false, false));
		// this.texture.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}

	public void clearTexture() {
		try {
			this.pixmap.dispose();
		} catch (final Exception ex) {
			log.error("pixmap dispose failed");
		}

		try {
			this.texture.dispose();
		} catch (final Exception ex) {
			log.error("texture dispose failed");
		}
	}

	@Override
	public void dispose() {
		try {
			this.explosionMask.dispose();
		} catch (final Exception ex) {
			log.error("explosionMask dispose failed");
		}

		try {
			this.explosionMaskBig.dispose();
		} catch (final Exception ex) {
			log.error("explosionMaskBig dispose failed");
		}

		try {
			this.explosionStarMask.dispose();
		} catch (final Exception ex) {
			log.error("explosionStarMask dispose failed");
		}

		try {
			this.pixmap.dispose();
		} catch (final Exception ex) {
			log.error("pixmap dispose failed");
		}

		try {
			this.texture.dispose();
		} catch (final Exception ex) {
			log.error("texture dispose failed");
		}

		try {
			this.renderPixmap32.dispose();
		} catch (final Exception ex) {
			log.error("renderPixmap32 dispose failed");
		}

		try {
			this.renderPixmap64.dispose();
		} catch (final Exception ex) {
			log.error("renderPixmap64 dispose failed");
		}

		try {
			this.renderPixmap128.dispose();
		} catch (final Exception ex) {
			log.error("renderPixmap128 dispose failed");
		}

		try {
			this.renderPixmap256.dispose();
		} catch (final Exception ex) {
			log.error("renderPixmap256 dispose failed");
		}

		try {
			this.renderPixmap512.dispose();
		} catch (final Exception ex) {
			log.error("renderPixmap512 dispose failed");
		}

	}
}
