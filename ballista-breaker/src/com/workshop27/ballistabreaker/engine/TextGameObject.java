package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.BitmapFont.TextBounds;
import com.badlogic.gdx.graphics.g2d.BitmapFontCache;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.workshop27.ballistabreaker.GdxGame;

public class TextGameObject extends GameObject {
	// Variables
	protected final BitmapFontCache textCache;
	protected String text = "";

	protected static BitmapFont fontTutorial30;
	protected static BitmapFont fontTutorial30WhiteBold;
	protected static BitmapFont fontTutorial45;

	protected static BitmapFont fontPopupMessage20;

	protected static BitmapFont font45;
	protected static BitmapFont font60;
	protected static BitmapFont font30;
	protected static BitmapFont font22;

	protected TextBounds textBounds;

	protected static TextureRegion bitmapFontTextureRegionPopupMessage20;

	protected static TextureRegion bitmapFontTextureRegionTutorial30;
	protected static TextureRegion bitmapFontTextureRegionTutorial30WhiteBold;
	protected static TextureRegion bitmapFontTextureRegionTutorial45;

	protected static TextureRegion bitmapFontTextureRegion22;
	protected static TextureRegion bitmapFontTextureRegion30;
	protected static TextureRegion bitmapFontTextureRegion45;
	protected static TextureRegion bitmapFontTextureRegion60;

	protected int activeFontSize = 0;

	protected int wrapWidth = GdxGame.VIRTUAL_GAME_WIDTH;

	protected String fontType = "";

	public TextGameObject(String name, boolean addToRoot, String fontType,
			int fontSize) {
		super(name, addToRoot);

		this.activeFontSize = fontSize;
		this.fontType = fontType;

		if (fontSize == 30 && fontType.equals("tutorial")) {
			this.textCache = new BitmapFontCache(fontTutorial30);
		}

		else if (fontSize == 30 && fontType.equals("tutorial_white_bold")) {
			this.textCache = new BitmapFontCache(fontTutorial30WhiteBold);
		}

		else if (fontSize == 45 && fontType.equals("tutorial")) {
			this.textCache = new BitmapFontCache(fontTutorial45);
		}

		else if (fontSize == 20 && fontType.equals("popup")) {
			this.textCache = new BitmapFontCache(fontPopupMessage20);
		}

		else {
			this.textCache = null;
		}

		this.skinName = "";
	}

	public TextGameObject(String name, boolean addToRoot, int fontSize) {
		super(name, addToRoot);

		this.activeFontSize = fontSize;

		if (fontSize == 45) {
			this.textCache = new BitmapFontCache(font45);
		}

		else if (fontSize == 60) {
			this.textCache = new BitmapFontCache(font60);
		}

		else if (fontSize == 22) {
			this.textCache = new BitmapFontCache(font22);
		}

		else {
			this.textCache = new BitmapFontCache(font30);
		}

		this.skinName = "";
	}

	public static void initFonts() {
		bitmapFontTextureRegionTutorial30 = new TextureRegion(new Texture(
				Gdx.files.internal("data/fonts/Cinnamon30.png")));
		fontTutorial30 = new BitmapFont(
				Gdx.files.internal("data/fonts/Cinnamon30.fnt"),
				bitmapFontTextureRegionTutorial30, false);

		bitmapFontTextureRegionTutorial30WhiteBold = new TextureRegion(
				new Texture(Gdx.files
						.internal("data/fonts/Cinnamon30WhiteBold.png")));
		fontTutorial30WhiteBold = new BitmapFont(
				Gdx.files.internal("data/fonts/Cinnamon30WhiteBold.fnt"),
				bitmapFontTextureRegionTutorial30WhiteBold, false);

		bitmapFontTextureRegionTutorial45 = new TextureRegion(new Texture(
				Gdx.files.internal("data/fonts/Cinnamon45.png")));
		fontTutorial45 = new BitmapFont(
				Gdx.files.internal("data/fonts/Cinnamon45.fnt"),
				bitmapFontTextureRegionTutorial45, false);

		bitmapFontTextureRegionPopupMessage20 = new TextureRegion(new Texture(
				Gdx.files.internal("data/fonts/Cinnamon20White.png")));
		fontPopupMessage20 = new BitmapFont(
				Gdx.files.internal("data/fonts/Cinnamon20White.fnt"),
				bitmapFontTextureRegionPopupMessage20, false);

		bitmapFontTextureRegion45 = new TextureRegion(new Texture(
				Gdx.files.internal("data/fonts/Fridge45.png")));
		font45 = new BitmapFont(Gdx.files.internal("data/fonts/Fridge45.fnt"),
				bitmapFontTextureRegion45, false);

		bitmapFontTextureRegion60 = new TextureRegion(new Texture(
				Gdx.files.internal("data/fonts/Fridge60.png")));
		font60 = new BitmapFont(Gdx.files.internal("data/fonts/Fridge60.fnt"),
				bitmapFontTextureRegion60, false);

		bitmapFontTextureRegion22 = new TextureRegion(new Texture(
				Gdx.files.internal("data/fonts/Fridge22.png")));
		font22 = new BitmapFont(Gdx.files.internal("data/fonts/Fridge22.fnt"),
				bitmapFontTextureRegion22, false);

		bitmapFontTextureRegion30 = new TextureRegion(new Texture(
				Gdx.files.internal("data/fonts/Fridge30.png")));
		font30 = new BitmapFont(Gdx.files.internal("data/fonts/Fridge30.fnt"),
				bitmapFontTextureRegion30, false);
	}

	public TextBounds getTextBounds() {
		return textBounds;
	}

	// Methods

	// Returns center X of the text
	@Override
	public float getPositionX() {
		return this.getX();
	}

	// Returns center Y of the text
	@Override
	public float getPositionY() {
		return this.getY();
	}

	// Returns left X of the text
	public float getTextCachePositionX() {
		return this.getX() - this.textBounds.width / 2;
	}

	// Returns upper Y of the text
	public float getTextCachePositionY() {
		return this.getY() + this.textBounds.height / 2;
	}

	public void setText(String s) {
		if (s == null) {
			this.text = "0";
		} else {
			this.text = s;
		}

		this.textBounds = this.textCache.setText(text, this.getPositionX(),
				this.getPositionY());
	}

	public void setText(int i) {
		this.text = Integer.toString(i);
		this.textBounds = this.textCache.setText(text, this.getPositionX(),
				this.getPositionY());
	}

	public void setText(float f) {
		this.text = Float.toString(f);
		this.textBounds = this.textCache.setText(text, this.getPositionX(),
				this.getPositionY());
	}

	public void setTextCenteredOnPosition(String s, float x, float y,
			int wrapWidth, boolean respectScreenSize) {
		this.wrapWidth = wrapWidth;

		setTextCenteredOnPosition(s, x, y, respectScreenSize);
	}

	public void setTextCenteredOnPosition(String s, float x, float y,
			boolean respectScreenSize) {
		if (s == null) {
			this.text = "0";
		} else {
			this.text = s;
		}

		if (respectScreenSize) {
			if (x > 360) {
				x = 360;
			} else if (x < 120) {
				x = 120;
			}
			if (y > 750) {
				y = 750;
			}
		}

		this.textBounds = this.textCache.setWrappedText(text, 0, 0,
				this.wrapWidth);
		this.textCache.setPosition(x - this.textBounds.width / 2, y
				+ this.textBounds.height / 2);
		this.setPositionXY(x, y);
	}

	public String getText() {
		return this.text;
	}

	public void clearText() {
		this.text = "";
	}

	public void setColor(float r, float g, float b, float a) {
		this.getColor().r = r;
		this.getColor().g = g;
		this.getColor().b = b;
		this.getColor().a = a;

		this.textCache.setColor(this.getColor().r, this.getColor().g, this.getColor().b, this.getColor().a);
	}

	public void setScale(float x, float y) {
		setScaleX(x);
		setScaleY(y);
	}

	public void setAngle(float angle) {
		setRotation(angle);
	}

	public void setOrigin(float x, float y) {
		setOriginX(x);
		setOriginY(y);
	}

	@Override
	public void setPositionXY(float x, float y) {
		this.setX(x * GdxGame.HD_X_RATIO);
		this.setY(y * GdxGame.HD_Y_RATIO);

		this.textCache.setPosition(x, y);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.text.length() > 0) {
			this.textCache.draw(batch);
		}
	}
}
