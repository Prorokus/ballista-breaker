package com.workshop27.ballistabreaker.engine;

import java.text.DecimalFormat;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.RotateToAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.screens.GameScreen;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class JarObject extends GameObject {
	private final String prog_jam_name;
	private final String prog_ellipse_name;
	private final String prog_cover_name;
	private final String prog_mus_name;
	private final String star1_name;
	private final String star2_name;
	private final String star3_name;

	private Vector2 prog_jamXY;
	private Vector2 prog_ellipseXY;
	private Vector2 prog_coverXY;
	private Vector2 prog_musXY;
	private Vector2 star1XY;
	private Vector2 star2XY;
	private Vector2 star3XY;

	private Vector2 prog_jamWH;
	private Vector2 prog_ellipseWH;
	private Vector2 prog_coverWH;
	private Vector2 prog_musWH;
	private Vector2 star1WH;
	private Vector2 star2WH;
	private Vector2 star3WH;

	private TextureRegion prog_jamRegion;
	private TextureRegion prog_ellipseRegion;
	private TextureRegion prog_coverRegion;
	private TextureRegion prog_musRegion;
	private TextureRegion star1Region;
	private TextureRegion star2Region;
	private TextureRegion star3Region;

	private static TextGameObject percentTxt;
	private final DecimalFormat df = new DecimalFormat("##");

	private boolean showMus = false;
	private static boolean showStar1 = false;
	private static boolean showStar2 = false;
	private static boolean showStar3 = false;

	private static boolean collectedStar1 = false;
	private static boolean collectedStar2 = false;
	private static boolean collectedStar3 = false;

	private static int percents;

	private float jamScaleX = 1;
	private float jamScaleY = 1;

	private static float jamV1 = 0;
	private static float jamV2 = 0;

	public JarObject(String name, String prog_back, String prog_jam,
			String prog_ellipse, String prog_cover, String prog_mus,
			String star1, String star2, String star3) {
		super(name, prog_back, false);
		super.skinName = "";

		this.prog_jam_name = prog_jam;
		this.prog_ellipse_name = prog_ellipse;
		this.prog_cover_name = prog_cover;
		this.prog_mus_name = prog_mus;

		this.star1_name = star1;
		this.star2_name = star2;
		this.star3_name = star3;

		RotateToAction musRotate1 = rotateTo(-10, 0.05f);

		RotateToAction musRotate2 = rotateTo(10, 0.05f);

		RotateToAction musRotate3 = rotateTo(0, 0.05f);

		this.addAction(forever(sequence(musRotate1, musRotate2, musRotate1,
				musRotate2, musRotate1, musRotate2, musRotate3, delay(5))));
	}

	public void resetTxtFields() {
		percentTxt = null;
	}

	private void setPercentText(int f) {
		if (percentTxt != null) {
			if (f < 10) {
				percentTxt.setPositionXY(55, 43);
			} else if (f < 99) {
				percentTxt.setPositionXY(50, 43);
			} else {
				percentTxt.setPositionXY(43, 43);
			}

			percentTxt.setText(df.format(f) + "%");
		}
	}

	public int getProgress() {
		return percents;
	}

	public void initTxtObjects() {
		percentTxt = new TextGameObject("jar-percent-txt", false, 30);
		setPercentText(0);
	}

	public static int getStars() {
		int a = 0;

		if (collectedStar1) {
			a++;
		}

		if (collectedStar2) {
			a++;
		}

		if (collectedStar3) {
			a++;
		}

		return a;
	}

	public void collectStar1(boolean s) {
		collectedStar1 = s;
	}

	public void collectStar2(boolean s) {
		collectedStar2 = s;
	}

	public void collectStar3(boolean s) {
		collectedStar3 = s;
	}

	public void showStar1(boolean s) {
		showStar1 = s;
	}

	public void showStar2(boolean s) {
		showStar2 = s;
	}

	public void showStar3(boolean s) {
		showStar3 = s;
	}

	public void setProgress(float pixelsLabelValue) {
		if (pixelsLabelValue == 0) {
			showStar1(false);
			showStar2(false);
			showStar3(false);

			collectStar1(false);
			collectStar2(false);
			collectStar3(false);
		}

		if (pixelsLabelValue < 15 || pixelsLabelValue > 90) {
			this.jamScaleY = 0;
		} else {
			this.jamScaleY = pixelsLabelValue / 75 + 0.25f;
		}

		if (pixelsLabelValue > 70) {
			jamScaleX = 1 - (100 - pixelsLabelValue) / 100;
		} else {
			jamScaleX = 1;
		}

		JarObject.percents = (int) Math.floor(pixelsLabelValue);

		setPercentText(percents);

		float a = pixelsLabelValue;
		if (a > 90) {
			a = 90;
		}

		this.prog_jamRegion.setV(JarObject.jamV2
				- ((JarObject.jamV2 - JarObject.jamV1) / 90 * a));

		prog_jamWH = new Vector2(prog_jamRegion.getRegionWidth(),
				prog_jamRegion.getRegionHeight());

		if (pixelsLabelValue < 90) {
			this.showMus = false;

		}

		if (pixelsLabelValue >= 90) {
			this.showMus = true;
		}

		if (pixelsLabelValue >= 100) {
			if (GameScreen.reloadTimer <= 0) {
				GameScreen.reloadTimer = Constants.RELOADING_DELAY;
			}

			GameScreen.isLevelEnd = true;
			GameScreen.isEarlyCompletion = true;
			GameScreen.showCompleteTimer += Constants.RELOADING_DELAY;
		}
	}

	@Override
	public void clearSkin() {
		this.prog_jamRegion = null;
		this.prog_coverRegion = null;

		this.prog_musRegion = null;

		this.star1Region = null;
		this.star2Region = null;
		this.star3Region = null;

		this.prog_ellipseRegion = null;
	}

	public static float getJarJamV1() {
		return jamV1;
	}

	public static float getJarJamV2() {
		return jamV2;
	}

	@Override
	public void setSkin() {
		this.prog_jamRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(prog_jam_name);

		if (JarObject.jamV1 == 0 && JarObject.jamV2 == 0) {
			JarObject.jamV1 = this.prog_jamRegion.getV();
			JarObject.jamV2 = this.prog_jamRegion.getV2();

		}

		this.prog_jamRegion.setV(JarObject.jamV2);

		this.prog_ellipseRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(prog_ellipse_name);

		this.prog_coverRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(prog_cover_name);

		this.prog_musRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(prog_mus_name);

		this.star1Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(star1_name);
		this.star2Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(star2_name);
		this.star3Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion(star3_name);

		prog_jamXY = new Vector2(39 * GdxGame.HD_X_RATIO,
				25 * GdxGame.HD_Y_RATIO);
		prog_ellipseXY = new Vector2(40 * GdxGame.HD_X_RATIO,
				14 * GdxGame.HD_Y_RATIO);

		prog_coverXY = new Vector2(0, 0 * GdxGame.HD_Y_RATIO);
		prog_musXY = new Vector2(30 * GdxGame.HD_X_RATIO,
				50 * GdxGame.HD_Y_RATIO);

		star1XY = new Vector2(46 * GdxGame.HD_X_RATIO, 122 * GdxGame.HD_Y_RATIO);
		star2XY = new Vector2(70 * GdxGame.HD_X_RATIO, 120 * GdxGame.HD_Y_RATIO);
		star3XY = new Vector2(94 * GdxGame.HD_X_RATIO, 111 * GdxGame.HD_Y_RATIO);

		prog_jamWH = new Vector2(prog_jamRegion.getRegionWidth(),
				prog_jamRegion.getRegionHeight());
		prog_coverWH = new Vector2(prog_coverRegion.getRegionWidth(),
				prog_coverRegion.getRegionHeight());
		prog_musWH = new Vector2(prog_musRegion.getRegionWidth(),
				prog_musRegion.getRegionHeight());
		star1WH = new Vector2(star1Region.getRegionWidth(),
				star1Region.getRegionHeight());
		star2WH = new Vector2(star2Region.getRegionWidth(),
				star2Region.getRegionHeight());
		star3WH = new Vector2(star3Region.getRegionWidth(),
				star3Region.getRegionHeight());

		prog_ellipseWH = new Vector2(prog_ellipseRegion.getRegionWidth(),
				prog_ellipseRegion.getRegionHeight());
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(this.getColor());

		batch.draw(this.prog_jamRegion, this.getX() + prog_jamXY.x, this.getY()
				+ prog_jamXY.y, getOriginX(), getOriginY(), prog_jamWH.x, prog_jamWH.y,
				getScaleX(), getScaleY(), 0);

		batch.draw(this.prog_ellipseRegion, this.getX() + prog_ellipseXY.x, this.getY()
				+ prog_jamXY.y + prog_jamWH.y - prog_ellipseWH.y
				+ (1 - this.jamScaleY) * prog_ellipseWH.y / 2 + 4
				* GdxGame.HD_Y_RATIO, 31, 0 * GdxGame.HD_Y_RATIO,
				prog_ellipseWH.x, prog_ellipseWH.y, this.jamScaleX,
				this.jamScaleY, 0);

		batch.draw(this.prog_coverRegion, this.getX() + prog_coverXY.x, this.getY()
				+ prog_coverXY.y, getOriginX(), getOriginY(), prog_coverWH.x,
				prog_coverWH.y, getScaleX(), getScaleY(), 0);

		if (this.showMus) {
			batch.draw(this.prog_musRegion, this.getX() + prog_musXY.x, this.getY()
					+ prog_musXY.y, 41 * GdxGame.HD_X_RATIO,
					22 * GdxGame.HD_Y_RATIO, prog_musWH.x, prog_musWH.y,
					getScaleX(), getScaleY(), getRotation());
		}

		if (JarObject.showStar1) {
			batch.draw(this.star1Region, this.getX() + star1XY.x,
					this.getY() + star1XY.y, getOriginX(), getOriginY(), star1WH.x, star1WH.y,
					getScaleX(), getScaleY(), 0);
		}

		if (JarObject.showStar2) {
			batch.draw(this.star2Region, this.getX() + star2XY.x,
					this.getY() + star2XY.y, getOriginX(), getOriginY(), star2WH.x, star2WH.y,
					getScaleX(), getScaleY(), 0);
		}

		if (JarObject.showStar3) {
			batch.draw(this.star3Region, this.getX() + star3XY.x,
					this.getY() + star3XY.y, getOriginX(), getOriginY(), star3WH.x, star3WH.y,
					getScaleX(), getScaleY(), 0);
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!isVisible()) {
			return;
		}

		if (this.prog_jamRegion != null && this.prog_coverRegion != null
				&& this.prog_musRegion != null && this.star1Region != null
				&& this.star2Region != null && this.star3Region != null
				&& this.prog_ellipseRegion != null) {
			//this.drawSkin(batch, parentAlpha);
		}

		if (percentTxt == null) {
			initTxtObjects();
		} else {
			//percentTxt.draw(batch, parentAlpha);
		}
	}

}
