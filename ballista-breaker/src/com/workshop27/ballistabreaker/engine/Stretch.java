package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.workshop27.ballistabreaker.GdxGame;

public class Stretch extends GameObject {
	// variables
	private final float[] ropeVertices = new float[20];

	private Texture tex;

	private final String skinName;

	private final Color col = new Color(1.0f, 1.0f, 1.0f, 1.0f);
	private final float colFloat = col.toFloatBits();

	private final Vector2 v = new Vector2(0, 0);
	private float cos;
	private float sin;
	private float x1;
	private float y1;
	private float x2;
	private float y2;
	private float x3;
	private float y3;
	private float x4;
	private float y4;

	// constructors
	public Stretch(String name, String skinName) {
		super(name);

		this.skinName = skinName;
	}

	@Override
	public void clearSkin() {
		this.tex = null;
	}

	public void setSkin(String skinName) {
		if (!isVisible()) {
			return;
		}

		this.tex = ImageCache.getManager()
				.get("data/atlas/main/pack", TextureAtlas.class)
				.findRegion(skinName).getTexture();

		if (this.tex == null) {
			return;
		}

		this.tex.setFilter(TextureFilter.Linear, TextureFilter.Linear);
	}

	public void calcRopeVertices() {
		this.v.x = getWidth() - getX();
		this.v.y = getHeight() - getY();

		this.setRotation(v.angle());
		this.cos = MathUtils.cosDeg(v.angle());
		this.sin = MathUtils.sinDeg(v.angle());

		this.x1 = cos - sin * 8 * GdxGame.HD_X_RATIO;
		this.y1 = sin + cos * 8 * GdxGame.HD_Y_RATIO;

		this.x2 = cos - sin * (-56) * GdxGame.HD_X_RATIO;
		this.y2 = sin + cos * (-56) * GdxGame.HD_Y_RATIO;

		this.x3 = cos - sin * 8 * GdxGame.HD_X_RATIO;
		this.y3 = sin + cos * 8 * GdxGame.HD_Y_RATIO;

		this.x4 = cos - sin * (-56) * GdxGame.HD_X_RATIO;
		this.y4 = sin + cos * (-56) * GdxGame.HD_Y_RATIO;

		ropeVertices[0] = getX() + x1;
		ropeVertices[1] = getY() + y1;
		ropeVertices[2] = colFloat;
		ropeVertices[3] = 0;
		ropeVertices[4] = 0;

		ropeVertices[5] = getX() + x2;
		ropeVertices[6] = getY() + y2;
		ropeVertices[7] = colFloat;
		ropeVertices[8] = 0;
		ropeVertices[9] = 1;

		ropeVertices[10] = getWidth() + x3;
		ropeVertices[11] = getHeight() + y3;
		ropeVertices[12] = colFloat;
		ropeVertices[13] = 1;
		ropeVertices[14] = 0;

		ropeVertices[15] = getWidth() + x4;
		ropeVertices[16] = getHeight() + y4;
		ropeVertices[17] = colFloat;
		ropeVertices[18] = 1;
		ropeVertices[19] = 1;
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		calcRopeVertices();

		if (this.tex == null) {
			setSkin(skinName);
			batch.setColor(this.getColor());
			batch.draw(tex, ropeVertices, 0, 20);
		} else {
			batch.setColor(this.getColor());
			batch.draw(tex, ropeVertices, 0, 20);
		}
	}
}
