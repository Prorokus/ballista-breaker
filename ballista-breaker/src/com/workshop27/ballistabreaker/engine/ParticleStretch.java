package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.workshop27.ballistabreaker.GdxGame;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class ParticleStretch extends GameObject {
	// variables
	private final float begX;
	private final float begY;
	private final float endX;
	private final float endY;

	private final String particleName;

	ParticleEffect effect;

	// constructors
	public ParticleStretch(String name, String particleName, float begX,
			float begY, float endX, float endY) {
		super(name);

		this.begX = begX;
		this.begY = begY;
		this.endX = endX;
		this.endY = endY;

		this.particleName = particleName;

		GdxGame.beetleStage.addActor(this);

		createParticle(particleName);
		MoveToAction m1 = moveTo(this.endX, this.endY, 0.1f);
		MoveToAction m2 = moveTo(this.begX, this.begY, 0.1f);

		this.addAction(forever(sequence(m1, m2)));
	}

	@Override
	public float getPositionX() {
		return this.getX();
	}

	@Override
	public float getPositionY() {
		return this.getY();
	}

	@Override
	public void loadParticleData(String s) {
		this.effect.load(
				Gdx.files.internal("data/" + "/particles/" + s + ".txt"),
				ImageCache.getManager().get("data/atlas/main/pack",
						TextureAtlas.class));

		this.effect.setPosition(this.getPositionX(), this.getPositionY());
	}

	public void createParticle(String name) {
		this.effect = new ParticleEffect();
	}

	@Override
	public void clearParticles() {
		this.effect.dispose();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.effect != null) {
			this.effect.setPosition(this.getPositionX(), this.getPositionY());

			if (this.effect.getEmitters().size == 0) {
				loadParticleData(particleName);
			}

			this.effect.draw(batch, Gdx.graphics.getDeltaTime());
		}

	}
}
