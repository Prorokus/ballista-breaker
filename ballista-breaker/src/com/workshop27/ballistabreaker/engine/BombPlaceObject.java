package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.workshop27.ballistabreaker.Slingshot;

public class BombPlaceObject extends GameObject {

	public BombPlaceObject(String string, String string2, boolean b) {
		super(string, string2, b);

        setTouchable(Touchable.enabled);

        initClickListener();
	}


    private void initClickListener()
    {
        ClickListener clickListener = new ClickListener(){
            @Override
            public boolean touchDown(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y, int pointer, int button)
            {
                super.touchDown(event, x, y, pointer, button);
                return Slingshot.bombTouchDown(x, y);
            }

            public void touchDragged(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y, int pointer)
            {
                super.touchDragged(event, x, y, pointer);
                Slingshot.bombTouchDragged(x, y);
            }

            public void touchUp(com.badlogic.gdx.scenes.scene2d.InputEvent event, float x, float y, int pointer, int button)
            {
                super.touchUp(event, x, y, pointer, button);
                Slingshot.bombTouchUp(x, y);
            }

        };

        addListener(clickListener);
    }

	/*@Override
	public boolean touchDown(float arg_x, float arg_y, int arg2) {
		return Slingshot.bombTouchDown(arg_x, arg_y);
	}*/

	/*@Override
	public void touchDragged(float arg_x, float arg_y, int arg2) {
		Slingshot.bombTouchDragged(arg_x, arg_y);
	}*/

	/*@Override
	public void touchUp(float arg_x, float arg_y, int arg2) {
		Slingshot.bombTouchUp(arg_x, arg_y);
	}*/
}
