package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.BBInventory.AvaliableBonuses;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class BonusPanel extends GameObject {
	private Vector2 laserXY;
	private Vector2 coockieXY;
	private Vector2 explXY;
	private Vector2 egg1XY;
	private Vector2 egg2XY;

	private Vector2 laserWH;
	private Vector2 coockieWH;
	private Vector2 explWH;
	private Vector2 egg1WH;
	private Vector2 egg2WH;

	private Vector2 laserOrXY;
	private Vector2 coockieOrXY;
	private Vector2 explOrXY;
	private Vector2 egg1OrXY;
	private Vector2 egg2OrXY;

	private TextureRegion laserRegion;
	private TextureRegion coockieRegion;
	private TextureRegion explRegion;
	private TextureRegion egg1Region;
	private TextureRegion egg2Region;

	private boolean showLaserIcon = false;
	private boolean showCoockieIcon = false;
	private boolean showExplIcon = false;
	private boolean showEgg1Icon = false;
	private boolean showEgg2Icon = false;

	private final float margin = 5 * GdxGame.HD_X_RATIO;

	private float bonusY = 0;

	public BonusPanel(String name) {
		super(name, false);

		super.skinName = "";

		this.folderName = "inventory";

		this.setPositionXY(0, 400);
	}

	@Override
	public void clearSkin() {
		this.laserRegion = null;
		this.coockieRegion = null;
		this.explRegion = null;
		this.egg1Region = null;
		this.egg2Region = null;
	}

	@Override
	public void setSkin() {
		this.laserRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("laser-bonus");

		this.coockieRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("power-bonus");

		this.explRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("radius-bonus");

		this.egg1Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("egg1-bonus");

		this.egg2Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("egg2-bonus");

		laserXY = new Vector2(13 * GdxGame.HD_X_RATIO, 0 * GdxGame.HD_Y_RATIO);
		coockieXY = new Vector2(15 * GdxGame.HD_X_RATIO, 0 * GdxGame.HD_Y_RATIO);
		explXY = new Vector2(15 * GdxGame.HD_X_RATIO, 0 * GdxGame.HD_Y_RATIO);
		egg1XY = new Vector2(15 * GdxGame.HD_X_RATIO, 0 * GdxGame.HD_Y_RATIO);
		egg2XY = new Vector2(15 * GdxGame.HD_X_RATIO, 0 * GdxGame.HD_Y_RATIO);

		this.setWidth(1);
		this.setHeight(1);

		laserWH = new Vector2(laserRegion.getRegionWidth(),
				laserRegion.getRegionHeight());
		coockieWH = new Vector2(coockieRegion.getRegionWidth(),
				coockieRegion.getRegionHeight());
		explWH = new Vector2(explRegion.getRegionWidth(),
				explRegion.getRegionHeight());
		egg1WH = new Vector2(egg1Region.getRegionWidth(),
				egg1Region.getRegionHeight());
		egg2WH = new Vector2(egg2Region.getRegionWidth(),
				egg2Region.getRegionHeight());

		laserOrXY = new Vector2(laserWH.x / 2, laserWH.y / 2);
		coockieOrXY = new Vector2(coockieWH.x / 2, coockieWH.y / 2);
		explOrXY = new Vector2(explWH.x / 2, explWH.y / 2);
		egg1OrXY = new Vector2(egg1WH.x / 2, egg1WH.y / 2);
		egg2OrXY = new Vector2(egg2WH.x / 2, egg2WH.y / 2);

		Update();
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(1, 1, 1, 0.95f);

		if (this.showLaserIcon) {
			batch.draw(this.laserRegion, this.getX() + laserXY.x,
					this.getY() + laserXY.y, laserOrXY.x, laserOrXY.y, laserWH.x,
					laserWH.y, getScaleX(), getScaleY(), getRotation());
		}

		if (this.showCoockieIcon) {
			batch.draw(this.coockieRegion, this.getX() + coockieXY.x, this.getY()
					+ coockieXY.y, coockieOrXY.x, coockieOrXY.y, coockieWH.x,
					coockieWH.y, getScaleX(), getScaleY(), getRotation());
		}

		if (this.showExplIcon) {
			batch.draw(this.explRegion, this.getX() + explXY.x, this.getY() + explXY.y,
					explOrXY.x, explOrXY.y, explWH.x, explWH.y, getScaleX(), getScaleY(),
					getRotation());
		}

		if (this.showEgg1Icon) {
			batch.draw(this.egg1Region, this.getX() + egg1XY.x, this.getY() + egg1XY.y,
					egg1OrXY.x, egg1OrXY.y, egg1WH.x, egg1WH.y, getScaleX(), getScaleY(),
					getRotation());
		}

		if (this.showEgg2Icon) {
			batch.draw(this.egg2Region, this.getX() + egg2XY.x, this.getY() + egg2XY.y,
					egg2OrXY.x, egg2OrXY.y, egg2WH.x, egg2WH.y, getScaleX(), getScaleY(),
					getRotation());
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!this.isVisible() || !GdxGame.mInventory.isVisible()) {
			return;
		}

		if (this.laserRegion != null && this.coockieRegion != null
				&& this.explRegion != null && this.egg1Region != null
				&& this.egg2Region != null) {

			this.drawSkin(batch, parentAlpha);
			super.draw(batch, parentAlpha);
		}
	}

	public void Update() {
		if (this.laserRegion == null || this.coockieRegion == null
				|| this.explRegion == null || this.egg1Region == null
				|| this.egg2Region == null) {
			return;
		}

		this.showLaserIcon = false;
		this.showCoockieIcon = false;
		this.showExplIcon = false;
		this.showEgg1Icon = false;
		this.showEgg2Icon = false;

		this.bonusY = 0;

		for (int i = 0; i < GdxGame.mInventory.getActiveBonuses().size(); i++) {

			if (GdxGame.mInventory.getActiveBonuses().get(i)
					.equals(AvaliableBonuses.LASER)) {
				this.showLaserIcon = true;

				laserXY.y = this.bonusY * this.getScaleY();
				this.bonusY += this.laserWH.y + margin;
			}

			if (GdxGame.mInventory.getActiveBonuses().get(i).equals(AvaliableBonuses.COOCKIE) && GameScreen.bManager.getBeetleEnrageTimer() == 0) 
			{
				this.showCoockieIcon = true;

				coockieXY.y = this.bonusY * this.getScaleY();
				this.bonusY += coockieWH.y + margin;
			}

			if (GdxGame.mInventory.getActiveBonuses().get(i)
					.equals(AvaliableBonuses.EXPLOSION_RADIUS)
					&& GdxGame.mInventory.bManager.checkEggBeetles()) {
				this.showExplIcon = true;

				explXY.y = this.bonusY * this.getScaleY();
				this.bonusY += explWH.y + margin;
			}

			if (GdxGame.mInventory.getActiveBonuses().get(i)
					.equals(AvaliableBonuses.BONUS_EGG1)
					&& GdxGame.mInventory.bManager.checkEggBeetles()
					&& !GdxGame.mInventory.getActiveBonuses().contains(
							AvaliableBonuses.BONUS_EGG2)) {
				this.showEgg1Icon = true;

				egg1XY.y = this.bonusY * this.getScaleY();
				this.bonusY += egg1WH.y + margin;
			}

			if (GdxGame.mInventory.getActiveBonuses().get(i).equals(AvaliableBonuses.BONUS_EGG2) && GdxGame.mInventory.bManager.checkEggBeetles()) {
				this.showEgg2Icon = true;

				egg2XY.y = this.bonusY * this.getScaleY();
				this.bonusY += egg2WH.y + margin;
			}
		}
	}

}
