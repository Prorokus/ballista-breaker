package com.workshop27.ballistabreaker.engine;

import aurelienribon.tweenengine.TweenAccessor;

import com.badlogic.gdx.math.Vector2;

public class GameObjectAccessor extends GameObject implements
		TweenAccessor<GameObject> {

	public static final int POSITION_X = 1;
	public static final int POSITION_Y = 2;
	public static final int POSITION_XY = 3;

	private final Vector2 rotationVector = new Vector2(0, 0);

	public float rotationAngleOffset = 0;

	public GameObjectAccessor(String name, String skinName, boolean addToRoot) {
		super(name, skinName, addToRoot);
	}

	public GameObjectAccessor() {
		super();
	}

	@Override
	public int getValues(GameObject target, int tweenType, float[] returnValues) {
		switch (tweenType) {
		case POSITION_X:
			returnValues[0] = target.getPositionX();
			return 1;
		case POSITION_Y:
			returnValues[1] = target.getPositionY();
			return 1;
		case POSITION_XY:
			returnValues[0] = target.getPositionX();
			returnValues[1] = target.getPositionY();
			return 2;
		default:
			assert false;
			return -1;
		}
	}

	@Override
	public void setValues(GameObject target, int tweenType, float[] newValues) {
		switch (tweenType) {
		case POSITION_X:
			target.setPositionX(newValues[0]);
			break;
		case POSITION_Y:
			target.setPositionY(newValues[1]);
			break;
		case POSITION_XY:
			rotationVector.x = (target.getPositionX() - newValues[0]);
			rotationVector.y = (target.getPositionY() - newValues[1]);
			target.setRotation(rotationVector.angle() + rotationAngleOffset);

			target.setPositionXLiterally(newValues[0]);
			target.setPositionYLiterally(newValues[1]);
			break;
		default:
			assert false;
			break;
		}
	}
}
