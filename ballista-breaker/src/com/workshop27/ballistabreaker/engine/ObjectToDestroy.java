package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Logger;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.screens.GameScreen;

import java.util.Scanner;

public class ObjectToDestroy extends Group {

	// variables
	private final Logger log = new Logger("ObjectToDestroy");

	public PixmapHelper pixmapHelper = null;
	public Pixmap pixmapHelperTexture = null;

	private byte[][] imageAlphaMap;

	private int imageAlphaMapX;
	private int imageAlphaMapY;

	public float pixelsToDestroy = 0;

	private int maxPixmapSize;

	public boolean isFilterInited = false;

	public Vector2 localPoint = new Vector2(0, 0);

	// ANGLE
	public int rotationAngles[] = new int[10];
	public int prevAngleIndex = 0;
	public int currAngleIndex = 0;
	public int maxAngleIndex = 0;
	public float currRotationSpeed = 0;
	private int rotationType = 0;

	private final Vector2 v = new Vector2(0, 0);
	
	private float pendulumAngle = 0;

	private final GameObject bigRope;

	// constructors
	public ObjectToDestroy(String name, GameObject bigRope) {
		super();
		this.setName(name);

		if (Constants.IS_HD) {
			this.maxPixmapSize = 1024;
		} else {
			this.maxPixmapSize = 512;
		}

		this.bigRope = bigRope;

		initPixmapHelper(name);
	}

	public void reInit() {
		/*this.pixelsToDestroy = 0;

		Pixmap pixmapFormatTexture = new Pixmap(
				Gdx.files.internal("data/atlas/level" + GameScreen.levelNumb
						+ "/fruit1.jpg"));
		Pixmap pixmapFormatTextureA = new Pixmap(
				Gdx.files.internal("data/atlas/level" + GameScreen.levelNumb
						+ "/fruit1a.jpg"));

		this.setPositionXY(
				(GdxGame.VIRTUAL_GAME_WIDTH - pixmapFormatTexture.getWidth()) / 2,
				GdxGame.VIRTUAL_GAME_HEIGHT - 60
						- pixmapFormatTexture.getHeight());

		pixmapHelperTexture = new Pixmap(this.maxPixmapSize,
				this.maxPixmapSize, Format.RGBA8888);
		pixmapHelperTexture
				.drawPixmap(pixmapFormatTexture, 0, 0, 0, 0,
						pixmapFormatTexture.getWidth(),
						pixmapFormatTexture.getHeight());

		this.setWidth(pixmapFormatTexture.getWidth());
		this.setHeight(pixmapFormatTexture.getHeight());

		Color color1 = new Color();
		Color color2 = new Color();

		for (int a = 0; a < this.maxPixmapSize; a++) {
			for (int b = 0; b < this.maxPixmapSize; b++) {
				this.imageAlphaMap[a][b] = 0;
				PixmapHelper.imageAMap[a][b] = 0;
			}
		}

		// Create 24+alpha texture from 2 jpg files
		// Calculate destroyable pixels
		for (int a = 0; a < this.getWidth(); a++) {
			for (int b = 0; b < this.getHeight(); b++) {
				Color.rgba8888ToColor(color2,
						pixmapFormatTextureA.getPixel(a, b));

				if (color2.g > 0.99f) {
					this.imageAlphaMap[a][b] = 1;
					PixmapHelper.imageAMap[a][b] = 1;
					this.pixelsToDestroy++;

				} else if (color2.g <= 0.99f && color2.g >= 0.01f) {
					this.imageAlphaMap[a][b] = 2;
					PixmapHelper.imageAMap[a][b] = 2;
					this.pixelsToDestroy++;

					Color.rgba8888ToColor(color1,
							pixmapHelperTexture.getPixel(a, b));
					pixmapHelperTexture.drawPixel(a, b, Color.rgba8888(
							color1.r, color1.g, color1.b, color2.g));

				} else if (color2.g < 0.01f) {
					pixmapHelperTexture.drawPixel(a, b,
							Color.rgba8888(0, 0, 0, 0));
				}
			}
		}
		pixmapFormatTexture.dispose();
		pixmapFormatTextureA.dispose();

		this.pixmapHelper.restartOnEnter(pixmapHelperTexture, this.getWidth(),
				this.getHeight());

		if (pixmapHelper.getCurentMask() != GdxGame.stageNumber) {
			pixmapHelper = null;
			pixmapHelper = new PixmapHelper(getWidth(), getHeight(),
					pixmapHelperTexture);
		}

		setOriginX(getWidth() / 2);
		setOriginY(getHeight() / 2);

		this.currRotationSpeed = 0;
		this.pendulumAngle = 0;
		this.setRotation(0);
		this.bigRope.setRotation(0);

		addSpeeds(GameScreen.levelNumb);

		this.isFilterInited = false;*/
	}

	private void initPixmapHelper(String name) {
		Pixmap pixmapFormatTexture = new Pixmap(
				Gdx.files.internal("data/atlas/level" + GameScreen.levelNumb
						+ "/fruit1.jpg"));
		Pixmap pixmapFormatTextureA = new Pixmap(
				Gdx.files.internal("data/atlas/level" + GameScreen.levelNumb
						+ "/fruit1a.jpg"));

		this.setPositionXY(
				(GdxGame.VIRTUAL_GAME_WIDTH - pixmapFormatTexture.getWidth()) / 2,
				GdxGame.VIRTUAL_GAME_HEIGHT - 60
						- pixmapFormatTexture.getHeight());

		pixmapHelperTexture = new Pixmap(this.maxPixmapSize,
				this.maxPixmapSize, Format.RGBA8888);
		pixmapHelperTexture
				.drawPixmap(pixmapFormatTexture, 0, 0, 0, 0,
						pixmapFormatTexture.getWidth(),
						pixmapFormatTexture.getHeight());

		this.imageAlphaMap = new byte[this.maxPixmapSize][this.maxPixmapSize];
		PixmapHelper.imageAMap = new byte[this.maxPixmapSize][this.maxPixmapSize];

		this.setWidth(pixmapFormatTexture.getWidth());
		this.setHeight(pixmapFormatTexture.getHeight());

		Color color1 = new Color();
		Color color2 = new Color();

		// Create 24+alpha texture from 2 jpg files
		// Calculate destroyable pixels
		for (int a = 0; a < this.getWidth(); a++) {
			for (int b = 0; b < this.getHeight(); b++) {
				Color.rgba8888ToColor(color2,
						pixmapFormatTextureA.getPixel(a, b));

				if (color2.g < 0.01f) {
					pixmapHelperTexture.drawPixel(a, b,
							Color.rgba8888(0, 0, 0, 0));
				}

				else if (color2.g > 0.99f) {
					this.imageAlphaMap[a][b] = 1;
					PixmapHelper.imageAMap[a][b] = 1;
					this.pixelsToDestroy++;
				}

				else if (color2.g <= 0.99f && color2.g >= 0.01f) {
					this.imageAlphaMap[a][b] = 2;
					PixmapHelper.imageAMap[a][b] = 2;
					this.pixelsToDestroy++;

					Color.rgba8888ToColor(color1,
							pixmapHelperTexture.getPixel(a, b));
					pixmapHelperTexture.drawPixel(a, b, Color.rgba8888(
							color1.r, color1.g, color1.b, color2.g));
				}
			}
		}
		pixmapFormatTexture.dispose();
		pixmapFormatTextureA.dispose();

		pixmapHelper = new PixmapHelper(getWidth(), getHeight(),
				pixmapHelperTexture);

		setOriginX(getWidth() / 2);
		setOriginY(getHeight() / 2);

		this.currRotationSpeed = 0;
		this.pendulumAngle = 0;
		this.setRotation(0);
		this.bigRope.setRotation(0);

		addSpeeds(GameScreen.levelNumb);

		this.isFilterInited = false;
	}

	public void restart() {
		// for (int a = 0; a < this.width; a++) {
		// for (int b = 0; b < this.height; b++) {
		// PixmapHelper.imageAMap[a][b] = this.imageAlphaMap[a][b];
		// }
		// }

		for (int i = 0; i < this.imageAlphaMap.length; i++) {
			System.arraycopy(this.imageAlphaMap[i], 0,
					PixmapHelper.imageAMap[i], 0, this.imageAlphaMap[0].length);
		}

		// System.arraycopy(this.imageAlphaMap, 0, PixmapHelper.imageAMap, 0,
		// this.imageAlphaMap.length);

		this.pixmapHelper.restart(pixmapHelperTexture);
		this.isFilterInited = false;

		this.currRotationSpeed = 0;
		this.pendulumAngle = 0;
		this.setRotation(0);
		this.bigRope.setRotation(0);
	}

	public void addSpeeds(int levelNumb) {
		String speeds = null;

		for (int i = 0; i < this.rotationAngles.length; i++) {
			this.rotationAngles[i] = 0;
		}

		try {
			speeds = GdxGame.files.getScenario("speed", "level" + levelNumb);
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Scanner s = new Scanner(speeds).useDelimiter(" ");
		this.rotationType = s.nextInt();

		if (this.rotationType == 0) {
			setOriginX(getWidth() / 2);
			setOriginY(getHeight() / 2);
			this.bigRope.setVisible(false);
		} else {
			setOriginX(getWidth() / 2);
			setOriginY(getHeight() + 120);

			this.bigRope.setVisible(true);
			this.bigRope.setPositionY(s.nextInt());
			this.bigRope.setOriginX(26);
			this.bigRope.setOriginY((this.getPositionY() + this.getOriginY())
					- (this.bigRope.getPositionY()));
		}

		for (int i = 0; i < this.rotationAngles.length; i++) {
			if (s.hasNext()) {
				this.rotationAngles[i] = s.nextInt();
				this.maxAngleIndex = i;
			}
		}
	}

	public boolean checkCollision(float center_x, float center_y, float w,
			float h) {
		w -= w / 2;
		h -= h / 2;

		v.x = center_x;
		v.y = center_y;
		localPoint = this.parentToLocalCoordinates(v);

		this.imageAlphaMapX = (int) (localPoint.x);
		this.imageAlphaMapY = (int) (this.getHeight() - localPoint.y);

		int x = (int) (this.imageAlphaMapX - w / 2);
		int y = (int) (this.imageAlphaMapY - h / 2);
		for (int i = 0; i < 8; i++) {
			this.imageAlphaMapX = (int) (this.imageAlphaMapX + i * w / 8);

			for (int j = 0; j < 8; j++) {
				this.imageAlphaMapY = (int) (this.imageAlphaMapY + j * h / 8);

				if (imageAlphaMapX < this.getWidth()
						&& this.imageAlphaMapY < this.getHeight()
						&& imageAlphaMapX > 0
						&& this.imageAlphaMapY > 0
						&& (PixmapHelper.imageAMap[imageAlphaMapX][imageAlphaMapY] == 1 || PixmapHelper.imageAMap[imageAlphaMapX][imageAlphaMapY] == 2)) {

					return true;
				}

				this.imageAlphaMapY = y;
			}
			this.imageAlphaMapX = x;
		}

		return false;
	}

	public boolean checkLaserCollision(float center_x, float center_y, float w,
			float h) {
		w -= w / 2;
		h -= h / 2;

		v.x = center_x;
		v.y = center_y;
		localPoint = this.parentToLocalCoordinates(v);

		this.imageAlphaMapX = (int) (localPoint.x);
		this.imageAlphaMapY = (int) (this.getHeight() - localPoint.y);

		int x = (int) (this.imageAlphaMapX - w / 2);
		int y = (int) (this.imageAlphaMapY - h / 2);
		for (int i = 0; i < 1; i++) {
			this.imageAlphaMapX = (int) (this.imageAlphaMapX + i * w / 1);

			for (int j = 0; j < 1; j++) {
				this.imageAlphaMapY = (int) (this.imageAlphaMapY + j * h / 1);

				if (imageAlphaMapX < this.getWidth()
						&& this.imageAlphaMapY < this.getHeight()
						&& imageAlphaMapX > 0
						&& this.imageAlphaMapY > 0
						&& (PixmapHelper.imageAMap[imageAlphaMapX][imageAlphaMapY] == 1 || PixmapHelper.imageAMap[imageAlphaMapX][imageAlphaMapY] == 2)) {
					return true;
				}

				this.imageAlphaMapY = y;
			}
			this.imageAlphaMapX = x;
		}

		return false;
	}

	public void dispose() {
		try {
			pixmapHelperTexture.dispose();
		} catch (final Exception ex) {
			log.error("pixmapHelperTexture dispose failed");
		}

		pixmapHelper.clearTexture();
	}

//	public void setWidth(float width) {
//		this.setWidth(width);
//	}
//
//	public void setHeight(float height) {
//		this.setHeight(height);
//	}
//
//	public float getWidth() {
//		return getWidth();
//	}
//
//	public float getHeight() {
//		return getHeight();
//	}

	// position
	public void setPositionX(float x) {
		this.setX(x);
	}

	public void setPositionY(float y) {
		this.setY(y);
	}

	public void setPositionXY(float x, float y) {
		this.setX(x);
		this.setY(y);
	}

	public float getPositionX() {
		return this.getX();
	}

	public float getPositionY() {
		return this.getY();
	}

	public float getAngle() {
		return this.getRotation();
	}

	// origin
//	public void setOriginX(float x) {
//		this.setOriginX(x);
//	}
//
//	public void setOriginY(float y) {
//		this.setOriginY(y);
//	}

	public void setOrigin(float x, float y) {
		this.setOriginX(x);
		this.setOriginY(y);
	}

	public void setOriginCenter() {
		this.setOriginX(getWidth() / 2);
		this.setOriginY(getHeight() / 2);
	}

//	public float getOriginX() {
//		return this.getOriginX();
//	}
//
//	public float getOriginY() {
//		return this.getOriginY();
//	}
//
//	// scale
//	public void setScaleX(float x) {
//		this.setScaleX(x);
//	}
//
//	public void setScaleY(float y) {
//		this.setScaleY(y);
//	}

	public void setScale(float x, float y) {
		this.setScaleX(x);
		this.setScaleY(y);
	}

//	public float getScaleX() {
//		return this.getScaleX();
//	}
//
//	public float getScaleY() {
//		return this.getScaleY();
//	}

	@Override
	public void act(float delta) {
		if (!GameScreen.isGamePaused()) {
			super.act(delta);
		}
	}

	// draw
	public void draw(SpriteBatch batch) {
		draw(batch, 1.0f);
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!GameScreen.isGamePaused()) {
			if (this.rotationType == 0) {
				this.setRotation(this.getRotation() + this.currRotationSpeed * Gdx.graphics.getDeltaTime());
			} else {
				if (this.currRotationSpeed != 0) {
					pendulumAngle += this.currRotationSpeed
							* Gdx.graphics.getDeltaTime();

					if (this.getWidth() < 308) {
						this.setRotation(MathUtils.sinDeg(pendulumAngle) * (15 + (308 - this.getWidth()) * 0.04f));
					} else {
						this.setRotation(MathUtils.sinDeg(pendulumAngle) * (this.getWidth() * 0.04f));
					}

					this.bigRope.setRotation(this.getRotation());
				}
			}
		}

		if (this.pixmapHelper.texture != null) {
			if (!this.isFilterInited) {
				this.pixmapHelper.texture.setFilter(TextureFilter.Linear,
						TextureFilter.Linear);
				this.isFilterInited = true;
			}

			batch.draw(this.pixmapHelper.texture, getX(), getY(), getOriginX(), getOriginY(),
					getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation(), 0, 0, (int) getWidth(),
					(int) getHeight(), false, false);
		}
	}
}
