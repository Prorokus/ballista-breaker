package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Matrix4;

public class ParallaxCamera extends OrthographicCamera {
	private final Matrix4 parallaxNormal = new Matrix4();
	private final Matrix4 parallaxCombined = new Matrix4();
	private float lastCameraPosX = -1;
	private float lastParallaxX = 1;
	public boolean screenRotated = false;

	public ParallaxCamera(float viewportWidth, float viewportHeight) {
		super(viewportWidth, viewportHeight);
	}

	public ParallaxCamera() {
		super();
	}

	public Matrix4 calculateParallaxMatrix(float parallaxX) {
		if ((lastCameraPosX != position.x) || screenRotated) {
			screenRotated = false;
			lastCameraPosX = position.x;
			lastParallaxX = 1;
			parallaxNormal.set(projection);
			parallaxNormal.val[12] = -1 * parallaxNormal.val[0] * position.x;
			parallaxNormal.val[13] = -1 * parallaxNormal.val[5] * position.y;
		}

		if (parallaxX <= 0.99f || parallaxX >= 1.01f) {
			if (lastParallaxX != parallaxX) {
				lastParallaxX = parallaxX;
				parallaxCombined.set(parallaxNormal);
				parallaxCombined.val[12] = parallaxCombined.val[12] * parallaxX;
			}

			return parallaxCombined;
		}

		lastParallaxX = parallaxX;
		return parallaxNormal;
	}

	public boolean recalculateParallax(float parallaxX) {
		return (lastParallaxX != parallaxX || (lastCameraPosX != position.x) || screenRotated) ? true
				: false;
	}
}
