package com.workshop27.ballistabreaker.engine;

import aurelienribon.tweenengine.TweenAccessor;

import com.workshop27.ballistabreaker.GdxGame;

public class CameraAccessor extends ParallaxCamera implements
		TweenAccessor<ParallaxCamera> {

	public static final int POSITION_XY = 3;

	public CameraAccessor(GdxGame gdxGame, float viewportWidth,
			float viewportHeight) {
		super(viewportWidth, viewportHeight);
	}

	public CameraAccessor() {
		super();
	}

	@Override
	public int getValues(ParallaxCamera target, int tweenType,
			float[] returnValues) {
		switch (tweenType) {
		case POSITION_XY:
			returnValues[0] = target.position.x;
			returnValues[1] = target.position.y;
			return 2;
		default:
			assert false;
			return -1;
		}
	}

	@Override
	public void setValues(ParallaxCamera target, int tweenType,
			float[] newValues) {
		switch (tweenType) {
		case POSITION_XY:

			target.position.set(newValues[0], newValues[1], 0);
			break;
		default:
			assert false;
			break;
		}
	}
}
