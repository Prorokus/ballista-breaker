package com.workshop27.ballistabreaker.engine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.AlphaAction;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.Slingshot;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.constants.Texts;
import com.workshop27.ballistabreaker.engine.ShowTutorialObject.ArrowPosEnum;
import com.workshop27.ballistabreaker.interfaces.AbstractBeetle.BeetleState;
import com.workshop27.ballistabreaker.screens.GameScreen;
import com.workshop27.ballistabreaker.screens.ShopScreen;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class BBInventory extends GameObject {
	private Vector2 backXY;
	private Vector2 backArrowXY;
	private Vector2 laserXY;
	private Vector2 beetle1XY;
	private Vector2 beetle2XY;
	private Vector2 coockieXY;
	private Vector2 explXY;
	private Vector2 egg1XY;
	private Vector2 egg2XY;

	private Vector2 backWH;
	private Vector2 backArrowWH;
	private Vector2 laserWH;
	private Vector2 beetle1WH;
	private Vector2 beetle2WH;
	private Vector2 coockieWH;
	private Vector2 explWH;
	private Vector2 egg1WH;
	private Vector2 egg2WH;

	private Vector2 backOrXY;
	private Vector2 backArrowOrXY;
	private Vector2 laserOrXY;
	private Vector2 beetle1OrXY;
	private Vector2 beetle2OrXY;
	private Vector2 coockieOrXY;
	private Vector2 explOrXY;
	private Vector2 egg1OrXY;
	private Vector2 egg2OrXY;

	private TextureRegion backRegion;
	private TextureRegion backArrowRegion;
	private TextureRegion laserRegion;
	private TextureRegion beetle1Region;
	private TextureRegion beetle2Region;
	private TextureRegion coockieRegion;
	private TextureRegion explRegion;
	private TextureRegion egg1Region;
	private TextureRegion egg2Region;

	private static TextGameObject laserCount;
	private static TextGameObject beetle1Count;
	private static TextGameObject beetle2Count;
	public static TextGameObject coockieCount;
	public static TextGameObject explCount;
	public static TextGameObject egg1Count;
	public static TextGameObject egg2Count;

	private final float mInvMinX = -399 * GdxGame.HD_X_RATIO;
	private final float mInvY = 230 * GdxGame.HD_Y_RATIO;
	private float startDragX = 0;

	private boolean mResetingToOpened = false;

	private boolean isAutoOpenUsed = false;

	private String activeBName = "";

	private float newPosX;

	private final Color colorNormal = new Color(1, 1, 1, 1);
	private final Color colorBlocked = new Color(0.2f, 0.2f, 0.2f, 0.8f);

	private Color colorLaser = colorNormal;
	private Color colorBoom = colorNormal;
	private Color colorKarate = colorNormal;
	private Color colorCoockie = colorNormal;
	private Color colorExpl = colorNormal;
	private Color colorEgg1 = colorNormal;
	private Color colorEgg2 = colorNormal;

	private final BonusPanel bPanel;

	public enum InvState {
		CLOSED, OPENED, ANIMATING, DRAGGING, FOREVER_OPENED, AUTO_CLOSE, AUTO_OPEN;
	}

	public enum AvaliableBonuses {
		LASER, COOCKIE, BOOM_BUG, KARATE_BUG, EXPLOSION_RADIUS, BONUS_EGG1, BONUS_EGG2;
	}

	private final ArrayList<AvaliableBonuses> activeBonuses = new ArrayList<AvaliableBonuses>();

	private InvState mInvState = InvState.CLOSED;
	private InvState mInvStateBeforeDrag = InvState.CLOSED;

	private final GdxGame game;

	public BeetleManager bManager;

	private float arrowRotation = 0;

	private int bonusRadius = 0;
	private int bonusEggs = 0;
	Map<String, String> params = new HashMap<String, String>();

	public BBInventory(GdxGame game) {
		super("inventory", false);

		this.game = game;
		super.skinName = "";

		bPanel = new BonusPanel("bonusPanel");
	}

	public boolean getAutoOpenUsed() {
		return this.isAutoOpenUsed;
	}

	public void setAutoOpenUsed(boolean b) {
		this.isAutoOpenUsed = b;
	}

	@Override
	public void clearSkin() {
		this.backRegion = null;
		this.backArrowRegion = null;
		this.laserRegion = null;
		this.beetle1Region = null;
		this.beetle2Region = null;
		this.coockieRegion = null;
		this.explRegion = null;
		this.egg1Region = null;
		this.egg2Region = null;

		this.bPanel.clearSkin();
	}

	public int getBonusRadius() {
		return bonusRadius;
	}

	public void setBonusRadius(int r) {
		this.bonusRadius = r;
	}

	public int getBonusEggs() {
		return bonusEggs;
	}

	public void setBonusEggs(int e) {
		this.bonusEggs = e;
	}

	// updates UI to reflect model
	public static void updateLaser(int purchased, boolean upd_after_purchase) {
		if (laserCount == null) {
			return;
		}
		laserCount.setText(purchased);

		if (!GdxGame.isShopUsed && upd_after_purchase) {
			GdxGame.files.setOptionsAndProgress("shop", "shop", "used");
			GdxGame.isShopUsed = true;
		}
	}

	// updates UI to reflect model
	public static void updateBeetle1(int purchased, boolean upd_after_purchase) {
		if (beetle1Count == null) {
			return;
		}
		beetle1Count.setText(purchased);

		if (!GdxGame.isShopUsed && upd_after_purchase) {
			GdxGame.files.setOptionsAndProgress("shop", "shop", "used");
			GdxGame.isShopUsed = true;
		}
	}

	// updates UI to reflect model
	public static void updateBeetle2(int purchased, boolean upd_after_purchase) {
		if (beetle2Count == null) {
			return;
		}
		beetle2Count.setText(purchased);

		if (!GdxGame.isShopUsed && upd_after_purchase) {
			GdxGame.files.setOptionsAndProgress("shop", "shop", "used");
			GdxGame.isShopUsed = true;
		}
	}

	// updates UI to reflect model
	public static void updateCoockie(int purchased, boolean upd_after_purchase) {
		if (coockieCount == null) {
			return;
		}
		coockieCount.setText(purchased);

		if (!GdxGame.isShopUsed && upd_after_purchase) {
			GdxGame.files.setOptionsAndProgress("shop", "shop", "used");
			GdxGame.isShopUsed = true;
		}
	}

	// updates UI to reflect model
	public static void updateExpl(int purchased, boolean upd_after_purchase) {
		if (explCount == null) {
			return;
		}
		explCount.setText(purchased);

		if (!GdxGame.isShopUsed && upd_after_purchase) {
			GdxGame.files.setOptionsAndProgress("shop", "shop", "used");
			GdxGame.isShopUsed = true;
		}
	}

	// updates UI to reflect model
	public static void updateEgg1(int purchased, boolean upd_after_purchase) {
		if (egg1Count == null) {
			return;
		}
		egg1Count.setText(purchased);

		if (!GdxGame.isShopUsed && upd_after_purchase) {
			GdxGame.files.setOptionsAndProgress("shop", "shop", "used");
			GdxGame.isShopUsed = true;
		}
	}

	// updates UI to reflect model
	public static void updateEgg2(int purchased, boolean upd_after_purchase) {
		if (egg2Count == null) {
			return;
		}
		egg2Count.setText(purchased);

		if (!GdxGame.isShopUsed && upd_after_purchase) {
			GdxGame.files.setOptionsAndProgress("shop", "shop", "used");
			GdxGame.isShopUsed = true;
		}
	}

	public void resetTxtFields() {
		laserCount = null;
		beetle1Count = null;
		beetle2Count = null;
		coockieCount = null;
		explCount = null;
		egg1Count = null;
		egg2Count = null;
	}

	public void initTxtObjects() {
		laserCount = new TextGameObject("inv-laser-txt", false, 22);
		laserCount.setText(this.game.getLaserOwnedItems());
		laserCount.setPositionXY(89, 28);
		this.addActor(laserCount);

		beetle1Count = new TextGameObject("inv-beetle1-txt", false, 22);
		beetle1Count.setText(this.game.getBeetle1OwnedItems());
		beetle1Count.setPositionXY(169, 28);
		this.addActor(beetle1Count);

		beetle2Count = new TextGameObject("inv-beetle2-txt", false, 22);
		beetle2Count.setText(this.game.getBeetle2OwnedItems());
		beetle2Count.setPositionXY(249, 28);
		this.addActor(beetle2Count);

		coockieCount = new TextGameObject("inv-coockie-txt", false, 22);
		coockieCount.setText(this.game.getCoockieOwnedItems());
		coockieCount.setPositionXY(325, 28);
		this.addActor(coockieCount);

		explCount = new TextGameObject("inv-expl-txt", false, 22);
		explCount.setText(this.game.getExplOwnedItems());
		explCount.setPositionXY(80, 115);
		this.addActor(explCount);

		egg1Count = new TextGameObject("inv-egg1-txt", false, 22);
		egg1Count.setText(this.game.getEgg1OwnedItems());
		egg1Count.setPositionXY(165, 115);
		this.addActor(egg1Count);

		egg2Count = new TextGameObject("inv-egg2-txt", false, 22);
		egg2Count.setText(this.game.getEgg2OwnedItems());
		egg2Count.setPositionXY(248, 115);
		this.addActor(egg2Count);
	}

	@Override
	public void setSkin() {
		if (this.backRegion != null && this.backArrowRegion != null
				&& this.beetle1Region != null && this.beetle2Region != null
				&& this.laserRegion != null && this.coockieRegion != null
				&& this.explRegion != null && this.egg1Region != null
				&& this.egg2Region != null) {
			return;
		}

		this.backRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("back");

		this.backArrowRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("back-arrow");
		backArrowRegion.getTexture().setFilter(TextureFilter.Linear,
				TextureFilter.Linear);

		this.laserRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("inv-laser");

		this.beetle1Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("inv-bomb");

		this.beetle2Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("inv-karate");

		this.coockieRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("inv-coockie");

		this.explRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("inv-radius");

		this.egg1Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("inv-egg1");

		this.egg2Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("inv-egg2");

		backXY = new Vector2(0, 0);
		backArrowXY = new Vector2(402, 80);
		laserXY = new Vector2(0 * GdxGame.HD_X_RATIO, 7 * GdxGame.HD_Y_RATIO);
		beetle1XY = new Vector2(100 * GdxGame.HD_X_RATIO,
				2 * GdxGame.HD_Y_RATIO);
		beetle2XY = new Vector2(180 * GdxGame.HD_X_RATIO,
				7 * GdxGame.HD_Y_RATIO);
		coockieXY = new Vector2(268 * GdxGame.HD_X_RATIO,
				16 * GdxGame.HD_Y_RATIO);

		explXY = new Vector2(30 * GdxGame.HD_X_RATIO, 97 * GdxGame.HD_Y_RATIO);
		egg1XY = new Vector2(115 * GdxGame.HD_X_RATIO, 100 * GdxGame.HD_Y_RATIO);
		egg2XY = new Vector2(198 * GdxGame.HD_X_RATIO, 100 * GdxGame.HD_Y_RATIO);

		backWH = new Vector2(backRegion.getRegionWidth(),
				backRegion.getRegionHeight());
		backArrowWH = new Vector2(backArrowRegion.getRegionWidth(),
				backArrowRegion.getRegionHeight());
		this.setWidth(backWH.x);
		this.setHeight(backWH.y);

		laserWH = new Vector2(laserRegion.getRegionWidth(),
				laserRegion.getRegionHeight());
		beetle1WH = new Vector2(beetle1Region.getRegionWidth(),
				beetle1Region.getRegionHeight());
		beetle2WH = new Vector2(beetle2Region.getRegionWidth(),
				beetle2Region.getRegionHeight());
		coockieWH = new Vector2(coockieRegion.getRegionWidth(),
				coockieRegion.getRegionHeight());
		explWH = new Vector2(explRegion.getRegionWidth(),
				explRegion.getRegionHeight());
		egg1WH = new Vector2(egg1Region.getRegionWidth(),
				egg1Region.getRegionHeight());
		egg2WH = new Vector2(egg2Region.getRegionWidth(),
				egg2Region.getRegionHeight());

		backOrXY = new Vector2(backWH.x / 2, backWH.y / 2);
		backArrowOrXY = new Vector2(6, 11);
		laserOrXY = new Vector2(backWH.x - laserWH.x, backWH.y - laserWH.y);
		beetle1OrXY = new Vector2(backWH.x - beetle1WH.x, backWH.y
				- beetle1WH.y);
		beetle2OrXY = new Vector2(backWH.x - beetle2WH.x, backWH.y
				- beetle2WH.y);
		coockieOrXY = new Vector2(backWH.x - coockieWH.x, backWH.y
				- coockieWH.y);
		explOrXY = new Vector2(backWH.x - explWH.x, backWH.y - explWH.y);
		egg1OrXY = new Vector2(backWH.x - egg1WH.x, backWH.y - egg1WH.y);
		egg2OrXY = new Vector2(backWH.x - egg2WH.x, backWH.y - egg2WH.y);

		GameScreen.rootGroup.addActorAfter(this, this.bPanel);
		this.bPanel.setSkin();
	}

	@Override
	protected void drawSkin(SpriteBatch batch, float parentAlpha) {
		batch.setColor(this.getColor());

		batch.draw(this.backRegion, this.getX() + backXY.x, this.getY() + backXY.y,
				backOrXY.x, backOrXY.y, backWH.x, backWH.y, getScaleX(), getScaleY(),
				getRotation());

		arrowRotation = (180 / mInvMinX) * (this.getX() - mInvMinX);

		batch.draw(this.backArrowRegion, this.getX() + backArrowXY.x, this.getY()
				+ backArrowXY.y, backArrowOrXY.x, backArrowOrXY.y,
				backArrowWH.x, backArrowWH.y, getScaleX(), getScaleY(), arrowRotation);

		batch.setColor(this.colorLaser);
		batch.draw(this.laserRegion, this.getX() + laserXY.x, this.getY() + laserXY.y,
				laserOrXY.x, laserOrXY.y, laserWH.x, laserWH.y, getScaleX(), getScaleY(),
				getRotation());

		batch.setColor(this.colorBoom);
		batch.draw(this.beetle1Region, this.getX() + beetle1XY.x, this.getY()
				+ beetle1XY.y, beetle1OrXY.x, beetle1OrXY.y, beetle1WH.x,
				beetle1WH.y, getScaleX(), getScaleY(), getRotation());

		batch.setColor(this.colorKarate);
		batch.draw(this.beetle2Region, this.getX() + beetle2XY.x, this.getY()
				+ beetle2XY.y, beetle2OrXY.x, beetle2OrXY.y, beetle2WH.x,
				beetle2WH.y, getScaleX(), getScaleY(), getRotation());

		batch.setColor(this.colorCoockie);
		batch.draw(this.coockieRegion, this.getX() + coockieXY.x, this.getY()
				+ coockieXY.y, coockieOrXY.x, coockieOrXY.y, coockieWH.x,
				coockieWH.y, getScaleX(), getScaleY(), getRotation());

		batch.setColor(this.colorExpl);
		batch.draw(this.explRegion, this.getX() + explXY.x, this.getY() + explXY.y,
				explOrXY.x, explOrXY.y, explWH.x, explWH.y, getScaleX(), getScaleY(),
				getRotation());

		batch.setColor(this.colorEgg1);
		batch.draw(this.egg1Region, this.getX() + egg1XY.x, this.getY() + egg1XY.y,
				egg1OrXY.x, egg1OrXY.y, egg1WH.x, egg1WH.y, getScaleX(), getScaleY(),
				getRotation());

		batch.setColor(this.colorEgg2);
		batch.draw(this.egg2Region, this.getX() + egg2XY.x, this.getY() + egg2XY.y,
				egg2OrXY.x, egg2OrXY.y, egg2WH.x, egg2WH.y, getScaleX(), getScaleY(),
				getRotation());
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.backRegion != null && this.backArrowRegion != null
				&& this.laserRegion != null && this.beetle1Region != null
				&& this.beetle2Region != null && this.coockieRegion != null
				&& this.explRegion != null && this.egg1Region != null
				&& this.egg2Region != null) {

			//this.drawSkin(batch, parentAlpha);
			//super.draw(batch, parentAlpha);
		}

		//if (coockieCount == null) {
		//	initTxtObjects();
		//}

		//this.bPanel.draw(batch, parentAlpha);
	}

	public InvState getState() {
		return this.mInvState;
	}

	public void setState(InvState newState) {
		onStateChanged(this.mInvState, newState);
		this.mInvState = newState;
	}

	private void playOpenAnimation() {
		float time = Math.abs(mInvMinX / (mInvMinX - this.getX())) / 8;

		MoveToAction open_moveTo0 = moveTo(0, mInvY, time);
		this.addAction(open_moveTo0);

		GdxGame.tutorialObject.initTutorial(Texts.TUTORIAL_MESSAGE_INV, "inv",
				240, 530, 420, ArrowPosEnum.BOTTOM, 0.075f,
				GameScreen.rootGroup);

		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				setState(InvState.OPENED);
			}
		}, (int) (time * 1000));
	}

	private void playCloseAnimation() {
		float time = Math.abs(mInvMinX / this.getX()) / 12;

		MoveToAction open_moveTo0 = moveTo(mInvMinX, mInvY, time);
		this.addAction(open_moveTo0);

		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				setState(InvState.CLOSED);

				if (!GameScreen.pauseTxt.isVisible() && !GameScreen.failed.isVisible()
						&& !GameScreen.complete.isVisible()) {
					GameScreen.backgroundComplete.setVisible(false);
				}
			}
		}, (int) (time * 1000));
	}

	private void playAutoCloseAnimation() {
		float time = 0.5f;

		AlphaAction f = fadeOut(0.25f);
		GameScreen.backgroundComplete.addAction(f);

		MoveToAction open_moveTo0 = moveTo(mInvMinX, mInvY, time);
		this.addAction(open_moveTo0);

		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				setState(InvState.CLOSED);

				if (!GameScreen.pauseTxt.isVisible() && !GameScreen.failed.isVisible()
						&& !GameScreen.complete.isVisible()) {
					GameScreen.backgroundComplete.setVisible(false);
				}
			}
		}, (int) (time * 1000));
	}

	private void playAutoOpenAnimation() {
		float time = 0.5f;

		AlphaAction f = fadeIn(0.25f);
		GameScreen.backgroundComplete.addAction(f);
		GameScreen.backgroundComplete.setVisible(true);

		MoveToAction open_moveTo0 = moveTo(0, mInvY, time);
		this.addAction(open_moveTo0);

		GdxGame.tutorialObject.initTutorial(Texts.TUTORIAL_MESSAGE_INV, "inv",
				240, 530, 420, ArrowPosEnum.BOTTOM, 0.075f,
				GameScreen.rootGroup);

		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				setState(InvState.FOREVER_OPENED);
			}
		}, (int) (time * 1000));
	}

	public void onStateChanged(InvState prevState, InvState newState) {
		this.clearActions();
		this.bPanel.setVisible(true);

		if (newState == InvState.CLOSED) {
			this.setPositionXY(mInvMinX, mInvY);

			if (!GameScreen.pauseTxt.isVisible() && !GameScreen.failed.isVisible()
					&& !GameScreen.complete.isVisible()) {
				GameScreen.backgroundComplete.setVisible(false);
			}
		}

		else if (newState == InvState.FOREVER_OPENED) {
			this.bPanel.setVisible(false);
			this.setPositionXY(0, mInvY);
		}

		else if (mInvStateBeforeDrag == InvState.CLOSED
				&& newState == InvState.ANIMATING) {

			if (this.getX() >= mInvMinX * 0.8f) {
				playOpenAnimation();
			}

			else {
				playCloseAnimation();

				AlphaAction f = fadeOut(0.25f);
				GameScreen.backgroundComplete.addAction(f);
			}

		}

		else if (mInvStateBeforeDrag == InvState.OPENED
				&& newState == InvState.ANIMATING) {

			if (this.getX() >= mInvMinX * 0.05f) {
				playOpenAnimation();
				this.mResetingToOpened = true;

				AlphaAction f = fadeIn(0.25f);
				GameScreen.backgroundComplete.addAction(f);
			}

			else {
				playCloseAnimation();
			}
		}

		else if (mInvStateBeforeDrag == InvState.AUTO_CLOSE
				&& newState == InvState.ANIMATING) {
			playAutoCloseAnimation();
		}

		else if (newState == InvState.AUTO_OPEN) {
			playAutoOpenAnimation();
			setAutoOpenUsed(true);

			this.game.gameScreen.finish_level_btn.setVisible(true);
			this.game.gameScreen.finish_game_back.setVisible(true);
		}
	}

	/*@Override
	public boolean touchDown(float arg_x, float arg_y, int arg2) {
		if (getState() == InvState.CLOSED
				&& !GameScreen.backgroundComplete.isVisible()) {
			this.mInvStateBeforeDrag = getState();
			setState(InvState.DRAGGING);
			startDragX = arg_x;

			GameScreen.backgroundComplete.getColor().a = 0;
			GameScreen.backgroundComplete.setVisible(true);

			AlphaAction f = fadeIn(0.25f);
			GameScreen.backgroundComplete.addAction(f);
		}

		else if (getState() == InvState.OPENED
				&& GameScreen.backgroundComplete.isVisible()) {
			this.mInvStateBeforeDrag = getState();
			setState(InvState.DRAGGING);
			startDragX = arg_x;

			GameScreen.backgroundComplete.getColor().a = 1;
			GameScreen.backgroundComplete.setVisible(true);

			AlphaAction f = fadeOut(0.25f);
			GameScreen.backgroundComplete.addAction(f);
		}

		else if (getState() == InvState.FOREVER_OPENED) {
			this.mResetingToOpened = true;
		}

		return true;
	}*/

	public void setBManager(BeetleManager b) {
		this.bManager = b;
	}

	private void showShopYesNoBox(int shopId) {
		ShopScreen.mBtnClickedId = shopId;
		this.game.gameScreen.showYesNoBox(shopId);
	}

	public void activeBonusesAdd(AvaliableBonuses a) {
		this.activeBonuses.add(a);

		if (a.equals(AvaliableBonuses.LASER)) {
			this.colorLaser = this.colorBlocked;
		}

		if (a.equals(AvaliableBonuses.BOOM_BUG)) {
			this.colorBoom = this.colorBlocked;
		}

		if (a.equals(AvaliableBonuses.KARATE_BUG)) {
			this.colorKarate = this.colorBlocked;
		}

		if (a.equals(AvaliableBonuses.COOCKIE)) {
			this.colorCoockie = this.colorBlocked;
		}

		if (a.equals(AvaliableBonuses.EXPLOSION_RADIUS)) {
			this.colorExpl = this.colorBlocked;
		}

		if (a.equals(AvaliableBonuses.BONUS_EGG1)) {
			this.colorEgg1 = this.colorBlocked;
		}

		if (a.equals(AvaliableBonuses.BONUS_EGG2)) {
			this.colorEgg2 = this.colorBlocked;

			if (!this.activeBonuses.contains(AvaliableBonuses.BONUS_EGG1)) {
				this.activeBonuses.add(AvaliableBonuses.BONUS_EGG1);
				this.colorEgg1 = this.colorBlocked;
			}
		}

		this.bPanel.Update();
	}

	private boolean checkElementsClick(float x, float y) {
		if (x > laserXY.x && y > laserXY.y && x < laserXY.x + laserWH.x
				&& y < laserXY.y + laserWH.y) {

			final int a = this.game.getLaserOwnedItems();
			if (!activeBonuses.contains(AvaliableBonuses.LASER) && a > 0) {
				this.game.updateItem("android.laser.purchased", 1);

				params.clear();
				params.put("purchased", "android.laser.purchased");
				params.put("level", String.valueOf(GameScreen.levelNumb));
				this.game.flurryAgentLogEvent("Purchased", params);

				Slingshot.toggleShopLaser(true);
				activeBonusesAdd(AvaliableBonuses.LASER);

				if (this.game.gameScreen.finish_game_back.isVisible()) {
					this.game.gameScreen.retryLevel();
					initAutoClose();
				}

			} else if (a <= 0) {
				showShopYesNoBox(2);
			}

			return true;
		}

		if (x > coockieXY.x && y > coockieXY.y && x < coockieXY.x + coockieWH.x
				&& y < coockieXY.y + coockieWH.y) {

			final int a = this.game.getCoockieOwnedItems();
			if (!this.bManager.isBeetlesInRage() && a > 0
					&& !activeBonuses.contains(AvaliableBonuses.COOCKIE)) {
				this.game.updateItem("android.coockie.purchased", 1);

				params.clear();
				params.put("purchased", "android.coockie.purchased");
				params.put("level", String.valueOf(GameScreen.levelNumb));
				this.game.flurryAgentLogEvent("Purchased", params);

				this.bManager.setBeetleEnrageTimer(0);
				this.bManager.tryActivateRage();
				activeBonusesAdd(AvaliableBonuses.COOCKIE);

				if (this.game.gameScreen.finish_game_back.isVisible()) {
					this.game.gameScreen.retryLevel();
					initAutoClose();
				}

			} else if (a <= 0) {
				showShopYesNoBox(1);
			} else if (a > 0 && this.game.gameScreen.finish_game_back.isVisible()
					&& !activeBonuses.contains(AvaliableBonuses.COOCKIE)) {
				this.game.updateItem("android.coockie.purchased", 1);

				params.clear();
				params.put("purchased", "android.coockie.purchased");
				params.put("level", String.valueOf(GameScreen.levelNumb));
				this.game.flurryAgentLogEvent("Purchased", params);

				this.bManager.setBeetleEnrageTimer(0);
				this.bManager.tryActivateRage();
				activeBonusesAdd(AvaliableBonuses.COOCKIE);

				this.game.gameScreen.retryLevel();
				initAutoClose();
			}

			return true;
		}

		if (x > explXY.x && y > explXY.y && x < explXY.x + explWH.x
				&& y < explXY.y + explWH.y) {

			final int a = this.game.getExplOwnedItems();

			if (!activeBonuses.contains(AvaliableBonuses.EXPLOSION_RADIUS)
					&& a > 0 && this.bManager.checkEggBeetles()) {
				this.game.updateItem("android.expl.purchased", 1);

				params.clear();
				params.put("purchased", "android.expl.purchased");
				params.put("level", String.valueOf(GameScreen.levelNumb));
				this.game.flurryAgentLogEvent("Purchased", params);

				GdxGame.mInventory
						.setBonusRadius(Constants.EXPLOSION_CHAIN_LENGTH_BONUS);
				activeBonusesAdd(AvaliableBonuses.EXPLOSION_RADIUS);

				if (this.game.gameScreen.finish_game_back.isVisible()) {
					this.game.gameScreen.retryLevel();
					initAutoClose();
				}

			} else if (a <= 0) {
				showShopYesNoBox(5);
			}

			return true;
		}

		if (x > egg1XY.x && y > egg1XY.y && x < egg1XY.x + egg1WH.x
				&& y < egg1XY.y + egg1WH.y) {

			final int a = this.game.getEgg1OwnedItems();

			if (!activeBonuses.contains(AvaliableBonuses.BONUS_EGG1)
					&& !activeBonuses.contains(AvaliableBonuses.BONUS_EGG2)
					&& a > 0 && this.bManager.checkEggBeetles()) {
				this.game.updateItem("android.egg1.purchased", 1);

				params.clear();
				params.put("purchased", "android.egg1.purchased");
				params.put("level", String.valueOf(GameScreen.levelNumb));
				this.game.flurryAgentLogEvent("Purchased", params);

				GdxGame.mInventory.setBonusEggs(Constants.BONUS1_EGGS_QUANTITY);
				activeBonusesAdd(AvaliableBonuses.BONUS_EGG1);

				if (this.game.gameScreen.finish_game_back.isVisible()) {
					this.game.gameScreen.retryLevel();
					initAutoClose();
				}

			} else if (a <= 0) {
				showShopYesNoBox(6);
			}

			return true;
		}

		if (x > egg2XY.x && y > egg2XY.y && x < egg2XY.x + egg2WH.x
				&& y < egg2XY.y + egg2WH.y) {

			final int a = this.game.getEgg2OwnedItems();

			if (!activeBonuses.contains(AvaliableBonuses.BONUS_EGG2) && a > 0
					&& this.bManager.checkEggBeetles()) {
				this.game.updateItem("android.egg2.purchased", 1);

				params.clear();
				params.put("purchased", "android.egg2.purchased");
				params.put("level", String.valueOf(GameScreen.levelNumb));
				this.game.flurryAgentLogEvent("Purchased", params);

				GdxGame.mInventory.setBonusEggs(Constants.BONUS2_EGGS_QUANTITY);
				activeBonusesAdd(AvaliableBonuses.BONUS_EGG2);

				if (this.game.gameScreen.finish_game_back.isVisible()) {
					this.game.gameScreen.retryLevel();
					initAutoClose();
				}

			} else if (a <= 0) {
				showShopYesNoBox(7);
			}

			return true;
		}

		this.activeBName = "";
		if (this.bManager.getActiveBeetle() != null) {
			if (GameScreen.bManager.getActiveBeetle().getState() == BeetleState.INFLY
					|| GameScreen.bManager.getActiveBeetle().getState() == BeetleState.ANIM_TO_FLY) {
				return false;
			}

			this.activeBName = this.bManager.getActiveBeetle().getName();
		}

		if (x > beetle1XY.x && y > beetle1XY.y && x < beetle1XY.x + beetle1WH.x
				&& y < beetle1XY.y + beetle1WH.y) {

			final int a = this.game.getBeetle1OwnedItems();

			if (!this.activeBName.equals("boom-shop") && a > 0
					&& !activeBonuses.contains(AvaliableBonuses.BOOM_BUG)) {
				if ((this.activeBName.equals("") && getState() == InvState.FOREVER_OPENED)
						|| this.bManager.getActiveBeetle() != null) {
					this.game.updateItem("android.beetle1.purchased", 1);

					params.clear();
					params.put("purchased", "android.beetle1.purchased");
					params.put("level", String.valueOf(GameScreen.levelNumb));
					this.game.flurryAgentLogEvent("Purchased", params);

					activeBonusesAdd(AvaliableBonuses.BOOM_BUG);

					this.bManager.onShopBoomButtonClicked();

					if (getState() == InvState.FOREVER_OPENED) {
						initAutoClose();
					}
				}
			} else if (a <= 0) {
				showShopYesNoBox(4);
			}

			return true;
		}

		if (x > beetle2XY.x && y > beetle2XY.y && x < beetle2XY.x + beetle2WH.x
				&& y < beetle2XY.y + beetle2WH.y) {

			final int a = this.game.getBeetle2OwnedItems();

			if (!this.activeBName.equals("karate-shop") && a > 0
					&& !activeBonuses.contains(AvaliableBonuses.KARATE_BUG)) {
				if ((this.activeBName.equals("") && getState() == InvState.FOREVER_OPENED)
						|| this.bManager.getActiveBeetle() != null) {
					this.game.updateItem("android.beetle2.purchased", 1);

					params.clear();
					params.put("purchased", "android.beetle2.purchased");
					params.put("level", String.valueOf(GameScreen.levelNumb));
					this.game.flurryAgentLogEvent("Purchased", params);

					activeBonusesAdd(AvaliableBonuses.KARATE_BUG);
					this.bManager.onShopKarateButtonClicked();

					if (getState() == InvState.FOREVER_OPENED) {
						initAutoClose();
					}
				}
			} else if (a <= 0) {
				showShopYesNoBox(3);
			}

			return true;
		}

		return false;
	}

	public void initAutoClose() {
		mInvStateBeforeDrag = InvState.AUTO_CLOSE;
		setState(InvState.ANIMATING);
		this.game.gameScreen.isEndLevel = false;

		this.game.gameScreen.finish_level_btn.setVisible(false);
		this.game.gameScreen.finish_game_back.setVisible(false);
	}

	/*@Override
	public void touchDragged(float arg_x, float arg_y, int arg2) {
		if (getState() == InvState.DRAGGING) {
			this.newPosX = Gdx.input.getX() * 480 * GdxGame.HD_X_RATIO
					/ Gdx.graphics.getWidth() - startDragX;

			if (newPosX < 0 && newPosX > mInvMinX) {
				this.setX(newPosX);
			}
		}
	}*/

	/*@Override
	public void touchUp(float arg_x, float arg_y, int arg2) {
		if (getState() == InvState.DRAGGING) {
			setState(InvState.ANIMATING);
			startDragX = 0;
		}

		if (this.mResetingToOpened
				&& !this.game.getScreen().equals(this.game.shopScreen)) {
			this.mResetingToOpened = false;
			checkElementsClick(arg_x, arg_y);
		}
	}*/

	public ArrayList<AvaliableBonuses> getActiveBonuses() {
		return activeBonuses;
	}

	public void activeBonusesRemove(AvaliableBonuses a) {
		this.activeBonuses.remove(a);

		if (a.equals(AvaliableBonuses.LASER)) {
			this.colorLaser = this.colorNormal;
		}

		if (a.equals(AvaliableBonuses.BOOM_BUG)) {
			this.colorBoom = this.colorNormal;
		}

		if (a.equals(AvaliableBonuses.KARATE_BUG)) {
			this.colorKarate = this.colorNormal;
		}

		if (a.equals(AvaliableBonuses.COOCKIE)) {
			this.colorCoockie = this.colorNormal;
		}

		if (a.equals(AvaliableBonuses.EXPLOSION_RADIUS)) {
			this.colorExpl = this.colorNormal;
		}

		if (a.equals(AvaliableBonuses.BONUS_EGG1)) {
			this.colorEgg1 = this.colorNormal;
		}

		if (a.equals(AvaliableBonuses.BONUS_EGG2)) {
			this.colorEgg2 = this.colorNormal;
		}

		this.bPanel.Update();
	}

	public void resetActiveBonuses() {
		bManager.setBeetleEnrageTimer(3);

		Slingshot.toggleShopLaser(false);
		Slingshot.resetLaser();

		this.setBonusRadius(0);

		this.setBonusEggs(0);

		this.activeBonuses.clear();

		this.colorLaser = this.colorNormal;
		this.colorBoom = this.colorNormal;
		this.colorKarate = this.colorNormal;
		this.colorCoockie = this.colorNormal;
		this.colorExpl = this.colorNormal;
		this.colorEgg1 = this.colorNormal;
		this.colorEgg2 = this.colorNormal;

		this.bPanel.Update();
	}
}
