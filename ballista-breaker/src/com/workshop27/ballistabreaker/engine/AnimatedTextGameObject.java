package com.workshop27.ballistabreaker.engine;

import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;

public class AnimatedTextGameObject extends TextGameObject {
	// Variables
	private final Matrix4 matrix = new Matrix4();
	private final Matrix4 tmpMatrix = new Matrix4();

	private boolean isMatrixChanged = false;
	private boolean isFilterSet = false;

	public AnimatedTextGameObject(String name, boolean addToRoot,
			String fontType, int fontSize) {
		super(name, addToRoot, fontType, fontSize);
	}

	public AnimatedTextGameObject(String name, boolean addToRoot, int fontSize) {
		super(name, addToRoot, fontSize);
	}

	public void setFiltering(String fontType, TextureFilter f) {
		if (fontType.length() == 0) {
			setFiltering(f);
		}

		if (fontType.equals("popup") && this.activeFontSize == 20) {
			this.bitmapFontTextureRegionPopupMessage20.getTexture().setFilter(
					f, f);
		}

		else if (fontType.equals("tutorial") && this.activeFontSize == 30) {
			this.bitmapFontTextureRegionTutorial30.getTexture().setFilter(f, f);
		}

		else if (fontType.equals("tutorial") && this.activeFontSize == 45) {
			this.bitmapFontTextureRegionTutorial45.getTexture().setFilter(f, f);
		}
	}

	public void setFiltering(TextureFilter f) {
		switch (this.activeFontSize) {

		case 22:
			this.bitmapFontTextureRegion22.getTexture().setFilter(f, f);
			break;

		case 30:
			this.bitmapFontTextureRegion30.getTexture().setFilter(f, f);
			break;

		case 45:
			this.bitmapFontTextureRegion45.getTexture().setFilter(f, f);
			break;

		case 60:
			this.bitmapFontTextureRegion60.getTexture().setFilter(f, f);
			break;

		default:
			break;
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (this.text.length() > 0) {
			this.textCache.setPosition(this.getX() - this.textBounds.width / 2,
					this.getY() + this.textBounds.height / 2);
			this.setOrigin(this.getX(), this.getY());

			matrix.idt();
			matrix.translate(getOriginX(), getOriginY(), 0);

			this.isMatrixChanged = false;

			if (getRotation() != 0) {
				matrix.rotate(0, 0, 1, getRotation());
				this.isMatrixChanged = true;
			}

			if (getScaleX() != 1 || getScaleY() != 1) {
				matrix.scale(getScaleX(), getScaleY(), 1);
				this.isMatrixChanged = true;
			}

			matrix.translate(-getOriginX(), -getOriginY(), 0);
			this.textCache.setColor(this.getColor());

			if (this.isMatrixChanged) {
				if (!this.isFilterSet) {
					this.isFilterSet = true;

					setFiltering(this.fontType, TextureFilter.Linear);
				}

				this.tmpMatrix.idt();

				batch.setTransformMatrix(matrix);
				this.textCache.draw(batch);

				batch.setTransformMatrix(tmpMatrix);
				this.isMatrixChanged = false;
			}

			else {
				if (this.isFilterSet) {
					this.isFilterSet = false;
					setFiltering(this.fontType, TextureFilter.Nearest);
				}

				this.textCache.draw(batch);
			}
		}
	}
}
