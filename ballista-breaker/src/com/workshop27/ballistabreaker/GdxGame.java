package com.workshop27.ballistabreaker;

import aurelienribon.tweenengine.Tween;
import aurelienribon.tweenengine.TweenManager;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.workshop27.ballistabreaker.constants.Constants;
import com.workshop27.ballistabreaker.engine.*;
import com.workshop27.ballistabreaker.screens.*;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

public class GdxGame extends Game implements ApplicationListener {

	public MainMenuScreen mainMenuScreen;
	public StageScreen stageScreen;
	public GameScreen gameScreen;
	public LoadingScreen loadingScreen;
	public TutorialScreen tutorialScreen;
	public ShopScreen shopScreen;
	public SplashScreen splashScreen;

	public static FilePreferences files;

	public static int dayMinutes = 0;

	public static CameraAccessor camera;
	public static Stage beetleStage;

	public static int VIRTUAL_GAME_WIDTH;
	public static int VIRTUAL_GAME_HEIGHT;
	public static float VIRTUAL_X_RATIO;
	public static float VIRTUAL_Y_RATIO;
	public static int REAL_GAME_WIDTH_NO_FRAMES;
	public static int REAL_GAME_HEIGHT_NO_FRAMES;

	public static boolean enableParallax = false;

	public static TweenManager tweenManager;

	public boolean regularTutorial = false;
	public boolean fanTutorial = false;
	public boolean minerTutorial = false;
	public boolean boomTutorial = false;
	public boolean karateTutorial = false;

	public static boolean isShopUsed = false;

	public static float HD_X_RATIO;
	public static float HD_Y_RATIO;

	ActionResolver actionResolver;
	ShopActionResolver shopActionResolver;
	public static float lastPosition;
	public static boolean isStageMoving;

	public static TextGameObject points; // ---
	public static AnimatedTextGameObject level_numb;

	public static int stageNumber = 1;
	public static int collectedStars;

	public static BBInventory mInventory;

	public static int SCREEN_HEIGHT;
	public static int SCREEN_WIDTH;
	public static int START_X;
	public static int START_Y;
	public static boolean isStageDragged;

	public static boolean showPrizeMsg = false;

	public static boolean showSocialPrizeMsg = false;
	public static int socialPrizeId = -1;
	// public static int action = -1;

	public static ShowTutorialObject tutorialObject;

	public boolean isBonusBeetleTutorialShown = false;

	public static boolean isShopSpiderAnimationShown;

	// rest сomitted for clarity

	public GdxGame(ActionResolver resolver, ShopActionResolver shop_resolver) {
		this.actionResolver = resolver;
		this.shopActionResolver = shop_resolver;

		files = new FilePreferences();
		files.init();
	}

	@Override
	public void render() {
		super.render();

		tutorialObject.render();
	}

	@Override
	public void create() {
		// action = -1;
		isShopSpiderAnimationShown = false;

		tweenManager = new TweenManager();
		Tween.setWaypointsLimit(30);
		TextGameObject.initFonts();

		if (Constants.IS_HD) {
			HD_X_RATIO = 1.66666666f;
			HD_Y_RATIO = 1.6f;

			VIRTUAL_GAME_WIDTH = 800;
			VIRTUAL_GAME_HEIGHT = 1280;
		} else {
			HD_X_RATIO = 1;
			HD_Y_RATIO = 1;

			VIRTUAL_GAME_WIDTH = 480;
			VIRTUAL_GAME_HEIGHT = 800;
		}

		try {
			this.regularTutorial = GdxGame.files.getOptionsAndProgress(
					"tutorial", "regular").equals("yes");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			this.fanTutorial = GdxGame.files.getOptionsAndProgress("tutorial",
					"fan").equals("yes");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			this.minerTutorial = GdxGame.files.getOptionsAndProgress(
					"tutorial", "miner").equals("yes");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			this.boomTutorial = GdxGame.files.getOptionsAndProgress("tutorial",
					"boom").equals("yes");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			this.karateTutorial = GdxGame.files.getOptionsAndProgress(
					"tutorial", "karate").equals("yes");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			GdxGame.isShopUsed = GdxGame.files.getOptionsAndProgress("shop",
					"shop").equals("used");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			this.isBonusBeetleTutorialShown = GdxGame.files
					.getOptionsAndProgress("tutorial", "bonus_bug").equals(
							"yes");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Pixmap.setBlending(Blending.None);

		ImageCache.create();
		ImageCache.load("loading");
		ImageCache.load("splash");

		beetleStage = new Stage(VIRTUAL_GAME_WIDTH, VIRTUAL_GAME_HEIGHT,
				true);
		camera = new CameraAccessor(this, VIRTUAL_GAME_WIDTH,
				VIRTUAL_GAME_HEIGHT);

		camera.position.set(VIRTUAL_GAME_WIDTH / 2, VIRTUAL_GAME_HEIGHT / 2, 0);

		beetleStage.setCamera(camera);

		mInventory = new BBInventory(this);
		mInventory.resetTxtFields();
		mInventory.loadSkinOnDraw = true;
		mInventory.setTouchable(Touchable.enabled);
		mInventory.folderName = "inventory";

		Gdx.input.setInputProcessor(GdxGame.beetleStage);
		Gdx.input.setCatchBackKey(true);

		tutorialObject = new ShowTutorialObject(this);

		loadingScreen = new LoadingScreen(this);
		loadingScreen.setNextScreen("logo");
		changeScreen("loading");

		if (GameScreen.objectToDestroy != null) {
			GameScreen.objectToDestroy.dispose();
			GameScreen.objectToDestroy.pixmapHelper.dispose();
			GameScreen.objectToDestroy.pixmapHelper = null;
			GameScreen.objectToDestroy = null;
		}

		ImageCache.loadSounds();

		Tween.registerAccessor(GameObject.class, new GameObjectAccessor());
		Tween.registerAccessor(ParallaxCamera.class, new CameraAccessor());

		if (actionResolver != null && actionResolver.checkInetConnection()) {
			actionResolver.bootstrap();
		}

		points = new TextGameObject("points", false, 22);
		level_numb = new AnimatedTextGameObject("level_numb", false, 22);

		lastPosition = GdxGame.VIRTUAL_GAME_WIDTH / 2.0f;

		float ratio = ((float) Gdx.graphics.getHeight() / Gdx.graphics
				.getWidth());

		if (ratio < 1.66f) {
			SCREEN_HEIGHT = Gdx.graphics.getHeight();
			SCREEN_WIDTH = (int) (SCREEN_HEIGHT * 0.6);
			START_X = (Gdx.graphics.getWidth() - SCREEN_WIDTH) / 2;
		} else {
			SCREEN_WIDTH = Gdx.graphics.getWidth();
			SCREEN_HEIGHT = (int) (SCREEN_WIDTH / 0.6);
			START_Y = (Gdx.graphics.getHeight() - SCREEN_HEIGHT) / 2;
		}
	}

	public void initShop() {
		if (shopActionResolver != null) {
			shopActionResolver.initShop();
		}
	}

	public void showShop() {
		if (shopActionResolver != null) {
			shopActionResolver.show();
		}
	}

	public void updateItem(String productId, int quantity) {
		if (shopActionResolver != null) {
			shopActionResolver.updateItem(productId, quantity);
		}
	}

	public void buyItem(int buyId) {
		if (shopActionResolver != null) {
			shopActionResolver.buyItem(buyId);
		}
	}

	public int getLaserOwnedItems() {
		if (shopActionResolver != null) {
			return shopActionResolver.getLaserOwnedItems();
		}

		return 0;
	}

	public int getBeetle1OwnedItems() {
		if (shopActionResolver != null) {
			return shopActionResolver.getBeetle1OwnedItems();
		}

		return 0;
	}

	public int getBeetle2OwnedItems() {
		if (shopActionResolver != null) {
			return shopActionResolver.getBeetle2OwnedItems();
		}

		return 0;
	}

	public int getCoockieOwnedItems() {
		if (shopActionResolver != null) {
			return shopActionResolver.getCoockieOwnedItems();
		}

		return 0;
	}

	public int getExplOwnedItems() {
		if (shopActionResolver != null) {
			return shopActionResolver.getExplOwnedItems();
		}

		return 0;
	}

	public int getEgg1OwnedItems() {
		if (shopActionResolver != null) {
			return shopActionResolver.getEgg1OwnedItems();
		}

		return 0;
	}

	public int getEgg2OwnedItems() {
		if (shopActionResolver != null) {
			return shopActionResolver.getEgg2OwnedItems();
		}

		return 0;
	}

	public boolean checkConnection() {
		if (actionResolver != null) {
			return actionResolver.checkInetConnection();
		} else {
			return false;
		}
	}

	public void saveScore(int score) {
		// Submit to score loop
		if (actionResolver != null) {
			actionResolver.submitScore(score);
		}
	}

	public void showScoreloop() {
		if (actionResolver != null) {
			actionResolver.showScoreloop();
		}
	}

	public void refreshScore() {
		// Submit to score loop
		if (actionResolver != null) {
			actionResolver.refreshScores();
		}
	}

	public void startFacebook(String message, int action) {
		if (actionResolver != null) {
			actionResolver.startFacebook(message, action);
		}
	}

	public void startTwitter(String message, int action) {
		if (actionResolver != null) {
			actionResolver.startTwitter(message, action);
		}
	}

	public String getPostTime(String socialNetwork) {
		if (shopActionResolver != null) {
			return shopActionResolver.getPostTime(socialNetwork);
		}
		return "0";

	}

	public String getBonusGenerateTime(String socialNetwork) {
		if (shopActionResolver != null) {
			return shopActionResolver.getBonusGenerateTime(socialNetwork);
		}
		return "0";

	}

	public String getBonusId(String socialNetwork) {
		if (shopActionResolver != null) {
			return shopActionResolver.getBonusId(socialNetwork);
		}
		return "0";

	}

	public void generateNewDayBonus(String socialNetwork) {
		if (shopActionResolver != null) {
			shopActionResolver.generateNewDayBonus(socialNetwork);
		}

	}

	public void startFlurry() {
		if (actionResolver != null) {
			actionResolver.startFlurry();
		}
	}

	public void startBrowser(String url) {
		if (actionResolver != null) {
			actionResolver.startBrowser(url);
		}
	}

	public void stopFlurry() {
		if (actionResolver != null) {
			actionResolver.stopFlurry();
		}
	}

	public void flurryAgentLogEvent(String eventId) {
		if (actionResolver != null) {
			actionResolver.flurryAgentLogEvent(eventId);
		}
	}

	public void flurryAgentLogEvent(String eventId, boolean timed) {
		if (actionResolver != null) {
			actionResolver.flurryAgentLogEvent(eventId, timed);
		}
	}

	public void flurryAgentLogEvent(String eventId,
			Map<String, String> parameters) {
		if (actionResolver != null) {
			actionResolver.flurryAgentLogEvent(eventId, parameters);
		}
	}

	public void flurryAgentLogEvent(String eventId,
			Map<String, String> parameters, boolean timed) {
		if (actionResolver != null) {
			actionResolver.flurryAgentLogEvent(eventId, parameters, timed);
		}
	}

	public void flurryAgentEndTimedEvent(String eventId) {
		if (actionResolver != null) {
			actionResolver.flurryAgentEndTimedEvent(eventId);
		}
	}

	public void flurryAgentOnPageView() {
		if (actionResolver != null) {
			actionResolver.flurryAgentOnPageView();
		}
	}

	public void initScreens() {
		mainMenuScreen = new MainMenuScreen(this);
		stageScreen = new StageScreen(this);
		gameScreen = new GameScreen(this);
		splashScreen = new SplashScreen(this);
	}

	public void changeScreenAfter(String screen, float time) {
		final String s = screen;
		final int t = (int) (time * 1000);

		final Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				changeScreen(s);
				timer.cancel();
			}
		}, t, 1000);
	}

	public void changeScreen(String screen) {
		tutorialObject.closeTutorialFast();
		tutorialObject.clearTimers();

		if (screen.equals("menu")) {
			setScreen(mainMenuScreen);
		} else if (screen.equals("loading")) {
			setScreen(loadingScreen);
		} else if (screen.equals("stage")) {
			System.gc();
			setScreen(stageScreen);
		} else if (screen.equals("game")) {
			System.gc();
			setScreen(gameScreen);
		} else if (screen.equals("shop")) {
			setScreen(shopScreen);
		}
	}

	@Override
	public void resize(int width, int height) {
		beetleStage.setViewport(VIRTUAL_GAME_WIDTH, VIRTUAL_GAME_HEIGHT, true);
		if (getScreen().equals(this.stageScreen)) {
			camera.position.set(GdxGame.lastPosition,
					GdxGame.VIRTUAL_GAME_HEIGHT / 2, 0);
			// camera.position.set(VIRTUAL_GAME_WIDTH / 2, VIRTUAL_GAME_HEIGHT /
			// 2,
			// 0);
		}

		VIRTUAL_X_RATIO = (float) (VIRTUAL_GAME_WIDTH) / (width - START_X * 2);
		VIRTUAL_Y_RATIO = (float) (VIRTUAL_GAME_HEIGHT)
				/ (height - START_Y * 2);

		REAL_GAME_WIDTH_NO_FRAMES = width - START_X * 2;
		REAL_GAME_HEIGHT_NO_FRAMES = height - START_Y * 2;
	}

}
