package com.workshop27.ballistabreaker.bonus_rocks;

import java.util.Timer;
import java.util.TimerTask;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.interfaces.AbstractBonusBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class YellowBonusStar extends AbstractBonusBeetle {
	private Vector2 blick0XY;
	private Vector2 blick1XY;
	private Vector2 blick2XY;
	private Vector2 blick3XY;

	private Vector2 blick0WH;
	private Vector2 blick1WH;
	private Vector2 blick2WH;
	private Vector2 blick3WH;

	private Vector2 blick0OrXY;
	private Vector2 blick1OrXY;
	private Vector2 blick2OrXY;
	private Vector2 blick3OrXY;

	private TextureRegion blick0Region;
	private TextureRegion blick1Region;
	private TextureRegion blick2Region;
	private TextureRegion blick3Region;

	private boolean blick0Visible = false;
	private boolean blick1Visible = false;
	private boolean blick2Visible = false;
	private boolean blick3Visible = false;

	private float blickTimer = 0;
	private final Timer blicSchedule = new Timer();
	private boolean isAnimationActive = false;

	private float sin;
	private float cos;

	private float xRotated;
	private float yRotated;

	private float originXRotated;
	private float originYRotated;

	private float baseRotation;

	public YellowBonusStar(String name, String skinName) {
		super(name, skinName);
	}

	@Override
	public void clearSkin() {
		super.clearSkin();

		this.blick0Region = null;
		this.blick1Region = null;
		this.blick2Region = null;
		this.blick3Region = null;
	}

	@Override
	public void act(float delta) {
		if (!GameScreen.isGamePaused()) {
			super.act(delta);
		}
	}

	@Override
	public void setSkin() {
		super.setSkin();

		this.blick0Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("star-block0");
		this.blick1Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("star-block1");
		this.blick2Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("star-block2");
		this.blick3Region = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("star-block3");

		blick0XY = new Vector2(0 * GdxGame.HD_X_RATIO, 4 * GdxGame.HD_Y_RATIO);
		blick1XY = new Vector2(3 * GdxGame.HD_X_RATIO, -5 * GdxGame.HD_Y_RATIO);
		blick2XY = new Vector2(2 * GdxGame.HD_X_RATIO, -3 * GdxGame.HD_Y_RATIO);
		blick3XY = new Vector2(18 * GdxGame.HD_X_RATIO, 3 * GdxGame.HD_Y_RATIO);

		blick0WH = new Vector2(blick0Region.getRegionWidth(),
				blick0Region.getRegionHeight());
		blick1WH = new Vector2(blick1Region.getRegionWidth() + 1,
				blick1Region.getRegionHeight() + 2);
		blick2WH = new Vector2(blick2Region.getRegionWidth() + 1,
				blick2Region.getRegionHeight() + 1);
		blick3WH = new Vector2(blick3Region.getRegionWidth(),
				blick3Region.getRegionHeight());

		blick0OrXY = new Vector2(this.getOriginX() - blick0XY.x, this.getOriginY()
				- blick0XY.y);
		blick1OrXY = new Vector2(this.getOriginX() - blick1XY.x, this.getOriginY()
				- blick1XY.y);
		blick2OrXY = new Vector2(this.getOriginX() - blick2XY.x, this.getOriginY()
				- blick2XY.y);
		blick3OrXY = new Vector2(this.getOriginX() - blick3XY.x, this.getOriginY()
				- blick3XY.y);
	}

	@Override
	public Vector2 getCenter() {
		if (this.isStatic()) {
			return new Vector2(this.getX() + this.getWidth() / 2, this.getY() + this.getHeight()
					/ 2);
		}

		return new Vector2(this.xRotated + this.getWidth() / 2, this.yRotated
				+ this.getHeight() / 2);
	}

	@Override
	public float getPositionX() {
		if (this.isStatic()) {
			return this.getX();
		}

		return this.xRotated;
	}

	@Override
	public float getPositionY() {
		if (this.isStatic()) {
			return this.getY();
		}

		return this.yRotated;
	}

	private void calcNextBlickFrame() {
		if (!isAnimationActive) {
			return;
		}

		blickTimer += Gdx.graphics.getDeltaTime();

		if (blickTimer < 0.1f) {
			blick0Visible = true;
		} else {
			blick0Visible = false;
		}

		if (blickTimer >= 0.1f && blickTimer < 0.2f) {
			blick1Visible = true;
		} else {
			blick1Visible = false;
		}

		if (blickTimer >= 0.2f && blickTimer < 0.3f) {
			blick2Visible = true;
		} else {
			blick2Visible = false;
		}

		if (blickTimer >= 0.3f && blickTimer < 0.4f) {
			blick3Visible = true;
		} else {
			blick3Visible = false;
		}

		if (blickTimer >= 0.4f) {
			isAnimationActive = false;
		}
	}

	@Override
	public void playBlick() {
		blicSchedule.schedule(new TimerTask() {
			@Override
			public void run() {
				playBlick();
			}
		}, 8000);

		if (GameScreen.isGamePaused()) {
			return;
		}

		isBlickTimerActive = true;
		isAnimationActive = true;
		blickTimer = 0;
	}

	@Override
	public void recalcCoordinates() {
		if (originXRotated == 0 && originYRotated == 0) {
			originXRotated = GameScreen.objectToDestroy.getPositionX()
					+ GameScreen.objectToDestroy.getOriginX() - this.getWidth()
					/ 2;
			originYRotated = GameScreen.objectToDestroy.getPositionY()
					+ GameScreen.objectToDestroy.getOriginY()
					- this.getHeight() / 2;
			this.baseRotation = GameScreen.objectToDestroy.getRotation();
		}

		this.sin = MathUtils.sinDeg(GameScreen.objectToDestroy.getRotation());
		this.cos = MathUtils.cosDeg(GameScreen.objectToDestroy.getRotation());

		this.xRotated = originXRotated + (getX() - originXRotated) * this.cos
				- (getY() - originYRotated) * this.sin;
		this.yRotated = originYRotated + (getY() - originYRotated) * this.cos
				+ (getX() - originXRotated) * this.sin;

		if (this.getActiveState() != BonusBeetleState.DEACTIVATING) {
			setRotation(GameScreen.objectToDestroy.getRotation());
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		if (!this.isVisible() || this.blick0Region == null
				|| this.blick1Region == null || this.blick2Region == null
				|| this.blick3Region == null) {
			return;
		}

		batch.setColor(this.getColor());

		if (!this.isStatic()
				&& (getActiveState() == BonusBeetleState.BONUS
						|| getActiveState() == BonusBeetleState.DEACTIVATING || getActiveState() == BonusBeetleState.ACTIVATING)) {
			recalcCoordinates();

			batch.draw(this.skinRegion, this.xRotated, this.yRotated, getOriginX(),
					getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());

			if (isAnimationActive) {
				calcNextBlickFrame();

				batch.setBlendFunction(Gdx.gl10.GL_SRC_ALPHA, Gdx.gl10.GL_ONE);

				if (blick0Visible) {
					batch.draw(this.blick0Region, this.xRotated + blick0XY.x,
							this.yRotated + blick0XY.y, blick0OrXY.x,
							blick0OrXY.y, blick0WH.x, blick0WH.y, getScaleX(),
							getScaleY(), getRotation());
				}

				if (blick1Visible) {
					batch.draw(this.blick1Region, this.xRotated + blick1XY.x,
							this.yRotated + blick1XY.y, blick1OrXY.x,
							blick1OrXY.y, blick1WH.x, blick1WH.y, getScaleX(),
							getScaleY(), getRotation());
				}

				if (blick2Visible) {
					batch.draw(this.blick2Region, this.xRotated + blick2XY.x,
							this.yRotated + blick2XY.y, blick2OrXY.x,
							blick2OrXY.y, blick2WH.x, blick2WH.y, getScaleX(),
							getScaleY(), getRotation());
				}

				if (blick3Visible) {
					batch.draw(this.blick3Region, this.xRotated + blick3XY.x,
							this.yRotated + blick3XY.y, blick3OrXY.x,
							blick3OrXY.y, blick3WH.x, blick3WH.y, getScaleX(),
							getScaleY(), getRotation());
				}

				batch.setBlendFunction(Gdx.gl10.GL_SRC_ALPHA,
						Gdx.gl10.GL_ONE_MINUS_SRC_ALPHA);
			}
		}

		else {
			batch.draw(this.skinRegion, this.getX(), this.getY(), getOriginX(), getOriginY(),
					getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());

			if (isAnimationActive) {
				calcNextBlickFrame();

				batch.setBlendFunction(Gdx.gl10.GL_SRC_ALPHA, Gdx.gl10.GL_ONE);

				if (blick0Visible) {
					batch.draw(this.blick0Region, this.getX() + blick0XY.x, this.getY()
							+ blick0XY.y, blick0OrXY.x, blick0OrXY.y,
							blick0WH.x, blick0WH.y, getScaleX(), getScaleY(), getRotation());
				}

				if (blick1Visible) {
					batch.draw(this.blick1Region, this.getX() + blick1XY.x, this.getY()
							+ blick1XY.y, blick1OrXY.x, blick1OrXY.y,
							blick1WH.x, blick1WH.y, getScaleX(), getScaleY(), getRotation());
				}

				if (blick2Visible) {
					batch.draw(this.blick2Region, this.getX() + blick2XY.x, this.getY()
							+ blick2XY.y, blick2OrXY.x, blick2OrXY.y,
							blick2WH.x, blick2WH.y, getScaleX(), getScaleY(), getRotation());
				}

				if (blick3Visible) {
					batch.draw(this.blick3Region, this.getX() + blick3XY.x, this.getY()
							+ blick3XY.y, blick3OrXY.x, blick3OrXY.y,
							blick3WH.x, blick3WH.y, getScaleX(), getScaleY(), getRotation());
				}

				batch.setBlendFunction(Gdx.gl10.GL_SRC_ALPHA,
						Gdx.gl10.GL_ONE_MINUS_SRC_ALPHA);
			}
		}
	}
}
