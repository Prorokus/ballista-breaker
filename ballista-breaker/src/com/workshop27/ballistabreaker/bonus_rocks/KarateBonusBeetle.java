package com.workshop27.ballistabreaker.bonus_rocks;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.workshop27.ballistabreaker.GdxGame;
import com.workshop27.ballistabreaker.engine.ImageCache;
import com.workshop27.ballistabreaker.interfaces.AbstractBonusBeetle;
import com.workshop27.ballistabreaker.screens.GameScreen;

public class KarateBonusBeetle extends AbstractBonusBeetle {
	private Vector2 shadeXY;

	private Vector2 shadeWH;

	private Vector2 shadeOrXY;

	private TextureRegion shadeRegion;

	private TextureRegion dotRegion = null;
	private float dotX = 0;
	private float dotY = 0;
	private float dotX0 = 0;
	private float dotY0 = 0;
	private float dotAngle = 0;

	private int i1 = 0;
	private int i2 = 0;

	public KarateBonusBeetle(String name, String skinName) {
		super(name, skinName);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void clearSkin() {
		super.clearSkin();

		this.shadeRegion = null;
		this.dotRegion = null;
	}

	@Override
	public void act(float delta) {
		if (!GameScreen.isGamePaused()) {
			super.act(delta);
		}
	}

	@Override
	public void setSkin() {
		super.setSkin();

		this.shadeRegion = ImageCache
				.getManager()
				.get("data/atlas/" + this.folderName + "/pack",
						TextureAtlas.class).findRegion("karate-bonus-shade");

		this.shadeXY = new Vector2(0 * GdxGame.HD_X_RATIO,
				1 * GdxGame.HD_Y_RATIO);

		this.shadeWH = new Vector2(shadeRegion.getRegionWidth(),
				shadeRegion.getRegionHeight());

		this.shadeOrXY = new Vector2(this.getOriginX() - shadeXY.x,
				this.getOriginY() - shadeXY.y);

		// ---------------------------------------------------------
		this.dotRegion = ImageCache.getManager()
				.get("data/atlas/main/pack", TextureAtlas.class)
				.findRegion("bonus-bug-dot");

		if (this.dotRegion == null) {
			return;
		}

		if (getWidth() == 0) {
			setWidth(this.dotRegion.getRegionWidth());
		}

		if (getHeight() == 0) {
			setHeight(this.dotRegion.getRegionHeight());
		}
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		super.draw(batch, parentAlpha);

		if (this.shadeRegion != null) {
			batch.setColor(1, 1, 1, 1);
			batch.draw(this.shadeRegion, getX() + shadeXY.x, getY() + shadeXY.y,
					shadeOrXY.x, shadeOrXY.y, shadeWH.x, shadeWH.y, getScaleX(),
					getScaleY(), getRotation());
		}

		if (this.getBonusValueCount() > 0) {
			if (this.dotRegion != null) {

				this.dotX = this.getX() + this.getWidth() / 2;
				this.dotY = this.getY() + this.getHeight() / 2;

				this.i1 = 1;
				this.i2 = 0;

				for (int i = 1; i <= this.getBonusValueCount(); i++) {
					if (i % 2 == 0) {
						dotAngle = 90 + 20 * this.i1;
						this.i1++;
					} else {
						dotAngle = 90 - 20 * this.i2;
						this.i2++;
					}

					dotX0 = this.dotX + 37 * MathUtils.cosDeg(dotAngle);
					dotY0 = this.dotY + 37 * MathUtils.sinDeg(dotAngle);

					batch.draw(this.dotRegion, dotX0 - 5 / GdxGame.HD_X_RATIO,
							dotY0 - 5 / GdxGame.HD_Y_RATIO);
				}
			}
		}
	}

	@Override
	public void playBlick() {
		// TODO Auto-generated method stub

	}

}
